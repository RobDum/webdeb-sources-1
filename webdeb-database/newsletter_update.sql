alter table contributor add `newsletter` tinyint(1) DEFAULT '1' after `pedagogic`;
alter table contributor add `newsletter_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL after `confirmation_token`;
alter table contributor add `newsletter_token_expiration_date` timestamp NULL DEFAULT NULL after `auth_token_expiration_date`;