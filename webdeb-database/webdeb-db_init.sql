-- -----------------------------------------------------------------------------------------------
-- Copyright 2014 PReCISE - Girsef - CENTAL. This is part of WebDeb-web and is free
-- software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
-- Public License version 3 as published by the Free Software Foundation. It is distributed in the
-- hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- 
-- See the GNU Lesser General Public License (LGPL) for more details.
-- 
-- You should have received a copy of the GNU Lesser General Public License along with
-- WebDeb-web. If not, see <http://www.gnu.org/licenses/>.
-- -----------------------------------------------------------------------------------------------


-- --------------------------------------------------------
-- Initialize values in technical tables of webdeb database
-- Requires to add ft_min_word_len=2 in my.cnf mysql configSuration file under [mysqld]
--
-- @author Fabian Gilson (initial contribution)
-- @author Martin Rouffiange
-- --------------------------------------------------------

USE `webdeb` ;

-- -----------------------------------------------------
-- Default values for `webdeb`.`t_contribution_type`
-- -----------------------------------------------------

INSERT INTO `t_contribution_type` VALUES (0, 'ACTOR', 'actor', 'acteur', 'Actor');
INSERT INTO `t_contribution_type` VALUES (1, 'DEBATE', 'debate', 'debat', 'Argument');
INSERT INTO `t_contribution_type` VALUES (2, 'ARGUMENT', 'argument', 'affirmation', 'Argument');
INSERT INTO `t_contribution_type` VALUES (3, 'ARGUMENT_CONTEXT', 'contextualized argument', 'affirmation contextualisée', 'contextualized argument');
INSERT INTO `t_contribution_type` VALUES (4, 'EXCERPT', 'excerpt', 'extrait', 'uittreksel');
INSERT INTO `t_contribution_type` VALUES (5, 'TEXT', 'text', 'texte', 'Tekst');
INSERT INTO `t_contribution_type` VALUES (6, 'FOLDER', 'folder', 'dossier', 'map');
INSERT INTO `t_contribution_type` VALUES (7, 'AFFILIATION', 'affiliation', 'affiliation', 'Affiliatie');
INSERT INTO `t_contribution_type` VALUES (8, 'ARG_JUSTIFICATION', 'argument justification', 'justification d\'affirmation', 'Argumentatieve rechtvaardiging');
INSERT INTO `t_contribution_type` VALUES (9, 'ARG_SIMILARITY', 'argument simalarity', 'affirmation similaire', 'Argument gelijkenis');
INSERT INTO `t_contribution_type` VALUES (10, 'ARG_ILLUSTRATION', 'argument illustration', 'illustration d\'affirmation', 'Argument illustratie');
INSERT INTO `t_contribution_type` VALUES (11, 'ARG_TRANSLATION', 'argument translation', 'traduction d\'affirmation', 'Argumentvertaling');
INSERT INTO `t_contribution_type` VALUES (12, 'FOLDERLINK', 'folder link', 'lien entre dossiers', 'maplink');
INSERT INTO `t_contribution_type` VALUES (13, 'EXTERNAL_TEXT', 'external text', 'text externe', 'externe tekst');
INSERT INTO `t_contribution_type` VALUES (14, 'EXTERNAL_EXCERPT', 'external excerpt', 'extrait externe', 'externe uittreksel');

-- -----------------------------------------------------
-- Default values for `webdeb`.`t_actor_type`
-- -----------------------------------------------------

INSERT INTO `t_actor_type` VALUES (-1, 'UNKNOWN', 'unknown', 'inconnu', 'onbekend');
INSERT INTO `t_actor_type` VALUES (0, 'PERSON', 'person', 'personne', 'persoon');
INSERT INTO `t_actor_type` VALUES (1, 'ORGANIZATION', 'organization', 'organisation', 'organisatie');

-- -----------------------------------------------------
-- Default values for `webdeb`.`t_modification_status`
-- -----------------------------------------------------

INSERT INTO `t_modification_status` VALUES (0, 'CREATE', 'create', 'creation', 'Creëren');
INSERT INTO `t_modification_status` VALUES (1, 'UPDATE', 'update', 'mise à jour', 'Bijwerken');
INSERT INTO `t_modification_status` VALUES (2, 'DELETE', 'delete', 'suppression', 'Verwijderen');
INSERT INTO `t_modification_status` VALUES (3, 'MERGE', 'merge', 'fusion', 'Samenvoegen');
INSERT INTO `t_modification_status` VALUES (4, 'GROUP_REMOVAL', 'remove from group', 'supprimer du groupe', 'Verwijderen uit de groep');

-- -----------------------------------------------------
-- Default values for `webdeb`.`t_gender`
-- -----------------------------------------------------

INSERT INTO `t_gender` VALUES ('F', 'Female', 'Femme', 'Vrouw');
INSERT INTO `t_gender` VALUES ('M', 'Male', 'Homme', 'Man');
INSERT INTO `t_gender` VALUES ('X', 'Neutral', 'Neutre', 'Neutraal');

-- -----------------------------------------------------
-- Default values for `webdeb`.`t_country`
-- -----------------------------------------------------

INSERT INTO `t_country` VALUES ('ad', 'Andorra', 'Andorre', 'Andorra');
INSERT INTO `t_country` VALUES ('ae', 'United Arab Emirates', 'Émirats Arabes Unis', 'Verenigde Arabische Emiraten');
INSERT INTO `t_country` VALUES ('af', 'Afghanistan', 'Afghanistan', 'Afghanistan');
INSERT INTO `t_country` VALUES ('ag', 'Antigua and Barbuda', 'Antigua-et-Barbuda', 'Antigua en Barbuda');
INSERT INTO `t_country` VALUES ('ai', 'Anguilla', 'Anguilla', 'Anguilla');
INSERT INTO `t_country` VALUES ('al', 'Albania', 'Albanie', 'Albanië');
INSERT INTO `t_country` VALUES ('am', 'Armenia', 'Arménie', 'Armenië');
INSERT INTO `t_country` VALUES ('an', 'Netherlands Antilles', 'Antilles Néerlandaises', 'Nederlandse Antillen');
INSERT INTO `t_country` VALUES ('ao', 'Angola', 'Angola', 'Angola');
INSERT INTO `t_country` VALUES ('aq', 'Antarctica', 'Antartique', 'Antarctica');
INSERT INTO `t_country` VALUES ('ar', 'Argentina', 'Argentine', 'Argentinië');
INSERT INTO `t_country` VALUES ('as', 'American Samoa', 'Samoa américaines', 'Amerikaans Samoa');
INSERT INTO `t_country` VALUES ('at', 'Austria', 'Autriche', 'Oostenrijk');
INSERT INTO `t_country` VALUES ('au', 'Australia', 'Australie', 'Australië');
INSERT INTO `t_country` VALUES ('aw', 'Aruba', 'Aruba', 'Aruba');
INSERT INTO `t_country` VALUES ('ax', 'Åland Islands', 'Åland (Islande)', 'Ålandseilanden');
INSERT INTO `t_country` VALUES ('az', 'Azerbaijan', 'Azerbaïdjan', 'Azerbeidzjan');
INSERT INTO `t_country` VALUES ('ba', 'Bosnia and Herzegovina', 'Bosnie-Herzégovine', 'Bosnië-Herzegovina');
INSERT INTO `t_country` VALUES ('bb', 'Barbados', 'Barbade', 'Barbados');
INSERT INTO `t_country` VALUES ('bd', 'Bangladesh', 'Bangladesh', 'Bangladesh');
INSERT INTO `t_country` VALUES ('be', 'Belgium', 'Belgique', 'België');
INSERT INTO `t_country` VALUES ('bf', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso');
INSERT INTO `t_country` VALUES ('bg', 'Bulgaria', 'Bulgarie', 'Bulgarije');
INSERT INTO `t_country` VALUES ('bh', 'Bahrain', 'Bahreïn', 'Bahrein');
INSERT INTO `t_country` VALUES ('bi', 'Burundi', 'Burundi', 'Burundi');
INSERT INTO `t_country` VALUES ('bj', 'Benin', 'Bénin', 'Benin');
INSERT INTO `t_country` VALUES ('bl', 'Saint-Barthélemy', 'Saint-Barthélemy', 'Saint-Barthélemy');
INSERT INTO `t_country` VALUES ('bm', 'Bermuda', 'Bermude', 'Bermuda');
INSERT INTO `t_country` VALUES ('bn', 'Brunei Darussalam', 'Brunéi Darussalam', 'Brunei Darussalam');
INSERT INTO `t_country` VALUES ('bo', 'Bolivia', 'Bolivie', 'Bolivia');
INSERT INTO `t_country` VALUES ('br', 'Brazil', 'Brésil', 'Brazilië');
INSERT INTO `t_country` VALUES ('bs', 'Bahamas', 'Bahamas', 'Bahamaës');
INSERT INTO `t_country` VALUES ('bt', 'Bhutan', 'Bhoutan', 'Bhutan');
INSERT INTO `t_country` VALUES ('bv', 'Bouvet Island', 'Île Bouvet', 'Bouveteiland');
INSERT INTO `t_country` VALUES ('bw', 'Botswana', 'Botswana', 'Botswana');
INSERT INTO `t_country` VALUES ('by', 'Belarus', 'Belarus', 'Wit-Rusland');
INSERT INTO `t_country` VALUES ('bz', 'Belize', 'Belize', 'Belize');
INSERT INTO `t_country` VALUES ('ca', 'Canada', 'Canada', 'Canada');
INSERT INTO `t_country` VALUES ('cc', 'Cocos (Keeling) Islands', 'Îles Cocos', 'Cocoseilanden');
INSERT INTO `t_country` VALUES ('cd', 'Congo (Dem.Rep.)', 'Congo (Rép.Dém.)', 'Congo (Dem. Rep)');
INSERT INTO `t_country` VALUES ('cf', 'Central African Republic', 'République centraficaine', 'Centraal Afrikaanse Republiek');
INSERT INTO `t_country` VALUES ('cg', 'Congo (Brazzaville)', 'Congo (Brazzaville)', 'Congo (Brazzaville)');
INSERT INTO `t_country` VALUES ('ch', 'Switzerland', 'Suisse', 'Zwitserland');
INSERT INTO `t_country` VALUES ('ci', 'Côte d’Ivoire', 'Côte d’Ivoire', 'Ivoorkust');
INSERT INTO `t_country` VALUES ('ck', 'Cook Islands', 'Îles Cook', 'Cook Eilanden');
INSERT INTO `t_country` VALUES ('cl', 'Chile', 'Chili', 'Chili');
INSERT INTO `t_country` VALUES ('cm', 'Cameroon', 'Cameroun', 'Kameroen');
INSERT INTO `t_country` VALUES ('cn', 'China', 'Chine', 'China');
INSERT INTO `t_country` VALUES ('co', 'Colombia', 'Colombie', 'Colombia');
INSERT INTO `t_country` VALUES ('cr', 'Costa Rica', 'Costa Rica', 'Costa Rica');
INSERT INTO `t_country` VALUES ('ct', 'Canton and Enderbury Islands', 'Canton et Enderbury (Îles)', 'Canton en Enderbury');
INSERT INTO `t_country` VALUES ('cu', 'Cuba', 'Cuba', 'Cuba');
INSERT INTO `t_country` VALUES ('cv', 'Cape Verde', 'Cap-Vert', 'Kaapverdië');
INSERT INTO `t_country` VALUES ('cx', 'Christmas Island', 'Île Christmas', 'Kersteiland');
INSERT INTO `t_country` VALUES ('cy', 'Cyprus', 'Chypre', 'Cyprus');
INSERT INTO `t_country` VALUES ('cz', 'Czech Republic', 'République Tchèque', 'Tsjechische Republiek');
INSERT INTO `t_country` VALUES ('de', 'Germany', 'Allemagne', 'Duitsland');
INSERT INTO `t_country` VALUES ('dj', 'Djibouti', 'Djibouti', 'Djibouti');
INSERT INTO `t_country` VALUES ('dk', 'Denmark', 'Danemark', 'Denemarken');
INSERT INTO `t_country` VALUES ('dm', 'Dominica', 'Dominique', 'Dominica');
INSERT INTO `t_country` VALUES ('do', 'Dominican Republic', 'République Dominicaine', 'Dominicaanse Republiek');
INSERT INTO `t_country` VALUES ('dz', 'Algeria', 'Algérie', 'Algerije');
INSERT INTO `t_country` VALUES ('ec', 'Ecuador', 'Équateur', 'Ecuador');
INSERT INTO `t_country` VALUES ('ee', 'Estonia', 'Estonie', 'Estland');
INSERT INTO `t_country` VALUES ('eg', 'Egypt', 'Égypte', 'Egypte');
INSERT INTO `t_country` VALUES ('eh', 'Western Sahara', 'Sahara occidental', 'Westelijke Sahara');
INSERT INTO `t_country` VALUES ('er', 'Eritrea', 'Érythrée', 'Eritrea');
INSERT INTO `t_country` VALUES ('es', 'Spain', 'Espagne', 'Spanje');
INSERT INTO `t_country` VALUES ('et', 'Ethiopia', 'Éthiopie', 'Ethiopië');
INSERT INTO `t_country` VALUES ('fi', 'Finland', 'Finlande', 'Finland');
INSERT INTO `t_country` VALUES ('fj', 'Fiji', 'Fidji', 'Fiji');
INSERT INTO `t_country` VALUES ('fk', 'Falkland Islands', 'Îles Malouines', 'Falkland Eilanden');
INSERT INTO `t_country` VALUES ('fm', 'Micronesia', 'Micronésie', 'Micronesië');
INSERT INTO `t_country` VALUES ('fo', 'Faroe Islands', 'Îles Féroé', 'Faro‘r Eilanden');
INSERT INTO `t_country` VALUES ('fr', 'France', 'France', 'Frankrijk');
INSERT INTO `t_country` VALUES ('ga', 'Gabon', 'Gabon', 'Gabon');
INSERT INTO `t_country` VALUES ('gb', 'United Kingdom', 'Grande Bretagne', 'Verenigd Koninkrijk');
INSERT INTO `t_country` VALUES ('gd', 'Grenada', 'Grenade', 'Grenada');
INSERT INTO `t_country` VALUES ('ge', 'Georgia', 'Géorgie', 'Georgië');
INSERT INTO `t_country` VALUES ('gf', 'French Guiana', 'Guyane Française', 'Frans Guyana');
INSERT INTO `t_country` VALUES ('gg', 'Guernsey', 'Guernesey', 'Guernsey');
INSERT INTO `t_country` VALUES ('gh', 'Ghana', 'Ghana', 'Ghana');
INSERT INTO `t_country` VALUES ('gi', 'Gibraltar', 'Gibraltar', 'Gibraltar');
INSERT INTO `t_country` VALUES ('gl', 'Greenland', 'Groenland', 'Groenland');
INSERT INTO `t_country` VALUES ('gm', 'Gambia', 'Gambie', 'Gambia');
INSERT INTO `t_country` VALUES ('gn', 'Guinea', 'Guinée', 'Guinea');
INSERT INTO `t_country` VALUES ('gp', 'Guadeloupe', 'Guadeloupe', 'Guadeloupe');
INSERT INTO `t_country` VALUES ('gq', 'Equatorial Guinea', 'Guinée Équatoriale', 'Equatoriaal-Guinea');
INSERT INTO `t_country` VALUES ('gr', 'Greece', 'Grèce', 'Griekenland');
INSERT INTO `t_country` VALUES ('gs', 'South Georgia and the South Sandwich Islands', 'Géorgie du Sud-et-les Îles Sandwich du Sud', 'Zuid-Georgia en de Zuidelijke Sandwicheilanden');
INSERT INTO `t_country` VALUES ('gt', 'Guatemala', 'Guatemala', 'Guatemala');
INSERT INTO `t_country` VALUES ('gu', 'Guam', 'Guam', 'Guam');
INSERT INTO `t_country` VALUES ('gw', 'Guinea-Bissau', 'Guinée-Bissau', 'Guinea-Bissau');
INSERT INTO `t_country` VALUES ('gy', 'Guyana', 'Guyane', 'Guyana');
INSERT INTO `t_country` VALUES ('hk', 'Hong Kong (SAR China)', 'Hong Kong (RAS Chine)', 'Hong Kong (SAR China)');
INSERT INTO `t_country` VALUES ('hm', 'Heard Island and McDonald Islands', 'Îles Heard-et-MacDonald', 'Heard en MacDonald Eilanden');
INSERT INTO `t_country` VALUES ('hn', 'Honduras', 'Honduras', 'Honduras');
INSERT INTO `t_country` VALUES ('hr', 'Croatia', 'Croatie', 'Kroatië');
INSERT INTO `t_country` VALUES ('ht', 'Haiti', 'Haïti', 'Haïti');
INSERT INTO `t_country` VALUES ('hu', 'Hungary', 'Hongrie', 'Hongarije');
INSERT INTO `t_country` VALUES ('id', 'Indonesia', 'Indonésie', 'Indonesië');
INSERT INTO `t_country` VALUES ('ie', 'Ireland', 'Irlande', 'Ierland');
INSERT INTO `t_country` VALUES ('il', 'Israel', 'Israël', 'Israel');
INSERT INTO `t_country` VALUES ('im', 'Isle of Man', 'Île de Man', 'Man Eiland');
INSERT INTO `t_country` VALUES ('in', 'India', 'Inde', 'India');
INSERT INTO `t_country` VALUES ('io', 'British Indian Ocean Territory', 'Territoire britannique de l\'océan Indien', 'Brits Indische Oceaanterritorium');
INSERT INTO `t_country` VALUES ('iq', 'Iraq', 'Iraq', 'Irak');
INSERT INTO `t_country` VALUES ('ir', 'Iran (Islam.Rep.)', 'Iran (Rep.Islam.)', 'Iran (Islam.Rep.)');
INSERT INTO `t_country` VALUES ('is', 'Iceland', 'Islande', 'Ijsland');
INSERT INTO `t_country` VALUES ('it', 'Italy', 'Italie', 'Italië');
INSERT INTO `t_country` VALUES ('je', 'Jersey', 'Jersey', 'Jersey');
INSERT INTO `t_country` VALUES ('jm', 'Jamaica', 'Jamaïque', 'Jamaica');
INSERT INTO `t_country` VALUES ('jo', 'Jordan', 'Jordanie', 'Jordanië');
INSERT INTO `t_country` VALUES ('jp', 'Japan', 'Japon', 'Japan');
INSERT INTO `t_country` VALUES ('jt', 'Johnston Island', 'Atoll Johnston', 'Jonsthon Atoll');
INSERT INTO `t_country` VALUES ('ke', 'Kenya', 'Kénya', 'Kenya');
INSERT INTO `t_country` VALUES ('kg', 'Kyrgyzstan', 'Kirghizistan', 'Kirgizië');
INSERT INTO `t_country` VALUES ('kh', 'Cambodia', 'Cambodge', 'Cambodja');
INSERT INTO `t_country` VALUES ('ki', 'Kiribati', 'Kiribati', 'Kiribati');
INSERT INTO `t_country` VALUES ('km', 'Comoros', 'Comores', 'Comoren');
INSERT INTO `t_country` VALUES ('kn', 'Saint Kitts and Nevis', 'Saint-Christophe-et-Niévès', 'Saint Kitts en Nevis');
INSERT INTO `t_country` VALUES ('kp', 'Korea (Dem.Peo.Rep)', 'Corée (Rép.Pop.Dém.', 'Korea (Dem. Volksrep.)');
INSERT INTO `t_country` VALUES ('kr', 'Korea (Rep.)', 'Corée (Rép.)', 'Korea (Rep.)');
INSERT INTO `t_country` VALUES ('kw', 'Kuwait', 'Koweït', 'Koeweit');
INSERT INTO `t_country` VALUES ('ky', 'Cayman Islands', 'Îles Caïmans', 'Kaaimaneilanden');
INSERT INTO `t_country` VALUES ('kz', 'Kazakhstan', 'Kazakhstan', 'Kazachstan');
INSERT INTO `t_country` VALUES ('la', 'Lao (Peo.Dem.Rep)', 'Laos (Rép.Dém.Pop.)', 'Laos (Dem. Volksrep.)');
INSERT INTO `t_country` VALUES ('lb', 'Lebanon', 'Liban', 'Libanon');
INSERT INTO `t_country` VALUES ('lc', 'Saint Lucia', 'Sainte-Lucie', 'Saint Lucia');
INSERT INTO `t_country` VALUES ('li', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein');
INSERT INTO `t_country` VALUES ('lk', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka');
INSERT INTO `t_country` VALUES ('lr', 'Liberia', 'Liberia', 'Liberia');
INSERT INTO `t_country` VALUES ('ls', 'Lesotho', 'Lesotho', 'Lesotho');
INSERT INTO `t_country` VALUES ('lt', 'Lithuania', 'Lituanie', 'Litouwen');
INSERT INTO `t_country` VALUES ('lu', 'Luxembourg', 'Luxembourg', 'Luxemburg');
INSERT INTO `t_country` VALUES ('lv', 'Latvia', 'Lettonie', 'Letland');
INSERT INTO `t_country` VALUES ('ly', 'Libya', 'Lybie', 'Libië');
INSERT INTO `t_country` VALUES ('ma', 'Morocco', 'Maroc', 'Marokko');
INSERT INTO `t_country` VALUES ('mc', 'Monaco', 'Monaco', 'Monaco');
INSERT INTO `t_country` VALUES ('md', 'Moldova', 'Moldavie', 'Moldavië');
INSERT INTO `t_country` VALUES ('me', 'Montenegro', 'Montenegro', 'Montenegro');
INSERT INTO `t_country` VALUES ('mf', 'Saint Martin', 'Saint-Martin', 'Sint Maarten');
INSERT INTO `t_country` VALUES ('mg', 'Madagascar', 'Madagascar', 'Madagascar');
INSERT INTO `t_country` VALUES ('mh', 'Marshall Islands', 'Îles Marshall', 'Marshall Eilanden');
INSERT INTO `t_country` VALUES ('mk', 'Macedonia (Rep.)', 'Macédoine (Rép.)', 'Macedonië');
INSERT INTO `t_country` VALUES ('ml', 'Mali', 'Mali', 'Mali');
INSERT INTO `t_country` VALUES ('mm', 'Myanmar', 'Myanmar', 'Myanmar');
INSERT INTO `t_country` VALUES ('mn', 'Mongolia', 'Mongolie', 'Mongolië');
INSERT INTO `t_country` VALUES ('mo', 'Macau (SAR China)', 'Macao (RAS Chine)', 'Macau (SAR China)');
INSERT INTO `t_country` VALUES ('mp', 'Northern Mariana Islands', 'Îles Mariannes du Nord', 'Noordelijke Marianen');
INSERT INTO `t_country` VALUES ('mq', 'Martinique', 'Martinique', 'Martinique');
INSERT INTO `t_country` VALUES ('mr', 'Mauritania', 'Mauritanie', 'Mauretanië');
INSERT INTO `t_country` VALUES ('ms', 'Montserrat', 'Montserrat', 'Montserrat');
INSERT INTO `t_country` VALUES ('mt', 'Malta', 'Malte', 'Malta');
INSERT INTO `t_country` VALUES ('mu', 'Mauritius', 'Maurice', 'Mauritius');
INSERT INTO `t_country` VALUES ('mv', 'Maldives', 'Maldives', 'Malediven');
INSERT INTO `t_country` VALUES ('mw', 'Malawi', 'Malawi', 'Malawi');
INSERT INTO `t_country` VALUES ('mx', 'Mexico', 'Mexique', 'Mexico');
INSERT INTO `t_country` VALUES ('my', 'Malaysia', 'Malaisie', 'Maleisië');
INSERT INTO `t_country` VALUES ('mz', 'Mozambique', 'Mozambique', 'Mozambique');
INSERT INTO `t_country` VALUES ('na', 'Namibia', 'Namibie', 'Namibië');
INSERT INTO `t_country` VALUES ('nc', 'New Caledonia', 'Nouvelle Calédonie', 'Nieuw-Caledonië');
INSERT INTO `t_country` VALUES ('ne', 'Niger', 'Niger', 'Niger');
INSERT INTO `t_country` VALUES ('nf', 'Norfolk Island', 'Île Norfolk', 'Norfolk Eiland');
INSERT INTO `t_country` VALUES ('ng', 'Nigeria', 'Nigeria', 'Nigeria');
INSERT INTO `t_country` VALUES ('ni', 'Nicaragua', 'Nicaragua', 'Nicaragua');
INSERT INTO `t_country` VALUES ('nl', 'Netherlands', 'Pays-Bas', 'Nederland ');
INSERT INTO `t_country` VALUES ('no', 'Norway', 'Norvège', 'Noorwegen');
INSERT INTO `t_country` VALUES ('np', 'Nepal', 'Népal', 'Nepal');
INSERT INTO `t_country` VALUES ('nq', 'Dronning Maud Land', 'Terre de la Reine-Maud', 'Koningin Maudland');
INSERT INTO `t_country` VALUES ('nr', 'Nauru', 'Nauru', 'Nauru');
INSERT INTO `t_country` VALUES ('nu', 'Niue', 'Niue', 'Niue');
INSERT INTO `t_country` VALUES ('nz', 'New Zealand', 'Nouvelle Zélande', 'Nieuw Zeeland');
INSERT INTO `t_country` VALUES ('om', 'Oman', 'Oman', 'Oman');
INSERT INTO `t_country` VALUES ('pa', 'Panama', 'Panama', 'Panama');
INSERT INTO `t_country` VALUES ('pe', 'Peru', 'Pérou', 'Peru');
INSERT INTO `t_country` VALUES ('pf', 'French Polynesia', 'Polynésie Française', 'Frans Polynesië');
INSERT INTO `t_country` VALUES ('pg', 'Papua New Guinea', 'Papouasie-Nouvelle-Guinée', 'Papua Nieuw Guinea');
INSERT INTO `t_country` VALUES ('ph', 'Philippines', 'Philippines', 'Filippijnen');
INSERT INTO `t_country` VALUES ('pk', 'Pakistan', 'Pakistan', 'Pakistan');
INSERT INTO `t_country` VALUES ('pl', 'Poland', 'Pologne', 'Polen');
INSERT INTO `t_country` VALUES ('pm', 'Saint Pierre and Miquelon', 'Saint-Pierre-et-Miquelon', 'Saint Pierre en Miquelon');
INSERT INTO `t_country` VALUES ('pn', 'Pitcairn', 'Îles Pitcairn', 'Pitcairn Eilanden');
INSERT INTO `t_country` VALUES ('pr', 'Puerto Rico', 'Puerto Rico', 'Puerto Rico');
INSERT INTO `t_country` VALUES ('ps', 'Palestinian Territory', 'Territoires palestiniens', 'Palestijnse Gebieden');
INSERT INTO `t_country` VALUES ('pt', 'Portugal', 'Portugal', 'Portugal');
INSERT INTO `t_country` VALUES ('pw', 'Palau', 'Palaos', 'Palau');
INSERT INTO `t_country` VALUES ('py', 'Paraguay', 'Paraguay', 'Paraguay');
INSERT INTO `t_country` VALUES ('qa', 'Qatar', 'Qatar', 'Qatar');
INSERT INTO `t_country` VALUES ('re', 'Réunion', 'Rénion', 'RŽunion');
INSERT INTO `t_country` VALUES ('ro', 'Romania', 'Roumanie', 'Roemenië');
INSERT INTO `t_country` VALUES ('rs', 'Serbia', 'Serbie', 'Servië');
INSERT INTO `t_country` VALUES ('ru', 'Russia (Fed.)', 'Russie (Féd.)', 'Rusland (Fed.)');
INSERT INTO `t_country` VALUES ('rw', 'Rwanda', 'Rwanda', 'Rwanda');
INSERT INTO `t_country` VALUES ('sa', 'Saudi Arabia', 'Arabie saoudite', 'Saudi Arabië');
INSERT INTO `t_country` VALUES ('sb', 'Solomon Islands', 'Salomon', 'Salomon Eilanden');
INSERT INTO `t_country` VALUES ('sc', 'Seychelles', 'Seychelles', 'Seychellen');
INSERT INTO `t_country` VALUES ('sd', 'Sudan', 'Soudan', 'Sudan');
INSERT INTO `t_country` VALUES ('se', 'Sweden', 'Suède', 'Zweden');
INSERT INTO `t_country` VALUES ('sg', 'Singapore', 'Singapour', 'Singapore');
INSERT INTO `t_country` VALUES ('sh', 'Saint Helena', 'Sainte-Hélène', 'Sint Helena');
INSERT INTO `t_country` VALUES ('si', 'Slovenia', 'Slovénie', 'Sloveni‘');
INSERT INTO `t_country` VALUES ('sj', 'Svalbard and Jan Mayen Islands', 'Svalbard et Jan Mayen (Îles)', 'Spitsbergen en Jan Mayen');
INSERT INTO `t_country` VALUES ('sk', 'Slovakia', 'Slovaquie', 'Slovakije');
INSERT INTO `t_country` VALUES ('sl', 'Sierra Leone', 'Sierra Leone', 'Sierra Leone');
INSERT INTO `t_country` VALUES ('sm', 'San Marino', 'Saint Marin', 'San Marino');
INSERT INTO `t_country` VALUES ('sn', 'Senegal', 'Sénégal', 'Senegal');
INSERT INTO `t_country` VALUES ('so', 'Somalia', 'Somalie', 'Somalië');
INSERT INTO `t_country` VALUES ('sr', 'Suriname', 'Suriname', 'Suriname');
INSERT INTO `t_country` VALUES ('st', 'Sao Tome and Príncipe', 'Sao Tomé-et-Principe', 'Sao Tomé en Principe');
INSERT INTO `t_country` VALUES ('sv', 'El Salvador', 'El Salvador', 'El Salvador');
INSERT INTO `t_country` VALUES ('sy', 'Syria (Arab Rep.)', 'Syrie (Rép.Arabe)', 'Syrië (Arab.Rep.)');
INSERT INTO `t_country` VALUES ('sz', 'Swaziland', 'Swaziland', 'Swaziland');
INSERT INTO `t_country` VALUES ('tc', 'Turks and Caicos Islands', 'Îles Turques-et-Caïques', 'Turks en Caicoseilanden');
INSERT INTO `t_country` VALUES ('td', 'Chad', 'Tchad', 'Tsjaad');
INSERT INTO `t_country` VALUES ('tf', 'French Southern Territories', 'Terres australes et antarctiques françaises', 'Franse zuidelijke gebieden');
INSERT INTO `t_country` VALUES ('tg', 'Togo', 'Togo', 'Togo');
INSERT INTO `t_country` VALUES ('th', 'Thailand', 'Thaïlande', 'Thailand');
INSERT INTO `t_country` VALUES ('tj', 'Tajikistan', 'Tadjikistan', 'Tadzjikistan');
INSERT INTO `t_country` VALUES ('tk', 'Tokelau', 'Tokelau', 'Tokelau');
INSERT INTO `t_country` VALUES ('tl', 'Timor-Leste', 'Timor oriental', 'Oost-Timor');
INSERT INTO `t_country` VALUES ('tm', 'Turkmenistan', 'Turkménistan', 'Turkmenistan');
INSERT INTO `t_country` VALUES ('tn', 'Tunisia', 'Tunisie', 'Tunesiè');
INSERT INTO `t_country` VALUES ('to', 'Tonga', 'Tonga', 'Tonga');
INSERT INTO `t_country` VALUES ('tr', 'Turkey', 'Turquie', 'Turkije');
INSERT INTO `t_country` VALUES ('tt', 'Trinidad and Tobago', 'Trinité-et-Tobago', 'Trinidad en Tobago');
INSERT INTO `t_country` VALUES ('tv', 'Tuvalu', 'Tuvalu', 'Tuvalu');
INSERT INTO `t_country` VALUES ('tw', 'Taiwan (Rep.China)', 'Taïwan (Rép.Chine)', 'Taiwan (Rep.China)');
INSERT INTO `t_country` VALUES ('tz', 'Tanzania (U.Rep.)', 'Tanzanie (Rép.U.)', 'Tanzania (U.Rep.)');
INSERT INTO `t_country` VALUES ('ua', 'Ukraine', 'Ukraine', 'Oekraine');
INSERT INTO `t_country` VALUES ('ug', 'Uganda', 'Ouganda', 'Uganda');
INSERT INTO `t_country` VALUES ('um', 'United States Minor Outlying Islands', 'Îles mineures éloignées des États-Unis', 'Kleine afgelegen eilanden van de US');
INSERT INTO `t_country` VALUES ('us', 'United States of America', 'États-Unis d\'Amérique', 'Verenigde Staten van Amerika');
INSERT INTO `t_country` VALUES ('uy', 'Uruguay', 'Uruguay', 'Uruguay');
INSERT INTO `t_country` VALUES ('uz', 'Uzbekistan', 'Ouzbékistan', 'Oezbekistan');
INSERT INTO `t_country` VALUES ('va', 'Vatican City', 'Vatican (Cité du)', 'Vaticaanstad');
INSERT INTO `t_country` VALUES ('vc', 'Saint Vincent and Grenadines', 'Saint-Vincent-et-les-Grenadines', 'Saint Vincent en de Grenadines');
INSERT INTO `t_country` VALUES ('ve', 'Venezuela', 'Venezuela', 'Venezuela');
INSERT INTO `t_country` VALUES ('vg', 'British Virgin Islands', 'Îles Vierges britanniques', 'Britse Maagdeneilanden');
INSERT INTO `t_country` VALUES ('vi', 'Virgin Islands (US)', 'Îles Vierges (EU)', 'Maagdeneilanden (VS)');
INSERT INTO `t_country` VALUES ('vn', 'Viet Nam', 'Viêt Nam', 'Vietnam');
INSERT INTO `t_country` VALUES ('vu', 'Vanuatu', 'Vanuatu', 'Vanuatu');
INSERT INTO `t_country` VALUES ('wf', 'Wallis and Futuna Islands', 'Wallis et Futuna', 'Wallis en Futuna');
INSERT INTO `t_country` VALUES ('ws', 'Samoa', 'Samoa', 'Samoa');
INSERT INTO `t_country` VALUES ('ye', 'Yemen', 'Yémen', 'Jemen');
INSERT INTO `t_country` VALUES ('yt', 'Mayotte', 'Mayotte', 'Mayotte');
INSERT INTO `t_country` VALUES ('za', 'South Africa', 'Afrique du Sud', 'Zuid-Afrika');
INSERT INTO `t_country` VALUES ('zm', 'Zambia', 'Zambie', 'Zambia');
INSERT INTO `t_country` VALUES ('zw', 'Zimbabwe', 'Zimbabwe', 'Zimbabwe');


-- -----------------------------------------------------
-- Default values for `webdeb`.`t_legal_status`
-- -----------------------------------------------------

INSERT INTO `t_legal_status` VALUES (0, 'Company', 'Entreprise', 'Bedrijf');
INSERT INTO `t_legal_status` VALUES (1, 'Political party', 'Parti politique', 'Politieke Partij');
INSERT INTO `t_legal_status` VALUES (2, 'Public administration, public authorities, political or consultative decision instance', 'Administration publique, pouvoir public, instance de décision politique ou instance d’avis', 'Openbaar bestuur, overheidsinstanties, politieke of adviesbeslissingsinstantie');
INSERT INTO `t_legal_status` VALUES (3, 'Union, organisational federation, lobby, social movement', 'Syndicat, fédération d’organisations, lobby ou mouvement social', 'Unie, organisatorische federatie, lobby, sociale beweging');
INSERT INTO `t_legal_status` VALUES (4, 'Any other kind of non profit organization', 'Toute autre type d\'organisation non lucrative', 'Een ander soort non-profit organisatie');
INSERT INTO `t_legal_status` VALUES (5, 'Project or event', 'Projet ou événement', 'Project of evenement');
INSERT INTO `t_legal_status` VALUES (6, 'Prize, distinction, election, competition', 'Prix, distinction, élection, compétition', 'Prijs, onderscheid, verkiezing, competitie');
INSERT INTO `t_legal_status` VALUES (7, 'Product, brand', 'Produit, marque (y compris programme d’étude)', 'Product, merk');
INSERT INTO `t_legal_status` VALUES (8, 'Label', 'Label', 'Label');

-- -----------------------------------------------------
-- Default values for `webdeb`.`t_text_type`
-- -----------------------------------------------------

INSERT INTO `t_text_type` VALUES (0, 'Discussion or debate transcript', 'Entretien ou retranscription de débats', 'Discussie of transcript van debat');
INSERT INTO `t_text_type` VALUES (1, 'Artistic (novel, poem, script,...)', 'Artistique (roman, poème, scénario,...)', 'Artistiek (roman, gedicht, script)');
INSERT INTO `t_text_type` VALUES (2, 'Informative (statistical or progress report, encyclopaedia, schoolbook,...)', 'Informatif (rapport statistique ou d\'activité, encyclopédie, manuel scolaire,...)', 'Informatief (statistisch of voortgangsrapport, encyclopedie, schoolboek)');
INSERT INTO `t_text_type` VALUES (3, 'Journalistic (news article, press report,...)', 'Journalistique (article de presse, reportage,...)', 'Journalistiek (nieuwsartikel, persbericht)');
INSERT INTO `t_text_type` VALUES (4, 'Normative (law, rule, treaty, standard,...)', 'Normatif (loi, règlement, traité, norme, standard,...)', 'Normatief (wet, regel, verdrag, standaard)');
INSERT INTO `t_text_type` VALUES (5, 'Opinion (defence, editorial, essay, artistic review, critique,...)', 'Opinion (plaidoyer, éditorial, essai, critique artistique,...)', 'Opinie (pleidooi, editoriaal, essay, artistieke review, kritiek)');
INSERT INTO `t_text_type` VALUES (6, 'Prospective (project, plan, programme,...)', 'Prospectif (projet, plan, programme,...)', 'Prospectief (project, plan, programma)');
INSERT INTO `t_text_type` VALUES (7, 'Practical (technical manual, tourist guide,...)', 'Pratique (manuel technique ou pratique, guide touristique,...)', 'Praktisch (technische handleiding, toeristische gids)');
INSERT INTO `t_text_type` VALUES (8, 'Advertising', 'Publicitaire', 'Reclame');
INSERT INTO `t_text_type` VALUES (9, 'Scientific (scientific article, book or report, Master or Ph.D thesis, market research,...)', 'Scientifique (article/livre/rapport scientifique, thèse, mémoire, étude de marché,...)', 'Wetenschappelijk (artikel/boek/wetenschappelijk rapport, thesis, verhandeling, marktonderzoek)');
INSERT INTO `t_text_type` VALUES (10, 'Other', 'Autre', 'Andere');

-- -----------------------------------------------------
-- Default values for `webdeb`.`t_text_visibility`
-- -----------------------------------------------------

INSERT INTO `t_text_visibility` VALUES (0, 'publicly visible', 'visible publiquement', 'Openbaar zichtbaar');
INSERT INTO `t_text_visibility` VALUES (1, 'visible for pedagogic purpose only', 'visible pour un usage pédagogique uniquement', 'Zichtbaar enkel voor pedagogische doeleinden');
INSERT INTO `t_text_visibility` VALUES (2, 'visible for private usage', 'visible pour un usage privé', 'Zichtbaar voor privaat gebruik');

-- -----------------------------------------------------
-- Default values for `webdeb`.`t_business_sector`
-- -----------------------------------------------------

INSERT INTO `t_business_sector` VALUES (0, 'Agriculture and fisheries', 'Agriculture, sylviculture et pêche', 'Landbouw en visserij');
INSERT INTO `t_business_sector` VALUES (1, 'Art and culture', 'Arts et culture', 'Kunst en cultuur');
INSERT INTO `t_business_sector` VALUES (2, 'Sport and leisure', 'Loisirs et sports', 'Sport en vrijetijd');
INSERT INTO `t_business_sector` VALUES (3, 'Retail trade', 'Commerce de détail', 'Detailhandel');
INSERT INTO `t_business_sector` VALUES (4, 'Building and real estate', 'Construction et immobilier', 'Bouwen en onroerend goed');
INSERT INTO `t_business_sector` VALUES (5, 'Education and training', 'Education et formation', 'Onderwijs en opleiding');
INSERT INTO `t_business_sector` VALUES (6, 'Energy', 'Energie', 'Energie');
INSERT INTO `t_business_sector` VALUES (7, 'Environment', 'Environnement', 'Milieu');
INSERT INTO `t_business_sector` VALUES (8, 'Finance, holding, banking and insurance', 'Finances, holding, banques et assurances', 'Financiën, holding, bankieren en verzekeren');
INSERT INTO `t_business_sector` VALUES (9, 'Hospitality, catering and tourism', 'Hôtellerie, restauration et tourisme', 'Gastvrijheid, horeca en toerisme');
INSERT INTO `t_business_sector` VALUES (10, 'Industry', 'Industrie', 'Industrie');
INSERT INTO `t_business_sector` VALUES (11, 'Mining industry', 'Industries extractives (matières premières)', 'Mijnbouwindustrie');
INSERT INTO `t_business_sector` VALUES (12, 'Justice', 'Justice', 'Justitie');
INSERT INTO `t_business_sector` VALUES (13, 'Press and media', 'Presse et médias', 'Pers en media');
INSERT INTO `t_business_sector` VALUES (14, 'Research and expertise', 'Recherche et expertise', 'Onderzoek en expertise');
INSERT INTO `t_business_sector` VALUES (15, 'Health', 'Santé', 'Gezondheid');
INSERT INTO `t_business_sector` VALUES (16, 'Security', 'Sécurité', 'Veiligheid');
INSERT INTO `t_business_sector` VALUES (17, 'Information and Communications Technology', 'Technologies de l\'information et de la communication', 'Informatie en communicatietechnologie');
INSERT INTO `t_business_sector` VALUES (18, 'Transport and logistics', 'Transports et logistique', 'Transport en logistiek');
INSERT INTO `t_business_sector` VALUES (19, 'Other services to individuals or organisations', 'Autres services aux particuliers et organisations', 'Andere diensten voor particulieren en organisaties');
INSERT INTO `t_business_sector` VALUES (20, 'Cross-sectoral', 'Transversale aux secteurs', 'Sectoroverschrijdende');

-- -----------------------------------------------------
-- Default values for `webdeb`.`t_language`
-- -----------------------------------------------------

INSERT INTO `t_language` VALUES ('aa', 'Afar', 'afar', 'Afaraf', 'Afar');
INSERT INTO `t_language` VALUES ('ab', 'Abkhaz', 'abkhaze', 'аҧсуа бызшәа, аҧсшәа', 'Abchazisch');
INSERT INTO `t_language` VALUES ('ae', 'Avestan', 'avestique', 'avesta', 'Avesta');
INSERT INTO `t_language` VALUES ('af', 'Afrikaans', 'afrikaans', 'Afrikaans', 'Afrikaans');
INSERT INTO `t_language` VALUES ('ak', 'Akan', 'akan', 'Akan', 'Akan');
INSERT INTO `t_language` VALUES ('am', 'Amharic', 'amharique', 'አማርኛ', 'Amhaars');
INSERT INTO `t_language` VALUES ('an', 'Aragonese', 'aragonois', 'aragonés', 'Aragonees');
INSERT INTO `t_language` VALUES ('ar', 'Arabic', 'arabe', 'العربية ', 'Arabisch');
INSERT INTO `t_language` VALUES ('as', 'Assamese', 'assamais', 'অসমীয়া', 'Assamees');
INSERT INTO `t_language` VALUES ('av', 'Avaric', 'avar', 'авар мацӀ, магӀарул мацӀ', 'Avaars');
INSERT INTO `t_language` VALUES ('ay', 'Aymara', 'aymara', 'aymar aru', 'Aymara');
INSERT INTO `t_language` VALUES ('az', 'Azerbaijani', 'azeri', 'azərbaycan dili', 'Azerbeidzjaans');
INSERT INTO `t_language` VALUES ('ba', 'Bashkir', 'bachkir', 'башҡорт теле', 'Bashkir');
INSERT INTO `t_language` VALUES ('be', 'Belarusian', 'biélorusse', 'беларуская мова', 'Wit Russisch');
INSERT INTO `t_language` VALUES ('bg', 'Bulgarian', 'bulgare', 'български език', 'Bulgaars');
INSERT INTO `t_language` VALUES ('bh', 'Bihari', 'berbères', 'भोजपुरी', 'Bihari');
INSERT INTO `t_language` VALUES ('bi', 'Bislama', 'bichlamar', 'Bislama', 'Bislama');
INSERT INTO `t_language` VALUES ('bm', 'Bambara', 'bambara', 'bamanankan', 'Bambarra');
INSERT INTO `t_language` VALUES ('bn', 'Bengali', 'bengali', 'বাংলা', 'Bengaals');
INSERT INTO `t_language` VALUES ('bo', 'Tibetan', 'tibétain', 'བོད་ཡིག', 'Tibetaans');
INSERT INTO `t_language` VALUES ('br', 'Breton', 'breton', 'brezhoneg', 'Bretoens');
INSERT INTO `t_language` VALUES ('bs', 'Bosnian', 'bosniaque', 'bosanski jezik', 'Bosnisch');
INSERT INTO `t_language` VALUES ('ca', 'Catalan', 'catalan', 'català', 'Catalaans');
INSERT INTO `t_language` VALUES ('ce', 'Chechen', 'tchétchène', 'нохчийн мотт', 'Tsjetsjeens');
INSERT INTO `t_language` VALUES ('ch', 'Chamorro', 'chamorro', 'Chamoru', 'Chamorro');
INSERT INTO `t_language` VALUES ('co', 'Corsican', 'corse', 'corsu, lingua corsa', 'Corsicaans');
INSERT INTO `t_language` VALUES ('cr', 'Cree', 'cree', 'ᓀᐦᐃᔭᐍᐏᐣ', 'Cree');
INSERT INTO `t_language` VALUES ('cs', 'Czech', 'tchèque', 'čeština, český jazyk', 'Tsjechisch');
INSERT INTO `t_language` VALUES ('cv', 'Chuvash', 'tchouvache', 'чӑваш чӗлхи', 'Tsjoevasjisch');
INSERT INTO `t_language` VALUES ('cy', 'Welsh', 'gallois', 'Cymraeg', 'Welsh');
INSERT INTO `t_language` VALUES ('da', 'Danish', 'danois', 'dansk', 'Deens');
INSERT INTO `t_language` VALUES ('de', 'German', 'allemand', 'Deutsch', 'Duits');
INSERT INTO `t_language` VALUES ('dv', 'Divehi, Dhivehi, Maldivian', 'maldivien', 'ދިވެހި ', 'Divehi');
INSERT INTO `t_language` VALUES ('dz', 'Dzongkha', 'dzongkha', 'རྫོང་ཁ', 'Dzongkha');
INSERT INTO `t_language` VALUES ('ee', 'Ewe', 'éwé', 'Eʋegbe', 'Ewe');
INSERT INTO `t_language` VALUES ('el', 'Greek (modern)', 'grec (moderne)', 'ελληνικά', 'Grieks');
INSERT INTO `t_language` VALUES ('en', 'English', 'anglais', 'English', 'Engels');
INSERT INTO `t_language` VALUES ('eo', 'Esperanto', 'espéranto', 'Esperanto', 'Esperanto');
INSERT INTO `t_language` VALUES ('es', 'Spanish', 'espagnol', 'español', 'Spaans');
INSERT INTO `t_language` VALUES ('et', 'Estonian', 'estonien', 'eesti, eesti keel', 'Ests');
INSERT INTO `t_language` VALUES ('eu', 'Basque', 'basque', 'euskara, euskera', 'Baskisch');
INSERT INTO `t_language` VALUES ('fa', 'Persian', 'persan', 'فارسی ', 'Perzisch (Farsi)');
INSERT INTO `t_language` VALUES ('ff', 'Fulah', 'peul', 'Fulfulde, Pulaar, Pular', 'Fula');
INSERT INTO `t_language` VALUES ('fi', 'Finnish', 'finnois', 'suomi, suomen kieli', 'Fins');
INSERT INTO `t_language` VALUES ('fj', 'Fijian', 'fidjien', 'vosa Vakaviti', 'Fijisch');
INSERT INTO `t_language` VALUES ('fo', 'Faroese', 'féroïen', 'føroyskt', 'Faeršers');
INSERT INTO `t_language` VALUES ('fr', 'French', 'français', 'français', 'Frans');
INSERT INTO `t_language` VALUES ('fy', 'Western Frisian', 'frison occidental', 'Frysk', 'Westerlauwers Fries');
INSERT INTO `t_language` VALUES ('ga', 'Irish', 'irlandais', 'Gaeilge', 'Iers');
INSERT INTO `t_language` VALUES ('gd', 'Scottish Gaelic, Gaelic', 'gaélique écossais, gaélique', 'Gàidhlig', 'Schots-Gaelisch');
INSERT INTO `t_language` VALUES ('gl', 'Galician', 'galicien', 'galego', 'Galicisch');
INSERT INTO `t_language` VALUES ('gn', 'Guarani', 'guarani', 'Avañe\'ẽ', 'Guarani');
INSERT INTO `t_language` VALUES ('gu', 'Gujarati', 'gudjrati', 'ગુજરાતી', 'Gujarati');
INSERT INTO `t_language` VALUES ('gv', 'Manx', 'manx, mannois', 'Gaelg, Gailck', 'Manx-Gaelisch');
INSERT INTO `t_language` VALUES ('ha', 'Hausa', 'haoussa', '(Hausa) هَوُسَ ', 'Hausa');
INSERT INTO `t_language` VALUES ('he', 'Hebrew', 'hébreu', 'עברית ', 'Hebreeuws');
INSERT INTO `t_language` VALUES ('hi', 'Hindi', 'hindi', 'हिन्दी', 'Hindi');
INSERT INTO `t_language` VALUES ('ho', 'Hiri Motu', 'hiri motu', 'Hiri Motu', 'Hiri Motu');
INSERT INTO `t_language` VALUES ('hr', 'Croatian', 'croate', 'hrvatski jezik', 'Kroatisch');
INSERT INTO `t_language` VALUES ('ht', 'Haitian, Haitian Creole', 'haïtien, créole haïtien', 'Kreyòl ayisyen', 'Haitiaans Creools');
INSERT INTO `t_language` VALUES ('hu', 'Hungarian', 'hongrois', 'magyar', 'Hongaars');
INSERT INTO `t_language` VALUES ('hy', 'Armenian', 'arménien', 'Հայերեն', 'Armeens');
INSERT INTO `t_language` VALUES ('hz', 'Herero', 'herero', 'Otjiherero', 'Herero');
INSERT INTO `t_language` VALUES ('id', 'Indonesian', 'indonésien', 'Bahasa Indonesia', 'Indonesisch');
INSERT INTO `t_language` VALUES ('ig', 'Igbo', 'igbo', 'Asụsụ Igbo', 'Igbo');
INSERT INTO `t_language` VALUES ('ii', 'Nuosu', 'yi de Sichuan', 'Nuosuhxop', 'Yi');
INSERT INTO `t_language` VALUES ('ik', 'Inupiaq', 'inupiaq', 'Iñupiaq, Iñupiatun', 'Inupiak');
INSERT INTO `t_language` VALUES ('io', 'Ido', 'ido', 'Ido', 'Ido');
INSERT INTO `t_language` VALUES ('is', 'Icelandic', 'Islandais', 'Íslenska', 'IJslands');
INSERT INTO `t_language` VALUES ('it', 'Italian', 'italien', 'italiano', 'Italiaans');
INSERT INTO `t_language` VALUES ('iu', 'Inuktitut', 'inuktitut', 'ᐃᓄᒃᑎᑐᑦ', 'Inuktitut');
INSERT INTO `t_language` VALUES ('ja', 'Japanese', 'japonais', '日本語 (にほんご)', 'Japans');
INSERT INTO `t_language` VALUES ('jv', 'Javanese', 'javanais', 'basa Jawa', 'Javaans');
INSERT INTO `t_language` VALUES ('ka', 'Georgian', 'géorgien', 'ქართული', 'Georgisch');
INSERT INTO `t_language` VALUES ('kg', 'Kongo', 'kongo', 'Kikongo', 'Kongo');
INSERT INTO `t_language` VALUES ('ki', 'Kikuyu, Gikuyu', 'kikuyu', 'Gĩkũyũ', 'Gikuyu');
INSERT INTO `t_language` VALUES ('kj', 'Kwanyama, Kuanyama', 'kwanyama, kuanyama', 'Kuanyama', 'Kwanyama');
INSERT INTO `t_language` VALUES ('kk', 'Kazakh', 'kazakh', 'қазақ тілі', 'Kazachs');
INSERT INTO `t_language` VALUES ('kl', 'Kalaallisut, Greenlandic', 'groenlandais', 'kalaallisut, kalaallit oqaasii', 'Groenlands');
INSERT INTO `t_language` VALUES ('km', 'Khmer', 'khmer', 'ខ្មែរ, ខេមរភាសា, ភាសាខ្មែរ', 'Khmer, Cambodjaans');
INSERT INTO `t_language` VALUES ('kn', 'Kannada', 'kannada', 'ಕನ್ನಡ', 'Kannada (Kanarees, Kanara)');
INSERT INTO `t_language` VALUES ('ko', 'Korean', 'coréen', '한국어, 조선어', 'Koreaans');
INSERT INTO `t_language` VALUES ('kr', 'Kanuri', 'kanouri', 'Kanuri', 'Kanuri');
INSERT INTO `t_language` VALUES ('ks', 'Kashmiri', 'kashmiri', 'कश्मीरी, كشميري‎', 'Kasjmiri');
INSERT INTO `t_language` VALUES ('ku', 'Kurdish', 'kurde', 'Kurdî, كوردی‎', 'Koerdisch');
INSERT INTO `t_language` VALUES ('kv', 'Komi', 'kom', 'коми кыв', 'Zurjeens (Komi)');
INSERT INTO `t_language` VALUES ('kw', 'Cornish', 'cornique', 'Kernewek', 'Cornisch');
INSERT INTO `t_language` VALUES ('ky', 'Kyrgyz', 'kirgiz', 'Кыргызча, Кыргыз тили', 'Kirgizisch');
INSERT INTO `t_language` VALUES ('la', 'Latin', 'latin', 'latine, lingua latina', 'Latijn');
INSERT INTO `t_language` VALUES ('lb', 'Luxembourgish, Letzeburgesch', 'luxembourgeois', 'Lëtzebuergesch', 'Luxemburgs');
INSERT INTO `t_language` VALUES ('lg', 'Ganda', 'ganda', 'Luganda', 'Luganda');
INSERT INTO `t_language` VALUES ('ln', 'Lingala', 'lingala', 'Lingála', 'Lingala');
INSERT INTO `t_language` VALUES ('lo', 'Lao', 'lao', 'ພາສາລາວ', 'Laotiaans');
INSERT INTO `t_language` VALUES ('lt', 'Lithuanian', 'lithuanien', 'lietuvių kalba', 'Lithouws');
INSERT INTO `t_language` VALUES ('lu', 'Luba-Katanga', 'luba-katanga', 'Tshiluba', 'Luba-Katanga');
INSERT INTO `t_language` VALUES ('lv', 'Latvian', 'letton', 'latviešu valoda', 'Lets');
INSERT INTO `t_language` VALUES ('mg', 'Malagasy', 'malgache', 'fiteny malagasy', 'Plateaumalagasi');
INSERT INTO `t_language` VALUES ('mh', 'Marshallese', 'marshall', 'Kajin M̧ajeļ', 'Marshallees');
INSERT INTO `t_language` VALUES ('mi', 'Māori', 'maori', 'te reo Māori', 'Maori');
INSERT INTO `t_language` VALUES ('mk', 'Macedonian', 'macédonien', 'македонски јазик', 'Macedonisch');
INSERT INTO `t_language` VALUES ('ml', 'Malayalam', 'malayalam', 'മലയാളം', 'Malayalam');
INSERT INTO `t_language` VALUES ('mn', 'Mongolian', 'mongole', 'Монгол хэл', 'Mongools');
INSERT INTO `t_language` VALUES ('mr', 'Marathi (Marāṭhī)', 'marathe', 'मराठी', 'Marathi');
INSERT INTO `t_language` VALUES ('ms', 'Malay', 'malais', 'bahasa Melayu, بهاس ملايو‎', 'Maleis');
INSERT INTO `t_language` VALUES ('mt', 'Maltese', 'maltais', 'Malti', 'Maltees');
INSERT INTO `t_language` VALUES ('my', 'Burmese', 'birman', 'ဗမာစာ', 'Birmaans, Birmees');
INSERT INTO `t_language` VALUES ('na', 'Nauru', 'nauruan', 'Ekakairũ Naoero', 'Nauruaans');
INSERT INTO `t_language` VALUES ('nb', 'Norwegian Bokmål', 'norvégien bokmål', 'Norsk bokmål', 'Bokmål-Noors');
INSERT INTO `t_language` VALUES ('nd', 'Northern Ndebele', 'ndébélé du Nord', 'isiNdebele', 'Noord-Ndebele');
INSERT INTO `t_language` VALUES ('ne', 'Nepali', 'népalais', 'नेपाली', 'Nepalees');
INSERT INTO `t_language` VALUES ('ng', 'Ndonga', 'ndonga', 'Owambo', 'Ndonga');
INSERT INTO `t_language` VALUES ('nl', 'Dutch', 'néerlandais', 'Nederlands', 'Nederlands');
INSERT INTO `t_language` VALUES ('nn', 'Norwegian', 'norvégien', 'Norsk', 'Nynorsk (Nieuw-Noors)');
INSERT INTO `t_language` VALUES ('nr', 'Southern Ndebele', 'ndébéle du sud', 'isiNdebele', 'Zuid-Ndebele');
INSERT INTO `t_language` VALUES ('nv', 'Navajo, Navaho', 'navaho', 'Diné bizaad', 'Navajo');
INSERT INTO `t_language` VALUES ('oc', 'Occitan', 'occitan', 'occitan, lenga d\'òc', 'Occitaans');
INSERT INTO `t_language` VALUES ('oj', 'Ojibwa', 'ojibwa', 'ᐊᓂᔑᓈᐯᒧᐎᓐ', 'Ojibweg');
INSERT INTO `t_language` VALUES ('om', 'Oromo', 'galla', 'Afaan Oromoo', 'Afaan Oromo');
INSERT INTO `t_language` VALUES ('or', 'Oriya', 'oriya', 'ଓଡ଼ିଆ', 'Odia (Oriya)');
INSERT INTO `t_language` VALUES ('os', 'Ossetian, Ossetic', 'ossète', 'ирон æвзаг', 'Ossetisch');
INSERT INTO `t_language` VALUES ('pa', 'Panjabi, Punjabi', 'pendjabi', 'ਪੰਜਾਬੀ, پنجابی‎', 'Punjabi');
INSERT INTO `t_language` VALUES ('pi', 'Pāli', 'pāli', 'पाऴि', 'Pali');
INSERT INTO `t_language` VALUES ('pl', 'Polish', 'polonais', 'język polski, polszczyzna', 'Pools');
INSERT INTO `t_language` VALUES ('ps', 'Pashto, Pushto', 'pachto', 'پښتو ', 'Pasjtoe');
INSERT INTO `t_language` VALUES ('pt', 'Portuguese', 'portugais', 'português', 'Portugees');
INSERT INTO `t_language` VALUES ('qu', 'Quechua', 'quechua', 'Runa Simi, Kichwa', 'Quechua');
INSERT INTO `t_language` VALUES ('rm', 'Romansh', 'romanche', 'rumantsch grischun', 'Reto-Romaans');
INSERT INTO `t_language` VALUES ('ro', 'Romanian', 'roumain', 'limba română', 'Roemeens');
INSERT INTO `t_language` VALUES ('ru', 'Russian', 'russe', 'Русский', 'Russisch');
INSERT INTO `t_language` VALUES ('rw', 'Kinyarwanda', 'rwanda', 'Ikinyarwanda', 'Kinyarwanda');
INSERT INTO `t_language` VALUES ('sa', 'Sanskrit', 'sanskrit', 'संस्कृतम्', 'Sanskriet');
INSERT INTO `t_language` VALUES ('sc', 'Sardinian', 'sarde', 'sardu', 'Sardijns');
INSERT INTO `t_language` VALUES ('sd', 'Sindhi', 'sindhi', 'सिन्धी, سنڌي، سندھی‎', 'Sindhi');
INSERT INTO `t_language` VALUES ('se', 'Northern Sami', 'sami du nord', 'Davvisámegiella', 'Noord-Samisch (Noord-Laps)');
INSERT INTO `t_language` VALUES ('sg', 'Sango', 'sango', 'yângâ tî sängö', 'Sango');
INSERT INTO `t_language` VALUES ('si', 'Sinhala, Sinhalese', 'singhalais', 'සිංහල', 'Singalees');
INSERT INTO `t_language` VALUES ('sk', 'Slovak', 'slovaque', 'slovenčina, slovenský jazyk', 'Slovaaks');
INSERT INTO `t_language` VALUES ('sl', 'Slovene', 'slovène', 'slovenski jezik, slovenščina', 'Sloveens');
INSERT INTO `t_language` VALUES ('sm', 'Samoan', 'samoan', 'gagana fa\'a Samoa', 'Samoaans');
INSERT INTO `t_language` VALUES ('sn', 'Shona', 'shona', 'Shona', 'Shona');
INSERT INTO `t_language` VALUES ('so', 'Somali', 'somali', 'Soomaaliga, af Soomaali', 'Somalisch');
INSERT INTO `t_language` VALUES ('sq', 'Albanian', 'albanais', 'Shqip', 'Albanees');
INSERT INTO `t_language` VALUES ('sr', 'Serbian', 'serbe', 'српски језик', 'Servisch');
INSERT INTO `t_language` VALUES ('ss', 'Swati', 'swati', 'SiSwati', 'Swazi');
INSERT INTO `t_language` VALUES ('st', 'Southern Sotho', 'sotho du sud', 'Sesotho', 'Zuid-Sotho');
INSERT INTO `t_language` VALUES ('su', 'Sundanese', 'soudanais', 'Basa Sunda', 'Soendanees');
INSERT INTO `t_language` VALUES ('sv', 'Swedish', 'suédois', 'svenska', 'Zweeds');
INSERT INTO `t_language` VALUES ('sw', 'Swahili', 'swahili', 'Kiswahili', 'Swahili');
INSERT INTO `t_language` VALUES ('ta', 'Tamil', 'tamoul', 'தமிழ்', 'Tamil');
INSERT INTO `t_language` VALUES ('te', 'Telugu', 'télougou', 'తెలుగు', 'Telugu, Teloegoe');
INSERT INTO `t_language` VALUES ('tg', 'Tajik', 'tadjik', 'тоҷикӣ, toçikī, تاجیکی‎', 'Tadzjieks');
INSERT INTO `t_language` VALUES ('th', 'Thai', 'thaï', 'ไทย', 'Thai');
INSERT INTO `t_language` VALUES ('ti', 'Tigrinya', 'tigrigna', 'ትግርኛ', 'Tigrinya');
INSERT INTO `t_language` VALUES ('tk', 'Turkmen', 'turkmène', 'Türkmen, Түркмен', 'Turkmeens');
INSERT INTO `t_language` VALUES ('tl', 'Tagalog', 'tagalog', 'Wikang Tagalog', 'Tagalog');
INSERT INTO `t_language` VALUES ('tn', 'Tswana', 'tswana', 'Setswana', 'Tswana');
INSERT INTO `t_language` VALUES ('to', 'Tonga', 'tonga', 'faka Tonga', 'Tongaans, Tonga');
INSERT INTO `t_language` VALUES ('tr', 'Turkish', 'turque', 'Türkçe', 'Turks');
INSERT INTO `t_language` VALUES ('ts', 'Tsonga', 'tsonga', 'Xitsonga', 'Tsonga');
INSERT INTO `t_language` VALUES ('tt', 'Tatar', 'tatar', 'татар теле, tatar tele', 'Tataars');
INSERT INTO `t_language` VALUES ('tw', 'Twi', 'twi', 'Twi', 'Twi');
INSERT INTO `t_language` VALUES ('ty', 'Tahitian', 'tahïtien', 'Reo Tahiti', 'Tahitiaans');
INSERT INTO `t_language` VALUES ('ug', 'Uyghur', 'ouïgour', 'ئۇيغۇرچە‎, Uyghurche', 'Oeigoers');
INSERT INTO `t_language` VALUES ('uk', 'Ukrainian', 'ukrainien', 'українська мова', 'Oekraëens');
INSERT INTO `t_language` VALUES ('ur', 'Urdu', 'ourdu', 'اردو ', 'Urdu');
INSERT INTO `t_language` VALUES ('uz', 'Uzbek', 'ouszbek', 'Oʻzbek, Ўзбек, أۇزبېك‎', 'Oezbeeks');
INSERT INTO `t_language` VALUES ('ve', 'Venda', 'venda', 'Tshivenḓa', 'Venda');
INSERT INTO `t_language` VALUES ('vi', 'Vietnamese', 'vietnamien', 'Việt Nam', 'Vietnamees');
INSERT INTO `t_language` VALUES ('vo', 'Volapük', 'volapük', 'Volapük', 'VolapŸk, Volapuk');
INSERT INTO `t_language` VALUES ('wo', 'Wolof', 'wolof', 'Wollof', 'Wolof');
INSERT INTO `t_language` VALUES ('xh', 'Xhosa', 'xhosa', 'isiXhosa', 'Xhosa');
INSERT INTO `t_language` VALUES ('yi', 'Yiddish', 'yiddish', 'ייִדיש ', 'Jiddisch');
INSERT INTO `t_language` VALUES ('yo', 'Yoruba', 'yoruba', 'Yorùbá', 'Yoruba');
INSERT INTO `t_language` VALUES ('za', 'Zhuang, Chuang', 'zhuang, chuang', 'Saɯ cueŋƅ, Saw cuengh', 'Zhuang, Tsjoeang');
INSERT INTO `t_language` VALUES ('zh', 'Chinese', 'chinois', '中文 (Zhōngwén), 汉语, 漢語', 'Chinees');
INSERT INTO `t_language` VALUES ('zu', 'Zulu', 'zoulou', 'isiZulu', 'Zoeloe');

-- -----------------------------------------------------
-- Default values t_affiliation_subtype and t_affiliation_actor_type
-- -----------------------------------------------------

INSERT INTO `t_affiliation_actor_type` VALUES (0, 'ORGANIZATION / PERSON');
INSERT INTO `t_affiliation_actor_type` VALUES (1, 'ORGANIZATION');
INSERT INTO `t_affiliation_actor_type` VALUES (2, 'PERSON');

INSERT INTO `t_affiliation_subtype` VALUES (-1, 'ALL');
INSERT INTO `t_affiliation_subtype` VALUES (0, 'AFFILIATION');
INSERT INTO `t_affiliation_subtype` VALUES (1, 'AFFILIATED');
INSERT INTO `t_affiliation_subtype` VALUES (2, 'FILIATION');

-- -----------------------------------------------------
-- Default values t_affiliation_type
-- -----------------------------------------------------

INSERT INTO `t_affiliation_type` VALUES (-1, 0, -1, 'unset', 'indéfini', 'onbepaald');
INSERT INTO `t_affiliation_type` VALUES (0, 1, 0, '50% or more owned or financed by', 'détenu ou financé à 50 % ou plus par', 'Voor minstens 50% in handen van of gefinancierd door');
INSERT INTO `t_affiliation_type` VALUES (1, 1, 0, 'between 25 and 49,9% owned or financed by', 'détenu ou financé entre 25 et 49,9 % par', 'Tussen 25 and 49,9% in handen van of gefinancierd door');
INSERT INTO `t_affiliation_type` VALUES (2, 1, 0, 'less than 25% owned or financed by', 'détenu ou financé à moins de 25 % par', 'Minder dan 25% in handen van of gefinancierd door');
INSERT INTO `t_affiliation_type` VALUES (3, 1, 0, 'partially owned or financed by (unknown %)', 'détenu ou financé par (% indéterminé)', 'in handen van of gefinancierd door (onbekend %)');
INSERT INTO `t_affiliation_type` VALUES (4, 1, 0, 'department of', 'division de', 'Departement van');
INSERT INTO `t_affiliation_type` VALUES (5, 1, 0, 'member of', 'membre de', 'Lid van');
INSERT INTO `t_affiliation_type` VALUES (6, 1, 0, 'produced or organized by', 'produit ou organisé par', 'geproduceerd of georganiseerd door');
INSERT INTO `t_affiliation_type` VALUES (7, 0, 0, 'awarded or labbeled by', 'distingué ou labellisé par', 'Gecertifieerd door');
INSERT INTO `t_affiliation_type` VALUES (8, 0, 0, 'participating in', 'participant à', 'Neemt deel aan');
INSERT INTO `t_affiliation_type` VALUES (9, 1, 0, 'cabinet of', 'cabinet de', 'Kabinet van');
INSERT INTO `t_affiliation_type` VALUES (10, 1, 0, 'is graduating from', 'est dipolomer de', 'studeert af');
INSERT INTO `t_affiliation_type` VALUES (11, 2, 2, 'has as parent', 'a comme parent', 'heeft als ouder');
INSERT INTO `t_affiliation_type` VALUES (12, 0, 1, 'owns or finances at least 50% of', 'possède ou finance au moins 50 % de', 'Bezit of financiert meer dan 50% van');
INSERT INTO `t_affiliation_type` VALUES (13, 0, 1, 'owns or finances between 25 and 49,9% of ', 'possède ou finance 25 à 49,9 % de', 'Bezit of financiert tussen 25 and 49,9% van');
INSERT INTO `t_affiliation_type` VALUES (14, 0, 1, 'owns or finances less than 25% of', 'possède ou finance moins de 25 % de', 'Bezit of financiert minder dan 25% van');
INSERT INTO `t_affiliation_type` VALUES (15, 0, 1, 'holds shares of or finances (unknown %)', 'est actionnaire ou finance (% indéterminé)', 'Bezit aandelen van of financiert (% onbekend)');
INSERT INTO `t_affiliation_type` VALUES (16, 1, 1, 'has as division', 'a pour département', 'Heeft als departement');
INSERT INTO `t_affiliation_type` VALUES (17, 1, 1, 'has as member', 'a pour membre', 'Heeft als lid');
INSERT INTO `t_affiliation_type` VALUES (18, 1, 1, 'produces or organizes', 'produit ou organise', 'Produceert of regelt');
INSERT INTO `t_affiliation_type` VALUES (19, 0, 1, 'has awarded or labelled', 'distingue ou prime', 'Certificeert');
INSERT INTO `t_affiliation_type` VALUES (20, 1, 1, 'has as participant', 'a comme participant', 'Heeft als deelnemers');
INSERT INTO `t_affiliation_type` VALUES (21, 2, 1, 'has as cabinet', 'a comme cabinet', 'Heeft als kabinet');

-- -----------------------------------------------------
-- Default values for t_profession_type and t_profession_subtype
-- -----------------------------------------------------

INSERT INTO `t_profession_subtype` VALUES (0, 'HIERARCHY');
INSERT INTO `t_profession_subtype` VALUES (1, 'TYPE');

INSERT INTO `t_profession_type` VALUES (0, 0, 'other', '', '');
INSERT INTO `t_profession_type` VALUES (1, 1, 'formation', '', '');

-- -----------------------------------------------------
-- Default values for t_precision_date_type
-- -----------------------------------------------------

INSERT INTO `t_precision_date_type` VALUES (0, 1, 'Exactly since', 'exactement depuis', 'Precies sinds');
INSERT INTO `t_precision_date_type` VALUES (1, 1, 'At least since', 'au moins depuis', 'Ten minste sinds');
INSERT INTO `t_precision_date_type` VALUES (2, 0, 'Exactly until', 'exactement jusqu’à', 'Precies tot');
INSERT INTO `t_precision_date_type` VALUES (3, 0, 'At least until', 'au moins jusqu’à', 'Ten minste tot');
INSERT INTO `t_precision_date_type` VALUES (4, 0, 'Expected until', 'prévu jusqu’à', 'Gepland tot');
INSERT INTO `t_precision_date_type` VALUES (5, 0, 'Ongoing', 'en cours', 'Onderweg');

-- -----------------------------------------------------
-- Default values for visibility
-- -----------------------------------------------------

-- visibility for contributions
INSERT INTO `t_contribution_visibility` VALUES (0, 'contributions are publicly visible');
INSERT INTO `t_contribution_visibility` VALUES (1, 'contributions are visible in group only');
INSERT INTO `t_contribution_visibility` VALUES (2, 'contributions are only visible by contributor and group owner');

-- visibility for contributors
INSERT INTO `t_member_visibility` VALUES (0, 'details of members are publicly visible and as being part of related group');
INSERT INTO `t_member_visibility` VALUES (1, 'details of members are only visible in related group');
INSERT INTO `t_member_visibility` VALUES (2, 'details of members are not visible, except by group owner');

-- -----------------------------------------------------
-- Default values for permissions
-- -----------------------------------------------------

INSERT INTO `permission` VALUES (0, 'view_contribution', 'visualize contribution');
INSERT INTO `permission` VALUES (1, 'add_contribution', 'add new contribution');
INSERT INTO `permission` VALUES (2, 'edit_contribution', 'edit existing contribution');
INSERT INTO `permission` VALUES (3, 'del_contribution', 'delete contribution');
INSERT INTO `permission` VALUES (4, 'view_member', 'view group members and details');
INSERT INTO `permission` VALUES (5, 'add_member', 'add a member into a group');
INSERT INTO `permission` VALUES (6, 'edit_member', 'edit group member (contributor) profile');
INSERT INTO `permission` VALUES (7, 'block_member', 'block member from contributing into group');
INSERT INTO `permission` VALUES (8, 'merge_contribution', 'merge contributions from group into main database');
INSERT INTO `permission` VALUES (9, 'block_contributor', 'prevent contributor to contributute into webdeb');
INSERT INTO `permission` VALUES (10, 'assign_role', 'assign role to contributor');
INSERT INTO `permission` VALUES (11, 'create_group', 'create a new group');
INSERT INTO `permission` VALUES (12, 'delete_group', 'delete a group');
INSERT INTO `permission` VALUES (13, 'disable_annotation', 'disable text annotation service');

-- -----------------------------------------------------
-- Default roles: roles are valid inside a group (except admin that superseedes everything)
-- -----------------------------------------------------

INSERT INTO `role` VALUES (0, 'viewer');
INSERT INTO `role` VALUES (1, 'contributor');
INSERT INTO `role` VALUES (2, 'group_owner');
INSERT INTO `role` VALUES (3, 'admin');

-- -----------------------------------------------------
-- Mapping between roles and permissions (note that permissions may be completed by group permissions and visibility)
-- -----------------------------------------------------

-- viewer may only view
INSERT INTO `role_has_permission` VALUES (0, 0);

-- contributor may view, add, edit contributions and create a group
INSERT INTO `role_has_permission` VALUES (1, 0);
INSERT INTO `role_has_permission` VALUES (1, 1);
INSERT INTO `role_has_permission` VALUES (1, 2);
INSERT INTO `role_has_permission` VALUES (1, 4);
INSERT INTO `role_has_permission` VALUES (1, 11);

-- group owner may view, add, edit contributions and add or block members
INSERT INTO `role_has_permission` VALUES (2, 0);
INSERT INTO `role_has_permission` VALUES (2, 1);
INSERT INTO `role_has_permission` VALUES (2, 2);
INSERT INTO `role_has_permission` VALUES (2, 4);
INSERT INTO `role_has_permission` VALUES (2, 5);
INSERT INTO `role_has_permission` VALUES (2, 7);

-- admin may do everything
INSERT INTO `role_has_permission` VALUES (3, 0);
INSERT INTO `role_has_permission` VALUES (3, 1);
INSERT INTO `role_has_permission` VALUES (3, 2);
INSERT INTO `role_has_permission` VALUES (3, 3);
INSERT INTO `role_has_permission` VALUES (3, 4);
INSERT INTO `role_has_permission` VALUES (3, 5);
INSERT INTO `role_has_permission` VALUES (3, 6);
INSERT INTO `role_has_permission` VALUES (3, 7);
INSERT INTO `role_has_permission` VALUES (3, 8);
INSERT INTO `role_has_permission` VALUES (3, 9);
INSERT INTO `role_has_permission` VALUES (3, 10);
INSERT INTO `role_has_permission` VALUES (3, 11);
INSERT INTO `role_has_permission` VALUES (3, 12);
INSERT INTO `role_has_permission` VALUES (3, 13);

-- -----------------------------------------------------
-- Default public group (opened, members are visible globally, contributions are visible globally), anyone may add, edit contributions and view members
-- -----------------------------------------------------

INSERT INTO `contributor_group` VALUES (-1, 'webdeb public', '', 1, 0, 0, 0, now());
UPDATE `contributor_group` SET id_group = 0 WHERE id_group = -1;
INSERT INTO `group_has_permission` VALUES (0, 1);
INSERT INTO `group_has_permission` VALUES (0, 2);

-- default user for WebDeb administration
INSERT INTO `contributor` values (0, 'Admin', 'Admin', 'WebDeb', 'admin@webdeb.be', null, now(), 'F', 2016, null, '', null, null, 1, 0, 0, 1, null, 1, null, now());
INSERT INTO `contributor_has_group` values (0, 0, 3, 0, true, null, now());

-- -----------------------------------------------------
-- Default values for warned words type (the context where a word need a warning)
-- -----------------------------------------------------

INSERT INTO `t_warned_word_type` VALUES (0, 'profession', 'profession', 'beroep');
INSERT INTO `t_warned_word_type` VALUES (1, 'argument', 'affirmation', 'argument');
INSERT INTO `t_warned_word_type` VALUES (2, 'text', 'texte', 'tekst');
INSERT INTO `t_warned_word_type` VALUES (3, 'folder', 'dossier', 'map');

-- -----------------------------------------------------
-- Default values for warned words (words that need warning when they appeard in contribution names)
-- -----------------------------------------------------

INSERT INTO `t_warned_word` VALUES (0, 'ex-', 'ex-',0,0, 'ex-');
INSERT INTO `t_warned_word` VALUES (1, 'former', 'ancien',0,0, 'voormalige');
INSERT INTO `t_warned_word` VALUES (2, 'honorary', 'honoraire',1,0, 'ere-');
INSERT INTO `t_warned_word` VALUES (3, 'emeritus', 'émérite',1,0, 'emeritus');
INSERT INTO `t_warned_word` VALUES (4, 'federal', 'fédéral',1,0, 'federaal');
INSERT INTO `t_warned_word` VALUES (5, 'i', 'je',1,1, 'ik');
INSERT INTO `t_warned_word` VALUES (6, 'you', 'tu',1,1, 'je');
INSERT INTO `t_warned_word` VALUES (7, 'he', 'il',1,1, 'hij');
INSERT INTO `t_warned_word` VALUES (8, 'she', 'elle',1,1, 'ze');
INSERT INTO `t_warned_word` VALUES (9, 'we', 'nous',1,1, 'we');
INSERT INTO `t_warned_word` VALUES (10, 'you', 'vous',1,1, 'jullie');
INSERT INTO `t_warned_word` VALUES (11, 'they', 'ils',1,1, 'u');
INSERT INTO `t_warned_word` VALUES (12, 'her', 'elles',1,1, 'het');
INSERT INTO `t_warned_word` VALUES (13, 'it', 'on',1,1, 'hen');
INSERT INTO `t_warned_word` VALUES (14, 'him', 'lui',1,1, 'hun');
INSERT INTO `t_warned_word` VALUES (15, 'think', 'estime',0,1, 'denk');
INSERT INTO `t_warned_word` VALUES (16, 'think', 'estimons',0,1, 'denken');
INSERT INTO `t_warned_word` VALUES (17, 'think', 'pense',0,1, 'denk');
INSERT INTO `t_warned_word` VALUES (18, 'think', 'pensons',0,1, 'denken');
INSERT INTO `t_warned_word` VALUES (19, 'news', 'actualité',3,1, 'actualiteit');

-- -----------------------------------------------------
-- Default values for place type
-- -----------------------------------------------------

INSERT INTO `t_place_type` VALUES (0, 'Continent', 'Continent', 'Continent');
INSERT INTO `t_place_type` VALUES (1, 'World region, union of countries', 'Region du monde, regroupement de pays', 'Wereldregio, landenunie');
INSERT INTO `t_place_type` VALUES (2, 'Country', 'Pays', 'Land');
INSERT INTO `t_place_type` VALUES (3, 'Region, state', 'Region, état', 'Regio, Staat');
INSERT INTO `t_place_type` VALUES (4, 'Province, sub-region', 'Province, sous-région', 'Provincie, Sub-region');
INSERT INTO `t_place_type` VALUES (5, 'City, place', 'Ville, lieu', 'Stad, plaats');

-- -----------------------------------------------------
-- Default values for place (all continents and their names)
-- -----------------------------------------------------

-- continent place
INSERT INTO `place` VALUES (0, null, null, '50.75', '4.5', 0, null, null, null, null);
INSERT INTO `place` VALUES (1, null, 'EU', '51.00000', '10.00000', 0, null, null, null, null);
INSERT INTO `place` VALUES (2, null, 'NA','51,0000002', '-109,0000002', 0, null, null, null, null);
INSERT INTO `place` VALUES (3 null, 'SA', '-21,0002179', '-61,0006565', 0, null, null, null, null);
INSERT INTO `place` VALUES (4, null, 'AS','56,0000002', '103,9999998', 0, null, null, null, null);
INSERT INTO `place` VALUES (5, null, 'OC', '-18,3128000', '138,5156000', 0, null, null, null, null);
INSERT INTO `place` VALUES (6, null, 'AF', '2,0000003', '15,9999997', 0, null, null, null, null);

-- continent place names
INSERT INTO `place_i18names` VALUES (0, 'World', 'en');
INSERT INTO `place_i18names` VALUES (0, 'Monde', 'fr');
INSERT INTO `place_i18names` VALUES (0, 'Wereld', 'nl');
INSERT INTO `place_i18names` VALUES (0, 'Welt', 'de');
INSERT INTO `place_i18names` VALUES (1, 'Europe', 'en');
INSERT INTO `place_i18names` VALUES (1, 'Europe', 'fr');
INSERT INTO `place_i18names` VALUES (1, 'Europa', 'nl');
INSERT INTO `place_i18names` VALUES (1, 'Europa', 'de');
INSERT INTO `place_i18names` VALUES (2, 'North America', 'en');
INSERT INTO `place_i18names` VALUES (2, 'Amerique du nord', 'fr');
INSERT INTO `place_i18names` VALUES (2, 'Noord-Amerika', 'nl');
INSERT INTO `place_i18names` VALUES (2, 'Nordamerika', 'de');
INSERT INTO `place_i18names` VALUES (3, 'South America', 'en');
INSERT INTO `place_i18names` VALUES (3, 'Amerique du sud', 'fr');
INSERT INTO `place_i18names` VALUES (3, 'Zuid-Amerika', 'nl');
INSERT INTO `place_i18names` VALUES (3, 'Südamerika', 'de');
INSERT INTO `place_i18names` VALUES (4, 'Asia', 'en');
INSERT INTO `place_i18names` VALUES (4, 'Asie', 'fr');
INSERT INTO `place_i18names` VALUES (4, 'Azië', 'nl');
INSERT INTO `place_i18names` VALUES (4, 'Asien', 'de');
INSERT INTO `place_i18names` VALUES (5, 'Oceania', 'en');
INSERT INTO `place_i18names` VALUES (5, 'Océanie', 'fr');
INSERT INTO `place_i18names` VALUES (5, 'Oceanië', 'nl');
INSERT INTO `place_i18names` VALUES (5, 'Ozeanien', 'de');
INSERT INTO `place_i18names` VALUES (6, 'Africa', 'en');
INSERT INTO `place_i18names` VALUES (6, 'Amerique du sud', 'fr');
INSERT INTO `place_i18names` VALUES (6, 'Afrika', 'nl');
INSERT INTO `place_i18names` VALUES (6, 'Afrika', 'de');

-- -----------------------------------------------------
-- Default values for word gender
-- -----------------------------------------------------

INSERT INTO `t_word_gender` VALUES ('F', 'feminine', 'féminin', 'vrouwelijk');
INSERT INTO `t_word_gender` VALUES ('M', 'masculine', 'masculin', 'mannelijk');
INSERT INTO `t_word_gender` VALUES ('N', 'neuter', 'neutre', 'neutraal');

-- -----------------------------------------------------
-- Default values for folder type
-- -----------------------------------------------------

INSERT INTO `t_folder_type` VALUES (0, 'Simple folder', 'Dossier normal', 'Normaal map');
INSERT INTO `t_folder_type` VALUES (1, 'Tag folder', 'Dossier tag', 'Label map');
INSERT INTO `t_folder_type` VALUES (2, 'Composite folder', 'Dossier composite', 'Samengestelde map');

-- -----------------------------------------------------
-- Default argument
-- -----------------------------------------------------

INSERT INTO `contribution` VALUES (0, 2, 0, '',	1, 1, 1, 0, now());
INSERT INTO `contribution_in_group` VALUES (0, 0);
INSERT INTO `contribution_has_contributor` VALUES (0, 0, now(),	0, '');
INSERT INTO `argument_dictionary` VALUES (0, '', 'fr');
INSERT INTO `argument` VALUES (0, 0, 0);

-- -----------------------------------------------------
-- Default values for automatic similarity and justification validation state
-- -----------------------------------------------------

INSERT INTO `t_validation_state` VALUES (0, 'to validate', 'à valider', 'valideren');
INSERT INTO `t_validation_state` VALUES (1, 'validate', 'valider', 'bevestigen');
INSERT INTO `t_validation_state` VALUES (2, 'not validate', 'non validé', 'niet gevalideerd');

-- -----------------------------------------------------
-- Default values t_argument_type
-- -----------------------------------------------------

INSERT INTO `t_argument_type` VALUES (0, 'Constative', 'Descriptive', 'Descriptief');
INSERT INTO `t_argument_type` VALUES (1, 'Prescriptive', 'Prescriptive', 'Prescriptief');
INSERT INTO `t_argument_type` VALUES (2, 'First argument', 'Affirmation faitière', 'First argument');


-- -----------------------------------------------------
-- Default values t_arg_shade_type (id, type, timing, en, fr)
-- -----------------------------------------------------

-- constative
INSERT INTO `t_arg_shade_type` VALUES (0,0, 'There is no doubt that', 'Il est vrai que', 'Het is waar dat');
INSERT INTO `t_arg_shade_type` VALUES (1,0, 'It is untrue that', 'Il est faux de dire que', 'Het is niet waar dat');

-- prescriptive
INSERT INTO `t_arg_shade_type` VALUES (2,1, 'It is necessary to', 'Il faut', 'We moeten');
INSERT INTO `t_arg_shade_type` VALUES (3,1, 'It is necessary not to', 'Il ne faut pas', 'We moeten niet');

-- debate
INSERT INTO `t_arg_shade_type` VALUES (4,2, 'Should we', 'Faut-il ou non', 'Moeten we of niet');
INSERT INTO `t_arg_shade_type` VALUES (5,2, 'Is it true or false that', 'Est-il vrai ou faux que', 'Is het waar of niet dat');

-- -----------------------------------------------------
-- Default values t_argument_linktype
-- -----------------------------------------------------

INSERT INTO `t_argument_linktype` VALUES (0, 'Similarity', 'Similitude', 'Gelijkenis');
INSERT INTO `t_argument_linktype` VALUES (1, 'Justification', 'Justification', 'Verantwoording');
INSERT INTO `t_argument_linktype` VALUES (2, 'Illustration', 'Illustration', 'Illustratie');

-- -----------------------------------------------------
-- Default values t_link_shade_type
-- -----------------------------------------------------

-- similarity
INSERT INTO `t_link_shade_type` VALUES (0,0, 'is similar to', 'est similaire à', 'is vergelijkbaar met');
INSERT INTO `t_link_shade_type` VALUES (1,0, 'opposes', 's\'oppose à', 'staat tegenover');

-- justification
INSERT INTO `t_link_shade_type` VALUES (2,1, 'supports', 'soutient/justifie', 'ondersteunt');
INSERT INTO `t_link_shade_type` VALUES (3,1, 'rejects', 'rejette', 'verwerpt');

-- illustration
INSERT INTO `t_link_shade_type` VALUES (4,2, 'illustrate', 'illustre', 'illustrate');
INSERT INTO `t_link_shade_type` VALUES (5,2, 'shaded example', 'exemple nuancé', 'shaded example');
INSERT INTO `t_link_shade_type` VALUES (6,2, 'counter example', 'contre example', 'counter example');


INSERT INTO `t_external_source_name` VALUES (0, 'Twitter');
INSERT INTO `t_external_source_name` VALUES (1, 'Chrome Extension');
INSERT INTO `t_external_source_name` VALUES (2, 'Firefox Extension');
