/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.accessor.api;

import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;

import java.util.List;
import java.util.Map;

/**
 * This interface represents an accessor for debates persisted into the database
 *
 * @author Martin Rouffiange
 */
public interface DebateAccessor extends ContributionAccessor  {

    /**
     * Retrieve a Debate by its id
     *
     * @param id a debate id
     * @param hit true if this retrieval must be counted as a visualization
     * @return the argument concrete object corresponding to the given id, null if not found
     */
   Debate retrieve(Long id, boolean hit);

    /**
     * Save a debate on behalf of a given contributor If debate.getId has been set, update the
     * debate, otherwise create argument and update contribution id.
     *
     * @param contribution a contribution argument to save
     * @param currentGroup the current group id from which the contributor triggered the save action (for auto-created actors)
     * @param contributor the contributor id that asked to save the contribution
     * @return a map of Contribution type and a possibly empty list of Contributions (Actors or Folders) created automatically with this
     * save action (new contributions)
     *
     * @throws PermissionException if given contributor may not publish in current group or given contribution may not
     * be published in current group, or given contribution does not belong to current group
     * @throws PersistenceException if an error occurred, a.o., unset required field or no version number for
     * an existing contribution (id set). The exception message will contain a more complete description of the
     * error.
     */
    Map<Integer, List<Contribution>> save(Debate contribution, int currentGroup, Long contributor) throws PermissionException, PersistenceException;

    /**
     * Get a randomly chose Debate
     *
     * @return a Debate
     */
    Debate random();
}
