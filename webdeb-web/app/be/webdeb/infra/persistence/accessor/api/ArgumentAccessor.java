/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.accessor.api;

import be.webdeb.core.api.argument.*;
import be.webdeb.core.api.contribution.ContextContribution;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This interface represents an accessor for arguments persisted into the database
 *
 * @author Fabian Gilson
 */
public interface ArgumentAccessor extends ContributionAccessor {

  /**
   * Retrieve an Argument by its id
   *
   * @param id an argument id
   * @param hit true if this retrieval must be counted as a visualization
   * @return the argument concrete object corresponding to the given id, null if not found
   */
  Argument retrieve(Long id, boolean hit);

  /**
   * Retrieve a contextualized argument by its id
   *
   * @param id a contextualized argument id
   * @param hit true if this retrieval must be counted as a visualization
   * @return the contextualized argument concrete object corresponding to the given id, null if not found
   */
  ArgumentContext retrieveContextualized(Long id, boolean hit);

  /**
   * Retrieve a contextualized argument from an argument or dictionary id
   *
   * @param id an argument or dictionary id
   * @param dic true if given id is a dictionary id
   * @return the contextualized argument concrete object corresponding to the given id, null if not found
   */
  ArgumentContext retrieveContextualizedFromArg(Long id, boolean dic);

  /**
   * Retrieve an argument dictionary from a dictionary id
   *
   * @param id an argument dictionary id
   * @return the argument dictionary concrete object corresponding to the given id, null if not found
   */
  ArgumentDictionary retrieveDictionary(Long id);

  /**
   * Retrieve an argument justification link by its id
   *
   * @param id a Contribution id
   * @return an ArgumentJustification if given id is an argument justification link, null otherwise
   */
  ArgumentJustification retrieveJustificationLink(Long id);

  /**
   * Retrieve an argument similarity link by its id
   *
   * @param id a Contribution id
   * @return an ArgumentSimilarity if given id is an argument similarity link, null otherwise
   */
  ArgumentSimilarity retrieveSimilarityLink(Long id);

  /**
   * Retrieve an argument illustration link between an excerpt by its id
   *
   * @param id a Contribution id
   * @return an ArgumentIllustration if given id is an argument illustration link, null otherwise
   */
  ArgumentIllustration retrieveIllustrationLink(Long id);

  /**
   * Save an argument dictionary on behalf of a given contributor If argument.getId has been set, update the
   * argument, otherwise create argument and update contribution id.
   *
   * @param dictionary an argument dictionary to save
   *
   * @throws PersistenceException if an error occurred, a.o., unset required field.
   * The exception message will contain a more complete description of the error.
   */
  void save(ArgumentDictionary dictionary) throws PersistenceException;

  /**
   * Save an argument on behalf of a given contributor If argument.getId has been set, update the
   * argument, otherwise create argument and update contribution id.
   *
   * All passed contribution (affiliation ids (aha) and folders) are also considered as valid.If an contribution has no id,
   * the contribution is considered as non-existing and created. This contribution is then returned.
   *
   * @param contribution a contribution argument to save
   * @param currentGroup the current group id from which the contributor triggered the save action (for auto-created actors)
   * @param contributor the contributor id that asked to save the contribution
   * @return a map of Contribution type and a possibly empty list of Contributions (Actors or Folders) created automatically with this
   * save action (new contributions)
   *
   * @throws PermissionException if given contributor may not publish in current group or given contribution may not
   * be published in current group, or given contribution does not belong to current group
   * @throws PersistenceException if an error occurred, a.o., unset required field or no version number for
   * an existing contribution (id set). The exception message will contain a more complete description of the
   * error.
   */
  Map<Integer, List<Contribution>> save(Argument contribution, int currentGroup, Long contributor) throws PermissionException, PersistenceException;

  /**
   * Save a contextualized argument on behalf of a given contributor If contextualized argument.getId has been set, update the
   * argument, otherwise create contextualized argument and update contribution id.
   *
   * All passed contribution (affiliation ids (aha) and folders) are also considered as valid.If an contribution has no id,
   * the contribution is considered as non-existing and created. This contribution is then returned.
   *
   * @param contribution a contribution argument to save
   * @param currentGroup the current group id from which the contributor triggered the save action (for auto-created actors)
   * @param contributor the contributor id that asked to save the contribution
   * @return a map of Contribution type and a possibly empty list of Contributions (Actors or Folders) created automatically with this
   * save action (new contributions)
   *
   * @throws PermissionException if given contributor may not publish in current group or given contribution may not
   * be published in current group, or given contribution does not belong to current group
   * @throws PersistenceException if an error occurred, a.o., unset required field or no version number for
   * an existing contribution (id set). The exception message will contain a more complete description of the
   * error.
   */
  Map<Integer, List<Contribution>> save(ArgumentContext contribution, int currentGroup, Long contributor) throws PermissionException, PersistenceException;

  /**
   * Save an argument justification link. Both contextualized arguments provided in given link must exist, as well as the contributor.
   *
   * @param link the link to save
   * @param currentGroup the current group id from which the contributor triggered the save action (for auto-created actors)
   * @param contributor the contributor id that asked to save the contribution
   *
   * @throws PermissionException if given contributor may not publish in current group or given contribution may not
   * be published in current group, or given contribution does not belong to current group
   * @throws PersistenceException if an error occurred at the database layer (concrete error will be wrapped)
   */
  void save(ArgumentJustification link, int currentGroup, Long contributor) throws PermissionException, PersistenceException;

  /**
   * Save an argument similarity link. Both arguments provided in given link must exist, as well as the contributor.
   *
   * @param link the link to save
   * @param currentGroup the current group id from which the contributor triggered the save action (for auto-created actors)
   * @param contributor the contributor id that asked to save the contribution
   *
   * @throws PermissionException if given contributor may not publish in current group or given contribution may not
   * be published in current group, or given contribution does not belong to current group
   * @throws PersistenceException if an error occurred at the database layer (concrete error will be wrapped)
   */
  void save(ArgumentSimilarity link, int currentGroup, Long contributor) throws PermissionException, PersistenceException;

  /**
   * Save an argument illustration link. Both argument and excerpt provided in given link must exist, as well as the contributor.
   *
   * @param link the link to save
   * @param currentGroup the current group id from which the contributor triggered the save action (for auto-created actors)
   * @param contributor the contributor id that asked to save the contribution
   *
   * @throws PermissionException if given contributor may not publish in current group or given contribution may not
   * be published in current group, or given contribution does not belong to current group
   * @throws PersistenceException if an error occurred at the database layer (concrete error will be wrapped)
   */
  void save(ArgumentIllustration link, int currentGroup, Long contributor) throws PermissionException, PersistenceException;

  /**
   * Get all justification links that are justified by the given contextualized argument.
   *
   * @param id the given contextualized argument id
   * @return the (possibly empty) list of justification links that are justified by the given contextualized argument
   */
  List<ArgumentJustification> getJustificationLinksThatAreJustified(Long id);

  /**
   * Get all justification links that justify the given contextualized argument.
   *
   * @param id the given contextualized argument id
   * @return the (possibly empty) list of justification links that justify the given contextualized argument
   */
  List<ArgumentJustification> getJustificationLinksThatJustify(Long id);

  /**
   * Get all justification links that justify this contextualized argument and their similars arguments.
   *
   * @param id the given contextualized argument id
   * @return the (possibly empty) list of justification links that justify this contextualized argument and similars
   */
  List<ArgumentJustification> getAllJustificationLinksThatJustify(Long id);

  /**
   * Get all similarity links that are bound to the given argument.
   *
   * @param id the given argument id
   * @return the (possibly empty) list of similarity links involving the given argument
   */
  List<ArgumentSimilarity> getSimilarityLinks(Long id);

  /**
   * Get all similar arguments id that match with given shade for the given argument.
   *
   * @param id the argument id
   * @param shade the link shade to keep
   * @return the (possibly empty) list of similar argument id involving this argument
   */
  Set<Long> getSimilarIds(Long id, EArgumentLinkShade shade);

  /**
   * Get all illustration links that are bound to the given contextualized argument.
   *
   * @param id the given contextualized argument id
   * @return the (possibly empty) list of illustration links involving the given contextualized argument
   */
  List<ArgumentIllustration> getIllustrationLinks(Long id);

  /**
   * Get all illustration links that are bound to this contextualized argument.
   *
   * @param id the given contextualized argument id
   * @return the (possibly empty) list of illustration links involving this contextualized argument
   */
  List<ArgumentIllustration> getIllustrationLinksOfJustification(Long id);

  /**
   * Get the opposite contextualized argument in the context, if any
   *
   * @param id the given contextualized argument id
   * @return an opposite contextualized argument
   */
  ArgumentContext getOppositeArgumentContext(Long id);

  /**
   * Get all illustration links that are bound to this contextualized argument and the illustrations linked to
   * the opposite / similars argument of this one if any.
   *
   * @param id the given contextualized argument id
   * @param positive true if the given argument must be concidered as positive
   * @return the (possibly empty) list of illustration links involving this contextualized argument and its possible
   * opposite / similars.
   */
  List<ArgumentIllustration> getAllIllustrationLinks(Long id, boolean positive);

  /**
   * Get all translations of a given argument dictionary.
   *
   * @param id the given argument dictionary id
   * @return the (possibly empty) list of illustration links involving the given contextualized argument
   */
  List<ArgumentDictionary> getArgumentTranslations(Long id);

  /**
   * Retrieve the debate where the given contextualized argument is the first argument of the context contribution
   *
   * @param id a contextualized argument id
   * @return the contextualized argument concrete object corresponding to the given id, null if not found
   */
  ContextContribution retrieveDebateFirstArgument(Long id);

  /**
   * Retrieve all argument types
   *
   * @return a list of argument types
   */
  List<ArgumentType> getArgumentTypes();

  /**
   * Retrieve all argument link types
   *
   * @return a list of link types
   */
  List<ArgumentLinkType> getArgumentLinktypes();

  /**
   * Get the amount of argument links linked with a given contribution that a given contributor can see by given shade
   *
   * @param contribution a contribution id
   * @param contributor a contributor
   * @param shades the list of shades to compute
   * @return a map of shade / amount
   */
  Map<EArgumentLinkShade, Integer> getArgumentShadesAmount(Long contribution, Long contributor, List<EArgumentLinkShade> shades);

  /**
   * Check if an argument is similar to another one
   *
   * @param argToCompare the argument to compare with the other one
   * @param argument an argument
   * @return true if both arguments are similar
   */
  boolean isSimilarWith(Long argToCompare, Long argument);

  /**
   * Check if a link exists between two given contributions.
   *
   * @param origin an argument or contextualized argument id
   * @param destination an argument, acontextualized argument or an excerpt id
   * @return true if such a link already exists
   */
  boolean linkAlreadyExists(Long origin, Long destination);

  /**
   * Check if a justification link exists between two given contextualized arguments.
   *
   * @param origin a contextualized argument id
   * @param destination a contextualized argument id
   * @return true if such a link already exists
   */
  boolean justificationAlreadyExists(Long origin, Long destination);

  /**
   * Check if a similarity link exists between two given arguments
   *
   * @param origin an origin id
   * @param destination an argument id
   * @return true if such a link already exists
   */
  boolean similarityAlreadyExists(Long origin, Long destination);

  /**
   * Check if a illustration exists between a given contextualized argument an a given excerpt.
   *
   * @param argument a contextualized argument id
   * @param excerpt an excerpt id
   * @return true if such a link already exists
   */
  boolean illustrationAlreadyExists(Long argument, Long excerpt);

  /**
   * Find an Argument by title
   *
   * @param title an argument title
   * @return a corresponding argument or null
   */
  Argument findUniqueByTitle(String title);

  /**
   * Find a list of ArgumentDictionary by title
   *
   * @param title an argument dictionary title
   * @param lang a two char iso-639-1 language code
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a possibly empty list of ArgumentDictionary with their title containing the given title
   */
  List<ArgumentDictionary> findByTitle(String title, String lang, int fromIndex, int toIndex);

  /**
   * Get all texts where this argument (or similars) appeared.
   *
   * @param id an argument id
   * @return a possibly empty list of texts
   */
  List<Text> getAllTexts(Long id);

  /**
   * Get all debates where this argument (or similars) appeared.
   *
   * @param id an argument id
   * @return a possibly empty list of debates
   */
  List<Debate> getAllDebates(Long id);

  /**
   * Get all folders linked with this argument and similars
   *
   * @param id an argument id
   * @return a possibly empty list of folders
   */
  List<Folder> getAllFolders(Long id);

  /**
   * Get all potential arguments that could justify the given contextualized argument.
   *
   * @param argumentId the argument id
   * @param contextId the argument id
   * @return the (possibly empty) list of justification links that could justify the given contextualized argument
   */
  List<ArgumentContext> getPotentialJustificationLinks(Long argumentId, Long contextId);

  /**
   * Get all excerpts that could be illustrated the given contextualized argument.
   *
   * @param argumentId the argument id
   * @param contextId the argument id
   * @return the (possibly empty) list of illustration links that could be illustrated the given contextualized argument
   */
  List<Excerpt> getPotentialIllustrationLinks(Long argumentId, Long contextId);

  /**
   * Retrieve the amount of excerpts linked to the given argument by shade as integer map
   *
   * @param argumentId the argument id
   * @param contributorId the id of the contributor for which we need that stats
   * @param groupId the group where see the stats
   * @return the amount of excerpts that are connected to the given argument by shade as integer map
   */
  Map<Integer, Integer> getSimpleExcerptMetrics(Long argumentId, Long contributorId, int groupId);

}
