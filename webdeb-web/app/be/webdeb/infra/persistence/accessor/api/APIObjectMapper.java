/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.accessor.api;

import be.webdeb.infra.persistence.model.Contribution;
import be.webdeb.infra.persistence.model.Place;
import be.webdeb.infra.persistence.model.TCopyrightfreeSource;
import be.webdeb.core.exception.FormatException;
import be.webdeb.infra.persistence.model.*;
import be.webdeb.infra.persistence.model.Actor;
import be.webdeb.infra.persistence.model.Argument;
import be.webdeb.infra.persistence.model.Contributor;
import be.webdeb.infra.persistence.model.Group;

/**
 * Object mapper to convert database objects into api objects
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public interface APIObjectMapper {

  /**
   * Map a DB Contributor to an API contributor
   *
   * @param contributor a DB contributor
   * @return the corresponding API object
   *
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.contributor.Contributor toContributor(Contributor contributor) throws FormatException;

  /**
   * Map a DB TmpContributor to an API tmpcontributor
   *
   * @param contributor a DB tmpcontributor
   * @return the corresponding API object
   *
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.contributor.TmpContributor toTmpContributor(TmpContributor contributor) throws FormatException;

  /**
   * Map a DB affiliation to an API affiliation
   *
   * @param affiliation a DB contributor's affiliation
   * @return the corresponding API object
   *
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.actor.Affiliation toAffiliation(ContributorHasAffiliation affiliation) throws FormatException;

  /**
   * Map a DB affiliation to an API affiliation
   *
   * @param affiliation a DB Actor's affiliation
   * @return the corresponding API object
   *
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.actor.Affiliation toAffiliation(ActorHasAffiliation affiliation) throws FormatException;

  /**
   * Map a DB affiliation to an API affiliation denoting a affiliation member
   *
   * @param affiliation a DB Actor's affiliation
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.actor.Affiliation toMember(ActorHasAffiliation affiliation) throws FormatException;

  /**
   * Map a DB Actor to an API Actor
   *
   * @param actor a DB Actor
   * @return an api Actor containing all (actorwise) data from given Actor
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.actor.Actor toActor(Actor actor) throws FormatException;

  /**
   * Map a DB Actor to an API person
   *
   * @param actor an Actor
   * @return an api Person containing all data from given Actor, null if given Actor is not a person
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.actor.Person toPerson(Actor actor) throws FormatException;

  /**
   * Map a DB Actor to an API organization
   *
   * @param actor an Actor
   * @return an api Person containing all data from given Actor, null if given Actor is not a person
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.actor.Organization toOrganization(Actor actor) throws FormatException;

  /**
   * Map a DB text to an API text
   *
   * @param text a DB text
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.text.Text toText(Text text) throws FormatException;

  /**
   * Map a DB external text to an API external text
   *
   * @param text a DB external text
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.text.ExternalText toExternalText(ExternalText text) throws FormatException;

  /**
   * Map a DB text source to an API text source
   *
   * @param source a DB text source
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.text.TextSourceName toSourceName(TextSourceName source) throws FormatException;

  /**
   * Map a DB text free copyright source to an API text free copyright source
   *
   * @param freeSource a DB text free copyright source
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.text.TextCopyrightfreeSource toFreeSource(TCopyrightfreeSource freeSource) throws FormatException;


  /**
   * Map a DB argument dictionary to an API argument duictionary (translation links are not resolved to avoid cyclic never-ending calls)
   *
   * @param argument a DB argument dictionary
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.argument.ArgumentDictionary toArgumentDictionary(ArgumentDictionary argument) throws FormatException;

  /**
   * Map a DB argument to an API argument (links are not resolved to avoid cyclic never-ending calls)
   *
   * @param argument a DB argument
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.argument.Argument toArgument(Argument argument) throws FormatException;


  /**
   * Map a DB excerpt to an API excerpt
   *
   * @param excerpt a DB excerpt
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.excerpt.Excerpt toExcerpt(Excerpt excerpt) throws FormatException;

  /**
   * Map a DB  contextualized argument to an API excerpt (links are not resolved to avoid cyclic never-ending calls)
   *
   * @param argumentContext a DB contextualized argument
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.argument.ArgumentContext toArgumentContext(ArgumentContext argumentContext) throws FormatException;

  /**
   * Map a DB debate to an API debate
   *
   * @param debate a DB debate
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.debate.Debate toDebate(Debate debate) throws FormatException;

  /**
   * Map a DB external argument to an API external excerpt
   *
   * @param argument a DB external excerpt
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.excerpt.ExternalExcerpt toExternalExcerpt(ExternalExcerpt argument) throws FormatException;

  /**
   * Map a DB context element to an API context element
   *
   * @param contextElement a DB context element
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.excerpt.ContextElement toContextElement(ContextElement contextElement) throws FormatException;

  /**
   * Map a DB external author to an API external author
   *
   * @param author a DB external author
   * @return the corresponding API object
   */
  be.webdeb.core.api.actor.ExternalAuthor toExternalAuthor(ExternalAuthor author);

  /**
   * Map a DB illustration link between a contextualized argument and an excerpt to an API link
   *
   * @param link a DB illustration link between a contextualized argument and an excerpt
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.argument.ArgumentIllustration toArgumentIllustration(ArgumentHasExcerpt link) throws FormatException;

  /**
   * Map a DB illustration link between a contextualized argument and an excerpt to an API link. This method is used
   * when creating many illustrations in order to avoid recreating too many unnecessary API excerpts.
   *
   * @param link a DB illustration link between a contextualized argument and an excerpt
   * @param excerpt the API excerpt for the link
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.argument.ArgumentIllustration toArgumentIllustration(ArgumentHasExcerpt link, be.webdeb.core.api.excerpt.Excerpt excerpt) throws FormatException;

  /**
   * Map a DB illustration link between a contextualized argument and an excerpt to an API link. This method is used
   * when creating many illustrations in order to avoid recreating too many unnecessary API arguments.
   *
   * @param link a DB illustration link between a contextualized argument and an excerpt
   * @param argumentContext the API argument for the link
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.argument.ArgumentIllustration toArgumentIllustration(ArgumentHasExcerpt link,
                  be.webdeb.core.api.argument.ArgumentContext argumentContext) throws FormatException;

  /**
   * Map a DB argument similarity link to an API link
   *
   * @param link a DB argument similarity link between two arguments
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.argument.ArgumentSimilarity toArgumentSimilarity(ArgumentSimilarity link) throws FormatException;

  /**
   * Map a DB argument similarity link to an API link. This method is used when creating many arguments in order to
   * avoid recreating too many unnecessary API arguments.
   *
   * @param link a DB argument similarity link between two arguments
   * @param argument the API argument being one of the arguments in the link
   * @param isSource true if given argument is the source of the link
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.argument.ArgumentSimilarity toArgumentSimilarity(ArgumentSimilarity link,
                be.webdeb.core.api.argument.Argument argument, boolean isSource) throws FormatException;

  /**
   * Map a DB argument justification link to an API link
   *
   * @param link a DB argument link between two arguments
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.argument.ArgumentJustification toArgumentJustification(ArgumentJustification link) throws FormatException;

  /**
   * Map a DB argument justification to an API link. This method is used when creating many contextualized arguments
   * in order to avoid recreating too many unnecessary API arguments.
   *
   * @param link a DB argument link between two arguments
   * @param argumentContext the API argument being one of the arguments in the link
   * @param isSource true if given argument is the source of the link
   * @return the corresponding API object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.argument.ArgumentJustification toArgumentJustification(ArgumentJustification link,
      be.webdeb.core.api.argument.ArgumentContext argumentContext, boolean isSource) throws FormatException;

  /**
   * Create an API Actor role for given Actor and contribution with given "Contribution has Actor"
   *
   * @param cha a contribution has Actor relationship
   * @param actor an API Actor
   * @param contribution an API contribution to which given Actor is related
   * @return an API Actor role
   */
  be.webdeb.core.api.actor.ActorRole toActorRole(
      ContributionHasActor cha, be.webdeb.core.api.actor.Actor actor,
      be.webdeb.core.api.contribution.Contribution contribution);

  /**
   * Dispatcher method to map any kind of contribution
   *
   * @param contribution a contribution
   * @return an API contribution in its concrete object type, or null if given contribution has an unknown type
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.contribution.Contribution toContribution(Contribution contribution) throws FormatException;

  /**
   * Dispatcher method to map any kind of context contribution
   *
   * @param contribution a context contribution
   * @return an API context contribution in its concrete object type, or null if given context contribution has an unknown type
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.contribution.ContextContribution toContextContribution(Contribution contribution) throws FormatException;

  /**
   * Dispatcher method to map any kind of external contribution
   *
   * @param externalContribution a external contribution
   * @return an API external contribution in its concrete object type, or null if given external contribution has an unknown type
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.contribution.ExternalContribution toExternalContribution(ExternalContribution externalContribution) throws FormatException;

  /**
   * Create an API group from a database group
   *
   * @param group a database group to cast
   * @return the mapped API group object
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.contributor.Group toGroup(Group group) throws FormatException;

  /**
   * Create an API group subscription from a database group
   *
   * @param chg a database joint table contributor_has_group to cast
   * @param contributor the contributor that must be bound into subscription
   * @param group the group to bind into subscription
   * @return the mapped API group subscription object between given contributor and group
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.contributor.GroupSubscription toGroupSubscription(ContributorHasGroup chg,
      be.webdeb.core.api.contributor.Contributor contributor,
      be.webdeb.core.api.contributor.Group group) throws FormatException;

  /**
   * Create a default API group subscription with given group for which no contributor is defined and the default
   * EContributorRole.VIEWER is set as role.
   *
   * @param group a group
   * @return a subscription with no contributor and the default VIEWER role
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.contributor.GroupSubscription toGroupSubscription(be.webdeb.core.api.contributor.Group group) throws FormatException;

  /**
   * Create an API place from a database place
   *
   * @param place a place
   * @return an API place
   */
  be.webdeb.core.api.contribution.Place toPlace(Place place);

  /**
   * Map a DB folder to an API folder
   *
   * @param folder a DB Folder
   * @return the corresponding API object
   *
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.folder.Folder toFolder(Folder folder) throws FormatException;

  /**
   * Map a DB folder link to an API link
   *
   * @param link a DB Folder's link
   * @return the corresponding API object
   *
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.folder.FolderLink toFolderLink(FolderLink link) throws FormatException;

  /**
   * Map a DB project to an API project
   *
   * @param project a DB Project
   * @return the corresponding API object
   *
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.project.Project toProject(Project project) throws FormatException;

  /**
   * Map a DB project group to an API project group
   *
   * @param group a DB ProjectGroup
   * @return the corresponding API object
   *
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.project.ProjectGroup toProjectGroup(ProjectGroup group) throws FormatException;

  /**
   * Map a DB project subgroup to an API project subgroup
   *
   * @param subgroup a DB ProjectSubgroup
   * @return the corresponding API object
   *
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.project.ProjectSubgroup toProjectSubgroup(ProjectSubgroup subgroup) throws FormatException;

  /**
   * Map a DB ContirbutionToExplore to an API ContirbutionToExplore
   *
   * @param contirbutionToExplore a DB ContirbutionToExplore
   * @return the corresponding API object
   *
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.contribution.ContributionToExplore toContributionToExplore(ContributionToExplore contirbutionToExplore) throws FormatException;

  /**
   * Map a DB Advice to an API Advice
   *
   * @param advice a DB Advice
   * @return the corresponding API object
   *
   * @throws FormatException if the object retrieved from database is corrupted
   */
  be.webdeb.core.api.contributor.Advice toAdvice(Advice advice) throws FormatException;
}
