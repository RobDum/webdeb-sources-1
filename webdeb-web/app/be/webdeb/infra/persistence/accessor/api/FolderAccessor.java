/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.accessor.api;

import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.PartialContributions;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.folder.*;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.persistence.model.Contributor;

import java.util.List;
import java.util.Map;

/**
 * This interface represents an accessor for folders persisted into the database
 *
 * @author Martin Rouffiange
 */
public interface FolderAccessor extends ContributionAccessor {

  /**
   * Retrieve an Folder by its id
   *
   * @param id a folder id
   * @param hit true if this retrieval must be counted as a visualization
   * @return the folder concrete object corresponding to the given id, null if not found
   */
  Folder retrieve(Long id, boolean hit);

  /**
   * Retrieve a FolderLink between two folders
   *
   * @param parent the parent folder id
   * @param child the child folder id
   * @return a FolderLink if given ids are folder and linked, null otherwise
   */
  FolderLink retrieveLink(Long parent, Long child);

  /**
   * Find a list of Folder by their (partial) name
   *
   * @param name a name
   * @param type the Folder type to search
   * @return the list of Folders with their names containing the given name, or an empty list if none found
   */
  List<Folder> findByName(String name, EFolderType type);

  /**
   * Find a folder by its complete name and the name lang
   *
   * @param name the name to find
   * @param lang a two-char iso-639-1 language code
   * @return a the matched Folder, null otherwise
   */
  Folder findUniqueByNameAndLang(String name, String lang);

  /**
   * Get a randomly chose Folder
   *
   * @return a Folder
   */
  Folder random();

  /**
   * Save a folder on behalf of a given contributor If folder.getId has been set, update the
   * folder, otherwise create folder and update contribution id.
   *
   * @param contribution a contribution folder to save
   * @param contributor the contributor id that asked to save the contribution
   * @return a map of Contribution type and a possibly empty list of Contributions created folders
   *
   * @throws PermissionException if given contributor may not publish in WebDeb public group
   * @throws PersistenceException if an error occurred, a.o., unset required field or no version number for
   * an existing contribution (id set). The exception message will contain a more complete description of the
   * error.
   */
  Map<Integer, List<Contribution>> save(Folder contribution, Long contributor) throws PermissionException, PersistenceException;

  /**
   * Save a folder link. Both folders provided in given link must exist, as well as the contributor.
   *
   * @param link the link to save
   * @param contributor the contributor id that asked to save the contribution
   *
   * @throws PermissionException if given contributor may not publish in WebDeb public group
   * @throws PersistenceException if an error occurred at the database layer (concrete error will be wrapped)
   */
  void save(FolderLink link, Long contributor) throws PermissionException, PersistenceException;

  /**
   * Save a folder link between two given db folders from for a given contributor.
   *
   * @param parentFolder the db folder as parent
   * @param childFolder the db folder as child
   * @param contribution the contribution in case of link update
   * @param dbContributor the contributor id that asked to save the contribution
   *
   * @throws PersistenceException if an error occurred at the database layer (concrete error will be wrapped)
   */
  void saveFolderLink(be.webdeb.infra.persistence.model.Folder parentFolder, be.webdeb.infra.persistence.model.Folder childFolder,
                             be.webdeb.infra.persistence.model.Contribution contribution, Contributor dbContributor) throws PersistenceException;

  /**
   * Retrieve all folder types
   *
   * @return a list of folder types
   */
  List<FolderType> getFolderTypes();

  /**
   * Find the list of parents folder for a given folder id
   *
   * @param folder a folder id
   * @return the list of parents Folder
   */
  List<Folder> getParents(Long folder);

  /**
   * Find the list of children folder for a given folder id
   *
   * @param folder a folder id
   * @return the list of children Folder
   */
  List<Folder> getChildren(Long folder);

  /**
   * Find the list of contributions that are contained in a given folder
   *
   * @param folder a folder id
   * @return the list of contributions in the folder
   */
  List<Contribution> getFolderContributions(Long folder);

  /**
   * Find the list of contextualized arguments that are contained in a given folder limited by index
   *
   * @param folder a folder id
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return the list of contextualized arguments in the folder
   */
  PartialContributions<ArgumentContext> getContextualizedArguments(Long folder, int fromIndex, int toIndex);

  /**
   * Find the list of excerpts that are contained in a given folder limited by index
   *
   * @param folder a folder id
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return the list of excerpts in the folder
   */
  PartialContributions<Excerpt> getExcerpts(Long folder, int fromIndex, int toIndex);

  /**
   * Find the list of debates that are contained in a given folder limited by index
   *
   * @param folder a folder id
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return the list of debates in the folder
   */
  PartialContributions<Debate> getDebates(Long folder, int fromIndex, int toIndex);

  /**
   * Find the list of texts that are contained in a given folder limited by index
   *
   * @param folder a folder id
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return the list of texts in the folder
   */
  PartialContributions<Text> getTexts(Long folder, int fromIndex, int toIndex);

  /**
   * Find a list of rewording name from a given folder
   *
   * @param folder a folder id
   * @return a list of FolderName
   * @see FolderName
   */
  List<FolderName> getFolderRewordingNames(Long folder);

  /**
   * Get the hierarchy tree of a given folder
   *
   * @param folder a folder id
   * @return a hierarchy tree
   * @see HierarchyTree
   */
  HierarchyTree getFolderHierarchyTree(Long folder);

  /**
   * Check if a folder (as hierarchy) can be add in the hierarchy of a given folder
   *
   * @param folder a folder
   * @param hierarchy a folder to add at the given folder hierarchy
   * @param isParent true if the given folder will be the parent of the hierarchy folder
   * @return a hierarchy code depending of the issue
   * @see EHierarchyCode
   */
  EHierarchyCode checkHierarchy(Folder folder, Folder hierarchy, boolean isParent);

  /**
   * Get the number of contributions contained in this folder
   *
   * @param folder a folder id
   * @param contributor a contributor id
   * @param group a group id
   * @return the number of contributions contained in this folder
   */
  int getNbContributions(Long folder, Long contributor, int group);

}
