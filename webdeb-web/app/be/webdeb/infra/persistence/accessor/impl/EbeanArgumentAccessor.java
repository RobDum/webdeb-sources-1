/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.accessor.impl;

import be.webdeb.core.api.actor.ActorRole;
import be.webdeb.core.api.argument.*;
import be.webdeb.core.api.argument.Argument;
import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentDictionary;
import be.webdeb.core.api.argument.ArgumentJustification;
import be.webdeb.core.api.argument.ArgumentSimilarity;
import be.webdeb.core.api.contribution.*;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.ObjectNotFoundException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.persistence.accessor.api.ArgumentAccessor;
import be.webdeb.infra.persistence.model.*;
import be.webdeb.infra.persistence.model.Contribution;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Transaction;
import com.avaje.ebean.TxScope;
import com.sun.org.apache.xpath.internal.Arg;

import javax.inject.Singleton;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This accessor handles argument-related persistence actions.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
@Singleton
public class EbeanArgumentAccessor extends AbstractContributionAccessor<ArgumentFactory> implements ArgumentAccessor {

  private List<ArgumentType> argumentTypes = null;
  private List<ArgumentLinkType> argumentLinkTypes = null;

  @Override
  public Argument retrieve(Long id, boolean hit) {
    be.webdeb.infra.persistence.model.Argument argument = be.webdeb.infra.persistence.model.Argument.findById(id);
    if (argument != null) {
      try {
        Argument api =  mapper.toArgument(argument);
        if (hit) {
          argument.getContribution().addHit();
          argument.getContribution().update();
        }
        return api;
      } catch (FormatException e) {
        logger.error("unable to cast retrieved argument " + id, e);
      }
    } else {
      logger.warn("no argument found for id " + id);
    }
    return null;
  }

  @Override
  public ArgumentContext retrieveContextualized(Long id, boolean hit) {
    be.webdeb.infra.persistence.model.ArgumentContext argument = be.webdeb.infra.persistence.model.ArgumentContext.findById(id);
    if (argument != null) {
      try {
        ArgumentContext api =  mapper.toArgumentContext(argument);
        if (hit) {
          argument.getContribution().addHit();
          argument.getContribution().update();
        }
        return api;
      } catch (FormatException e) {
        logger.error("unable to cast retrieved argument " + id, e);
      }
    } else {
      logger.warn("no argument found for id " + id);
    }
    return null;
  }

  @Override
  public ArgumentContext retrieveContextualizedFromArg(Long id, boolean dic) {
    be.webdeb.infra.persistence.model.ArgumentContext argumentContext = null;
    if(dic){
      be.webdeb.infra.persistence.model.ArgumentDictionary dictionary = be.webdeb.infra.persistence.model.ArgumentDictionary.findById(id);
      if(dictionary != null && !dictionary.getArguments().isEmpty() && !dictionary.getArguments().get(0).getContextualizedArguments().isEmpty()){
        argumentContext = dictionary.getArguments().get(0).getContextualizedArguments().get(0);
      }
    }
    else{
      be.webdeb.infra.persistence.model.Argument argument = be.webdeb.infra.persistence.model.Argument.findById(id);
      if(argument != null && !argument.getContextualizedArguments().isEmpty()){
        argumentContext = argument.getContextualizedArguments().get(0);
      }
    }

    if(argumentContext != null){
      try {
        return mapper.toArgumentContext(argumentContext);
      } catch (FormatException e) {
        logger.debug("Unparsable argument context ", e);
      }
    }

    return null;
  }

  @Override
  public ArgumentDictionary retrieveDictionary(Long id) {
    be.webdeb.infra.persistence.model.ArgumentDictionary dictionary =
            be.webdeb.infra.persistence.model.ArgumentDictionary.findById(id);
    if(dictionary != null){
      try {
        return mapper.toArgumentDictionary(dictionary);
      } catch (FormatException e) {
        logger.error("unable to cast retrieved argument dictionary " + id, e);
      }
    }
    return null;
  }

  @Override
  public ArgumentJustification retrieveJustificationLink(Long id) {
    be.webdeb.infra.persistence.model.ArgumentJustification link = be.webdeb.infra.persistence.model.ArgumentJustification.findById(id);
    if (link != null) {
      try {
        return mapper.toArgumentJustification(link);
      } catch (FormatException e) {
        logger.error("unable to cast retrieved argument justification link " + id, e);
      }
    } else {
      logger.warn("no argument justification link found for id " + id);
    }
    return null;
  }

  @Override
  public ArgumentSimilarity retrieveSimilarityLink(Long id) {
    be.webdeb.infra.persistence.model.ArgumentSimilarity link = be.webdeb.infra.persistence.model.ArgumentSimilarity.findById(id);
    if (link != null) {
      try {
        return mapper.toArgumentSimilarity(link);
      } catch (FormatException e) {
        logger.error("unable to cast retrieved argument similarity link " + id, e);
      }
    } else {
      logger.warn("no argument similarity link found for id " + id);
    }
    return null;
  }

  @Override
  public ArgumentIllustration retrieveIllustrationLink(Long id) {
    be.webdeb.infra.persistence.model.ArgumentHasExcerpt link = be.webdeb.infra.persistence.model.ArgumentHasExcerpt.findById(id);
    if (link != null) {
      try {
        return mapper.toArgumentIllustration(link);
      } catch (FormatException e) {
        logger.error("unable to cast retrieved argument illustration link " + id, e);
      }
    } else {
      logger.warn("no argument illustration link found for id " + id);
    }
    return null;
  }

  @Override
  public void save(ArgumentDictionary dictionary) throws PersistenceException {
    logger.debug("try to save argument dictionary " + dictionary.toString());

    be.webdeb.infra.persistence.model.ArgumentDictionary dbDictionary =
            be.webdeb.infra.persistence.model.ArgumentDictionary.findById(dictionary.getId());

    // open transaction and handle the whole save chain
    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      if (dbDictionary == null) {
        // new argument dictionary
        logger.debug("start creation of new argument dictionary " + dictionary.getTitle());

        try {
          dbDictionary = new be.webdeb.infra.persistence.model.ArgumentDictionary(dictionary.getTitle(), dictionary.getLanguage().getCode());
          dbDictionary.save();
        } catch (Exception e) {
          logger.error("error while saving argument dictionary " + dictionary.getTitle(), e);
          throw new PersistenceException(PersistenceException.Key.SAVE_ARGUMENT_DICTIONARY, e);
        }
      } else {
        // update argument dictionary
        logger.debug("start update of existing argument dictionary " + dictionary.getId());

        try {
          dbDictionary.setTitle(dictionary.getTitle());
          dbDictionary.setLanguage(TLanguage.find.byId(dictionary.getLanguage().getCode()));
          dbDictionary.update();
        } catch (Exception e) {
          logger.error("error while updating argument dictionary " + dictionary.getId());
          throw new PersistenceException(PersistenceException.Key.SAVE_ARGUMENT_DICTIONARY, e);
        }
      }

      // set new id for given dictionary
      dictionary.setId(dbDictionary.getIdDictionary());

      transaction.commit();
      logger.info("saved " + dictionary.toString());

    } finally {
      transaction.end();
    }
  }

  @Override
  public Map<Integer, List<be.webdeb.core.api.contribution.Contribution>> save(Argument contribution, int currentGroup, Long contributor)
      throws PermissionException, PersistenceException {
    logger.debug("try to save argument " + contribution.toString() + " in group " + contribution.getInGroups());
    Map<Integer, List<be.webdeb.core.api.contribution.Contribution>> contributions = new HashMap<>();

    Contribution c = checkContribution(contribution, contributor, currentGroup);
    Contributor dbContributor = checkContributor(contributor, currentGroup);

    // open transaction and handle the whole save chain
    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    String title = convertToUTF8(contribution.getDictionary().getTitle());

    try {
      EModificationStatus status;
      be.webdeb.infra.persistence.model.Argument argument = updateArgument(contribution);
      if (argument.getIdContribution() == null) {
        // new argument
        logger.debug("start creation of new argument " + title);
        status = EModificationStatus.CREATE;

        // create contribution super type
        c = initContribution(EContributionType.ARGUMENT.id(), title);

        // create argument and binding
        c.setArgument(argument);
        argument.setContribution(c);

        // update groups
        updateGroups(contribution, c);
        try {
          c.save();
        } catch (Exception e) {
          logger.error("error while saving contribution " + title);
          throw new PersistenceException(PersistenceException.Key.SAVE_ARGUMENT, e);
        }

        // set id of argument
        argument.setIdContribution(c.getIdContribution());

        try {
          argument.save();
        } catch (Exception e) {
          logger.error("error while saving argument " + title, e);
          throw new PersistenceException(PersistenceException.Key.SAVE_ARGUMENT, e);
        }
      } else {
        logger.debug("start update of existing argument " + argument.getIdContribution());
        status = EModificationStatus.UPDATE;

        // manage groups for this argument and originating text
        updateGroups(contribution, argument.getContribution());
        argument.getContribution().setSortkey(title);

        try {
          // force update if only classes or bindings have been changed
          argument.update();
        } catch (Exception e) {
          logger.error("error while updating argument " + argument.getIdContribution());
          throw new PersistenceException(PersistenceException.Key.SAVE_ARGUMENT, e);
        }
      }
      // bind contributor to this argument
      bindContributor(argument.getContribution(), dbContributor, status);
      // set new id for given contribution
      contribution.setId(argument.getIdContribution());

      transaction.commit();
      logger.info("saved " + argument.toString());

    } finally {
      transaction.end();
    }

    return contributions;
  }

  @Override
  public Map<Integer, List<be.webdeb.core.api.contribution.Contribution>> save(ArgumentContext contribution, int currentGroup, Long contributor) throws PermissionException, PersistenceException {
    logger.debug("try to save contextualized argument " + contribution.toString() + " in group " + contribution.getInGroups());
    Map<Integer, List<be.webdeb.core.api.contribution.Contribution>> contributions = new HashMap<>();

    be.webdeb.infra.persistence.model.Contribution c = checkContribution(contribution, contributor, currentGroup);
    Contributor dbContributor = checkContributor(contributor, currentGroup);

    // check if argument id and context id exists, otherwise -> ObjectNotFound
    be.webdeb.infra.persistence.model.Argument argument =
            be.webdeb.infra.persistence.model.Argument.findById(contribution.getArgumentId());
    if (argument == null) {
      logger.error("unable to retrieve argument " + contribution.getArgumentId());
      throw new ObjectNotFoundException(Argument.class, contribution.getArgumentId());
    }
    be.webdeb.infra.persistence.model.Contribution context =
            be.webdeb.infra.persistence.model.Contribution.findById(contribution.getContextId());
    if (context == null) {
      logger.error("unable to retrieve context " + contribution.getContextId());
      throw new ObjectNotFoundException(Contribution.class, contribution.getContextId());
    }

    // open transaction and handle the whole save chain
    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());

    try {
      EModificationStatus status;
      be.webdeb.infra.persistence.model.ArgumentContext argumentContext;

      if (c == null) {
        // new argument
        logger.debug("start creation of new contextualized argument " + contribution.toString());
        status = EModificationStatus.CREATE;

        // create contribution super type
        c = initContribution(EContributionType.ARGUMENT_CONTEXTUALIZED.id(), contribution.getArgument().getDictionary().getTitle());

        // create contextualized argument and binding
        argumentContext = new be.webdeb.infra.persistence.model.ArgumentContext();
        c.setArgumentContext(argumentContext);
        argumentContext.setContribution(c);

        // update groups
        updateGroups(contribution, c);
        // TODO vérifier si les débats doivent être dans un groupe et texte
        // update groups of debate and context also, in case debate and context has been created in another group
        // we must add this group for bound debate and context (for visibility purpose)
        updateGroups(contribution, context);
        // manage places
        savePlaces(contribution.getPlaces(), c);

        c.save();
        // manage bindings to folders
        contributions.put(EContributionType.FOLDER.id(),
                bindFolders(c.getIdContribution(), contribution.getFoldersAsList(), contributor));

        // set id of argument and bind to argument and context
        argumentContext.setIdContribution(c.getIdContribution());
        argumentContext.setArgument(argument);
        argumentContext.setContext(context);

        // manage bindings to actors
        List<be.webdeb.core.api.contribution.Contribution> wrappers = new ArrayList<>();
        for (ActorRole a : contribution.getActors()) {
          logger.debug("will bind actor " + a.toString() + " to " + c.getIdContribution());
          wrappers.addAll(bindActor(c.getIdContribution(), a, currentGroup, contributor));
        }
        contributions.put(EContributionType.ACTOR.id(), wrappers);

        try {
          argumentContext.save();
        } catch (Exception e) {
          logger.error("error while saving contextualized argument " + argumentContext, e);
          throw new PersistenceException(PersistenceException.Key.SAVE_ARGUMENT_CONTEXT, e);
        }

        // set new id for given contribution
        contribution.setId(c.getIdContribution());

      } else {
        logger.debug("start update of existing contextualized argument " + contribution.getId());
        status = EModificationStatus.UPDATE;
        argumentContext = c.getArgumentContext();

        argumentContext.setArgument(argument);
        argumentContext.setContext(context);

        // manage bindings to actors, retrieve them, save unexisting ones and remove others
        contributions.put(EContributionType.ACTOR.id(),
                updateActorBindings(contribution, argumentContext.getContribution(), currentGroup, contributor));

        // manage folders
        contributions.put(EContributionType.FOLDER.id(),
                bindFolders(c.getIdContribution(), contribution.getFoldersAsList(), contributor));
        // manage places
        savePlaces(contribution.getPlaces(), c);

        // manage groups for this argument and originating text
        updateGroups(contribution, argumentContext.getContribution());
        updateGroups(contribution, argumentContext.getContext());

        try {
          // force update if only classes or bindings have been changed
          argumentContext.update();
        } catch (Exception e) {
          logger.error("error while updating contextualized argument " + argumentContext.getIdContribution());
          throw new PersistenceException(PersistenceException.Key.SAVE_ARGUMENT_CONTEXT, e);
        }
      }
      // bind contributor to this contextualized argument
      bindContributor(argumentContext.getContribution(), dbContributor, status);

      transaction.commit();
      logger.info("saved " + argumentContext.toString());

    } finally {
      transaction.end();
    }

    return contributions;
  }

  @Override
  public void save(ArgumentJustification link, int currentGroup, Long contributor) throws PermissionException, PersistenceException {
    logger.debug("try to save justification link " + link.toString() + " in group " + currentGroup);

    // ensure given parameters are ok
    Contribution contribution = checkContribution(link, contributor, currentGroup);
    Contributor dbContributor = checkContributor(contributor, currentGroup);

    // check if given link contains valid contextualized arguments
    be.webdeb.infra.persistence.model.ArgumentContext argumentFrom =
        be.webdeb.infra.persistence.model.ArgumentContext.findById(link.getOrigin().getId());
    if (argumentFrom == null) {
      logger.error("unable to retrieve origin argument context " + link.getOrigin().getId());
      throw new ObjectNotFoundException(ArgumentContext.class, link.getOrigin().getId());
    }

    be.webdeb.infra.persistence.model.ArgumentContext argumentTo =
        be.webdeb.infra.persistence.model.ArgumentContext.findById(link.getDestination().getId());
    if (argumentTo == null) {
      logger.error("unable to retrieve destination argument context " + link.getDestination().getId());
      throw new ObjectNotFoundException(ArgumentContext.class, link.getDestination().getId());
    }

    be.webdeb.infra.persistence.model.Contribution context =
            be.webdeb.infra.persistence.model.Contribution.findById(link.getContextId());
    if (context == null) {
      logger.error("unable to retrieve context contribution" + link.getDestination().getId());
      throw new ObjectNotFoundException(ArgumentContext.class, link.getDestination().getId());
    }

    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      EModificationStatus status;
      be.webdeb.infra.persistence.model.ArgumentJustification dbLink;

      if (contribution == null) {
        status = EModificationStatus.CREATE;

        // create and save contribution supertype
        contribution = initContribution(EContributionType.ARG_JUSTIFICATION.id(), null);
        try {
          contribution.save();
        } catch (Exception e) {
          logger.error("error while saving contribution for justification link between " + link.getOrigin().getId() + " "
              + link.getDestination().getId() + " of type " + link.getArgumentLinkType().getEType().name());
          throw new PersistenceException(PersistenceException.Key.SAVE_JUSTIFICATION_LINK, e);
        }

        // create link
        dbLink = new be.webdeb.infra.persistence.model.ArgumentJustification();
        contribution.setJustificationLink(dbLink);
        dbLink.setContribution(contribution);
        dbLink = updateJustificationLink(link, dbLink, argumentFrom, argumentTo, context);
        updateGroups(link, contribution);
        // save link
        try {
          contribution.update();
          dbLink.setIdContribution(contribution.getIdContribution());
          dbLink.save();
          logger.debug("saved justification link " + dbLink);

        } catch (Exception e) {
          logger.error("error while saving argument justification link from " + link.getOrigin().getId() + " to "
              + link.getDestination().getId(), e);
          throw new PersistenceException(PersistenceException.Key.SAVE_JUSTIFICATION_LINK, e);
        }

      } else {
        status = EModificationStatus.UPDATE;
        // update existing link
        dbLink = updateJustificationLink(link, contribution.getJustificationLink(), argumentFrom, argumentTo, context);
        updateGroups(link, dbLink.getContribution());

        try {
          dbLink.update();
          logger.debug("updated justification link " + dbLink);
        } catch (Exception e) {
          logger.error("error while updating argument justification link " + link.getId() + " from " + link.getOrigin().getId()
              + " to " + link.getDestination().getId(), e);
          throw new PersistenceException(PersistenceException.Key.SAVE_JUSTIFICATION_LINK, e);
        }
      }
      // save link to contributor
      bindContributor(dbLink.getContribution(), dbContributor, status);

      transaction.commit();
      logger.info("saved " + dbLink.toString());

    } finally {
      transaction.end();
    }
  }

  @Override
  public void save(ArgumentSimilarity link, int currentGroup, Long contributor) throws PermissionException, PersistenceException {
    logger.debug("try to save similarity link " + link.toString() + " in group " + currentGroup);

    // ensure given parameters are ok
    Contribution contribution = checkContribution(link, contributor, currentGroup);
    Contributor dbContributor = checkContributor(contributor, currentGroup);

    // check if given link contains valid arguments
    be.webdeb.infra.persistence.model.Argument argumentFrom =
            be.webdeb.infra.persistence.model.Argument.findById(link.getOrigin().getId());
    if (argumentFrom == null) {
      logger.error("unable to retrieve origin argument " + link.getOrigin().getId());
      throw new ObjectNotFoundException(Argument.class, link.getOrigin().getId());
    }

    be.webdeb.infra.persistence.model.Argument argumentTo =
            be.webdeb.infra.persistence.model.Argument.findById(link.getDestination().getId());
    if (argumentTo == null) {
      logger.error("unable to retrieve destination argument " + link.getDestination().getId());
      throw new ObjectNotFoundException(Argument.class, link.getDestination().getId());
    }

    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      EModificationStatus status;
      be.webdeb.infra.persistence.model.ArgumentSimilarity dbLink;

      if (contribution == null) {
        status = EModificationStatus.CREATE;

        // create and save contribution supertype
        contribution = initContribution(EContributionType.ARG_SIMILARITY.id(), null);
        try {
          contribution.save();
        } catch (Exception e) {
          logger.error("error while saving contribution for similarity link between " + link.getOrigin().getId() + " "
                  + link.getDestination().getId() + " of type " + link.getArgumentLinkType().getEType().name());
          throw new PersistenceException(PersistenceException.Key.SAVE_SIMILARITY_LINK, e);
        }

        // create link
        dbLink = new be.webdeb.infra.persistence.model.ArgumentSimilarity();
        contribution.setSimilarityLink(dbLink);
        dbLink.setContribution(contribution);
        dbLink = updateSimilarityLink(link, dbLink, argumentFrom, argumentTo);
        updateGroups(link, contribution);
        // save link
        try {
          contribution.save();
          dbLink.setIdContribution(contribution.getIdContribution());
          dbLink.save();
          logger.debug("saved similarity link " + dbLink);

        } catch (Exception e) {
          logger.error("error while saving argument similarity link from " + link.getOrigin().getId() + " to "
                  + link.getDestination().getId(), e);
          throw new PersistenceException(PersistenceException.Key.SAVE_SIMILARITY_LINK, e);
        }

      } else {
        status = EModificationStatus.UPDATE;
        // update existing link
        dbLink = updateSimilarityLink(link, contribution.getSimilarityLink(), argumentFrom, argumentTo);
        updateGroups(link, dbLink.getContribution());

        try {
          dbLink.update();
          logger.debug("updated similarity link " + dbLink);
        } catch (Exception e) {
          logger.error("error while updating argument similarity link " + link.getId() + " from " + link.getOrigin().getId()
                  + " to " + link.getDestination().getId(), e);
          throw new PersistenceException(PersistenceException.Key.SAVE_SIMILARITY_LINK, e);
        }
      }
      // save link to contributor
      bindContributor(dbLink.getContribution(), dbContributor, status);

      transaction.commit();
      logger.info("saved " + dbLink.toString());

    } finally {
      transaction.end();
    }
  }

  @Override
  public void save(ArgumentIllustration link, int currentGroup, Long contributor) throws PermissionException, PersistenceException {
    logger.debug("try to save illustration link " + link.toString() + " in group " + currentGroup);

    // ensure given parameters are ok
    Contribution contribution = checkContribution(link, contributor, currentGroup);
    Contributor dbContributor = checkContributor(contributor, currentGroup);

    // check if given link contains valid contextualized arguments
    be.webdeb.infra.persistence.model.ArgumentContext argument =
            be.webdeb.infra.persistence.model.ArgumentContext.findById(link.getArgument().getId());
    if (argument == null) {
      logger.error("unable to retrieve argument context " + link.getArgument().getId());
      throw new ObjectNotFoundException(ArgumentContext.class, link.getArgument().getId());
    }

    be.webdeb.infra.persistence.model.Excerpt excerpt =
            be.webdeb.infra.persistence.model.Excerpt.findById(link.getExcerpt().getId());
    if (excerpt == null) {
      logger.error("unable to retrieve excerpt " + link.getExcerpt().getId());
      throw new ObjectNotFoundException(ArgumentContext.class, link.getExcerpt().getId());
    }

    Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
    try {
      EModificationStatus status;
      be.webdeb.infra.persistence.model.ArgumentHasExcerpt dbLink;

      if (contribution == null) {
        status = EModificationStatus.CREATE;

        // create and save contribution supertype
        contribution = initContribution(EContributionType.ARG_ILLUSTRATION.id(), null);
        try {
          contribution.save();
        } catch (Exception e) {
          logger.error("error while saving contribution for illustration link between " + link.getArgument().getId() + " "
                  + link.getExcerpt().getId() + " of type " + link.getArgumentLinkType().getEType().name());
          throw new PersistenceException(PersistenceException.Key.SAVE_ILLUSTRATION_LINK, e);
        }

        // create link
        dbLink = new be.webdeb.infra.persistence.model.ArgumentHasExcerpt();
        contribution.setillustrationLink(dbLink);
        dbLink.setContribution(contribution);
        dbLink = updateIllustrationLink(link, dbLink, argument, excerpt);
        updateGroups(link, contribution);
        // save link
        try {
          contribution.save();
          dbLink.setIdContribution(contribution.getIdContribution());
          dbLink.save();
          logger.debug("saved illustration link " + dbLink);

        } catch (Exception e) {
          logger.error("error while saving argument illustration link from " + link.getArgument().getId() + " to "
                  + link.getExcerpt().getId(), e);
          throw new PersistenceException(PersistenceException.Key.SAVE_ILLUSTRATION_LINK, e);
        }

      } else {
        status = EModificationStatus.UPDATE;
        // update existing link
        dbLink = updateIllustrationLink(link, contribution.getIllustrationLink(), argument, excerpt);
        updateGroups(link, dbLink.getContribution());

        try {
          dbLink.update();
          logger.debug("updated illustration link " + dbLink);
        } catch (Exception e) {
          logger.error("error while updating argument illustration link " + link.getId() + " from " + link.getArgument().getId()
                  + " to " + link.getExcerpt().getId(), e);
          throw new PersistenceException(PersistenceException.Key.SAVE_ILLUSTRATION_LINK, e);
        }
      }
      // save link to contributor
      bindContributor(dbLink.getContribution(), dbContributor, status);

      transaction.commit();
      logger.info("saved " + dbLink.toString());

    } finally {
      transaction.end();
    }
  }

  @Override
  public synchronized List<ArgumentJustification> getJustificationLinksThatAreJustified(Long id) {
    be.webdeb.infra.persistence.model.ArgumentContext arg =
            be.webdeb.infra.persistence.model.ArgumentContext.findById(id);
    List<ArgumentJustification> result = new ArrayList<>();
    if (arg != null) {
      try {
        ArgumentContext a = mapper.toArgumentContext(arg);
        result.addAll(buildJustificationLinkList(arg.getArgumentJustificationsTo(), a, false));
      } catch (FormatException e) {
        logger.error("unable to cast contextualized argument " + arg, e);
      }
    }
    return result;
  }

  @Override
  public synchronized List<ArgumentJustification> getJustificationLinksThatJustify(Long id) {
    be.webdeb.infra.persistence.model.ArgumentContext arg =
            be.webdeb.infra.persistence.model.ArgumentContext.findById(id);
    List<ArgumentJustification> result = new ArrayList<>();
    if (arg != null) {
      try {
        ArgumentContext a = mapper.toArgumentContext(arg);
        result.addAll(buildJustificationLinkList(arg.getArgumentJustificationsFrom(), a, true));
      } catch (FormatException e) {
        logger.error("unable to cast contextualized argument " + arg, e);
      }
    }
    return result;
  }

  @Override
  public synchronized List<ArgumentJustification> getAllJustificationLinksThatJustify(Long id) {
    be.webdeb.infra.persistence.model.ArgumentContext arg =
            be.webdeb.infra.persistence.model.ArgumentContext.findById(id);
    List<ArgumentJustification> result = new ArrayList<>();
    if (arg != null) {
      try {
        ArgumentContext a = mapper.toArgumentContext(arg);

        Map<Long, be.webdeb.infra.persistence.model.ArgumentJustification> justificationMap = new HashMap<>();
        EArgumentShade shade = arg.getArgument().getShade().getEArgumentShade();

        arg.getSimilarArguments().forEach((k, v) ->
          be.webdeb.infra.persistence.model.ArgumentJustification.getJustificationsTo(k).forEach(e -> {
            if(!justificationMap.containsKey(e.getArgumentTo().getIdContribution())){
              if((shade.isApprobation() && !v.isApprobation()) || (shade.isOpposition() && !v.isOpposition())){
                e.getShade().setIdLinkShade(e.getShade().getELinkShade().getOpposite().id());
              }
              justificationMap.put(e.getArgumentTo().getIdContribution(), e);
            }
        }));

        result.addAll(buildJustificationLinkList(new ArrayList<>(justificationMap.values()), a, true));
      } catch (FormatException e) {
        logger.error("unable to cast contextualized argument " + arg, e);
      }
    }
    return result;
  }
  @Override
  public synchronized List<ArgumentSimilarity> getSimilarityLinks(Long id) {
    be.webdeb.infra.persistence.model.Argument arg =
            be.webdeb.infra.persistence.model.Argument.findById(id);
    List<ArgumentSimilarity> result = new ArrayList<>();
    if (arg != null) {
      result.addAll(buildSimilarityLinkList(arg.getSimilarArguments()));
    }
    return result;
  }

  @Override
  public synchronized Set<Long> getSimilarIds(Long id, EArgumentLinkShade shade) {
    be.webdeb.infra.persistence.model.Argument arg =
            be.webdeb.infra.persistence.model.Argument.findById(id);
    Set<Long> result = new HashSet<>();
    if (arg != null) {
      arg.getSimilarArguments().forEach(e -> {
        if(e.getArgumentTo().getIdContribution().equals(id)){
          result.add(e.getArgumentFrom().getIdContribution());
        }else{
          result.add(e.getArgumentTo().getIdContribution());
        }
      });
    }
    return result;
  }

  @Override
  public synchronized List<ArgumentIllustration> getIllustrationLinks(Long id) {
    be.webdeb.infra.persistence.model.ArgumentContext arg =
            be.webdeb.infra.persistence.model.ArgumentContext.findById(id);
    List<ArgumentIllustration> result = new ArrayList<>();
    if (arg != null) {
      try {
        ArgumentContext a = mapper.toArgumentContext(arg);
        result.addAll(buildIllustrationLinkList(arg.getArgumentIllustrations(), a));
      } catch (FormatException e) {
        logger.error("unable to cast contextualized argument ", e);
      }
    }
    return result;
  }

  @Override
  public List<ArgumentIllustration> getIllustrationLinksOfJustification(Long id) {
    be.webdeb.infra.persistence.model.ArgumentContext arg =
            be.webdeb.infra.persistence.model.ArgumentContext.findById(id);
    List<ArgumentIllustration> result = new ArrayList<>();
    if (arg != null) {
      try {
        ArgumentContext a = mapper.toArgumentContext(arg);

        Map<Long, be.webdeb.infra.persistence.model.ArgumentHasExcerpt> illustrationMap = new HashMap<>();

        // TODO use DB like getAllIllustration
        for(be.webdeb.infra.persistence.model.ArgumentJustification e : arg.getArgumentJustificationsFrom()){
          EArgumentLinkShade shade = e.getShade().getELinkShade();
          for(ArgumentHasExcerpt e2 : e.getArgumentTo().getArgumentIllustrations()){
            if(!illustrationMap.containsKey(e2.getExcerpt().getIdContribution())) {
              illustrationMap.put(e2.getExcerpt().getIdContribution(), e2);
              if (shade.isOpposition()) {
                illustrationMap.get(e2.getExcerpt().getIdContribution()).setShade(e2.getShade().getELinkShade().getOpposite());
              }
            }
          }
        }

        result.addAll(buildIllustrationLinkList(new ArrayList<>(illustrationMap.values()), a));
      } catch (FormatException e) {
        logger.error("unable to cast contextualized argument ", e);
      }
    }
    return result;
  }

  @Override
  public synchronized ArgumentContext getOppositeArgumentContext(Long id) {
    be.webdeb.infra.persistence.model.ArgumentContext arg =
            be.webdeb.infra.persistence.model.ArgumentContext.findById(id);
    if (arg != null) {
      be.webdeb.infra.persistence.model.ArgumentContext opposite = arg.getOppositeArgument();
      if(opposite == null){
        return null;
      }
      try {
        return mapper.toArgumentContext(opposite);
      } catch (FormatException e) {
        logger.error("unable to cast contextualized argument " + opposite.getIdContribution(), e);
      }
    }
    return null;
  }

  @Override
  public synchronized List<ArgumentIllustration> getAllIllustrationLinks(Long id, boolean positive) {
    be.webdeb.infra.persistence.model.ArgumentContext arg =
            be.webdeb.infra.persistence.model.ArgumentContext.findById(id);
    List<ArgumentIllustration> result = new ArrayList<>();
    if (arg != null) {
      try {
        ArgumentContext a = mapper.toArgumentContext(arg);

        Map<Long, be.webdeb.infra.persistence.model.ArgumentHasExcerpt> illustrationMap = new HashMap<>();
        EArgumentShade shade = positive ? arg.getArgument().getShade().getEArgumentShade().getPositive()
                : arg.getArgument().getShade().getEArgumentShade();

        arg.getSimilarArguments().forEach((k, v) ->
          be.webdeb.infra.persistence.model.ArgumentHasExcerpt.getIllustrations(k).forEach(e -> {
          if(!illustrationMap.containsKey(e.getExcerpt().getIdContribution())){
            if((shade.isApprobation() && !v.isApprobation()) || (shade.isOpposition() && !v.isOpposition())){
              e.setShade(e.getShade().getELinkShade().getOpposite());
            }
            illustrationMap.put(e.getExcerpt().getIdContribution(), e);
          }
        }));

        result.addAll(buildIllustrationLinkList(new ArrayList<>(illustrationMap.values()), a));
      } catch (FormatException e) {
        logger.error("unable to cast contextualized argument ", e);
      }
    }
    return result;
  }

  @Override
  public synchronized List<ArgumentDictionary> getArgumentTranslations(Long id) {
    be.webdeb.infra.persistence.model.ArgumentDictionary arg = be.webdeb.infra.persistence.model.ArgumentDictionary.findById(id);
    List<ArgumentDictionary> result = new ArrayList<>();
    if (arg != null) {
      result.addAll(buildArgDictionaryList(arg.getTranslations()));
    }
    return result;
  }

  @Override
  public be.webdeb.core.api.contribution.ContextContribution retrieveDebateFirstArgument(Long id) {
    Debate debate = Debate.findDebateByFirstArgumentId(id);
    if(debate != null){
      try {
        return mapper.toDebate(debate);
      } catch (FormatException e) {
        logger.error("unable to cast debate " + debate.getIdContribution(), e);
      }
    }
    return null;
  }

  /*
   * GETTERS FOR PREDEFINED VALUES
   */

  @Override
  public synchronized List<ArgumentType> getArgumentTypes() {
    if (argumentTypes == null) {
      argumentTypes = new ArrayList<>();

      // build list of types only (for names)
      Map<Integer, Map<String, String>> types = new HashMap<>();
      TArgumentType.find.all().forEach(t ->
        types.put(t.getIdArgtype(), new LinkedHashMap<>(t.getTechnicalNames()))
      );

      // now build all concrete types (shades)
      TArgShadeType.find.all().forEach(t -> {
        int type = t.getArgumentType().getIdArgtype();
        argumentTypes.add(factory.createArgumentType(
            type, types.get(type), t.getIdShade(), new LinkedHashMap<>(t.getTechnicalNames())));
      });
    }
    return argumentTypes;
  }

  @Override
  public synchronized List<ArgumentLinkType> getArgumentLinktypes() {
    if (argumentLinkTypes == null) {
      argumentLinkTypes = new ArrayList<>();
      Map<Integer, Map<String, String>> linktypes = new HashMap<>();
      TArgumentLinktype.find.all().forEach(t ->
        linktypes.put(t.getIdLinktype(), new LinkedHashMap<>(t.getTechnicalNames()))
      );

      TLinkShadeType.find.all().forEach(s -> {
        int linktype = s.getArgumentLinktype().getIdLinktype();
        argumentLinkTypes.add(factory.createArgumentLinkType(
            linktype, linktypes.get(linktype), s.getIdLinkShade(), new LinkedHashMap<>(s.getTechnicalNames())));
      });
    }
    return argumentLinkTypes;
  }

  @Override
  public Map<EArgumentLinkShade, Integer> getArgumentShadesAmount(Long contribution, Long contributor, List<EArgumentLinkShade> shades) {
    Map<EArgumentLinkShade, Integer> amounts = new HashMap<>();
    Contributor contributor1 = Contributor.findById(contributor);
    be.webdeb.infra.persistence.model.Contribution c = be.webdeb.infra.persistence.model.Contribution.findById(contribution);
    if(c != null && contributor1 != null && shades != null){
      shades.forEach(e -> amounts.put(e, be.webdeb.infra.persistence.model.Argument.getAmountOfArgumentlinksByShade(c, contributor, e)));
    }
    return amounts;
  }

  @Override
  public boolean isSimilarWith(Long argToCompare, Long argument) {
    be.webdeb.infra.persistence.model.Argument toCompare = be.webdeb.infra.persistence.model.Argument.findById(argToCompare);
    be.webdeb.infra.persistence.model.Argument compared = be.webdeb.infra.persistence.model.Argument.findById(argument);
    return (toCompare != null && compared != null && be.webdeb.infra.persistence.model.Argument.similarArguments(toCompare, compared));
  }

  @Override
  public boolean justificationAlreadyExists(Long origin, Long destination) {
    be.webdeb.infra.persistence.model.ArgumentJustification justification =
            be.webdeb.infra.persistence.model.ArgumentJustification.findByOriginAndDestination(origin, destination);
    if(justification != null){
      return true;
    }

    be.webdeb.infra.persistence.model.ArgumentContext argOrigin = be.webdeb.infra.persistence.model.ArgumentContext.findById(origin);
    be.webdeb.infra.persistence.model.ArgumentContext argDestination = be.webdeb.infra.persistence.model.ArgumentContext.findById(destination);

    if(argOrigin != null && argDestination != null){
      return argOrigin.getArgumentJustificationsFrom().stream().anyMatch(e ->
              e.getArgumentTo().getArgument().getIdContribution().equals(argDestination.getArgument().getIdContribution()));
    }

    return false;
  }

  @Override
  public boolean similarityAlreadyExists(Long origin, Long destination) {
    return be.webdeb.infra.persistence.model.ArgumentSimilarity.findByOriginAndDestination(origin, destination) != null;
  }

  @Override
  public boolean illustrationAlreadyExists(Long argument, Long excerpt) {
    return ArgumentHasExcerpt.findByArgumentAndExcerpt(argument, excerpt) != null;
  }

  @Override
  public boolean linkAlreadyExists(Long origin, Long destination) {
    be.webdeb.core.api.contribution.Contribution c_origin = retrieveContribution(origin);
    be.webdeb.core.api.contribution.Contribution c_destination = retrieveContribution(destination);
    if(c_origin != null && c_destination != null){
      if(c_origin.getType() == EContributionType.TEXT && c_destination.getType() == EContributionType.ARGUMENT_CONTEXTUALIZED){
        be.webdeb.infra.persistence.model.ArgumentContext arg = be.webdeb.infra.persistence.model.ArgumentContext.findById(destination);
        return origin.equals(arg.getContext().getIdContribution());
      }else {
        switch (c_destination.getType()) {
          case ARGUMENT:
            return similarityAlreadyExists(origin, destination);
          case ARGUMENT_CONTEXTUALIZED:
            return justificationAlreadyExists(origin, destination);
          case EXCERPT:
            return illustrationAlreadyExists(origin, destination);
          default:
            logger.debug("unexpected contribution type.");
        }
      }
    }
    return false;
  }

  @Override
  public Argument findUniqueByTitle(String title) {
    Argument argument = null;
    be.webdeb.infra.persistence.model.Argument a = be.webdeb.infra.persistence.model.Argument.findUniqueByTitle(title);
    if(a != null){
    try {
        argument = mapper.toArgument(a);
      } catch(FormatException e){
        logger.error("unable to cast argument " + argument, e);
      }
    }
    return argument;
  }

  @Override
  public List<ArgumentDictionary> findByTitle(String title, String lang, int fromIndex, int toIndex) {
    return buildArgDictionaryList(be.webdeb.infra.persistence.model.ArgumentDictionary.findByTitleAndLang(title, lang, fromIndex, toIndex));
  }

  @Override
  public synchronized List<Text> getAllTexts(Long id) {
    be.webdeb.infra.persistence.model.ArgumentContext arg = be.webdeb.infra.persistence.model.ArgumentContext.findById(id);
    if (arg != null) {
      return buildTextList(arg.getAllTexts());
    }
    return new ArrayList<>();
  }

  @Override
  public synchronized List<be.webdeb.core.api.debate.Debate> getAllDebates(Long id) {
    be.webdeb.infra.persistence.model.ArgumentContext arg = be.webdeb.infra.persistence.model.ArgumentContext.findById(id);
    if (arg != null) {
      return buildDebateList(arg.getAllDebates());
    }
    return new ArrayList<>();
  }

  @Override
  public synchronized List<Folder> getAllFolders(Long id) {
    be.webdeb.infra.persistence.model.ArgumentContext arg = be.webdeb.infra.persistence.model.ArgumentContext.findById(id);
    if (arg != null) {
      return new ArrayList<>(buildFolderSet(arg.getAllFolders()));
    }
    return new ArrayList<>();
  }

  @Override
  public synchronized List<ArgumentContext> getPotentialJustificationLinks(Long argumentId, Long contextId) {
    be.webdeb.infra.persistence.model.ArgumentContext argument = be.webdeb.infra.persistence.model.ArgumentContext.findById(argumentId);
    be.webdeb.infra.persistence.model.Contribution context = be.webdeb.infra.persistence.model.Contribution.findById(contextId);

    if(argument != null && context != null){
      List<be.webdeb.infra.persistence.model.ArgumentContext> candidates = new ArrayList<>();
      candidates.addAll(be.webdeb.infra.persistence.model.ArgumentContext.getAllNotLinkedArguments(contextId, argumentId));
      candidates.addAll(be.webdeb.infra.persistence.model.ArgumentContext.getPotentialJustifications(contextId, argumentId, argument.getArgument().getTitleDictionary().getIdDictionary()));

      return buildContextualizedArgList(candidates);
    }

    return new ArrayList<>();
  }

  @Override
  public synchronized List<be.webdeb.core.api.excerpt.Excerpt> getPotentialIllustrationLinks(Long argumentId, Long contextId) {

    return null;
  }

  @Override
  public Map<Integer, Integer> getSimpleExcerptMetrics(Long argumentId, Long contributorId, int groupId) {
    be.webdeb.infra.persistence.model.ArgumentContext argument = be.webdeb.infra.persistence.model.ArgumentContext.findById(argumentId);

    if(argument != null){
      return argument.getSimpleExcerptMetrics(contributorId, groupId);
    }

    return new LinkedHashMap<>();
  }

  /*
   * PRIVATE HELPERS
   */

  /**
   * Update given db argument with API argument's shade and title
   *
   * @return the updated Argument
   */
  private be.webdeb.infra.persistence.model.Argument updateArgument(Argument apiArgument) {

    be.webdeb.infra.persistence.model.ArgumentDictionary dictionary = null;

    // Check if given dictionary exists
    be.webdeb.infra.persistence.model.Argument argument = be.webdeb.infra.persistence.model.Argument.findById(apiArgument.getId());
    if(argument != null) {
      dictionary = argument.getTitleDictionary();
    }else{
      argument = new be.webdeb.infra.persistence.model.Argument();
    }

    ArgumentDictionary apiDictionary = apiArgument.getDictionary();
    // If given dictionary doesn't exists or it doesn't match title or language, search if that dictionary exists.
    // If it not exists, create a new one.
    String argumentTitle = convertToUTF8(apiDictionary.getTitle().replaceAll("’", "'"));
    if(dictionary == null || !dictionary.getTitle().equals(argumentTitle) || !dictionary.getLanguage().getCode().equals(apiDictionary.getLanguage().getCode())){

      be.webdeb.infra.persistence.model.ArgumentDictionary otherDictionary =
              be.webdeb.infra.persistence.model.ArgumentDictionary.findUniqueByTitle(argumentTitle, apiDictionary.getLanguage().getCode());

      if(otherDictionary == null || !argumentTitle.equals(otherDictionary.getTitle())) {

        be.webdeb.infra.persistence.model.ArgumentDictionary newDictionary =
                new be.webdeb.infra.persistence.model.ArgumentDictionary(argumentTitle, apiDictionary.getLanguage().getCode());
        newDictionary.save();
        if(dictionary != null && dictionary.getArguments().size() <= 1){
          argument.setTitleDictionary(newDictionary);
          argument.update();
          dictionary.delete();
        }
        dictionary = newDictionary;
      }else{
        dictionary = otherDictionary;
      }
    }

    be.webdeb.infra.persistence.model.Argument arg2 = be.webdeb.infra.persistence.model.Argument
            .findUniqueByShadeAndDictionary(apiArgument.getArgumentType().getArgumentShade(), dictionary.getIdDictionary());
    if(arg2 == null) {
      argument.setShade(TArgShadeType.find.byId(apiArgument.getArgumentType().getArgumentShade()));
      argument.setTitleDictionary(dictionary);
    }else{
      argument = arg2;
    }
    return argument;
  }

  /**
   * Update given db justification link with api link, setting from and to contextualized arguments according
   * to shade duality of not
   *
   * @param apiLink an API justification link
   * @param link a DB justification link to update
   * @param from the "from" DB contextualized argument (corresponding to apiLink.getArgumentFrom)
   * @param to the "to" DB contextualized argument (corresponding to apiLink.getArgumentTo)
   * @param  context the "to" DB context contribution (corresponding to apiLink.getContext)
   * @return the updated DB argument justification link
   */
  private be.webdeb.infra.persistence.model.ArgumentJustification updateJustificationLink(ArgumentJustification apiLink,
    be.webdeb.infra.persistence.model.ArgumentJustification link,
    be.webdeb.infra.persistence.model.ArgumentContext from,
    be.webdeb.infra.persistence.model.ArgumentContext to,
    be.webdeb.infra.persistence.model.Contribution context) {

    // If we try to link an argument that is another context, we need to duplicate it in the given context.
    if(!to.getContext().getIdContribution().equals(context.getIdContribution())){
      to = new be.webdeb.infra.persistence.model.ArgumentContext(to, initContribution(to.getContribution()), context);
    }

    link.setArgumentFrom(from);
    link.setArgumentTo(to);
    link.setContext(context);
    TValidationState state = apiLink.getValidationState() != null ?
            TValidationState.find.byId(apiLink.getValidationState().getState()) : null;
    if(state == null) state = TValidationState.find.byId(EValidationState.VALIDATED.id());
    link.setValidationState(state);
    link.setShade(TLinkShadeType.find.byId(apiLink.getArgumentLinkType().getLinkShade()));

    return link;
  }

  /**
   * Update given db similarity link with api link, setting from and to arguments according to shade
   *
   * @param apiLink an API similarity link
   * @param link a DB similarity link to update
   * @param from the "from" DB argument (corresponding to apiLink.getArgumentFrom)
   * @param to the "to" DB argument (corresponding to apiLink.getArgumentTo)
   * @return the updated DB argument similarity  link
   */
  private be.webdeb.infra.persistence.model.ArgumentSimilarity updateSimilarityLink(ArgumentSimilarity apiLink,
                                                                             be.webdeb.infra.persistence.model.ArgumentSimilarity link,
                                                                             be.webdeb.infra.persistence.model.Argument from,
                                                                             be.webdeb.infra.persistence.model.Argument to) {
    link.setArgumentFrom(from);
    link.setArgumentTo(to);
    link.setShade(TLinkShadeType.find.byId(apiLink.getArgumentLinkType().getLinkShade()));
    return link;
  }

  /**
   * Update given db illustration link with api link, setting between an contextualized argument and an excerpt
   * according to shade
   *
   * @param apiLink an API illustration link
   * @param link a DB illustration link to update
   * @param argument the DB contextualized argument
   * @param excerpt the "to" DB excerpt
   * @return the updated DB argument justification link
   */
  private be.webdeb.infra.persistence.model.ArgumentHasExcerpt updateIllustrationLink(ArgumentIllustration apiLink,
                                                                                          be.webdeb.infra.persistence.model.ArgumentHasExcerpt link,
                                                                                          be.webdeb.infra.persistence.model.ArgumentContext argument,
                                                                                          be.webdeb.infra.persistence.model.Excerpt excerpt) {
    link.setArgument(argument);
    link.setExcerpt(excerpt);
    TValidationState state = apiLink.getValidationState() != null ?
            TValidationState.find.byId(apiLink.getValidationState().getState()) : null;
    if(state == null) state = TValidationState.find.byId(EValidationState.VALIDATED.id());
    link.setValidationState(state);
    link.setShade(TLinkShadeType.find.byId(apiLink.getArgumentLinkType().getLinkShade()));
    return link;
  }
}
