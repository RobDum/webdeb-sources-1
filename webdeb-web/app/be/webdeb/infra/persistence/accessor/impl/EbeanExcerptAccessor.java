/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.accessor.impl;

import be.webdeb.core.api.actor.ActorRole;
import be.webdeb.core.api.argument.ArgumentIllustration;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.argument.EArgumentShade;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contribution.EModificationStatus;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.excerpt.ExcerptFactory;
import be.webdeb.core.api.excerpt.ExternalExcerpt;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.ObjectNotFoundException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.persistence.accessor.api.ExcerptAccessor;
import be.webdeb.infra.persistence.model.*;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Transaction;
import com.avaje.ebean.TxScope;

import java.util.*;

public class EbeanExcerptAccessor extends AbstractContributionAccessor<ExcerptFactory> implements ExcerptAccessor {


    @Override
    public Excerpt retrieve(Long id, boolean hit) {
        be.webdeb.infra.persistence.model.Excerpt excerpt = be.webdeb.infra.persistence.model.Excerpt.findById(id);
        if (excerpt != null) {
            try {
                return mapper.toExcerpt(excerpt);
            }catch(FormatException e){
                logger.warn("unable to cast retrieved excerpt " + id);
            }
        } else {
            logger.warn("no excerpt found for id " + id);
        }
        return null;
    }

    @Override
    public ExternalExcerpt retrieveExternal(Long id) {
        be.webdeb.infra.persistence.model.ExternalExcerpt excerpt = be.webdeb.infra.persistence.model.ExternalExcerpt.findById(id);
        if (excerpt != null) {
            try {
                return mapper.toExternalExcerpt(excerpt);
            }catch(FormatException e){
                logger.warn("unable to cast retrieved external excerpt " + id);
            }
        } else {
            logger.warn("no external excerpt found for id " + id);
        }
        return null;
    }

    @Override
    public ExternalExcerpt retrieveExternal(Long id, int externalSource) {
        be.webdeb.infra.persistence.model.ExternalExcerpt excerpt = be.webdeb.infra.persistence.model.ExternalExcerpt.findByIdAndSource(id, externalSource);
        if (excerpt != null) {
            try {
                return mapper.toExternalExcerpt(excerpt);
            }catch(FormatException e){
                logger.warn("unable to cast retrieved external excerpt " + id);
            }
        } else {
            logger.warn("no external excerpt found for id " + id + " and source " + externalSource);
        }
        return null;
    }

    @Override
    public be.webdeb.core.api.excerpt.ContextElement retrieveContextElement(Long id) {
        be.webdeb.infra.persistence.model.ContextElement contextElement= be.webdeb.infra.persistence.model.ContextElement.findById(id);
        if (contextElement != null) {
            try {
                return mapper.toContextElement(contextElement);
            }catch(FormatException e){
                logger.warn("unable to cast retrieved context element " + id);
            }
        } else {
            logger.warn("no context element found for id " + id);
        }
        return null;
    }

    @Override
    public ArgumentIllustration retrieveIllustrationLink(Long argid, Long excid) {
        be.webdeb.infra.persistence.model.ArgumentHasExcerpt link = ArgumentHasExcerpt.findByArgumentAndExcerpt(argid, excid);
        if (link != null) {
            try {
                return mapper.toArgumentIllustration(link);
            }catch(FormatException e){
                logger.warn("unable to cast retrieved illustration link ");
            }
        }
        return null;
    }

    @Override
    public Map<Integer, List<Contribution>> save(Excerpt contribution, int currentGroup, Long contributor) throws PermissionException, PersistenceException {
        logger.debug("try to save excerpt " + contribution.toString() + " in group " + contribution.getInGroups());
        Map<Integer, List<be.webdeb.core.api.contribution.Contribution>> contributions = new HashMap<>();

        be.webdeb.infra.persistence.model.Contribution c = checkContribution(contribution, contributor, currentGroup);
        Contributor dbContributor = checkContributor(contributor, currentGroup);

        // check if textid exists, otherwise -> ObjectNotFound
        be.webdeb.infra.persistence.model.Text text = Text.findById(contribution.getTextId());
        if (text == null) {
            logger.error("unable to retrieve text " + contribution.getTextId());
            throw new ObjectNotFoundException(Text.class, contribution.getTextId());
        }

        // open transaction and handle the whole save chain
        Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());

        try {

            EModificationStatus status;
            be.webdeb.infra.persistence.model.Excerpt excerpt;

            if (c == null) {
                // new argument
                logger.debug("start creation of new excerpt " + contribution.getOriginalExcerpt());
                status = EModificationStatus.CREATE;

                // create contribution super type
                c = initContribution(EContributionType.EXCERPT.id(), convertToUTF8(contribution.getOriginalExcerpt()));

                // create argument and binding
                excerpt = updateExcerpt(contribution, new be.webdeb.infra.persistence.model.Excerpt());
                c.setExcerpt(excerpt);
                excerpt.setContribution(c);

                // update groups
                updateGroups(contribution, c);
                // update groups of text also, in case excerpt has been created in another group
                // we must add this group for bound text (for visibility purpose)
                updateGroups(contribution, text.getContribution());
                // manage places
                savePlaces(contribution.getPlaces(), c);

                try {
                    c.save();
                    if(contribution.getExternalExcerptId() != null){
                        be.webdeb.infra.persistence.model.ExternalExcerpt externalExcerpt =
                                be.webdeb.infra.persistence.model.ExternalExcerpt.findByIdAndContributor(contributor, contribution.getExternalExcerptId());
                        if(externalExcerpt != null){
                            externalExcerpt.getExternalContribution().setInternalContribution(c);
                            externalExcerpt.getExternalContribution().update();
                        }
                    }
                    // manage bindings to folders
                    contributions.put(EContributionType.FOLDER.id(),
                            bindFolders(c.getIdContribution(), contribution.getFoldersAsList(), contributor));
                } catch (Exception e) {
                    logger.error("error while saving contribution " + contribution.getOriginalExcerpt());
                    throw new PersistenceException(PersistenceException.Key.SAVE_EXCERPT, e);
                }

                // set id of excerpt and bind to text
                excerpt.setIdContribution(c.getIdContribution());
                excerpt.setText(text);

                // manage bindings to actors
                List<be.webdeb.core.api.contribution.Contribution> wrappers = new ArrayList<>();
                for (ActorRole a : contribution.getActors()) {
                    logger.debug("will bind actor " + a.toString() + " to " + c.getIdContribution());
                    wrappers.addAll(bindActor(c.getIdContribution(), a, currentGroup, contributor));
                }
                contributions.put(EContributionType.ACTOR.id(), wrappers);

                try {
                    excerpt.save();
                } catch (Exception e) {
                    logger.error("error while saving excerpt " + excerpt.getOriginalExcerpt(), e);
                    throw new PersistenceException(PersistenceException.Key.SAVE_EXCERPT, e);
                }

                // set new id for given contribution
                contribution.setId(c.getIdContribution());

            } else {
                logger.debug("start update of existing excerpt " + contribution.getId());
                excerpt = updateExcerpt(contribution, c.getExcerpt());
                status = EModificationStatus.UPDATE;

                // manage bindings to actors, retrieve them, save unexisting ones and remove others
                contributions.put(EContributionType.ACTOR.id(),
                        updateActorBindings(contribution, excerpt.getContribution(), currentGroup, contributor));
                // manage folders
                contributions.put(EContributionType.FOLDER.id(),
                        bindFolders(c.getIdContribution(), contribution.getFoldersAsList(), contributor));
                // manage places
                savePlaces(contribution.getPlaces(), c);

                // manage groups for this excerpt and originating text
                updateGroups(contribution, excerpt.getContribution());
                updateGroups(contribution, excerpt.getText().getContribution());
                excerpt.getContribution().setSortkey(contribution.getOriginalExcerpt());

                try {
                    // force update if only classes or bindings have been changed
                    excerpt.update();
                } catch (Exception e) {
                    logger.error("error while updating excerpt " + excerpt.getIdContribution());
                    throw new PersistenceException(PersistenceException.Key.SAVE_EXCERPT, e);
                }
            }
            // bind contributor to this excerpt
            bindContributor(excerpt.getContribution(), dbContributor, status);

            transaction.commit();
            logger.info("saved " + excerpt.toString());

        } finally {
            transaction.end();
        }

        return contributions;
    }

    @Override
    public Map<Integer, List<Contribution>> save(ExternalExcerpt contribution, int currentGroup, Long contributor) throws PermissionException, PersistenceException {
        logger.debug("try to save externalexcerpt " + contribution.getOriginalExcerpt() + " and id " + contribution.getId());
        Contributor dbContributor = Contributor.findById(contributor);

        // check if text exists, otherwise check for external text
        be.webdeb.infra.persistence.model.ExternalText externalText;
        be.webdeb.infra.persistence.model.Text text = Text.findById(contribution.getTextId());
        if (text == null) {
            externalText = ExternalText.findById(contribution.getTextId());
        }else{
            // check to an external text linked with a webdeb text id and contributor id
            externalText = be.webdeb.infra.persistence.model.ExternalText.findByTextAndContributor(text.getIdContribution(), contributor);
        }
        if (text == null && externalText == null) {
            logger.error("unable to retrieve internal or external text " + contribution.getTextId());
            throw new ObjectNotFoundException(Text.class, contribution.getTextId());
        }

        TExternalSourceName externalSource = TExternalSourceName.findById(contribution.getSourceId());
        if (externalSource == null) {
            logger.error("unable to retrieve external source " + contribution.getSourceName());
            throw new ObjectNotFoundException(TExternalSourceName.class, -1L);
        }

        if (dbContributor != null) {
            Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
            try {
                be.webdeb.infra.persistence.model.Contribution c;
                be.webdeb.infra.persistence.model.ExternalContribution externalContribution;
                be.webdeb.infra.persistence.model.ExternalExcerpt argument;

                // create contribution super type
                c = initContribution(EContributionType.EXTERNAL_EXCERPT.id(), contribution.getOriginalExcerpt(), true);
                updateGroups(contribution, c);
                c.save();

                // create external contribution object and binding
                externalContribution = initExternalContribution(contribution, c, externalSource);

                // create argument object and binding
                argument = new be.webdeb.infra.persistence.model.ExternalExcerpt();
                argument.setExternalContribution(externalContribution);

                // set argument data
                argument.setIdContribution(c.getIdContribution());
                argument.setOriginalExcerpt(contribution.getOriginalExcerpt().replaceAll("’", "'"));
                argument.setWorkingExcerpt(contribution.getWorkingExcerpt().replaceAll("’", "'"));
                argument.setText(text);
                argument.setExternalText(externalText);
                argument.save();

                contribution.setId(argument.getIdContribution());

                // bind contributor to this external argument
                bindContributor(c, dbContributor, EModificationStatus.CREATE);
                transaction.commit();
                logger.info("saved " + argument.toString());

            } catch (Exception e) {
                logger.error("unable to save external excerpt " + contribution.getOriginalExcerpt(), e);
                throw new PersistenceException(PersistenceException.Key.SAVE_EXTERNAL_EXCERPT, e);

            } finally {
                transaction.end();
            }
        }
        return null;
    }

    @Override
    public void save(be.webdeb.core.api.excerpt.ContextElement element) throws PersistenceException {
        logger.debug("try to save context element with id " + element.getId());

        ContextElement existing = ContextElement.findById(element.getId());

        // open transaction and handle the whole save chain
        Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());

        try {

            logger.debug("start saving of new context element");

            if (existing == null) {
                existing = new ContextElement();
                existing.save();
            }

            if(element.getContribution() != null) {
                existing.setContribution(be.webdeb.infra.persistence.model.Contribution.findById(element.getContribution().getId()));
            }

            existing.setSpellings(element.getSpellings());

            existing.update();

            transaction.commit();
            logger.info("saved context element");

        } finally {
            transaction.end();
        }
    }

    @Override
    public ExternalExcerpt findExistingExternalExcerpt(Long contributor, String originalExcerpt, Long textId){
        be.webdeb.infra.persistence.model.ExternalExcerpt excerpt =
                be.webdeb.infra.persistence.model.ExternalExcerpt.findExisting(contributor, originalExcerpt, textId);
        if(excerpt != null){
            try{
                return mapper.toExternalExcerpt(excerpt);
            }catch(FormatException e){
                logger.warn("unable to cast retrieved external excerpt " + excerpt);
            }
        } else {
            logger.warn("no external excerpt found for contributor " + contributor + ", original excerpt " + originalExcerpt + " textId " + textId);
        }
        return null;
    }

    @Override
    public List<ExternalExcerpt> getExternalExcerptsByExternalSource(int externalSource, int maxResults) {
        TExternalSourceName source = TExternalSourceName.findById(externalSource);
        if(source != null){
            return buildExternalExcerptList(be.webdeb.infra.persistence.model.ExternalExcerpt.findAll(source.getIdSource(), maxResults));
        }
        return new ArrayList<>();
    }

    @Override
    public List<ArgumentIllustration> getArguments(Long id) {
        be.webdeb.infra.persistence.model.Excerpt excerpt = be.webdeb.infra.persistence.model.Excerpt.findById(id);

        if(excerpt != null) {
            return buildIllustrationLinkList(new ArrayList<>(excerpt.getArguments()), excerpt);
        }

        return new ArrayList<>();
    }

    @Override
    public List<ArgumentIllustration> similars(Long id) {
        be.webdeb.infra.persistence.model.Excerpt excerpt = be.webdeb.infra.persistence.model.Excerpt.findById(id);
        if(excerpt != null) {

            Map<Long, EArgumentLinkShade> dominantLinkShade = new HashMap<>();
            Map<Long, EArgumentShade> dominantShade = new HashMap<>();
            List<be.webdeb.infra.persistence.model.ArgumentHasExcerpt> illustrations = excerpt.getAllArguments();

            illustrations.forEach(e -> {
                if(e.getExcerpt().getIdContribution().equals(id)) {
                    EArgumentLinkShade linkShade = e.getShade().getELinkShade();
                    if (!dominantLinkShade.containsKey(e.getArgument().getIdContribution())) {
                        dominantLinkShade.put(e.getArgument().getIdContribution(), linkShade);
                    } else if (dominantLinkShade.get(e.getArgument().getIdContribution()) != linkShade) {
                        dominantLinkShade.put(e.getIdContribution(), linkShade.getNeutral());
                    }

                    EArgumentShade shade = e.getArgument().getArgument().getShade().getEArgumentShade();
                    if (!dominantShade.containsKey(e.getArgument().getArgument().getTitleDictionary().getIdDictionary())) {
                        dominantShade.put(e.getArgument().getArgument().getTitleDictionary().getIdDictionary(), shade);
                    }
                }
            });

            illustrations.forEach(illustration -> {
                if(!illustration.getExcerpt().getIdContribution().equals(id)) {
                    if (dominantLinkShade.containsKey(illustration.getArgument().getIdContribution())) {
                        if (dominantLinkShade.get(illustration.getArgument().getIdContribution()) != illustration.getShade().getELinkShade()) {
                            illustration.setShade(illustration.getShade().getELinkShade().getNeutralOrOpposite());
                        }
                    }
                    if (dominantShade.containsKey(illustration.getArgument().getArgument().getTitleDictionary().getIdDictionary())) {
                        EArgumentShade currentShade = dominantShade.get(illustration.getArgument().getArgument().getTitleDictionary().getIdDictionary());
                        if (currentShade != illustration.getArgument().getArgument().getShade().getEArgumentShade()) {
                            illustration.setShade(illustration.getShade().getELinkShade().getNeutralOrOpposite());
                        }
                    }
                }else{
                    illustration.setShade(illustration.getShade().getELinkShade().getPositive());
                }
            });

            return buildIllustrationLinkList(illustrations);
        }
        return new ArrayList<>();
    }

    /*
     * PRIVATE HELPERS
     */

    /**
     * Update given db excerpt with API excerpt's shade and title
     *
     * @return the updated Excerpt
     */
    private be.webdeb.infra.persistence.model.Excerpt updateExcerpt(
            Excerpt apiExcerpt, be.webdeb.infra.persistence.model.Excerpt excerpt) {

        excerpt.setOriginalExcerpt(convertToUTF8(apiExcerpt.getOriginalExcerpt().replaceAll("’", "'")));
        excerpt.setWorkingExcerpt(convertToUTF8(apiExcerpt.getTechWorkingExcerpt().replaceAll("’", "'")));
        return excerpt;
    }

}
