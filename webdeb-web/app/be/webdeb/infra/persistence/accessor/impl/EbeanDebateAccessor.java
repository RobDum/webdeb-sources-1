/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.accessor.impl;

import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contribution.EModificationStatus;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.debate.DebateFactory;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.ObjectNotFoundException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.persistence.accessor.api.DebateAccessor;
import be.webdeb.infra.persistence.model.*;

import java.util.List;
import java.util.Map;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Transaction;
import com.avaje.ebean.TxScope;

public class EbeanDebateAccessor extends AbstractContributionAccessor<DebateFactory> implements DebateAccessor {

    @Override
    public Debate retrieve(Long id, boolean hit) {
        be.webdeb.infra.persistence.model.Debate debate = be.webdeb.infra.persistence.model.Debate.findById(id);
        if (debate != null) {
            try {
                Debate api =  mapper.toDebate(debate);
                if (hit) {
                    debate.getContribution().addHit();
                    debate.getContribution().update();
                }
                return api;
            } catch (FormatException e) {
                logger.error("unable to cast retrieved debate " + id, e);
            }
        } else {
            logger.warn("no debate found for id " + id);
        }
        return null;
    }

    @Override
    public Map<Integer, List<be.webdeb.core.api.contribution.Contribution>> save(Debate contribution, int currentGroup, Long contributor) throws PermissionException, PersistenceException {
        logger.debug("try to save debate " + contribution.getTitle() + " with id " + contribution.getId() + " in group " + contribution.getInGroups());
        Contribution c = checkContribution(contribution, contributor, currentGroup);
        Contributor dbContributor = checkContributor(contributor, currentGroup);

        be.webdeb.infra.persistence.model.ArgumentContext firstArgument = ArgumentContext.findById(contribution.getFirstArgumentId());
        // check if first argument is not already a first argument for another debate, otherwise -> ObjectNotFound. Should not append
        be.webdeb.infra.persistence.model.Debate d = be.webdeb.infra.persistence.model.Debate.findDebateByFirstArgumentId(contribution.getFirstArgumentId());
        if (c == null && d != null) {
            logger.error("The given first argument if already the first argument of another debate " + d.getIdContribution());
            throw new ObjectNotFoundException(Text.class, d.getIdContribution());
        }


        Transaction transaction = Ebean.getDefaultServer().beginTransaction(TxScope.required());
        try {

            EModificationStatus status;
            be.webdeb.infra.persistence.model.Debate debate;

            if (c == null) {
                // new text
                logger.debug("start creation of debate " + contribution.getTitle());
                status = EModificationStatus.CREATE;

                // create contribution super type
                c = initContribution(EContributionType.DEBATE.id(), contribution.getTitle());

                // create text object and binding
                debate = new be.webdeb.infra.persistence.model.Debate();
                c.setDebate(debate);
                debate.setContribution(c);
                debate.setShade(TArgShadeType.find.byId(contribution.getFirstArgumentType().getArgumentShade()));

                // update groups
                updateGroups(contribution, c);
                c.save();

                // set id of debate
                debate.setIdContribution(c.getIdContribution());
                debate.save();

                // set new id for given contribution and titles
                contribution.setId(c.getIdContribution());

                debate.setFirstContextArgument(firstArgument);
                debate.update();
            } else {
                // update existing
                logger.debug("update debate " + contribution.getTitle() + " with id " + contribution.getId());
                status = EModificationStatus.UPDATE;

                debate = c.getDebate();
                debate.setFirstContextArgument(firstArgument);
                debate.setShade(TArgShadeType.find.byId(contribution.getFirstArgumentType().getArgumentShade()));
                debate.getContribution().setSortkey(contribution.getTitle());

                for (be.webdeb.core.api.contributor.Group group : contribution.getInGroups()) {
                    debate.getContribution().addGroup(Group.findById(group.getGroupId()));
                }

                // update groups and sort key
                updateGroups(contribution, debate.getContribution());

                debate.update();
                logger.info("updated " + debate.toString());
            }

            // bind contributor to this text
            bindContributor(debate.getContribution(), dbContributor, status);

            transaction.commit();
            logger.info("saved " + debate.toString());

        } catch (Exception e) {
            logger.error("unable to save debate " + contribution.getTitle(), e);
            throw new PersistenceException(PersistenceException.Key.SAVE_DEBATE, e);

        } finally {
            transaction.end();
        }

        return null;
    }

    @Override
    public Debate random() {
        try {
            return mapper.toDebate(be.webdeb.infra.persistence.model.Debate.random());
        } catch (FormatException e) {
            logger.error("unable to cast retrieved random folder", e);
        }
        return null;
    }

    /*
     * PRIVATE HELPERS
     */
}
