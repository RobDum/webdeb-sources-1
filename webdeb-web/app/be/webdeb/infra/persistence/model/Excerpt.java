/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.core.api.contribution.EContributionType;
import com.avaje.ebean.*;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The persistent class for the excerpt database table, conceptual subtype of contribution.
 * Always linked to a text (pointing to start and end indices in that text). May also have
 * illustration links to / from other arguments (materialized by ArgumentLink objects).
 *
 * Contains full-text excerpt instead of simple offsets in text because a text content may be split into many
 * user-dependent files and crossing over all of them would be a bad idea and some excerpt may come from PDF files.
 * Also, speeds up annotation processes for text files.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "excerpt")
public class Excerpt extends WebdebModel {

    protected static final org.slf4j.Logger logger = play.Logger.underlying();
    private static final Model.Finder<Long, Excerpt> find = new Model.Finder<>(Excerpt.class);

    @Id
    @Column(name = "id_contribution", unique = true, nullable = false)
    private Long idContribution;

    // forcing updates from this object, deletions are handled at the contribution level
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "id_contribution", nullable = false, insertable = false, updatable = false)
    private Contribution contribution;

    @Column(name = "original_excerpt", nullable = false)
    private String originalExcerpt;

    @Column(name = "working_excerpt", nullable = true)
    private String workingExcerpt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_text", nullable = false)
    private Text text;

    // all arguments linked to this excerpt
    @OneToMany(mappedBy = "excerpt", cascade = CascadeType.PERSIST)
    private List<ArgumentHasExcerpt> arguments;

    /**
     * Get parent contribution object
     *
     * @return the parent contribution
     */
    public Contribution getContribution() {
        return contribution;
    }

    /**
     * Set parent contribution object
     *
     * @param contribution the parent contribution
     */
    public void setContribution(Contribution contribution) {
        this.contribution = contribution;
    }

    /**
     * Get the id of argument
     *
     * @return the argument id
     */
    public Long getIdContribution() {
        return idContribution;
    }

    /**
     * Set the id of argument
     *
     * @param idContribution the argument id
     */
    public void setIdContribution(Long idContribution) {
        this.idContribution = idContribution;
    }

    /**
     * Get the original excerpt
     *
     * @return the original excerpt
     */
    public String getOriginalExcerpt() {
        return originalExcerpt;
    }

    /**
     * Set the original excerpt
     *
     * @param  originalExcerpt the original excerpt
     */
    public void setOriginalExcerpt(String originalExcerpt) {
        this.originalExcerpt =  originalExcerpt;
    }

    /**
     * Get the working excerpt
     *
     * @return the working excerpt
     */
    public String getWorkingExcerpt() {
        return workingExcerpt;
    }

    /**
     * Set the working excerpt
     *
     * @param workingExcerpt the working excerpt
     */
    public void setWorkingExcerpt(String workingExcerpt) {
        this.workingExcerpt = workingExcerpt;
    }

    /**
     * Get the text from which this argument has bee,n extracted
     *
     * @return a text
     */
    public Text getText() {
        return text;
    }

    /**
     * Set the text from which this argument has bee,n extracted
     *
     * @param text the owning text
     */
    public void setText(Text text) {
        this.text = text;
    }

    /**
     * Get the list of arguments linked to this excerpt
     *
     * @return a list of arguments, may be empty
     */
    public List<ArgumentHasExcerpt> getArguments() {
        return arguments != null ? arguments : new ArrayList<>();
    }

    /**
     * Set the list of arguments linked to this excerpt
     *
     * @param arguments a list of arguments
     */
    public void setArguments(List<ArgumentHasExcerpt> arguments) {
        this.arguments = arguments;
    }

    /**
     * Get the list of arguments linked to this excerpt
     *
     * @return a list of arguments, may be empty
     */
    public List<ArgumentHasExcerpt> getAllArguments() {
        String select = "select ache.id_contribution from argument_context_has_excerpt ache " +
                "inner join argument_context ac on ac.id_contribution = ache.id_argument_context " +
                "inner join argument a on a.id_contribution = ac.id_argument " +
                "where a.id_argument_dictionary in ( " +
                "select a2.id_argument_dictionary from argument a2 " +
                "inner join argument_context ac2 on a2.id_contribution = ac2.id_argument " +
                "inner join argument_context_has_excerpt ache2 on ache2.id_argument_context = ac2.id_contribution " +
                "where ache2.id_excerpt = " + idContribution +" )";

        List<ArgumentHasExcerpt> result = Ebean.find(ArgumentHasExcerpt.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
        return result != null ? result : new ArrayList<>();
    }

    /**
     * Get the current version of this argument
     *
     * @return a timestamp with the latest update moment of this argument
     */
    public Timestamp getVersion() {
        return getContribution().getVersion();
    }

    @Override
    public String toString() {
        // must use getters and explicitly loop into references, otherwise ebean may send back deferred beanlist
        // (lazy load not triggered from toString methods)
        return new StringBuffer("argument [").append(getIdContribution())
                .append(", original excerpt: ").append(getOriginalExcerpt()).append(", working excerpt: ").append(getWorkingExcerpt())
                .append(", actors: {").append(getContribution().getActors().stream()
                        .map(ContributionHasActor::toString).collect(Collectors.joining(", "))).append("}")
                .append(", from text: ").append(getText().getContribution().getIdContribution())
                .append(", groups: ").append(getContribution().getGroups().stream()
                        .map(g -> String.valueOf(g.getIdGroup())).collect(Collectors.joining(",")))
                .append(" [version:").append(getContribution().getVersion()).append("]").toString();
    }

    /*
     * QUERIES
     */

    /**
     * Retrieve an excerpt by its id
     *
     * @param id an id
     * @return the excerpt corresponding to the given id, null if not found
     */
    public static Excerpt findById(Long id) {
        return id == null || id == -1L ? null : find.byId(id);
    }

    /*
     * PRIVATE HELPERS
     */

    /**
     * Get the number of arguments linked to this excerpts
     *
     * @param contributorId the id of the contributor for which we need that stats
     * @param groupId the group where see the stats
     * @return the number of arguments linked to this excerpts
     */
    public int getNbArguments(Long contributorId, int groupId){
        String select = "SELECT count(distinct a.id_contribution) as 'count' FROM argument_context_has_excerpt a" +
                getContributionStatsJoins("a.id_argument_context", contributorId) +
                " where a.id_excerpt = " + idContribution + getContributionStatsWhereClause(contributorId, groupId);
        return Ebean.createSqlQuery(select).findUnique().getInteger("count");
    }

}
