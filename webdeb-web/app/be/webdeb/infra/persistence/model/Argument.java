/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.core.api.argument.ArgumentIllustration;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contribution.EValidationState;
import com.avaje.ebean.*;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The persistent class for the argument database table, conceptual subtype of contribution.
 * May have justification or similarity links to / from other arguments (materialized by ArgumentJustification and
 * ArgumentSimilarity objects).
 *
 * An argument is a simple sentence written by a contributor and that can be added in a debate or a text justification tree.
 * The debate and the text are then the context with others data like folders, places an concerned actors.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "argument")
public class Argument extends Model {

  protected static final org.slf4j.Logger logger = play.Logger.underlying();
  private static final Model.Finder<Long, Argument> find = new Model.Finder<>(Argument.class);

  @Id
  @Column(name = "id_contribution", unique = true, nullable = false)
  private Long idContribution;

  // forcing updates from this object, deletions are handled at the contribution level
  @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
  @JoinColumn(name = "id_contribution", nullable = false, insertable = false, updatable = false)
  private Contribution contribution;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "shade", nullable = false)
  private TArgShadeType shade;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "id_argument_dictionary", nullable = false)
  private ArgumentDictionary title;

  // all arguments similar arguments
  @OneToMany(mappedBy = "argumentTo", cascade = CascadeType.PERSIST)
  private List<ArgumentSimilarity> similarArgumentsFrom;

  // all arguments similar arguments
  @OneToMany(mappedBy = "argumentFrom", cascade = CascadeType.PERSIST)
  private List<ArgumentSimilarity> similarArgumentsTo;

  // all contextualized argument with this argument
  @OneToMany(mappedBy = "argument", cascade = CascadeType.PERSIST)
  private List<ArgumentContext> contextualizedArguments;

  /**
   * Get parent contribution object
   *
   * @return the parent contribution
   */
  public Contribution getContribution() {
    return contribution;
  }

  /**
   * Set parent contribution object
   *
   * @param contribution the parent contribution
   */
  public void setContribution(Contribution contribution) {
    this.contribution = contribution;
  }

  /**
   * Get the id of argument
   *
   * @return the argument id
   */
  public Long getIdContribution() {
    return idContribution;
  }

  /**
   * Set the id of argument
   *
   * @param idContribution the argument id
   */
  public void setIdContribution(Long idContribution) {
    this.idContribution = idContribution;
  }

  /**
   * Get this argument shade
   *
   * @return the argument shade
   */
  public TArgShadeType getShade() {
    return shade;
  }

  /**
   * Set this argument shade
   *
   * @param shade a shade to set
   */
  public void setShade(TArgShadeType shade) {
    this.shade = shade;
  }

  /**
   * Get the title as ArgumentDictionary  of this argument
   *
   * @return the title of this argument
   */
  public ArgumentDictionary getTitleDictionary() {
    return title;
  }

  /**
   * Set the title as ArgumentDictionary of this argument
   *
   * @param title a title for this argument
   */
  public void setTitleDictionary(ArgumentDictionary title) {
    this.title = title;
  }

  /**
   * Get the list of similar arguments of this one
   *
   * @return a list of similar arguments
   */
  public List<ArgumentSimilarity> getSimilarArguments() {
    List<ArgumentSimilarity> similar = new ArrayList<>();
    similar.addAll(similarArgumentsFrom.stream().map(e -> {
      Argument a = e.getArgumentFrom();
      e.setArgumentFrom(e.getArgumentTo());
      e.setArgumentTo(a);
      return e;
    }).collect(Collectors.toList()));
    similar.addAll(similarArgumentsTo);
    return similar;
  }

  /**
   * Set the list of similarity where this argument is the origin
   *
   * @param similarArgumentsFrom a list of similar arguments
   */
  public void setArgumentJustificationsFrom(List<ArgumentSimilarity> similarArgumentsFrom) {
    this.similarArgumentsFrom = similarArgumentsFrom;
  }

  /**
   * Set the list of similarity where this argument is the destination
   *
   * @param similarArgumentsTo a list of similar arguments
   */
  public void setArgumentJustificationsTo(List<ArgumentSimilarity> similarArgumentsTo) {
    this.similarArgumentsTo = similarArgumentsTo;
  }

  /**
   * Get the list of contextualized arguments with this one.
   *
   * @return a list of contextualized arguments
   */
  public List<ArgumentContext> getContextualizedArguments() {
    return contextualizedArguments;
  }

  /**
   * Get the current version of this argument
   *
   * @return a timestamp with the latest update moment of this argument
   */
  public Timestamp getVersion() {
    return getContribution().getVersion();
  }

  @Override
  public String toString() {
    // must use getters and explicitly loop into references, otherwise ebean may send back deferred beanlist
    // (lazy load not triggered from toString methods)
    return new StringBuffer("argument [").append(getIdContribution())
        .append("], type: [").append(getShade().getIdShade()).append("] ").append(getShade().getEn())
        .append(", title: ").append(title.getTitle())
        .append(", actors: {").append(getContribution().getActors().stream()
            .map(ContributionHasActor::toString).collect(Collectors.joining(", "))).append("}")
        .append(", groups: ").append(getContribution().getGroups().stream()
            .map(g -> String.valueOf(g.getIdGroup())).collect(Collectors.joining(",")))
        .append(" [version:").append(getContribution().getVersion()).append("]").toString();
  }

  /*
   * QUERIES
   */

  /**
   * Retrieve an argument by its id
   *
   * @param id an id
   * @return the argument corresponding to the given id, null if not found
   */
  public static Argument findById(Long id) {
    return id == null || id == -1L ? null : find.byId(id);
  }

  /**
   * Check a list of ids and sends back ids that are actually arguments
   *
   * @param ids a list of ids
   * @return the given list of ids where the non-existing argument has been removed
   */
  public static List<Long> exists(List<Long> ids) {
    List<Argument> result = find.select("idContribution").where().in("idContribution", ids).findList();
    return result != null ?
        result.stream().map(a -> a.getContribution().getIdContribution()).collect(Collectors.toList())
        : new ArrayList<>();
  }

  /**
   * Check if an argument is similar to another one
   *
   * @param argToCompare the argument to compare with the other one
   * @param argument an argument
   * @return true if both arguments are similar
   */
  public static boolean similarArguments(Argument argToCompare, Argument argument){
    String select = "SELECT id_contribution FROM argument_similarity where " +
            "((id_argument_from = " + argToCompare.getIdContribution() + " and id_argument_to = " + argument.getIdContribution() + ") " +
            "or (id_argument_from = " + argument.getIdContribution() + " and id_argument_to = " + argToCompare.getIdContribution() + ") and shade = 0)";
    return Ebean.find(Contribution.class).setRawSql(RawSqlBuilder.parse(select).create()).findRowCount() > 0;
  }

  /**
   * Get the amount of argument links of a given contribution visible for a given contributor for a given shade
   *
   * @param contribution a contribution
   * @param contributor a contributor id
   * @param shade the argument link shade type to count
   * @return the amount of argument links for the given shade
   */
  public static Integer getAmountOfArgumentlinksByShade(Contribution contribution, Long contributor, EArgumentLinkShade shade){
    EContributionType type = EContributionType.value(contribution.getContributionType().getIdContributionType());
    String select;

    switch (type){
      case EXCERPT :
        select = "SELECT l.id_contribution FROM webdeb.argument_context_has_excerpt l " +
                "inner JOIN contribution_in_group cig ON cig.id_contribution=l.id_excerpt " +
                "inner JOIN contributor_has_group chg ON chg.id_group=cig.id_group " +
                "where l.id_excerpt = " + contribution + " and l.shade = " + shade +
                " and chg.id_contributor=" + contributor + " and chg2.id_contributor=" + contributor +
                " and validated = " + EValidationState.VALIDATED;
        return Ebean.find(ArgumentHasExcerpt.class).setRawSql(RawSqlBuilder.parse(select).create()).findRowCount();
      case ARGUMENT:
        select = "SELECT l.id_contribution FROM webdeb.argument_similarity l " +
                "inner JOIN contribution_in_group cig ON cig.id_contribution=l.id_argument_from " +
                "inner JOIN contribution_in_group cig2 ON cig2.id_contribution=l.id_argument_to " +
                "inner JOIN contributor_has_group chg ON chg.id_group=cig.id_group " +
                "inner JOIN contributor_has_group chg2 ON chg2.id_group=cig2.id_group " +
                "where (l.id_argument_to = " + contribution + " or l.id_argument_from=" + contribution + ") " +
                "and l.shade = " + shade + " and chg.id_contributor=" + contributor + " and chg2.id_contributor=" + contributor;
        return Ebean.find(ArgumentSimilarity.class).setRawSql(RawSqlBuilder.parse(select).create()).findRowCount();
      case ARGUMENT_CONTEXTUALIZED:
        if(shade.isIllustration()){
          select = "SELECT l.id_contribution FROM webdeb.argument_context_has_excerpt l " +
                  "inner JOIN contribution_in_group cig ON cig.id_contribution=l.id_argument_context " +
                  "inner JOIN contributor_has_group chg ON chg.id_group=cig.id_group " +
                  "where l.id_argument_context = " + contribution + " and l.shade = " + shade +
                  " and chg.id_contributor=" + contributor + " and chg2.id_contributor=" + contributor +
                  " and validated = " + EValidationState.VALIDATED;
          return Ebean.find(ArgumentHasExcerpt.class).setRawSql(RawSqlBuilder.parse(select).create()).findRowCount();
        }else if(shade.isJustification()){
          select = "SELECT l.id_contribution FROM webdeb.argument_justification l " +
                  "inner JOIN contribution_in_group cig ON cig.id_contribution=l.id_argument_from " +
                  "inner JOIN contribution_in_group cig2 ON cig2.id_contribution=l.id_argument_to " +
                  "inner JOIN contributor_has_group chg ON chg.id_group=cig.id_group " +
                  "inner JOIN contributor_has_group chg2 ON chg2.id_group=cig2.id_group " +
                  "where (l.id_argument_to = " + contribution + " or l.id_argument_from=" + contribution + ") " +
                  "and l.shade = " + shade + " and chg.id_contributor=" + contributor + " and chg2.id_contributor=" + contributor +
                  " and validated = " + EValidationState.VALIDATED;
          return Ebean.find(ArgumentIllustration.class).setRawSql(RawSqlBuilder.parse(select).create()).findRowCount();
        }
    }
    logger.debug("This type of contribution is not concerned by argument links.");
    return 0;
  }

  /**
   * Find an unique argument by a complete title
   *
   * @param title an argument title
   * @return a matched argument or null
   */
  public static Argument findUniqueByTitle(String title) {
    List<Argument> args = findByTitleAndLang(title, null);
    return args == null || args.isEmpty() ? null : args.get(0);
  }

  /**
   * Find a list of arguments by a complete title
   *
   * @param title an argument title
   * @return the list of Argument matching the given title
   */
  public static List<Argument> findByTitle(String title) {
    return findByTitleAndLang(title, null);
  }

  /**
   * Find a list of arguments by a complete title and lang
   *
   * @param title an argument title
   * @param lang a i18 lang
   * @return the list of Argument matching the given title and lang
   */
  public static List<Argument> findByTitleAndLang(String title, String lang) {
    List<Argument> result = null;
    if (title != null) {
      // will be automatically ordered by relevance
      String token = /* "\"" + */ title.trim()
              // protect single quotes
              .replace("'", "\\'")
              // add '%' for spaces
              .replace(" ", "%");

      String select = "select distinct a.id_contribution from argument a " +
              "inner JOIN argument_dictionary dic ON dic.id_argument_dictionary=a.id_argument_dictionary " +
              "where dic.title like '" + token + "'" + (lang == null ? "" : " and dic.lang = " + lang);

      logger.debug("search for argument : " + select);
      result = Ebean.find(Argument.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
    }
    return result != null ? result : new ArrayList<>();
  }

  /**
   * Find an unique argument by shade type and argument dictionary
   *
   * @param shade a argument shade
   * @param dictionary a dictionary id
   * @return a matched argument or null
   */
  public static Argument findUniqueByShadeAndDictionary(int shade, Long dictionary) {
    String select = "select distinct a.id_contribution from argument a " +
            "where id_argument_dictionary = " + dictionary + " and shade = " + shade;

    logger.debug("search for argument : " + select);
    return Ebean.find(Argument.class).setRawSql(RawSqlBuilder.parse(select).create()).findUnique();
  }

  /**
   * Get the default argument for first argument of text
   *
   * @return the default argument
   */
  public static Argument getDefaultArgument() {
    return findById(0L);
  }
}
