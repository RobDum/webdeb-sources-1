/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.infra.persistence.model.annotation.Unqueryable;
import com.avaje.ebean.*;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the argument_translation database table. Holds a translation link between two arguments dictionary.
 *
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "argument_translation")
@Unqueryable
public class ArgumentTranslation extends Model {

    protected static final org.slf4j.Logger logger = play.Logger.underlying();
    private static final Model.Finder<Long, ArgumentTranslation> find = new Model.Finder<>(ArgumentTranslation.class);

    @Id
    @Column(name = "id_contribution", unique = true, nullable = false)
    private Long idContribution;

    // forcing updates from this object, deletions are handled at the contribution level
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "id_contribution", nullable = false, insertable = false, updatable = false)
    private Contribution contribution;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_argument_dictionary_from", nullable = false)
    private ArgumentDictionary argumentFrom;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_argument_dictionary_to", nullable = false)
    private ArgumentDictionary argumentTo;

    /*
     * GETTERS / SETTERS
     */

    /**
     * Get the translation link id
     *
     * @return an id
     */
    public Long getIdContribution() {
        return idContribution;
    }

    /**
     * Set the translation link id
     *
     * @param idContribution an id
     */
    public void setIdContribution(Long idContribution) {
        this.idContribution = idContribution;
    }

    /**
     * Get "supertype" contribution object
     *
     * @return a contribution
     */
    public Contribution getContribution() {
        return contribution;
    }

    /**
     * Set "supertype" contribution object
     *
     * @param contribution a contribution
     */
    public void setContribution(Contribution contribution) {
        this.contribution = contribution;
    }

    /**
     * Get origin argument dictionary of this translation link
     *
     * @return an argument dictionary
     */
    public ArgumentDictionary getArgumentDictionaryFrom() {
        return argumentFrom;
    }

    /**
     * Set origin argument dictionary of this translation link
     *
     * @param argumentFrom an argument dictionary
     */
    public void setArgumentDictionaryFrom(ArgumentDictionary argumentFrom) {
        this.argumentFrom = argumentFrom;
    }

    /**
     * Get destination argument dictionary of this translation link
     *
     * @return an argument dictionary
     */
    public ArgumentDictionary getArgumentDictionaryTo() {
        return argumentTo;
    }

    /**
     * Set destination argument dictionary of this translation link
     *
     * @param argumentTo an argument dictionary
     */
    public void setArgumentDictionaryTo(ArgumentDictionary argumentTo) {
        this.argumentTo = argumentTo;
    }

    /*
     * CONVENIENCE METHODS
     */

    /**
     * Get the current version of this argument translation link
     *
     * @return a timestamp with the latest update moment of this argumenttranslation link
     */
    public Timestamp getVersion() {
        return getContribution().getVersion();
    }

    @Override
    public String toString() {
        // because of lazy load, must explicitly call getter
        return new StringBuffer("translation link [").append(getIdContribution()).append("] between ")
                .append(getArgumentDictionaryFrom().getIdDictionary()).append(" and ")
                .append(getArgumentDictionaryTo().getIdDictionary())
                .append(" [version:").append(getContribution().getVersion()).append("]").toString();
    }

    /*
     * QUERIES
     */

    /**
     * Retrieve a translation link by its id
     *
     * @param id the translation link id
     * @return the translation link corresponding to that id, null otherwise
     */
    public static ArgumentTranslation findById(Long id) {
        return id == null || id == -1L ? null : find.byId(id);
    }

}
