/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.core.api.contribution.EContributionType;
import com.avaje.ebean.*;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.stream.Collectors;

/**
 * The persistent class for the debate database table, conceptual subtype of contribution.
 * Always linked to a ridge tile assertion (the first argument).
 * May also have justification links between contextualized arguments (materialized by ArgumentJustification objects).
 *
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "debate")
public class Debate extends WebdebModel {

    protected static final org.slf4j.Logger logger = play.Logger.underlying();
    private static final Model.Finder<Long, Debate> find = new Model.Finder<>(Debate.class);

    @Id
    @Column(name = "id_contribution", unique = true, nullable = false)
    private Long idContribution;

    // forcing updates from this object, deletions are handled at the contribution level
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "id_contribution", nullable = false, insertable = false, updatable = false)
    private Contribution contribution;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_first_argument", nullable = false)
    private ArgumentContext firstContextArgument;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "shade", nullable = false)
    private TArgShadeType shade;

    /**
     * Get parent contribution object
     *
     * @return the parent contribution
     */
    public Contribution getContribution() {
        return contribution;
    }

    /**
     * Set parent contribution object
     *
     * @param contribution the parent contribution
     */
    public void setContribution(Contribution contribution) {
        this.contribution = contribution;
    }

    /**
     * Get the id of argument
     *
     * @return the argument id
     */
    public Long getIdContribution() {
        return idContribution;
    }

    /**
     * Set the id of argument
     *
     * @param idContribution the argument id
     */
    public void setIdContribution(Long idContribution) {
        this.idContribution = idContribution;
    }

    /**
     * Get the first contextualized argument of this debate.
     *
     * @return the first contextualized argument
     */
    public ArgumentContext getFirstContextArgument() {
        return firstContextArgument;
    }

    /**
     * Set the first contextualized argument of this debate.
     *
     * @param firstContextArgument the debate first contextualized argument
     */
    public void setFirstContextArgument(ArgumentContext firstContextArgument) {
        this.firstContextArgument = firstContextArgument;
    }

    /**
     * Get the first debate argument argument shade
     *
     * @return the first debate argument shade
     */
    public TArgShadeType getShade() {
        return shade;
    }

    /**
     * Set this first debate argument shade
     *
     * @param shade a shade to set
     */
    public void setShade(TArgShadeType shade) {
        this.shade = shade;
    }

    /**
     * Get the current version of this argument
     *
     * @return a timestamp with the latest update moment of this argument
     */
    public Timestamp getVersion() {
        return getContribution().getVersion();
    }

    @Override
    public String toString() {
        // must use getters and explicitly loop into references, otherwise ebean may send back deferred beanlist
        // (lazy load not triggered from toString methods)
        String name = (firstContextArgument != null && firstContextArgument.getArgument() != null ?
                firstContextArgument.getArgument().getTitleDictionary().getTitle() : "");
        return new StringBuffer("debate [").append(getIdContribution())
                .append("], name: [").append(name)
                .append(", groups: ").append(getContribution().getGroups().stream()
                        .map(g -> String.valueOf(g.getIdGroup())).collect(Collectors.joining(",")))
                .append(" [version:").append(getContribution().getVersion()).append("]").toString();
    }

    /*
     * QUERIES
     */

    /**
     * Retrieve a debate by its id
     *
     * @param id an id
     * @return the debate corresponding to the given id, null if not found
     */
    public static Debate findById(Long id) {
        return id == null || id == -1L ? null : find.byId(id);
    }

    /**
     * Retrieve a debate by its first argument id
     *
     * @param id a contextualized argument id
     * @return the corresponding debate or null
     */
    public static Debate findDebateByFirstArgumentId(Long id) {
        ArgumentContext arg = id == null || id == -1L ? null : ArgumentContext.findById(id);
        return arg != null ? arg.getDebate() : null;
    }

    /**
     * Get a randomly chosen debate from the database
     *
     * @return a random Debate
     */
    public static Debate random() {
        int type = EContributionType.DEBATE.id();
        String sql = "select contribution.id_contribution from contribution " +
                "where contribution_type = " + type + " order by rand() limit 1";
        return findById(Ebean.createSqlQuery(sql).findUnique().getLong("id_contribution"));
    }
}
