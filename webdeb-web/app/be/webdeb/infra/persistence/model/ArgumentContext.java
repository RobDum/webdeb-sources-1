/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.argument.EArgumentShade;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.infra.persistence.model.annotation.Unqueryable;
import com.avaje.ebean.*;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The persistent class for the argument context database table, conceptual subtype of contribution.
 * Always linked to an argument and a context (debate or text), and others elements like places, folders and concerned actors.
 * May have justification links to / from other arguments in a context (materialized by ArgumentJustification objects).
 *
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "argument_context")
public class ArgumentContext extends WebdebModel {

    protected static final org.slf4j.Logger logger = play.Logger.underlying();
    private static final Model.Finder<Long, ArgumentContext> find = new Model.Finder<>(ArgumentContext.class);

    @Id
    @Column(name = "id_contribution", unique = true, nullable = false)
    private Long idContribution;

    // forcing updates from this object, deletions are handled at the contribution level
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "id_contribution", nullable = false, insertable = false, updatable = false)
    private Contribution contribution;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_context", nullable = false)
    private Contribution context;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_argument")
    private Argument argument;

    // all arguments this is the "source" of the link
    @OneToMany(mappedBy = "argumentFrom", cascade = CascadeType.PERSIST)
    private List<ArgumentJustification> argumentJustificationsFrom;

    // all arguments this is the "target" of the link
    @OneToMany(mappedBy = "argumentTo", cascade = CascadeType.PERSIST)
    private List<ArgumentJustification> argumentJustificationsTo;

    // all arguments this is the "target" of the link
    @OneToMany(mappedBy = "argument", cascade = CascadeType.PERSIST)
    private List<ArgumentHasExcerpt> argumentIllustrations;

    // the debate that have that contextualized argument has first argument, if any
    @OneToOne(mappedBy = "firstContextArgument", cascade = CascadeType.PERSIST)
    private Debate debate;

    // all texts that have that contextualized argument has first argument
    @OneToOne(mappedBy = "firstContextArgument", cascade = CascadeType.PERSIST)
    private Text text;

    @Transient
    @Unqueryable
    private List<String> relatedArguments = null;

    /**
     * Default constructor
     */
    public ArgumentContext() {

    }

    /**
     * Constructor to clone a contextualized argument
     *
     * @param toClone         the existing to clone
     * @param newContribution the newContribution for the clone
     * @param newContext      the new context contribution for the clone, or null if we want the same
     */
    public ArgumentContext(ArgumentContext toClone, Contribution newContribution, Contribution newContext) {
        // create argument and binding
        newContribution.setArgumentContext(this);
        contribution = newContribution;
        newContribution.save();

        idContribution = newContribution.getIdContribution();
        context = newContext == null ? toClone.getContext() : newContext;
        argument = toClone.getArgument();
        this.save();

        // update groups
        toClone.getContribution().getGroups().forEach(g -> newContribution.addGroup(g));
        // update contributors
        toClone.getContribution().getContributionHasContributors().forEach(chc -> {
            ContributionHasContributor newChc =
                    new ContributionHasContributor(newContribution, chc.getContributor(), chc.getSerialization(), chc.getStatus());
            newChc.save();
        });
        // update places
        newContribution.addPlaces(toClone.getContribution().getPlaces());

        // update folders
        newContribution.setFolders(toClone.getContribution().getFolders());

        newContribution.update();
    }

    /**
     * Get parent contribution object
     *
     * @return the parent contribution
     */
    public Contribution getContribution() {
        return contribution;
    }

    /**
     * Set parent contribution object
     *
     * @param contribution the parent contribution
     */
    public void setContribution(Contribution contribution) {
        this.contribution = contribution;
    }

    /**
     * Get the id of argument
     *
     * @return the argument id
     */
    public Long getIdContribution() {
        return idContribution;
    }

    /**
     * Set the id of argument
     *
     * @param idContribution the argument id
     */
    public void setIdContribution(Long idContribution) {
        this.idContribution = idContribution;
    }

    /**
     * Get the context where the argument is
     *
     * @return the context contribution
     */
    public Contribution getContext() {
        return context;
    }

    /**
     * Set the context where the argument is
     *
     * @param context the context contribution
     */
    public void setContext(Contribution context) {
        this.context = context;
    }

    /**
     * Get the argument concerned by this object
     *
     * @return the argument placed in the context
     */
    public Argument getArgument() {
        return argument;
    }

    /**
     * Set the argument concerned by this object
     *
     * @param argument the argument placed in the context
     */
    public void setArgument(Argument argument) {
        this.argument = argument;
    }

    /**
     * Get the list of justifications where this argument is the origin
     *
     * @return a list of argument links, may be null
     */
    public List<ArgumentJustification> getArgumentJustificationsFrom() {
        return argumentJustificationsFrom != null ? argumentJustificationsFrom : new ArrayList<>();
    }

    /**
     * Set the list of justifications where this argument is the origin
     *
     * @param argumentJustificationsFrom a list of argument Justifications
     */
    public void setArgumentJustificationsFrom(List<ArgumentJustification> argumentJustificationsFrom) {
        this.argumentJustificationsFrom = argumentJustificationsFrom;
    }

    /**
     * Get the list of justifications where this argument is the destination
     *
     * @return a list of argument Justifications
     */
    public List<ArgumentJustification> getArgumentJustificationsTo() {
        return argumentJustificationsTo != null ? argumentJustificationsTo : new ArrayList<>();
    }

    /**
     * Set the list of justifications where this argument is the destination
     *
     * @param argumentJustificationsTo a list of argument Justifications, may be null
     */
    public void setArgumentJustificationsTo(List<ArgumentJustification> argumentJustificationsTo) {
        this.argumentJustificationsTo = argumentJustificationsTo;
    }

    /**
     * Get the complete list of justification links
     *
     * @return a list of argument Justifications
     */
    public List<ArgumentJustification> getAllArgumentJustifications() {
        List<ArgumentJustification> results = new ArrayList<>(getArgumentJustificationsFrom());
        results.addAll(getArgumentJustificationsTo());
        return results;
    }

    /**
     * Get the list of illustrations where this argument is.
     *
     * @return a list of argument illustrations
     */
    public List<ArgumentHasExcerpt> getArgumentIllustrations() {
        return argumentIllustrations != null ? argumentIllustrations : new ArrayList<>();
    }

    /**
     * Set the list of illustrations where this argument is.
     *
     * @param argumentIllustrations a list of argument illustrations
     */
    public void setArgumentIllustrations(List<ArgumentHasExcerpt> argumentIllustrations) {
        this.argumentIllustrations = argumentIllustrations;
    }

    /**
     * Get the debate that have that contextualized argument has first argument.
     *
     * @return a debate or null
     */
    public Debate getDebate() {
        return debate;
    }

    /**
     * Set the debate that have that contextualized argument has first argument.
     *
     * @param debates a debate
     */
    public void setDebate(Debate debates) {
        this.debate = debate;
    }

    /**
     * Get the text that have that contextualized argument has first argument.
     *
     * @return a text
     */
    public Text getText() {
        return text;
    }

    /**
     * Set the text that have that contextualized argument has first argument.
     *
     * @param text a text
     */
    public void setText(Text text) {
        this.text = text;
    }

    /**
     * Get the current version of this argument
     *
     * @return a timestamp with the latest update moment of this argument
     */
    public Timestamp getVersion() {
        return getContribution().getVersion();
    }

    @Override
    public String toString() {
        // must use getters and explicitly loop into references, otherwise ebean may send back deferred beanlist
        // (lazy load not triggered from toString methods)
        return new StringBuffer("argument_context [").append(getIdContribution())
                .append("], context: [").append(getContext().getIdContribution()).append("] ").append(getContext().getSortkey())
                .append("], argument: [").append(getArgument().getIdContribution()).append("] ").append(getArgument().getTitleDictionary().getTitle())
                .append(", actors: {").append(getContribution().getActors().stream()
                        .map(ContributionHasActor::toString).collect(Collectors.joining(", "))).append("}")
                .append(", groups: ").append(getContribution().getGroups().stream()
                        .map(g -> String.valueOf(g.getIdGroup())).collect(Collectors.joining(",")))
                .append(" [version:").append(getContribution().getVersion()).append("]").toString();
    }

    /*
     * QUERIES
     */

    /**
     * Retrieve an argument contextualized by its id
     *
     * @param id an id
     * @return the argument contextualized corresponding to the given id, null if not found
     */
    public static ArgumentContext findById(Long id) {
        return id == null || id == -1L ? null : find.byId(id);
    }

    /**
     * Get the opposite contextualized argument in the context, if any
     *
     * @return an opposite contextualized argument
     */
    public ArgumentContext getOppositeArgument() {
        /*String select = "select distinct id_argument_dictionary from argument_dictionary " +
                "where argument_dictionary.title like '" + token + "%'" + (lang == null ? "" : " and argument_dictionary.lang= '" + lang + "'");

        logger.debug("search for opposite argument : " + select);
        List<ArgumentContext> result = Ebean.find(ArgumentContext.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
        return result != null && !result.isEmpty() ? result.get(0) : null;*/
        return null;
    }

    public Map<Long, EArgumentShade> getSimilarArguments() {
        Long idDictionnary = argument.getTitleDictionary().getIdDictionary();
        String select = "SELECT distinct ac.id_contribution, IFNULL(asim1.shade,asim2.shade) as shade FROM webdeb.argument_context ac " +
                "left join argument a on a.id_contribution = ac.id_argument " +
                "left join argument_similarity asim1 on asim1.id_argument_from = a.id_contribution " +
                "left join argument_similarity asim2 on asim2.id_argument_to = a.id_contribution " +
                "where asim1.id_argument_to = " + idDictionnary + " OR asim2.id_argument_from = " + idDictionnary +
                " UNION SELECT distinct ac.id_contribution, case a.shade " +
                "when 0 then 0 when 1 then 1 when 2 then 0 when 3 then 1 end as shade FROM webdeb.argument_context ac " +
                "left join argument a on a.id_contribution = ac.id_argument " +
                "left join argument_dictionary ad on ad.id_argument_dictionary = a.id_argument_dictionary " +
                "where ad.id_argument_dictionary = " + idDictionnary;

        Map<Long, EArgumentShade> results = new HashMap<>();

        Ebean.createSqlQuery(select).findList().forEach(e ->
                results.put(e.getLong("id_contribution"), EArgumentShade.value(e.getInteger("shade"))));

        return results;
    }

    /**
     * Get all debates where this argument (the dictionary argument) appeared
     *
     * @return a list of debates
     */
    public List<Debate> getAllDebates() {
        String select = "select distinct d.id_contribution from debate d " +
                "left join argument_context ac on d.id_contribution = ac.id_context " +
                "where ac.id_contribution in (" + String.join(",", getRelatedArguments()) + ") " +
                "or d.id_first_argument in (" + String.join(",", getRelatedArguments()) + ")";
        List<Debate> results = Ebean.find(Debate.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
        return results == null ? new ArrayList<>() : results;
    }

    /**
     * Get all texts where this argument (the dictionary argument) appeared
     *
     * @return a list of texts
     */
    public List<Text> getAllTexts() {
        String select = "select distinct t.id_contribution from text t " +
                "left join argument_context ac on t.id_contribution = ac.id_context " +
                "where ac.id_contribution in (" + String.join(",", getRelatedArguments()) + ")";
        List<Text> results = Ebean.find(Text.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
        return results == null ? new ArrayList<>() : results;
    }

    /**
     * Get all folders where this argument (the dictionary argument) appeared
     *
     * @return a list of folders
     */
    public List<Folder> getAllFolders() {
        String select = "select distinct f.id_contribution from folder f " +
                "inner join contribution_has_folder chf on chf.folder = f.id_contribution " +
                "where chf.id_contribution in (" + String.join(",", getRelatedArguments()) + ")";
        List<Folder> results = Ebean.find(Folder.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
        return results == null ? new ArrayList<>() : results;
    }

    public static List<ArgumentContext> getAllNotLinkedArguments(Long contextId, Long argumentId) {
        String select = "SELECT distinct ac.id_contribution FROM argument_context ac " +
                "left join argument_justification aj on aj.id_argument_from = ac.id_contribution " +
                "left join argument_justification aj2 on aj2.id_argument_to = ac.id_contribution " +
                "where ac.id_context = " + contextId + " and aj.id_contribution is null and aj2.id_contribution is null " +
                "and ac.id_contribution != " + argumentId + " limit 0, 10";
        logger.debug(select);
        List<ArgumentContext> results = Ebean.find(ArgumentContext.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
        return results == null ? new ArrayList<>() : results;
    }

    public static List<ArgumentContext> getPotentialJustifications(Long contextId, Long argumentId, Long dictionaryId) {
        String select = "SELECT distinct ac.id_contribution FROM argument_context ac " +
                "left join argument_justification aj on aj.id_argument_to = ac.id_contribution " +
                "left join argument_context ac2 on ac2.id_contribution = aj.id_argument_from " +
                "left join argument a on a.id_contribution = ac2.id_argument " +
                "left join argument a2 on a2.id_contribution = ac.id_argument " +
                "where ac.id_context != " + contextId + " and a.id_argument_dictionary = " + dictionaryId +
                " and a2.id_argument_dictionary not in " +
                "(select distinct a.id_argument_dictionary from argument a " +
                "left join argument_context ac on a.id_contribution = ac.id_argument " +
                "left join argument_justification aj on aj.id_argument_to = ac.id_contribution " +
                "where aj.id_argument_from = " + argumentId + ") " +
                "limit 0, 10";
        List<ArgumentContext> results = Ebean.find(ArgumentContext.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
        return results == null ? new ArrayList<>() : results;
    }

    /*
     * PRIVATE HELPERS
     */

    /**
     * Get the number of excerpts linked to this argument
     *
     * @param contributorId the id of the contributor for which we need that stats
     * @param groupId       the group where see the stats
     * @return the number of excerpts linked to this argument
     */
    public int getNbExcerpts(Long contributorId, int groupId) {
        String select = "SELECT count(distinct ache.id_contribution) as 'occurs'" +
                "FROM argument_context_has_excerpt ache " +
                "inner join argument_context ac on ache.id_argument_context = ac.id_contribution  " +
                "inner join argument a on ac.id_argument = a.id_contribution " +
                getContributionStatsJoins("ache.id_excerpt", contributorId) +
                " where a.id_argument_dictionary = " + argument.getTitleDictionary().getIdDictionary() + " " +
                getContributionStatsWhereClause(contributorId, groupId);
        return Ebean.createSqlQuery(select).findUnique().getInteger("occurs");
    }

    /**
     * Retrieve the amount of excerpts linked to thid argument by shade as integer map
     *
     * @param contributorId the id of the contributor for which we need that stats
     * @param groupId       the group where see the stats
     * @return the amount of excerpts that are connected to this argument by shade as integer map
     */
    public Map<Integer, Integer> getSimpleExcerptMetrics(Long contributorId, int groupId) {
        EArgumentShade argumentShade = argument.getShade().getEArgumentShade();

        String select = "SELECT ache.shade as 'shade', a.shade as 'argshade', count(distinct ache.id_contribution) as 'occurs'" +
                "FROM argument_context_has_excerpt ache " +
                "inner join argument_context ac on ache.id_argument_context = ac.id_contribution  " +
                "inner join argument a on ac.id_argument = a.id_contribution " +
                getContributionStatsJoins("ache.id_excerpt", contributorId) +
                " where a.id_argument_dictionary = " + argument.getTitleDictionary().getIdDictionary() + " " +
                getContributionStatsWhereClause(contributorId, groupId) + " group by a.shade, ache.shade";
        Map<Integer, Integer> results = new HashMap<>();

        Ebean.createSqlQuery(select).findList().forEach(e -> {
            EArgumentLinkShade eShade = EArgumentLinkShade.value(e.getInteger("shade"));
            EArgumentShade argShade = EArgumentShade.value(e.getInteger("argshade"));

            if ((argumentShade.isApprobation() && !argShade.isApprobation()) ||
                    (!argumentShade.isApprobation() && argShade.isApprobation())) {
                eShade = eShade.getOpposite();
            }

            results.put(eShade.id(), e.getInteger("occurs"));
        });

        return results;
    }

    /**
     * Get all arguments that share the same argument dictionary
     *
     * @return a list of arguments id
     */
    private List<String> getRelatedArguments() {

        if (relatedArguments == null){
            relatedArguments = new ArrayList<>();

            String select = "select ac2.id_contribution as 'argument' from argument_context ac2 " +
                    " inner join argument a on ac2.id_argument = a.id_contribution " +
                    " where a.id_argument_dictionary = " + argument.getTitleDictionary().getIdDictionary();

                Ebean.createSqlQuery(select).

            findList().forEach(e -> relatedArguments.add(e.getString("argument")));
        }
        return relatedArguments;
    }
}
