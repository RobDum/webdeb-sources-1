/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.application.query.EQueryKey;
import be.webdeb.core.api.contribution.ModelDescription;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contribution.EDBRelationType;
import be.webdeb.core.api.contribution.EModificationStatus;
import be.webdeb.core.impl.contribution.ConcreteModelAttributeDescription;
import be.webdeb.core.impl.contribution.ConcreteModelDescription;
import be.webdeb.infra.persistence.model.annotation.Unqueryable;
import com.avaje.ebean.*;
import com.avaje.ebean.Query;
import com.avaje.ebean.annotation.CacheBeanTuning;
import org.reflections.Reflections;

import javax.persistence.*;
import javax.persistence.Version;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The persistent class for the contribution database table, conceptual abstract supertype of texts, actors,
 * arguments and links. May be bound to a list of Topic for texts and arguments.
 *
 * Because of ebean limitations, there is no way to create a class_per_table inheritance, so links to subtypes
 * must be handled by hand.
 *
 * This class is a soft deleted one because we need to trace the whole evolution of contributions in the
 * contribution_has_contributor table, ie, it is flagged as deleted (with the JPA @softdeleted annotation).
 * "Subtype" objects are hard deleted, on the other hand.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 * @see be.webdeb.core.api.contribution.EContributionType
 */
@Entity
@CacheBeanTuning
@Table(name = "contribution")
public class Contribution extends WebdebModel {

  // custom logger
  private static final org.slf4j.Logger logger = play.Logger.underlying();
  private static final Model.Finder<Long, Contribution> find = new Model.Finder<>(Contribution.class);

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_contribution", unique = true, nullable = false)
  private Long idContribution;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "contribution_type", nullable = false)
  private TContributionType contributionType;

  @Column(name = "hit")
  private Long hit = 0L;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "validated", nullable = false)
  private TValidationState validated;

  @Column(name = "locked")
  @Unqueryable
  private boolean locked;

  @Column(name = "sortkey")
  private String sortkey;

  @Column(name = "hidden")
  @Unqueryable
  private int hidden;

  @Column(name = "deleted")
  @Unqueryable
  private boolean deleted;

  @Version
  @Column(name = "version")
  @Unqueryable
  private Timestamp version;

  // SUBTYPES
  @OneToOne(mappedBy = "contribution", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @Unqueryable
  private Actor actor;

  @OneToOne(mappedBy = "contribution", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @Unqueryable
  private Debate debate;

  @OneToOne(mappedBy = "contribution", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @Unqueryable
  private Argument argument;

  @OneToOne(mappedBy = "contribution", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @Unqueryable
  private ArgumentContext argumentContext;

  @OneToOne(mappedBy = "contribution", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @Unqueryable
  private Excerpt excerpt;

  @OneToOne(mappedBy = "contribution", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @Unqueryable
  private Text text;

  @OneToOne(mappedBy = "contribution", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @Unqueryable
  private Folder folder;

  @OneToOne(mappedBy = "contribution", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @Unqueryable
  private ArgumentJustification justificationLink;

  @OneToOne(mappedBy = "contribution", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @Unqueryable
  private ArgumentSimilarity similarityLink;

  @OneToOne(mappedBy = "contribution", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @Unqueryable
  private ArgumentHasExcerpt illustrationLink;

  @OneToOne(mappedBy = "contribution", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @Unqueryable
  private FolderLink folderlink;

  @OneToOne(mappedBy = "contribution", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @Unqueryable
  private ExternalContribution externalContribution;

  @OneToOne(mappedBy = "internalContribution", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @Unqueryable
  private ExternalContribution originExternalContribution;

  // BINDINGS
  @OneToMany(mappedBy = "contribution", cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
  @Unqueryable
  private List<ContributionHasContributor> contributionHasContributors;

  @OneToMany(mappedBy = "contribution", cascade = CascadeType.ALL)
  private List<ContributionHasActor> contributionHasActors;

  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinTable(
      name="contribution_has_folder",
      joinColumns = { @JoinColumn(name="id_contribution", referencedColumnName = "id_contribution") },
      inverseJoinColumns = { @JoinColumn(name="folder", referencedColumnName = "id_contribution") }
  )
  private List<Folder> folders;

  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinTable(
          name="contribution_has_place",
          joinColumns = { @JoinColumn(name="id_contribution", referencedColumnName = "id_contribution") },
          inverseJoinColumns = { @JoinColumn(name="place", referencedColumnName = "id_place") }
  )
  private List<Place> places;

  @OneToMany(mappedBy = "context", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  private List<ArgumentContext> argumentContexts;

  // all argument justification links taken in the debate
  @OneToMany(mappedBy = "context", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  private List<ArgumentJustification> argumentJustificationLinks;

  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinTable(
      name="contribution_in_group",
      joinColumns = { @JoinColumn(name="id_contribution", referencedColumnName = "id_contribution") },
      inverseJoinColumns = { @JoinColumn(name="id_group", referencedColumnName = "id_group") }
  )
  private List<Group> groups;

  // because we are using raw sql to query database we must create variables to receive column aliases
  @Transient
  @Unqueryable
  private Double relevance;

  @Transient
  @Unqueryable
  private int searched;

  @Transient
  @Unqueryable
  private String name;

  @Transient
  @Unqueryable
  private Timestamp latest;

  @Transient
  @Unqueryable
  private int ctype;

  @Unqueryable
  private static final String ORDER_BY_LATEST = "latest";

  /**
   * Get the contribution id
   *
   * @return an id
   */
  public Long getIdContribution() {
    return idContribution;
  }

  /**
   * Set the contribution
   *
   * @param idContribution a unique id
   */
  public void setIdContribution(Long idContribution) {
    this.idContribution = idContribution;
  }

  /**
   * Get this contribution type
   *
   * @return the contribution type
   */
  public TContributionType getContributionType() {
    return contributionType;
  }

  /**
   * Set the contribution type
   *
   * @param contributionType the contribution type
   */
  public void setContributionType(TContributionType contributionType) {
    this.contributionType = contributionType;
  }

  /**
   * Get the hit (popularity) value
   *
   * @return the number of hit (amount of visualiation requests) of this contribution
   */
  public Long getHit() {
    return hit;
  }

  /**
   * Set the hit (popularity) value
   *
   * @param hit the number of hit (amount of visualiation requests) of this contribution
   */
  public void setHit(Long hit) {
    this.hit = hit;
  }

  /**
   * Check whether this contribution has been validated by a contributor
   *
   * @return the validated state
   */
  public TValidationState getValidated() {
    return validated;
  }

  /**
   * Set the state saying if this contribution has been validated by a contributor
   *
   * @param validated a validated state
   */
  public void setValidated(TValidationState validated) {
    this.validated = validated;
  }

  /**
   * Get the absolute sort key for that contribution (null for argument links) containing either the
   * actor's name, the argument's standard form or the text title
   *
   * @return an absolute search/sort key for that contribution
   */
  public String getSortkey() {
    return sortkey;
  }

  /**
   * Set the absolute sort key for that contribution (must be null for argument links) containing either the
   * actor's name, the argument's standard form or the text title, removing all non alphanumeric characters
   *
   * @param sortkey an absolute search/sort key for that contribution
   */
  public void setSortkey(String sortkey) {
    this.sortkey = sortkey != null && sortkey.length() > 700 ? sortkey.substring(0,699) : sortkey;
  }

  /**
   * Check whether this contribution has been locked by admin
   *
   * @return true if this contribution has been locked
   */
  public boolean isLocked() {
    return locked;
  }

  /**
   * Set whether this contribution has been locked by admins
   *
   * @param locked true if this contribution has been locked
   */
  public void setLocked(boolean locked) {
    this.locked = locked;
  }

  /**
   * Check whether this contribution has been deleted
   *
   * @return true if this contribution has been deleted
   */
  public boolean isDeleted() {
    return deleted;
  }

  /**
   * Set whether this contribution has been deleted
   *
   * @param deleted true if this contribution has been deleted
   */
  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }

  /**
   * Check if this contribution must is hidden, ie this contribution (text) only serves as property holder
   * for self-contained arguments (like tweets)
   *
   * @return true if this contribution is an empty shell
   */
  public boolean isHidden() {
    return hidden == 1;
  }

  /**
   * Set whether this contribution must is hidden, ie this contribution (text) only serves as property holder
   * for self-contained arguments (like tweets)
   *
   * @param hidden true if this contribution is an empty shell
   */
  public void isHidden(boolean hidden) {
    this.hidden = hidden ? 1 : 0;
  }

  /**
   * Get the list contributors for this contribution
   *
   * @return a list of joint-objects for contributors of this contribution
   */
  public List<ContributionHasContributor> getContributionHasContributors() {
    return contributionHasContributors;
  }

  /**
   * Set the list contributors for this contribution
   *
   * @param contributionHasContributors a list of joint-objects for contributors of this contribution
   */
  public void setContributionHasContributors(List<ContributionHasContributor> contributionHasContributors) {
    this.contributionHasContributors = contributionHasContributors;
  }

  /**
   * Get the list of actors bound to this contribution (joint-objects)
   *
   * @return the list of actors bound to this contribution
   */
  public List<ContributionHasActor> getContributionHasActors() {
    return contributionHasActors;
  }

  /**
   * Set the list of actors bound to this contribution (joint-objects)
   *
   * @param contributionHasActors the list of actors bound to this contribution
   */
  public void setContributionHasActors(List<ContributionHasActor> contributionHasActors) {
    this.contributionHasActors = contributionHasActors;
  }

  /**
   * Get the actor sub-object, if any
   *
   * @return an actor if this.contributionType = 0, null otherwise
   */
  public Actor getActor() {
    return actor;
  }

  /**
   * Set the actor sub-object (won't update contributionType accordingly)
   *
   * @param actor an actor object
   */
  public void setActor(Actor actor) {
    this.actor = actor;
  }

  /**
   * Get the debate sub-object, if any
   *
   * @return a debate object if this.contributionType = 1, null otherwise
   */
  public Debate getDebate() {
    return debate;
  }

  /**
   * Set the debate sub-object (won't update contributionType accordingly)
   *
   * @param debate a debate object
   */
  public void setDebate(Debate debate) {
    this.debate = debate;
  }

  /**
   * Get the argument sub-object, if any
   *
   * @return an argument object if this.contributionType = 3, null otherwise
   */
  public ArgumentContext getArgumentContext() {
    return argumentContext;
  }

  /**
   * Set the contextualized argument sub-object (won't update contributionType accordingly)
   *
   * @param argumentContext a contextualized argument object
   */
  public void setArgumentContext(ArgumentContext argumentContext) {
    this.argumentContext = argumentContext;
  }

  /**
   * Get the argument sub-object, if any
   *
   * @return an argument object if this.contributionType = 2, null otherwise
   */
  public Argument getArgument() {
    return argument;
  }

  /**
   * Set the argument sub-object (won't update contributionType accordingly)
   *
   * @param argument an argument object
   */
  public void setArgument(Argument argument) {
    this.argument = argument;
  }

  /**
   * Get the excerpt sub-object, if any
   *
   * @return an excerpt object if this.contributionType = 3, null otherwise
   */
  public Excerpt getExcerpt() {
    return excerpt;
  }

  /**
   * Set the excerpt sub-object (won't update contributionType accordingly)
   *
   * @param excerpt an excerpt object
   */
  public void setExcerpt(Excerpt excerpt) {
    this.excerpt = excerpt;
  }

  /**
   * Get the sub-object text, if any
   *
   * @return a text object if this.contributionType = 4, null otherwise
   */
  public Text getText() {
    return text;
  }

  /**
   * Set the text sub-object (won't update contributionType accordingly)
   *
   * @param text a text object
   */
  public void setText(Text text) {
    this.text = text;
  }

  /**
   * Get the sub-object folder, if any
   *
   * @return a folder object if this.contributionType = 5, null otherwise
   */
  public Folder getFolder() {
    return folder;
  }

  /**
   * Set the folder sub-object (won't update contributionType accordingly)
   *
   * @param folder a folder object
   */
  public void setFolder(Folder folder) {
    this.folder = folder;
  }

  /**
   * Get the justification link sub-object, if any
   *
   * @return a justification link if this.contributionType = 8, null otherwise
   */
  public ArgumentJustification getJustificationLink() {
    return justificationLink;
  }

  /**
   * Set the justification link sub-object (won't update contributionType accordingly)
   *
   * @param link an argument justification link object
   */
  public void setJustificationLink(ArgumentJustification link) {
    this.justificationLink = link;
  }

  /**
   * Get the similarity link sub-object, if any
   *
   * @return a similarity link if this.contributionType = 9, null otherwise
   */
  public ArgumentSimilarity getSimilarityLink() {
    return similarityLink;
  }

  /**
   * Set the similarity link sub-object (won't update contributionType accordingly)
   *
   * @param link an argument similarity link object
   */
  public void setSimilarityLink(ArgumentSimilarity link) {
    this.similarityLink = link;
  }

  /**
   * Get the illustration link sub-object, if any
   *
   * @return a illustration link if this.contributionType = 10, null otherwise
   */
  public ArgumentHasExcerpt getIllustrationLink() {
    return illustrationLink;
  }

  /**
   * Set the illustration link sub-object (won't update contributionType accordingly)
   *
   * @param link an argument illustration link object
   */
  public void setillustrationLink(ArgumentHasExcerpt link) {
    this.illustrationLink = link;
  }

  /**
   * Get the sub-object folder link, if any
   *
   * @return a folder link object if this.contributionType = 8, null otherwise
   */
  public FolderLink getFolderLink() {
    return folderlink;
  }

  /**
   * Set the folder link sub-object (won't update contributionType accordingly)
   *
   * @param folderlink a folder object
   */
  public void setFolderLink(FolderLink folderlink) {
    this.folderlink = folderlink;
  }

  /**
   * Get the external contribution where this contribution come from, if any
   *
   * @return a external contribution
   */
  public ExternalContribution getOriginExternalContribution() {
    return originExternalContribution;
  }

  /**
   * Get the sub-object external contribution, if any
   *
   * @return an external contribution objectif this.contributionType = 10 or 11, null otherwise
   */
  public ExternalContribution getExternalContribution() {
    return externalContribution;
  }

  /**
   * Set the external contribution sub-object (won't update contributionType accordingly)
   *
   * @param externalContribution a ExternalContribution object
   */
  public void setExternalContribution(ExternalContribution externalContribution) {
    this.externalContribution = externalContribution;
  }

  /**
   * Get the list of folders for this contribution
   *
   * @return the list of folders associated to this contribution
   */
  public List<Folder> getFolders() {
    return folders;
  }

  /**
   * Set the list of folders for this contribution
   *
   * @param folders the list of folders associated to this contribution
   */
  public void setFolders(List<Folder> folders) {
    this.folders = folders;
  }

  /**
   * Get the list of places that is concerned by this contribution
   *
   * @return a list of geographical places
   */
  public List<Place> getPlaces() {
    return places;
  }

  /**
   * Add a list of places that is concerned by this contribution
   *
   * @param places a list of geographical places
   */
  public void addPlaces(List<Place> places) {
    if(this.places == null || this.places.isEmpty()){
      this.places = places;
    }else{
      for(Place place : places){
        if(this.places.stream().noneMatch(e -> e.getId().equals(place.getId()))){
          this.places.add(place);
        }
      }
    }
  }

  /**
   * Set the list of places that is concerned by this contribution
   *
   * @param places a list of geographical places
   */
  public void setPlaces(List<Place> places) {
    this.places = places;
  }

    /**
     * Add given folder to this contribution.
     *
     * @param folder a folder
     */
    public void addFolder(Folder folder) {
        if(folders.stream().noneMatch(e -> e.getIdContribution().equals(folder.getIdContribution()))) {
            folders.add(folder);
        }
    }

    /**
     * Remove given folder to this contribution.
     *
     * @param folder a folder
     */
    public void removeFolder(Folder folder) {
        folders.removeIf(f -> f.getIdContribution().equals(folder.getIdContribution()));
    }

  /**
   * Get the list arguments contextualized by this contribution
   *
   * @return a list of argument context
   */
  public List<ArgumentContext> getArgumentContexts() {
    return argumentContexts;
  }

  /**
   * Set the list of arguments contextualized by this contribution
   *
   * @param argumentContexts a list of argument context
   */
  public void setArgumentContexts(List<ArgumentContext> argumentContexts) {
    this.argumentContexts = argumentContexts;
  }

  /**
   * Get the list of argument justification links contextualized by this contribution
   *
   * @return a list of argument justification links, may be empty
   */
  public List<ArgumentJustification> getArgumentJustificationLinks() {
    return argumentJustificationLinks != null ? argumentJustificationLinks: new ArrayList<>();
  }

  /**
   * Set the list of argument justification links contextualized by this contribution
   *
   * @param argumentJustificationLinks a list of argument justification links
   */
  public void setArgumentJustificationLinks(List<ArgumentJustification> argumentJustificationLinks) {
    this.argumentJustificationLinks = argumentJustificationLinks;
  }

  /**
   * Get the relevant groups for which this contribution belongs to, if any
   *
   * @return a list of groups
   */
  public List<Group> getGroups() {
    return groups != null ? groups : new ArrayList<>();
  }

  /**
   * Get the relevant group ids for which this contribution belongs to, if any
   *
   * @return a list of group ids
   */
  public Set<Integer> getGroupIds() {
    Set<Integer> ids = new HashSet<>();
    Ebean.createSqlQuery("SELECT id_group FROM contribution_in_group cig where cig.id_contribution = " + idContribution)
            .findEach(e ->  ids.add(e.getInteger("id_group")));
    return ids;
  }

  /**
   * Set the relevant groups where this contribution is visible
   *
   * @param groups a list of groups
   */
  public void setGroups(List<Group> groups) {
    this.groups = groups;
  }

  /**
   * Add given group visibility to this contribution.
   *
   * @param group a group
   */
  public void addGroup(Group group) {
    if (groups == null) {
      setGroups(new ArrayList<>());
    }
    if (groups.stream().noneMatch(g -> g.getIdGroup() == group.getIdGroup())) {
      groups.add(group);
    }
  }

  /**
   * Remove given group visibility to this contribution.
   *
   * @param group a group
   */
  public void removeGroup(Group group) {
    if (groups == null) {
      setGroups(new ArrayList<>());
    }
    groups.removeIf(g -> g.getIdGroup() == group.getIdGroup());
  }

  /**
   * Get the current version of this contribution
   *
   * @return a timestamp with the latest update moment of this contribution
   */
  public Timestamp getVersion() {
    return version;
  }

  /**
   * Set the version of this contribution
   *
   * @param version the timestamp with the latest update moment of this contribution
   */
  public void setVersion(Timestamp version) {
    this.version = version;
  }

  /*
   * HELPERS
   */

  /**
   * Increment the amount of hit for this contribution
   */
  public void addHit() {
    setHit(hit + 1);
  }

  /*
   * CONVENIENCE METHODS
   */
  @Override
  public String toString() {
    switch (EContributionType.value(getContributionType().getIdContributionType())) {
      case ACTOR:
        return getActor().toString();
      case DEBATE:
        return getDebate().toString();
      case ARGUMENT:
        return getArgument().toString();
      case ARGUMENT_CONTEXTUALIZED:
        return getArgumentContext().toString();
      case EXCERPT:
        return getExcerpt().toString();
      case TEXT:
        return getText().toString();
      case FOLDER:
        return getFolder().toString();
      case ARG_JUSTIFICATION:
        return getJustificationLink().toString();
      case ARG_SIMILARITY:
        return getSimilarityLink().toString();
      case ARG_ILLUSTRATION:
        return getIllustrationLink().toString();
      case FOLDERLINK:
        return getFolderLink().toString();
      case EXTERNAL_TEXT:
      case EXTERNAL_EXCERPT:
        return getExternalContribution().toString();
      default:
        logger.error("wrong contribution type " + getContributionType().getIdContributionType());
        return "";
    }
  }

  /**
   * Retrieve all actors involved in of this contribution
   *
   * @return the list<ContributionHasActor> with the isAuthor field set to true
   */
  public List<ContributionHasActor> getActors() {
    return getContributionHasActors();
  }

  /**
   * Retrieve all actors flagged as "authors" of this contribution
   *
   * @return the list<ContributionHasActor> with the isAuthor field set to true
   */
  public List<ContributionHasActor> getAuthors() {
    return getContributionHasActors().stream().filter(ContributionHasActor::isAuthor).collect(Collectors.toList());
  }

  /**
   * Get the map of number of linked elements with this contribution
   *
   * @param contributorId the id of the contributor for which we need that stats
   * @param groupId the id of the group where stats must be counted
   * @return the map of number of linked elements with this contribution by contribution type
   */
  public Map<EContributionType, Integer> getCountRelationsMap(Long contributorId, int groupId){
    Map<EContributionType, Integer> map = new LinkedHashMap<>();
    map.put(EContributionType.FOLDER, getNbFolders());

    switch (contributionType.getEContributionType()){
      case ARGUMENT_CONTEXTUALIZED:
        map.put(EContributionType.EXCERPT, argumentContext.getNbExcerpts(contributorId, groupId));
        break;
      case ACTOR:
        map.put(EContributionType.EXCERPT, actor.getNbExcerpts(true, contributorId, groupId));
        map.put(EContributionType.TECH_VALUE, actor.getNbExcerpts(false, contributorId, groupId));
        break;
      case EXCERPT:
        map.put(EContributionType.ARGUMENT_CONTEXTUALIZED, excerpt.getNbArguments(contributorId, groupId));
        break;
      case TEXT:
        map.put(EContributionType.EXCERPT, text.getNbExcerpts(contributorId, groupId));
        map.put(EContributionType.ARGUMENT_CONTEXTUALIZED, text.getNbArguments(contributorId, groupId));
        break;
    }

    return map;
  }

  @Override
  public boolean delete() {
    boolean r;
    setDeleted(true);
    update();
    getContributionHasActors().forEach(Model::delete);
    switch (EContributionType.value(getContributionType().getIdContributionType())) {
      case ACTOR:
        return getActor().delete();
      case DEBATE:
        return getDebate().delete();
      case ARGUMENT_CONTEXTUALIZED:
        return getArgumentContext().delete();
      case ARGUMENT:
        return getArgument().delete();
      case EXCERPT:
        return getExcerpt().delete();
      case TEXT:
        return getText().delete();
      case FOLDER:
        return getFolder().delete();
      case ARG_JUSTIFICATION:
        return getJustificationLink().delete();
      case ARG_SIMILARITY:
        return getSimilarityLink().delete();
      case ARG_ILLUSTRATION:
        return getIllustrationLink().delete();
      case FOLDERLINK:
        return getFolderLink().delete();
      case EXTERNAL_TEXT:
        r = getExternalContribution().getText().delete();
        r = r && getExternalContribution().delete();
        return r;
      case EXTERNAL_EXCERPT:
        r = getExternalContribution().getExcerpt().delete();
        r = r && getExternalContribution().delete();
        return r;
      default:
        logger.warn("trying to delete a wrong contribution " + getIdContribution());
        return false;
    }
  }

  /**
   * Check if bound contribution contains given searchText in Contribution's sortkey field
   * or, in case of actors, also checks in all names
   *
   * @param searchText a string to search for (ignore case)
   * @return true if given search text has been found, false otherwise
   */
  public boolean matchSearchForContribution(String searchText){
    if(searchText != null){
      searchText = searchText.toLowerCase();
      if(searchText.equals("") || searchText.contains(getSortkey().toLowerCase())
          || getSortkey().toLowerCase().contains(searchText)
          || analyseContributionsNames(searchText)){
        return true;
      }
    }
    return false;
  }

  private boolean analyseContributionsNames(String searchText){
    return getActor() != null && getActor().getFullNames().stream().anyMatch(n ->
          n.toLowerCase().contains(searchText) || searchText.contains(n.toLowerCase()));
  }

  /*
   * QUERIES
   */

  /**
   * Retrieve a Contribution by its id
   *
   * @param id an id
   * @return the Contribution corresponding to the given id, or null if not found
   */
  public static Contribution findById(Long id) {
    if (id == null || id == -1L) {
      return null;
    }

    return find.byId(id);
  }

  /**
   * Find a list of contributions containing given criteria
   *
   * @param value a value to search for in the aggregated sortKey column
   * @param group the group id to search in
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a (possibly empty) list of contributions matching given search value
   */
  public static List<Contribution> findBySortKey(String value, int group, int fromIndex, int toIndex) {
    if (value == null) {
      return new ArrayList<>();
    }

    Query<Contribution> query = Ebean.find(Contribution.class);
    // will be automatically ordered by relevance
    String token = getSearchToken(value);

    String select = "select contribution.id_contribution, sortkey from contribution left join contribution_in_group on " +
        "contribution.id_contribution = contribution_in_group.id_contribution where sortkey like '%" + token + "%' " +
        "and contribution.contribution_type in (0, 1, 3, 4, 5, 6) and (" +
        "id_group in (" + String.join(",", Group.getVisibleGroupsFor(group)) + ") " +
        "or id_group in (" + String.join(",", Group.getVisibleGroupsFor(Group.getPublicGroup().getIdGroup())) + ")) " +
        "and contribution.deleted = 0 and contribution.hidden = 0 " +
        "ORDER BY CASE WHEN contribution.sortkey like '" + token + "%' THEN 0 " +
        "WHEN sortkey like '" + token + " %' THEN 1 " +
        "WHEN sortkey like '% " + token + "%' THEN 2 " +
        "ELSE 3 END, sortkey" + getSearchLimit(fromIndex, toIndex);
    List<Contribution> result = query.setRawSql(RawSqlBuilder.parse(select).create()).findList();
    return result != null ? result : new ArrayList<>();
  }

  /**
   * Retrieve all contributions by given criteria, ie, key-value pairs as accepted by QueryExecutor
   *
   * @param criteria a list of key,value pairs
   * @param strict check if we must perform a strict search
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return the list of contributions that matches given criteria
   */
  public static List<Contribution> findByCriteria(List<Map.Entry<EQueryKey, String>> criteria, boolean strict, int fromIndex, int toIndex) {
    logger.debug("find by criteria " + criteria.toString());
    Set<Contribution> contributions;

    List<String> selects = new ArrayList<>();
    // handle given keys
    List<String> types = new ArrayList<>();
    List<String> contributionIds = new ArrayList<>();
    // contribution ids to ignore
    List<String> ignoreIds = new ArrayList<>();

    // context ids to ignore
    List<String> ignoreContextIds = new ArrayList<>();
    // context ids where to look
    List<String> lookTextIds = new ArrayList<>();

    // for texts
    List<String> textTitles = new ArrayList<>();
    List<String> textSources = new ArrayList<>();

    // actors
    List<String> actorNames = new ArrayList<>();
    List<String> actorTypes = new ArrayList<>();
    List<String> functions = new ArrayList<>();

    // contextualized arguments
    List<String> argumentContextTitles = new ArrayList<>();

    // debates
    List<String> debatesTitles = new ArrayList<>();

    // excerpts
    List<String> originalExcerpts = new ArrayList<>();

    // folders
    List<String> folders = new ArrayList<>();

    // involved actors in contribution
    EnumMap<EQueryKey, List<String>> involvedActors = new EnumMap<>(EQueryKey.class);
    involvedActors.put(EQueryKey.ACTOR, new ArrayList<>());
    involvedActors.put(EQueryKey.AUTHOR, new ArrayList<>());
    involvedActors.put(EQueryKey.REPORTER, new ArrayList<>());
    involvedActors.put(EQueryKey.SOURCE_AUTHOR, new ArrayList<>());

    // constant sql partial queries
    final String mode = "' in boolean mode)";
    final String relevance = " as relevance ";
    final String searched = " as searched ";
    boolean addSearched = false;
    boolean amongGroup = false;
    boolean allResults = false;
    final String from = " from contribution ";
    final String select_contribution_sortkey = "select contribution.id_contribution, contribution_type as ctype, sortkey as name ";
    String contributor = null;
    String ids = "";

    // for visibility management
    List<String> groups = new ArrayList<>();
    List<String> notInGroups = new ArrayList<>();
    List<String> public_groups = Group.getVisibleGroupsFor(Group.getPublicGroup().getIdGroup());
    final String groupJoin = " left join contribution_in_group on contribution.id_contribution = contribution_in_group.id_contribution ";
    final String groupFilter;
    final String simpleGroupFilter;
    final String groupPublicFilter;
    final String simpleGroupPublicFilter;
    final String groupMainFilter;
    String validatedFilter = ""; // empty by default
    String fetchedFilter = ""; // empty by default

    // default select template clause
    final String select = "select contribution.id_contribution, contribution_type as ctype, ";
    final String nameMatch = "match(first_or_acro, name, pseudo) against ('";

    // lower id to search from
    String lowest = "0";

    // order by key for contribution_type only requests
    String orderBy = "name";

    // sql where clause
    String where = "where ";

    // char lenght clause
    String charLength = ", char_length(sortkey) ";

    // relevance score
    String maxRelevance = "100";
    String hightRelevance = "* 1.2";
    String normalRelevance = "* 0.9";

    // build query helpers with given keys
    for (Map.Entry<EQueryKey, String>  c : criteria) {
      if (c.getValue() != null) {
        String token = c.getValue().trim()
            // sanitize curly quote
            .replace("’", "'")
            // protect single quotes
            .replace("'", "\\'")
            // remove parenthesis
            .replace("(", "").replace(")", "")
            // remove dashes, +, * and others
            .replace("-", "").replace("*", "").replace("+", "")
            .replace(">", "").replace("<", "").replace("~", "").trim() + "*\"";

        // remove ending "*" for perfect match for names
        String nameToken = token.substring(0, token.length() - 2);
        // replace spaces by any char if we are not performing a strict search
        if (!(strict || !token.contains("\""))) {
          token = token.replace(" ", "\"* \"");
        }
        // now remove double quotes, if any
        token = token.replace("\"", "");
        switch (c.getKey()) {
          // group-related
          case GROUP:
            groups.add(c.getValue());
            break;
          case AMONG_GROUP:
            amongGroup = true;
            groups.add(c.getValue());
            break;
          case NOT_IN_GROUP:
            if(c.getValue().equals(String.valueOf(Group.getPublicGroup().getIdGroup()))){
              notInGroups.addAll(public_groups);
            }else{
              notInGroups.add(c.getValue());
            }
            break;
          case VALIDATED:
            validatedFilter = " and validated = " + ("true".equals(c.getValue()) ? 1 : 0) + " ";
            break;
          case FETCHED:
            fetchedFilter = " and fetched = " + ("true".equals(c.getValue()) ? 1 : 0) + " ";
            break;
          // contributor
          case CONTRIBUTOR:
            contributor = c.getValue();
            break;
          // contribution id
          case ID_CONTRIBUTION:
            contributionIds.add(c.getValue());
            break;
          // contribution id to ignore
          case ID_IGNORE:
            ignoreIds.add(c.getValue());
            break;
          // context contribution id to ignore
          case CONTEXT_TO_IGNORE:
            ignoreContextIds.add(c.getValue());
            break;
          // text id where to look
          case TEXT_TO_LOOK:
            lookTextIds.add(c.getValue());
            break;
          // textual contributions
          case CONTRIBUTION_TYPE:
            types.add(c.getValue());
            break;
          case ACTOR:
          case AUTHOR:
          case REPORTER:
          case SOURCE_AUTHOR:
            involvedActors.get(c.getKey()).add(nameToken);
            break;
          // technical
          case FROMID:
            lowest = c.getValue();
            break;
          case ORDERBY:
            if ("id".equals(c.getValue())) {
              orderBy = "contribution.id_contribution";
            }
            break;

          // actors
          case ACTOR_NAME:
            actorNames.add(nameToken);
            break;
          case ACTOR_TYPE:
            actorTypes.add(c.getValue());
            break;
          case FUNCTION:
            functions.add(token);
            break;

          // texts
          case TEXT_TITLE:
            textTitles.add(token);
            break;
          case TEXT_SOURCE:
            textSources.add(token);
            break;

          // debates
          case DEBATE_TITLE:
            debatesTitles.add(token);
            break;

          // contextualized arguments
          case ARGUMENT_CONTEXT_TITLE:
            argumentContextTitles.add(token);
            break;

          // excerpts
          case EXCERPT_TITLE:
            allResults = token.equals("*");
            originalExcerpts.add(token);
            break;

          // folders
          case FOLDER_NAME:
            folders.add(token);
            break;
          case STRICT:
            break;
          default:
            logger.warn("unsupported key given " + c.getKey() + " with value " + c.getValue());
        }
      }
    }

    // add group filter, if none or public group passed, replace with ids of all public groups
    if (groups.isEmpty() || groups.contains(String.valueOf(Group.getPublicGroup().getIdGroup()))) {
      groups.addAll(public_groups);
    }

    String gf = validatedFilter + " and contribution.id_contribution >= " + lowest + " and contribution.deleted = 0 and contribution.hidden = 0";
    if(!notInGroups.isEmpty()) gf += " and contribution.id_contribution  not in (select id_contribution from contribution_in_group where id_group in (" + String.join(",", notInGroups) + "))";

    // set group, validated and lowest contribution id filters
    simpleGroupFilter = "id_group in (" + String.join(",", groups) + ")";
    groupFilter = " and "+ simpleGroupFilter + gf;

    // set groups public filters
    simpleGroupPublicFilter = "id_group in (" + String.join(",", public_groups) + ")";
    groupPublicFilter = " and (" +  simpleGroupPublicFilter + " or " + simpleGroupFilter + ")" + gf;

    // set public webdeb group for folders
    groupMainFilter = " and id_group = " + Group.getPublicGroup().getIdGroup() + gf;

    // specific type given, add clause to contribution types
    String cClause = "";
    if (!types.isEmpty() && types.size() != 3) {
      cClause = " and contribution_type in (" + String.join(", ", types) + ")";
    }

    // specific contribution ids
    if (!contributionIds.isEmpty()) {
      selects.add(select + maxRelevance + relevance + charLength + searched + from + where +
              " id_contribution in (" + String.join(", ", contributionIds) + ")");
    }

    String clause;
    String query;

    // ACTORS
    if (!actorNames.isEmpty()) {
      clause = nameMatch + String.join(" ", actorNames) + mode;
      selects.add(select + clause + hightRelevance + relevance + charLength + searched + from +
              "left join actor on contribution.id_contribution = actor.id_contribution right join actor_i18names " +
              "on actor.id_contribution = actor_i18names.id_contribution" + groupJoin + where +
              clause + groupPublicFilter);
      // actortype
      if (!actorTypes.isEmpty()) {
        String subclause = " and actortype in (" + String.join(",", actorTypes) + ")";
        selects.set(selects.size() - 1, selects.get(selects.size() - 1) + subclause);
      }

      // actors with given name as affiliation
      selects.add("select actor_has_affiliation.id_actor, contribution_type, " + clause + normalRelevance + relevance
              + charLength + searched + from +
              "right join actor on contribution.id_contribution = actor.id_contribution right join actor_i18names " +
              "on actor.id_contribution = actor_i18names.id_contribution right join actor_has_affiliation on " +
              "actor.id_contribution = actor_has_affiliation.id_actor_as_affiliation" + groupJoin + where +
              clause + groupPublicFilter);
    }

    if (!functions.isEmpty()) {
      // actors with given function
      clause = "match(spelling) against ('" + String.join(" ", functions) + mode;
      selects.add(select + clause + normalRelevance + relevance + ", char_length(spelling) " + searched + from +
              "left join actor on contribution.id_contribution = actor.id_contribution " +
              "left join actor_has_affiliation on actor.id_contribution = actor_has_affiliation.id_actor " +
              "left join profession on actor_has_affiliation.function = profession.id_profession " +
              "left join profession_i18names on profession.id_profession = profession_i18names.profession "
              + groupJoin + "where " + clause + groupPublicFilter);

      // contributions where an affiliation has this function
      selects.add(select + clause + normalRelevance + relevance + ", char_length(spelling) " + searched + from +
              "left join contribution_has_actor on contribution.id_contribution = contribution_has_actor.id_contribution " +
              "right join actor_has_affiliation on contribution_has_actor.actor_id_aha = actor_has_affiliation.id_aha " +
              "right join profession on actor_has_affiliation.function = profession.id_profession " +
              "left join profession_i18names on profession.id_profession = profession_i18names.profession " +
              groupJoin + "where " + clause + (amongGroup ? groupPublicFilter : groupFilter));
    }

    // TEXT
    if (!textTitles.isEmpty()) {
      clause = "(match(text_i18names.spelling) against ('" + String.join(" ", textTitles) + mode + ")";
      addSearched = true;

      if (types.contains(String.valueOf(EContributionType.TEXT.id()))) {
        // search texts with title or original title with given values
        selects.add(select + clause + relevance + ", char_length(text_i18names.spelling) " + searched + from +
                " right join text on contribution.id_contribution = text.id_contribution" +
                " right join text_i18names on text.id_contribution = text_i18names.id_contribution" + groupJoin +
                "where " + clause + fetchedFilter + (amongGroup ? groupPublicFilter : groupFilter));

      }
      if (types.contains(String.valueOf(EContributionType.EXCERPT.id()))) {
        // search all excerpts from text having these titles
        selects.add(select + clause + relevance + ", char_length(text_i18names.spelling) " + searched + from +
                " right join excerpt on excerpt.id_contribution = contribution.id_contribution" +
                " left join text on excerpt.id_text = text.id_contribution" +
                " right join text_i18names on excerpt.id_text = text_i18names.id_contribution" + groupJoin +
                "where " + clause + (amongGroup ? groupPublicFilter : groupFilter));
      }
    }

    if (!textSources.isEmpty()) {
      clause = "match(text_source_name.name) against ('" + String.join(" ", textSources) + mode;
      selects.add(select + clause + relevance + ", char_length(text_source_name.name) " + searched + from +
              "left join text on contribution.id_contribution = text.id_contribution left join " +
              "text_source_name on text.source_name = text_source_name.id_source" + groupJoin + where +
              clause + (amongGroup ? groupPublicFilter : groupFilter));
      addSearched = true;
    }

    // DEBATES
    if (!debatesTitles.isEmpty()) {
      clause = "match(argument_dictionary.title) against ('" + String.join(" ", debatesTitles) + mode;
      selects.add(select + clause + relevance + ", char_length(argument_dictionary.title) " + searched + from +
              "left join debate on contribution.id_contribution = debate.id_contribution" + groupJoin +
              " left join argument_context on debate.id_first_argument = argument_context.id_contribution " +
              "left join argument on argument_context.id_argument = argument.id_contribution " +
              "left join argument_dictionary on argument.id_argument_dictionary = argument_dictionary.id_argument_dictionary " +
              "where " + clause + (amongGroup ? groupPublicFilter : groupFilter) +
              (ignoreContextIds.isEmpty() ? "" : " and argument_context.id_context not in (" + String.join(", ", ignoreContextIds) + ")"));
      //+ " group by argument.id_argument_dictionary");
      addSearched = true;
    }

    // CONTEXTUALIZED ARGUMENTS
    if (!argumentContextTitles.isEmpty()) {
      clause = "match(argument_dictionary.title) against ('" + String.join(" ", argumentContextTitles) + mode;
      selects.add(select + clause + relevance + ", char_length(argument_dictionary.title) " + searched + from +
              "left join argument_context on contribution.id_contribution = argument_context.id_contribution" + groupJoin +
              "left join argument on argument_context.id_argument = argument.id_contribution " +
              "left join argument_dictionary on argument.id_argument_dictionary = argument_dictionary.id_argument_dictionary " +
              "where " + clause + (amongGroup ? groupPublicFilter : groupFilter) +
              (ignoreContextIds.isEmpty() ? "" : " and argument_context.id_context not in (" + String.join(", ", ignoreContextIds) + ")")
              + " group by argument.id_argument_dictionary");
      addSearched = true;
    }

    // EXCERPTS
    if (!originalExcerpts.isEmpty()) {
      clause = allResults ? "" : "match(excerpt.original_excerpt) against ('" + String.join(" ", originalExcerpts) + mode;
      String ch = (clause.equals("") ? select.substring(0, select.length()-2) : select + clause + relevance + ", char_length(excerpt.original_excerpt) " + searched) + from +
              "left join excerpt on contribution.id_contribution = excerpt.id_contribution" + groupJoin +
              "left join argument_context_has_excerpt ache on contribution.id_contribution = ache.id_excerpt " +
              "where " + (allResults ? "contribution.id_contribution > 0" : clause) + (amongGroup ? groupPublicFilter : groupFilter);
      if(!ignoreContextIds.isEmpty()){
        ch += " and (ache.id_argument_context is null or ache.id_argument_context not in (" + String.join(", ", ignoreContextIds) + "))";
      }
      if(!lookTextIds.isEmpty()){
        ch += " and excerpt.id_text in (" + String.join(", ", lookTextIds) + ")";
      }
      selects.add(ch);
      addSearched = true;
    }

    // FOLDERS
    if (!folders.isEmpty()) {
      clause = "(match(folder_i18names.name) against ('" + String.join(" ", folders) + mode + ")";
      addSearched = true;

      // search folders with name that match given values
      selects.add(select + clause + relevance + ", char_length(folder_i18names.name) " + searched + from +
              " left join folder on contribution.id_contribution = folder.id_contribution " +
              " right join folder_i18names on folder.id_contribution = folder_i18names.id_contribution" + groupJoin +
              where + clause + groupMainFilter);

      clause = "(match(folder_rewording_i18names.name) against ('" + String.join(" ", folders) + mode + ")*5";
      // search folders with rewording name that match given values
      selects.add(select + clause + relevance + ", char_length(folder_rewording_i18names.name) " + searched + from +
              " left join folder on contribution.id_contribution = folder.id_contribution" +
              " right join folder_rewording_i18names on folder.id_contribution = folder_rewording_i18names.id_contribution" + groupJoin +
              where + clause + fetchedFilter + groupFilter);
    }

    // involved actors
    String involvedQuery = " " + normalRelevance + " " + relevance + charLength + searched + from +
            "left join contribution_has_actor on contribution.id_contribution = contribution_has_actor.id_contribution " +
            "right join actor on contribution_has_actor.id_actor = actor.id_contribution right join actor_i18names " +
            "on actor.id_contribution = actor_i18names.id_contribution" + groupJoin + where;

    if (!involvedActors.get(EQueryKey.ACTOR).isEmpty()) {
      // contributions where this actor appears
      clause = nameMatch + String.join(" ", involvedActors.get(EQueryKey.ACTOR)) + mode;
      selects.add(select + clause + involvedQuery + clause + cClause + (amongGroup ? groupPublicFilter : groupFilter));
    }

    if (!involvedActors.get(EQueryKey.AUTHOR).isEmpty()) {
      // contributions where this actor appears as authors
      clause = nameMatch + String.join(" ", involvedActors.get(EQueryKey.AUTHOR)) + mode;
      selects.add(select + clause + involvedQuery + clause + "and is_author = 1" + cClause + (amongGroup ? groupPublicFilter : groupFilter));
    }

    if (!involvedActors.get(EQueryKey.REPORTER).isEmpty()) {
      // contributions where this actor appears as reporters
      clause = nameMatch + String.join(" ", involvedActors.get(EQueryKey.REPORTER)) + mode;
      selects.add(select + clause + involvedQuery + clause + "and is_speaker = 1" + cClause + (amongGroup ? groupPublicFilter : groupFilter));
    }

    if (!involvedActors.get(EQueryKey.SOURCE_AUTHOR).isEmpty()) {
      // contributions where this actor appears as source authors
      clause = nameMatch + String.join(" ", involvedActors.get(EQueryKey.SOURCE_AUTHOR)) + mode;
      selects.add(select + clause + involvedQuery + clause + "and is_source_author = 1" + cClause + (amongGroup ? groupPublicFilter : groupFilter));
    }

    // check if some specific criteria have been passed, otherwise, maybe only contribution types have been requested
    // so no specific selects have been built
    //String query;
    if (selects.isEmpty()) {
      StringBuilder join = new StringBuilder();
      join.append(contributor != null ?
              "inner join contribution_has_contributor on contribution.id_contribution = " +
                      "contribution_has_contributor.id_contribution" + groupJoin + where + "id_contributor = " + contributor
              : groupJoin);

      // use group filter var, but we need to remove the leading " and "
      where += groupFilter.substring(4);
      for (String t : types) {
        switch (t) {
          case "0":
            selects.add(select_contribution_sortkey + from + join + where + " and contribution_type = 0 and contribution.deleted = 0");
            break;

          case "1":
            selects.add(select_contribution_sortkey + from + join + where + " and contribution_type = 1 and contribution.deleted = 0");
            break;

          case "2":
            selects.add(select_contribution_sortkey + from + join + where + " and contribution_type = 2 and contribution.deleted = 0");
            break;

          case "3":
            selects.add(select_contribution_sortkey + from + join + "left join argument_context ac on ac.id_contribution = contribution.id_contribution" +
                    " left join argument on argument.id_contribution = ac.id_argument " + where + " and contribution_type = 3 and contribution.deleted = 0 group by argument.id_argument_dictionary");
            break;

          case "4":
            selects.add(select_contribution_sortkey + from + join + where + " and contribution_type = 4 and contribution.deleted = 0");
            break;

          case "5":
            if (!"".equals(fetchedFilter)) {
              join.append(" left join text on text.id_contribution = contribution.id_contribution ");
            }
            selects.add(select_contribution_sortkey + from + join + where + fetchedFilter
                    + " and contribution_type = 5 and contribution.deleted = 0");
            break;

          case "6":
            if (!"".equals(fetchedFilter)) {
              join.append(" left join folder on folder.id_contribution = contribution.id_contribution ");
            }
            selects.add(select_contribution_sortkey + from + join + where + fetchedFilter
                    + " and contribution_type = 6 and contribution.deleted = 0");
            break;

          default:
            // ignore
        }
      }
      // now finalize query and execute it
      query = String.join(" union ", selects) + " order by " + orderBy + " asc";
    } else {
      // force mysql to treat relevance as a numeric value by multiplying it by 1 (don't ask me why I must force casting)
      query = String.join(" union ", selects) + " order by " + (allResults ? "" : "relevance desc,") + "ctype asc";
      if (addSearched && !allResults) {
        query += ", searched asc";
      }
    }
    query += getSearchLimit(fromIndex, toIndex, 100);

    // execute query
    logger.debug("will now execute query " + query);
    RawSql rawSql = RawSqlBuilder.parse(query).create();
    contributions = Ebean.find(Contribution.class).setRawSql(rawSql).findSet();

    if(contributions != null){
      ignoreIds.forEach(id -> contributions.removeIf(e -> e.getIdContribution() == Long.parseLong(id)));
      return new ArrayList<>(contributions);
    }
    return new ArrayList<>();
  }

  /**
   * Get latest entries (based on version time) for actors, texts and/or arguments
   *
   * @param type a contribution type (may be -1 for actors, texts and arguments)
   * @param contributor a given contributor id (-1 to ignore)
   * @param amount the amount of contributions to retrieve (only strictly positive value are considered)
   * @param group the group id where to look for contributions (if public group is passed, any public contribution is returned, -1 to ignore)
   * @return a (possibly empty) list of contribution according to the given parameters
   */
  public static List<Contribution> findLatestEntries(int type, Long contributor, int amount, int group) {
    return getEntries(type == -1 ? Arrays.asList("0", "1", "3", "4", "5") : Collections.singletonList(String.valueOf(type)),
        contributor, 0, amount, ORDER_BY_LATEST, group);
  }

  /**
   * Get latest entries (based on version time) for actors, texts and/or arguments
   *
   * @param type a contribution type (may be -1 for actors, texts and arguments)
   * @param contributor a given contributor id (-1 to ignore)
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param group the group id where to look for contributions (if public group is passed, any public contribution is returned, -1 to ignore)
   * @return a (possibly empty) list of contribution according to the given parameters
   */
  public static List<Contribution> findLatestEntries(int type, Long contributor, int fromIndex, int toIndex, int group) {
    return getEntries(type == -1 ? Arrays.asList("0", "1", "3", "4", "5") : Collections.singletonList(String.valueOf(type)),
        contributor, fromIndex, toIndex, ORDER_BY_LATEST, group);
  }

  /**
   * Get latest external entries (based on version time) for texts and arguments
   *
   * @param contributor a given contributor id (-1 to ignore)
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param externalSource the source where contributions come from
   * @return a (possibly empty) list of contribution according to the given parameters
   */
  public static List<ExternalContribution> findLatestExternalEntries(Long contributor, int fromIndex, int toIndex, int externalSource) {
    List<Contribution> contributions = getEntries(Arrays.asList("13", "14"), contributor, fromIndex, toIndex, ORDER_BY_LATEST, 0, true);
    if (contributions != null) {
      return contributions.stream()
              .filter(e -> e.getExternalContribution() != null && externalSource == e.getExternalContribution().getExternalSource().getIdSource())
              .map(Contribution::getExternalContribution)
              .collect(Collectors.toList());
    }
    return new ArrayList<>();
  }

  /**
   * Get the most viewed contributions of given type, contributor
   *
   * @param type a contribution type (may be -1 for actors, texts and arguments)
   * @param contributor a given contributor id (-1 to ignore)
   * @param amount the amount of contributions to retrieve (only strictly positive value are considered)
   * @param group the group id where to look for contributions (if public group is passed, any public contribution is returned)
   * @return a (possibly empty) list of contributions according to given parameters
   */
  public static List<Contribution> findPopularEntries(int type, Long contributor, int amount, int group) {
    return getEntries(type == -1 ? Arrays.asList("0", "1", "2", "4", "5") : Collections.singletonList(String.valueOf(type)),
        contributor, 0, amount, "hit", group);
  }

  /**
   * Get a list of contribution according to given parameters
   *
   * @param type a list of contribution types to retrieve
   * @param contributor a given contributor id (-1 to ignore)
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param orderBy the property name to order the list on (either hit or latest for version)
   * @param group the group id where to look for contributions. If default public group is passed, any public
   *    contribution is returned. If -1 is passed, does not filter on group.
   * @return a (possibly empty) list of contributions according to given parameters
   */
  private static List<Contribution> getEntries(List<String> type, Long contributor, int fromIndex, int toIndex, String orderBy, int group) {
    return getEntries(type, contributor, fromIndex, toIndex, orderBy, group, false);
  }
  /**
   * Get a list of contribution according to given parameters
   *
   * @param type a list of contribution types to retrieve
   * @param contributor a given contributor id (-1 to ignore)
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param orderBy the property name to order the list on (either hit or latest for version)
   * @param hidden check if the contribution is hide or not
   * @param group the group id where to look for contributions. If default public group is passed, any public
   *    contribution is returned. If -1 is passed, does not filter on group.
   * @return a (possibly empty) list of contributions according to given parameters
   */
  private static List<Contribution> getEntries(List<String> type, Long contributor, int fromIndex, int toIndex, String orderBy, int group, boolean hidden) {
    String select = "select contribution.id_contribution " +
        (ORDER_BY_LATEST.equals(orderBy) || contributor != -1L ?
            ", max(contribution_has_contributor.version) as latest from contribution left join contribution_has_contributor " +
                "on contribution_has_contributor.id_contribution = contribution.id_contribution "
            : "from contribution ") +
        (group != -1 ?
            "left join contribution_in_group on contribution_in_group.id_contribution = contribution.id_contribution "
            : " ") +
        "where contribution_type in ("  + String.join(",", type) +  ") " +
        (contributor != -1L ?
            "and id_contributor = " + contributor + " ": " ") +
        (group != -1
            ? "and contribution_in_group.id_group in (" + (Group.getPublicGroup().getIdGroup() == group ?  String.join(",",Group.getVisibleGroupsFor(group)) : group) + ") "
            : " ") +
        "and contribution.deleted = 0 and contribution.hidden = " + (hidden ? "1" : "0") +
        " group by id_contribution order by " + orderBy + " desc" + getSearchLimit(fromIndex, toIndex);

    logger.debug("will execute query: " + select);
    List<Contribution> result = Ebean.find(Contribution.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
    return result != null ? result : new ArrayList<>();
  }

  /**
   * Get the amount of contribution by type (0 for actors, 1 for texts, 2 for arguments, 3 for links, -1 for all)
   *
   * @param type a contribution type id (-1 to ignore)
   * @param group the group id where to look for contributions (if public group is passed, any public contribution is returned) (-1 to ignore)
   * @return the amount of contributions in database, depending on given type
   */
  public static int getAmountOf(int type, int group) {
    return getAmountOf(null, type, group);
  }

  /**
   * Get the amount of contribution by type (0 for actors, 1 for texts, 2 for arguments, 3 for links, -1 for all)
   * in given group for given contributor
   *
   * @param contributor a contributor id (null to ignore)
   * @param type a contribution type id (-1 to ignore)
   * @param group the group id where to look for contributions (if public group is passed, any public contribution is returned) (-1 to ignore)
   * @return the amount of contributions in database, depending on given type, contributor and group
   */
  public static int getAmountOf(Long contributor, int type, int group) {
    List<String> groups = Group.getVisibleGroupsFor(group);
    if (type == -1) {
      return find.findRowCount();
    } else {
      // should use proper ebean dialect API..., but join sucks...
      String select = "select count(contribution.id_contribution) as 'count' from contribution " +
          (group != -1 ? "left join contribution_in_group on contribution.id_contribution = contribution_in_group.id_contribution " : "") +
          (contributor != null ? "left join contribution_has_contributor on " +
              "contribution.id_contribution = contribution_has_contributor.id_contribution " : "") +
          "where contribution_type = " + type + " and contribution.deleted = 0 and contribution.hidden = 0" +
          (group != -1 ? " and id_group in (" + String.join(",", groups) + ") " : "") +
          (contributor != null ? " and id_contributor = " + contributor : "");
      return Ebean.createSqlQuery(select).findUnique().getInteger("count");
    }
  }

  /**
   * Get the creator of a contribution
   *
   * @param id a contribution id
   * @return the contributor that created given contribution if the contribution exists, null otherwise
   */
  public static Contributor getCreator(Long id) {
    Contribution contribution = findById(id);
    if (contribution != null) {
      for (ContributionHasContributor chc : contribution.getContributionHasContributors()) {
        if (chc.getStatus().getIdStatus() == EModificationStatus.CREATE.id()) {
          return chc.getContributor();
        }
      }
    }
    return null;
  }

  /**
   * Get the creator of a contribution
   *
   * @param id a contribution id
   * @return the contributor that created given contribution if the contribution exists, null otherwise
   */
  public static Contributor getLastContributorInGroup(Long id, int group) {
    Contribution contribution = findById(id);
    Group g = Group.findById(group);
    if (contribution != null && g != null) {
      List<ContributionHasContributor> contributors = contribution.getContributionHasContributors();
      contributors.sort((o1, o2) -> o1.getVersion().after(o2.getVersion()) ? 1 : -1);
      for (ContributionHasContributor chc : contribution.getContributionHasContributors()) {
        if (chc.getContributor().getContributorHasGroups().stream().anyMatch(e -> e.getGroup().getIdGroup() == group)) {
          return chc.getContributor();
        }
      }
    }
    return getCreator(id);
  }

  /**
   * Execute the given sql query to perform it into the DB et get the results as a list of list of values. The first
   * list is the keys of sql columns name.
   *
   * @param query the sql query to execute
   * @return the result list, possibly empty
   */
  public static List<List<String>> executeApiQuery(String query){

    List<List<String>> response = new ArrayList<>();

    if(query != null && query.toLowerCase().startsWith("select") && !query.contains(";")){
      List<SqlRow> results = Ebean.createSqlQuery(query).findList();
      if(!results.isEmpty()) {
        // Collect keys
        response.add(new ArrayList<>(results.get(0).keySet()));
        // Collect values
        results.forEach(e ->
                response.add(e.values().stream().map(Object::toString).collect(Collectors.toList())));
      }
    }

    return response;
  }

  /**
   * Get all classes of the model description, with description of theirs attributes and relations with others classes.
   *
   * @return the list of classes of the model
   */
  public static List<ModelDescription> getModelDescriptions(){
    List<ModelDescription> descriptions = new ArrayList<>();

    Reflections reflections = new Reflections("be.webdeb.infra.persistence.model");

    Set<Class<? extends Model>> allClasses = reflections.getSubTypesOf(Model.class);
    for(Class<?> model : allClasses){
      if (model.getAnnotation(Unqueryable.class) == null) {
        descriptions.add(getModelAttributesDescription(model));
      }
    }

    return descriptions;
  }

  /**
   * Get all contribution id for a given contribution type
   *
   * @param type the contribution type to focus
   * @return a possibly empty list of contribution id for the given type
   */
  public static List<Long> getAllIdByContributionType(EContributionType type){
    List<Long> result = new ArrayList<>();
    if(type != null) {
      String select = "SELECT distinct c.id_contribution as 'id' FROM webdeb.contribution c " +
              "inner join contribution_in_group cig on cig.id_contribution = c.id_contribution " +
              "inner join contributor_group g on g.id_group = cig.id_group " +
              "where contribution_type = " + type.id() + " and hidden = 0 and deleted = 0 and g.contribution_visibility = 0";

      Ebean.createSqlQuery(select).findEach(e ->
              result.add(e.getLong("id")));
    }
    return result;
  }

  /**
   * Get all attributes of a class of the model.
   *
   * @return the list of attributes of a class of the model
   */
  private static ModelDescription getModelAttributesDescription(Class<?> model){
    ModelDescription description = new ConcreteModelDescription(model.getSimpleName());

    for (Field field : model.getDeclaredFields()) {
      if (field.getAnnotation(Unqueryable.class) == null) {
        Column column = field.getAnnotation(Column.class);
        if (column != null) {
          description.getAttributesMap().add(
                  new ConcreteModelAttributeDescription(field.getName(), column.name(), EDBRelationType.SIMPLE_FIELD));
        }

        List<Annotation> annotations = new ArrayList<>();
        annotations.add(field.getAnnotation(OneToOne.class));
        annotations.add(field.getAnnotation(ManyToOne.class));
        annotations.add(field.getAnnotation(OneToMany.class));
        annotations.add(field.getAnnotation(ManyToMany.class));
        Annotation annotation = getAnnotationInList(annotations);

        if (annotation != null) {
          Class<?> classToQuery;
          if (Collection.class.isAssignableFrom(field.getType())) {
            classToQuery = ((Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0]);
          } else {
            classToQuery = field.getType();
          }

          JoinColumn joinColumn = field.getAnnotation(JoinColumn.class);
          String columnName = null;

          if (joinColumn != null) {
            columnName = joinColumn.name();
          } else {
            String mappedBy = null;
            if (annotation instanceof OneToOne) {
              mappedBy = ((OneToOne) annotation).mappedBy();
            } else if (annotation instanceof OneToMany) {
              mappedBy = ((OneToMany) annotation).mappedBy();
            }
            try {
              if (mappedBy != null) {
                Field f = classToQuery.getDeclaredField(mappedBy);
                joinColumn = f.getAnnotation(JoinColumn.class);
                if (joinColumn != null) {
                  columnName = joinColumn.name();
                }
              }
            } catch (NoSuchFieldException e) {
              logger.debug("The relation field is not in the related class. Must not append if model is well define.");
            }
          }

          if (columnName != null) {
            description.getAttributesMap().add(
                    new ConcreteModelAttributeDescription(classToQuery.getSimpleName(), columnName, EDBRelationType.annotationToType(annotation)));
          }
        }
      }
    }

    return description;
  }

  /**
   * Get the annotation in given list that said the nature of the relation between classes.
   *
   * @return an annotation
   */
  private static Annotation getAnnotationInList(List<Annotation> annotations){
    for(Annotation a : annotations){
      if(a != null)
        return a;
    }
    return null;
  }

  /**
   * Get the number of folders linked to this contribution
   *
   * @return the number of folders linked to this contribution
   */
  private int getNbFolders(){
    return Ebean.createSqlQuery("SELECT count(*) as 'count' FROM contribution_has_folder where id_contribution = " + idContribution).findUnique().getInteger("count");
  }
}
