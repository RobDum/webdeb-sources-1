/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.infra.persistence.model.annotation.Unqueryable;
import com.avaje.ebean.*;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;

/**
 * The persistent class for the context_element spellings database table. Holds string element to add description on
 * element like citation.
 *
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "context_element_i18names")
@Unqueryable
public class ContextElementI18name extends Model {

    @EmbeddedId
    private ContextElementPK id;

    @Column(name = "spelling")
    private String spelling;

    @ManyToOne
    @JoinColumn(name = "id_context_element", nullable = false)
    private ContextElement element;

    /**
     * Create a new spelling for given context element
     *
     * @param id a context element id
     * @param lang a 2-char ISO 639 language code
     * @param spelling a context element spelling (language dependent)
     */
    public ContextElementI18name(long id, String lang, String spelling) {
        this.id = new ContextElementI18name.ContextElementPK(id, lang);
        this.spelling = spelling;
    }

    /**
     * Get the complex id for this context element spelling
     *
     * @return a complex id (concatenation of idElement and lang)
     */
    public ContextElementPK getId() {
        return id;
    }

    /**
     * Set the complex id for this context element spelling
     *
     * @param id a complex id (concatenation of idElement and lang)
     */
    public void setId(ContextElementPK id) {
        this.id = id;
    }

    /**
     * Get the current language code for this name
     *
     * @return a two-char iso-639-1 language code
     */
    public String getLang() {
        return id.getLang();
    }

    /**
     * Set the current language code for this name
     *
     * @param lang a two-char iso-639-1 language code
     */
    public void setLang(String lang) {
        id.setLang(lang);
    }

    /**
     * Get the context element spelling
     *
     * @return a context element spelling in associated lang
     */
    public String getSpelling() {
        return spelling;
    }

    /**
     * Set the context element spelling
     *
     * @param spelling a context element spelling
     */
    public void setSpelling(String spelling) {
        this.spelling = spelling;
    }

    /**
     * Get the element associated to this spelling
     *
     * @return a context element
     */
    public ContextElement getContextElement() {
        return element;
    }

    /**
     * Set the element associated to this spelling
     *
     * @param element a context element
     */
    public void setContextElement(ContextElement element) {
        this.element = element;
    }

    @Override
    public String toString() {
        return getId() + " " + getSpelling();
    }

    /**
     * The primary key class for the place multiple name database table.
     *
     * @author Martin Rouffiange
     */
    @Embeddable
    public static class ContextElementPK extends Model {

        @Column(name = "id_place", insertable = false, updatable = false, unique = true, nullable = false)
        private Long idElement;

        @Column(name = "lang", insertable = false, updatable = false, unique = true, nullable = false)
        private String lang;

        /**
         * Default no-arg constructor
         */
        public ContextElementPK() {
            // needed by ebean
        }

        /**
         * Create a complex key for given context element and language
         *
         * @param id a context element id
         * @param lang a 2-char iso 639 language code
         */
        public ContextElementPK(long id, String lang) {
            idElement = id;
            this.lang = lang;
        }

        /**
         * Get the context element id
         *
         * @return an id
         */
        public Long getIdElement() {
            return idElement;
        }

        /**
         * Set the place id
         *
         * @param idElement an id of a context element
         */
        public void setIdElement(Long idElement) {
            this.idElement = idElement;
        }

        /**
         * Get the language associated to this name (spelling)
         *
         * @return a two-char iso-639-1 language code
         */
        public String getLang() {
            return this.lang;
        }

        /**
         * Set the language associated to this name (spelling)
         *
         * @param lang a two-char iso-639-1 language code
         */
        public void setLang(String lang) {
            this.lang = lang;
        }

        @Override
        public boolean equals(Object other) {
            if (this == other) {
                return true;
            }
            if (!(other instanceof ContextElementPK)) {
                return false;
            }
            ContextElementPK castOther = (ContextElementPK) other;
            return this.idElement.equals(castOther.idElement)
                    && this.lang.equals(castOther.lang);
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int hash = 17;
            hash = hash * prime + this.idElement.hashCode();
            return hash * prime + this.lang.hashCode();
        }

        @Override
        public String toString() {
            return "[" + idElement + ":" + lang + "]";
        }
    }
}
