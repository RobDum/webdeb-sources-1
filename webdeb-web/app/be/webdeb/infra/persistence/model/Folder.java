/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.folder.EFolderType;
import be.webdeb.presentation.web.controllers.entry.folder.FolderNameForm;
import com.avaje.ebean.*;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The persistent class for the folder database table, conceptual subtype of contribution.
 * Concrete subtype is specified via the foldertype
 *
 * @author Martin Rouffiange
 * @see be.webdeb.core.api.folder.EFolderType
 */
@Entity
@CacheBeanTuning
@Table(name = "folder")
public class Folder extends WebdebModel {

  private static final org.slf4j.Logger logger = play.Logger.underlying();
  private static final Model.Finder<Long, Folder> find = new Model.Finder<>(Folder.class);

  @Id
  @Column(name = "id_contribution", unique = true, nullable = false)
  private Long idContribution;

  // forcing updates from this object, deletions are handled at the contribution level
  @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
  @JoinColumn(name = "id_contribution", nullable = false, insertable = false, updatable = false)
  private Contribution contribution;

  @OneToMany(mappedBy = "folder", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  private List<FolderI18name> names;

  @OneToMany(mappedBy = "folder", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  private List<FolderRewordingI18name> rewordingNames;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "foldertype", nullable = false)
  private TFolderType foldertype;

  // all parent folders of this folder
  @OneToMany(mappedBy = "folderChild", cascade = CascadeType.PERSIST)
  private List<FolderLink> parentFolders;

  // all child folders of this folder
  @OneToMany(mappedBy = "folderParent", cascade = CascadeType.PERSIST)
  private List<FolderLink> childFolders;

  // all parent folders of this folder as Folder
  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinTable(
      name="folder_link",
      joinColumns = { @JoinColumn(name="id_folder_to", referencedColumnName = "id_contribution") },
      inverseJoinColumns = { @JoinColumn(name="id_folder_from", referencedColumnName = "id_contribution") }
  )
  private List<Folder> parents;

  // all child folders of this folder as Folder
  @ManyToMany(mappedBy = "parents", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  private List<Folder> children;

  // all contributions in this folder
  @ManyToMany(mappedBy = "folders", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  private List<Contribution> contributions;

  /**
   * Get the actor id
   *
   * @return an id
   */
  public Long getIdContribution() {
    return this.idContribution;
  }

  /**
   * Set the actor id
   *
   * @param idContribution an id
   */
  public void setIdContribution(Long idContribution) {
    this.idContribution = idContribution;
  }

  /**
   * Get the list of names for this folder (names may have multiple spellings)
   *
   * @return a list of names
   */
  public List<FolderI18name> getNames() {
    return names;
  }

  /**
   * Get the name of the folder (prior in english, first one otherwise)
   *
   * @return a folder name
   */
  public String getDefaultName() {
    String name = "";
    if(names != null && !names.isEmpty()){
      Optional<FolderI18name> nameFound = names.stream().filter(n -> "en".equals(n.getLang())).findFirst();
      if(nameFound.isPresent()){
        name = nameFound.get().getName();
      }else{
        name = names.stream().findFirst().get().getName();
      }
    }

    return name;
  }

  /**
   * Set the list of names for this folder (names may have multiple spellings)
   *
   * @param names a list of names to set
   */
  public void setNames(List<FolderI18name> names) {
    if (names != null) {
      if (this.names == null) {
        this.names = new ArrayList<>();
      }
      // get previous languages for current names
      List<String> currentlangs = this.names.stream().map(FolderI18name::getLang).collect(Collectors.toList());

      // add/update new names
      names.forEach(this::addName);

      currentlangs.stream().filter(lang -> names.stream().noneMatch(n -> n.getLang().equals(lang))).forEach(lang ->
          this.names.removeIf(current -> current.getLang().equals(lang))
      );
    }
  }

  /**
   * Add a name to this folder, if such language already exists, will update existing name
   *
   * @param name a name structure
   */
  public void addName(FolderI18name name) {
    if (names == null) {
      names = new ArrayList<>();
    }
    Optional<FolderI18name> match = names.stream().filter(n ->
        n.getLang().equals(name.getLang())).findAny();

    if (!match.isPresent()) {
      names.add(name);
    }else{
      int i = names.lastIndexOf(match.get());
      if(i >= 0){
        names.get(i).setName(name.getName());
      }
    }
  }

  /**
   * Get the list of rewording names for this folder (names may have multiple language and rewording)
   *
   * @return a list of names
   */
  public List<FolderRewordingI18name> getRewordingNames() {
    return rewordingNames;
  }

  /**
   * Set the list of names for this folder (names may have multiple language and rewording)
   *
   * @param names a list of names to set
   */
  public void setRewordingNames(List<FolderRewordingI18name> names) {
    if (names != null) {
      if (this.rewordingNames == null) {
        this.rewordingNames = new ArrayList<>();
      }
      // get previous languages for current names
      List<FolderNameForm> currentNames = this.rewordingNames.stream().map(n -> new FolderNameForm(n.getLang(), n.getName())).collect(Collectors.toList());

      // add/update new names
      names.forEach(this::addRewordingName);

      currentNames.stream().filter(n -> names.stream().noneMatch(n2 -> n2.getLang().equals(n.getLang()) && n2.getName().equals(n.getName()))).forEach(n ->
          this.rewordingNames.removeIf(current -> current.getLang().equals(n.getLang()) && current.getName().equals(n.getName()))
      );
    }
  }

  /**
   * Add a rewording name to this folder.
   *
   * @param name a name structure
   */
  public void addRewordingName(FolderRewordingI18name name) {
    if (rewordingNames == null) {
      rewordingNames = new ArrayList<>();
    }
    Optional<FolderRewordingI18name> match = rewordingNames.stream().filter(n ->
        n.getLang().equals(name.getLang()) && n.getName().equals(name.getName())).findAny();
    if (!match.isPresent()) {
      rewordingNames.add(name);
    }
  }

  /**
   * Get this foldertype
   *
   * @return the foldertype
   */
  public TFolderType getFoldertype() {
    return foldertype;
  }

  /**
   * Set the foldertype
   *
   * @param foldertype the afoldertype to set
   */
  public void setFolderType(TFolderType foldertype) {
    this.foldertype = foldertype;
  }

  /**
   * Get the contribution parent object
   *
   * @return the contribution "supertype" object
   */
  public Contribution getContribution() {
    return contribution;
  }

  /**
   * Set the contribution parent object
   *
   * @param contribution the contribution "supertype" object
   */
  public void setContribution(Contribution contribution) {
    this.contribution = contribution;
  }

  /**
   * Get all parent folders
   *
   * @return a possibily empty list of folder links
   */
  public List<FolderLink> getParentsAsLinks() {
    return parentFolders;
  }

  /**
   * Get all parent folders as Folder
   *
   * @return a possibily empty list of folders
   */
  public List<Folder> getParentsAsFolders() {
    return parents;
  }

  /**
   * Initialize children folders
   *
   */
  public void initParentFolders() {
    this.parents = new ArrayList<>();
  }

  /**
   * Get all parent folders
   *
   * @param parentFolders a list of folder links
   */
  public void setParentFolders(List<FolderLink> parentFolders) {
    this.parentFolders = parentFolders;
  }

  /**
   * Get all parent folders
   *
   * @return a possibily empty list of folder links
   */
  public List<FolderLink> getChildrenAsLinks() {
    return childFolders;
  }

  /**
   * Get all parent folders as Folder
   *
   * @return a possibily empty list of folders
   */
  public List<Folder> getChildrenAsFolders() {
    return children;
  }


  /**
   * Initialize children folders
   *
   */
  public void initChildFolders() {
    if(this.childFolders == null){
      this.childFolders = new ArrayList<>();
    }
    this.childFolders = childFolders.stream()
            .filter(e -> e.getFolderChild().getFoldertype().getEContributionType() == EFolderType.COMPOSED).collect(Collectors.toList());
  }

  /**
   * Get all child folders
   *
   * @param childFolders a list of folder links
   */
  public void setChildFolders(List<FolderLink> childFolders) {
    this.childFolders = childFolders;
  }

  /**
   * Add a parent folder to this one.
   * As other fields, additions are persisted when calling save().
   *
   * @param link a parent folder to add
   */
  void addParent(FolderLink link){
    if(link != null){
      if(parentFolders == null){
        parentFolders = new ArrayList<>();
      }
      parentFolders.add(link);
    }
  }

  /**
   * Remove given folder from this one. If the folder is unfound, this one is unchanged.
   * As other fields, removals are persisted when calling save().
   *
   * @param folder a parent folder id to remove from this Folder
   */
  void removeParent(Long folder){
    FolderLink link = FolderLink.findByParentChild(folder, idContribution);
    if(link != null){
      link.delete();
    }
  }

  /**
   * Add a child folder to this one.
   * As other fields, additions are persisted when calling save().
   *
   * @param link a child folder to add
   */
  void addChild(FolderLink link){
    if(link != null){
      if(childFolders == null){
        childFolders = new ArrayList<>();
      }
      childFolders.add(link);
    }
  }

  /**
   * Remove given folder from this one. If the folder is unfound, this one is unchanged.
   * As other fields, removals are persisted when calling save().
   *
   * @param folder a child folder id to remove from this Folder
   */
  void removeChild(Long  folder){
    FolderLink link = FolderLink.findByParentChild(idContribution, folder);
    if(link != null){
      link.delete();
    }
  }

  /**
   * Get the contributions that are contained in this folder
   *
   * @return a possibly empty list of contributions
   */
  public List<Contribution> getContributions() {
    return contributions != null ? contributions : new ArrayList<>();
  }

  /**
   * Set the contributions that are contained in this folder
   *
   * @param contributions a possibly empty list of contributions
   */
  public void setContributions(List<Contribution> contributions) {
    this.contributions = contributions;
  }

  /*
   * CONVENIENCE METHODS
   */

  /**
   * Get the current version of this folder
   *
   * @return a timestamp with the latest update moment of this folder
   */
  public Timestamp getVersion() {
    return getContribution().getVersion();
  }

  @Override
  public String toString() {
    // must use getters and explicitly loop into references, otherwise ebean may send back deferred beanlist
    // (lazy load not triggered from toString methods)
    String name = (names != null && !names.isEmpty() ? names.get(0).getName() : "");
    return new StringBuffer("folder [").append(idContribution)
        .append("] named ").append(name)
        .append(", nb parents: ").append((parentFolders != null ? parentFolders.size() : "none"))
        .append(", nb children: ").append((childFolders != null ? childFolders.size() : "none"))
        .append(" [version:").append(getContribution().getVersion()).append("]").toString();
  }

  /*
   * QUERIES
   */

  /**
   * Retrieve a folder by its id
   *
   * @param id an id
   * @return the Folder corresponding to the given id, or null if not found
   */
  public static Folder findById(Long id) {
    return id == null || id == -1L ? null : find.byId(id);
  }

  /**
   * Retrieve a list of folder of the given type with their name containing the given name
   *
   * @param name folder name
   * @param lang the name lang
   * @param type folder type (int representation, pass -1 if not relevant)
   * @return a list of matches (may be empty)
   */
  public static List<Folder > findByPartialName(String name, String lang, int type) {
    if (name == null) {
      return new ArrayList<>();
    }

    // will be automatically ordered by relevance
    String token = /* "\"" + */ name.trim()
        // protect single quotes
        .replace("'", "\\'")
        // add '%' for spaces
        .replace(" ", "%");

    String select = "select distinct folder.id_contribution from folder right join folder_i18names " +
        "on folder.id_contribution = folder_i18names.id_contribution " +
        //"right join folder_rewording_i18names on folder.id_contribution = folder_rewording_i18names.id_contribution " +
        "where (folder_i18names.name like '%" + token + "%')" + // or folder_rewording_i18names.name like '%" + token + "%' )" +
        (lang == null ? "" : " and folder_i18names.lang = '" + lang + "'")+
        (type == -1 ? "" : " and foldertype = " + type);

    logger.debug("search for folder: " + select);
    List<Folder> result = Ebean.find(Folder.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
    return result != null ? result : new ArrayList<>();
  }

  /**
   * Get a randomly chosen folder from the database
   *
   * @return a random Folder
   */
  public static Folder random() {
    int type = EContributionType.FOLDER.id();
    String sql = "select contribution.id_contribution from contribution " +
        "where contribution_type = " + type + " order by rand() limit 1";
    return findById(Ebean.createSqlQuery(sql).findUnique().getLong("id_contribution"));
  }

  /**
   * Find a folder by its complete name and the name lang
   *
   * @param name the name to find
   * @param lang a two-char iso-639-1 language code
   * @return a the matched Folder, null otherwise
   */
  public static Folder findByCompleteNameAndLang(String name, String lang) {
    List<Folder> folders =  find.where().eq("name", name).eq("lang", lang).findList();
    return (!folders.isEmpty() ? folders.get(0) : null);
  }

  /**
   * Get the number of contributions contained in this folder
   *
   * @param folder a folder id
   * @param contributor a contributor id
   * @param group a group id
   * @return the number of contributions contained in this folder
   */
  public static int getNbContributions(Long folder, Long contributor, int group){
    String sql = "SELECT count(distinct chf.id_contribution) as 'count' FROM contribution_has_folder chf " +
            getContributionStatsJoins("chf.id_contribution", contributor) +
            " where chf.folder = " + folder +
            getContributionStatsWhereClause(contributor, group);
    return Ebean.createSqlQuery(sql).findUnique().getInteger("count");
  }

  /**
   * Find a composite folder by its parents id
   *
   * @param parents a list of parent ids
   * @return a the matched Folder, null otherwise
   */
  public static Folder findExistingCompositeFolderFromParentId(List<Long> parents){
    StringBuffer select =  new StringBuffer("select distinct folder.id_contribution from folder ");

    for(Long parent : parents){
      select.append("right join folder_link l" + parents.indexOf(parent) + " on folder.id_contribution = l" + parents.indexOf(parent) + ".id_folder_to ");
    }

    select.append("where folder.foldertype = 2 and ");

    for(Long parent : parents){
      select.append("l" + parents.indexOf(parent) + ".id_folder_from = " + parent);
      if(parents.indexOf(parent) < parents.size()-1){
        select.append(" and ");
      }
    }

    logger.debug("search for composite folder: " + select);
    List<Folder> result = Ebean.find(Folder.class).setRawSql(RawSqlBuilder.parse(select.toString()).create()).findList();
    if(result == null || result.isEmpty()) return null;
    Optional<Folder> o = result.stream().filter(e -> e.getParentsAsFolders().size() == parents.size()).findAny();
    return o.isPresent() ? o.get() : null;
  }

  /**
   * Get all contextualized arguments linked with this folder
   *
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a possibly empty list of contextualized arguments
   */
  public List<ArgumentContext> getLinkedContextualizedArguments(int fromIndex, int toIndex) {
    String select = "select distinct c.id_contribution from argument_context ac " +
            "left join contribution c on c.id_contribution = ac.id_contribution " +
            "inner join contribution_has_folder chf on c.id_contribution = chf.id_contribution " +
            "inner join argument a on a.id_contribution = ac.id_argument " +
            "where " + whereContributionOnlyLinkedWithFolder() +
            " group by a.id_argument_dictionary"
            + getOrderByContributionDate() + getSearchLimit(fromIndex, toIndex);
    return Ebean.find(ArgumentContext.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
  }

  /**
   * Get all excerpts linked with this folder
   *
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a possibly empty list of excerpts
   */
  public List<Excerpt> getLinkedExcerpts(int fromIndex, int toIndex) {
    String select = "select distinct c.id_contribution from excerpt e " +
            "left join contribution c on c.id_contribution = e.id_contribution " +
            "inner join contribution_has_folder chf on c.id_contribution = chf.id_contribution where "
            + whereContributionOnlyLinkedWithFolder()
            + getOrderByContributionDate() + getSearchLimit(fromIndex, toIndex);
    return Ebean.find(Excerpt.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
  }

  /**
   * Get all debates linked with this folder
   *
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a possibly empty list of debates
   */
  public List<Debate> getLinkedDebates(int fromIndex, int toIndex) {
    String select = "select distinct c.id_contribution from debate d " +
            "left join contribution c on c.id_contribution = d.id_contribution " +
            "inner join contribution_has_folder chf on d.id_first_argument = chf.id_contribution where "
            + whereContributionOnlyLinkedWithFolder(null, "d.id_first_argument")
            + getOrderByContributionDate() + getSearchLimit(fromIndex, toIndex);
    return Ebean.find(Debate.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
  }

  /**
   * Get all linked texts with this folder
   *
   * @return a possibly empty list of texts
   */
  public List<Text> getLinkedTexts(int fromIndex, int toIndex) {
    String select = "select distinct c.id_contribution from text t " +
            "left join contribution c on c.id_contribution = t.id_contribution " +
            "left join excerpt e on e.id_text = c.id_contribution " +
            "left join contribution_has_folder chf on c.id_contribution = chf.id_contribution " +
            "left join contribution_has_folder chf2 on e.id_contribution = chf2.id_contribution  " +
            "where " + whereContributionOnlyLinkedWithFolder() +
            "or " + whereContributionOnlyLinkedWithFolder("chf2", "e.id_contribution") +
            getOrderByContributionDate() + getSearchLimit(fromIndex, toIndex);
    return Ebean.find(Text.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
  }

  /**
   * Get the string for a sql request to only match with the most composed folder.
   *
   * @return the corresponding query string
   */
  private String whereContributionOnlyLinkedWithFolder(){
    return whereContributionOnlyLinkedWithFolder(null, null);
  }

  /**
   * Get the string for a sql request to only match with the most composed folder.
   *
   * @param chf the alias for the contribution has folder table
   * @param id the alias for the related contribution table
   * @return the corresponding query string
   */
  private String whereContributionOnlyLinkedWithFolder(String chf, String id){
    /*int nbLinks = foldertype.getIdType() == EFolderType.COMPOSED.id() ? (childFolders.size() == 3 ? 3 : 7 ) : 1;
    return (chf != null ? chf : "chf") + ".folder = " + idContribution + " and " + nbLinks +
            " = (select count(*) from contribution_has_folder where contribution_has_folder.id_contribution = " + (id != null ? id : "c.id_contribution") + ") ";*/
    return (chf != null ? chf : "chf") + ".folder = " + idContribution + " ";
  }

}
