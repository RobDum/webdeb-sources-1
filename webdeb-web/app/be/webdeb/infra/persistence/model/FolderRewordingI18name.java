/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;


/**
 * The persistent class for folder's rewording names database table. Folder names may have multiple spellings (depending
 * on the language), so they are externalized in a dedicated table with the 2-char ISO code corresponding to the language.
 *
 * It could be multiple rewording names in the same language
 *
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "folder_rewording_i18names")
public class FolderRewordingI18name extends Model {

  @Id
  @Column(name = "id_folder_rewording", insertable = false, updatable = false, unique = true, nullable = false)
  private Long idRewordingWord;

  @Column(name = "lang", nullable = false)
  private String lang;

  @ManyToOne
  @JoinColumn(name = "id_contribution", nullable = false)
  private Folder folder;

  @Column(name = "name", nullable = false)
  private String name;


  /**
   * Default constructor. Create folder name holder for given folder in given language.
   *
   * @param folder a folder
   * @param lang a 2-char ISO code of the title's language
   * @param name the String representing the folder name
   */
  public FolderRewordingI18name(Folder folder, String lang, String name) {
    idRewordingWord = 0L;
    this.lang = lang;
    this.name = name;
    this.folder = folder;
  }

  /*
   * GETTERS AND SETTERS
   */

  /**
   * Get the rewording name id
   *
   * @return an id
   */
  public Long getIdRewordingWord() {
    return idRewordingWord;
  }

  /**
   * Set the rewording name id
   *
   * @param idRewordingWord an id of an existing rewording name
   */
  public void setIdRewordingWord(Long idRewordingWord) {
    this.idRewordingWord = idRewordingWord;
  }

  /**
   * Get the language associated to this name
   *
   * @return a two-char iso-639-1 language code
   */
  public String getLang() {
    return this.lang;
  }

  /**
   * Set the language associated to this name
   *
   * @param lang a two-char iso-639-1 language code
   */
  public void setLang(String lang) {
    this.lang = lang;
  }

  /**
   * Get the folder name
   *
   * @return the name of the folder
   */
  public String getName() {
    return name;
  }

  /**
   * Set the folder's name
   *
   * @param name the name of the folder
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Get the folder linked to this name
   *
   * @return a folder
   */
  public Folder getFolder() {
    return this.folder;
  }

  /**
   * Set the folder linked to this name
   *
   * @param folder a folder
   */
  public void setFolder(Folder folder) {
    this.folder = folder;
  }

  @Override
  public String toString() {
    return getIdRewordingWord() + " " + getName();
  }

}
