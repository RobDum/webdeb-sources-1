/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import com.avaje.ebean.*;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * The persistent class for the folder_link database table. Holds a link between two folders.
 *
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "folder_link")
public class FolderLink extends Model {

  private static final Model.Finder<Long, FolderLink> find = new Model.Finder<>(FolderLink.class);

  @Id
  @Column(name = "id_contribution", unique = true, nullable = false)
  private Long idContribution;

  // forcing updates from this object, deletions are handled at the contribution level
  @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
  @JoinColumn(name = "id_contribution", nullable = false, insertable = false, updatable = false)
  private Contribution contribution;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "id_folder_from", nullable = false)
  private Folder folderParent;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "id_folder_to", nullable = false)
  private Folder folderChild;

  /*
   * GETTERS / SETTERS
   */

  /**
   * Get the link id
   *
   * @return an id
   */
  public Long getIdContribution() {
    return idContribution;
  }

  /**
   * Set the link id
   *
   * @param idContribution an id
   */
  public void setIdContribution(Long idContribution) {
    this.idContribution = idContribution;
  }

  /**
   * Get the parent folder of this link
   *
   * @return a folder
   */
  public Folder getFolderParent() {
    return folderParent;
  }

  /**
   * Set the parent folder of this link
   *
   * @param folderParent a folder
   */
  public void setFolderParent(Folder folderParent) {
    this.folderParent = folderParent;
  }

  /**
   * Get the child folder of this link
   *
   * @return a folder
   */
  public Folder getFolderChild() {
    return folderChild;
  }

  /**
   * Set child folder of this link
   *
   * @param folderChild a folder
   */
  public void setFolderChild(Folder folderChild) {
    this.folderChild = folderChild;
  }

  /**
   * Get "supertype" contribution object
   *
   * @return a contribution
   */
  public Contribution getContribution() {
    return contribution;
  }

  /**
   * Set "supertype" contribution object
   *
   * @param contribution a contribution
   */
  public void setContribution(Contribution contribution) {
    this.contribution = contribution;
  }

  /*
   * CONVENIENCE METHODS
   */

  /**
   * Get the current version of this folder link
   *
   * @return a timestamp with the latest update moment of this folder link
   */
  public Timestamp getVersion() {
    return getContribution().getVersion();
  }

  @Override
  public String toString() {
    // because of lazy load, must explicitly call getter
    return new StringBuffer("link [").append(getIdContribution()).append("] between ")
        .append(getFolderParent().getContribution().getIdContribution()).append(" and ")
        .append(getFolderChild().getContribution().getIdContribution())
        .append(" [version:").append(getContribution().getVersion()).append("]").toString();
  }

  /*
   * QUERIES
   */

  /**
   * Retrieve a link by its id
   *
   * @param id the link id
   * @return the link corresponding to that id, null otherwise
   */
  public static FolderLink findById(Long id) {
    return id == null || id == -1L ? null : find.byId(id);
  }

  /**
   * Retrieve a link by parent and child
   *
   * @param parent the parent folder id
   * @param child the child folder id
   * @return the link corresponding to given parent and child, null otherwise
   */
  public static FolderLink findByParentChild(Long parent, Long child) {
    if(parent == null || child == null){
      return null;
    }
    return find.where().eq("id_folder_from", parent).eq("id_folder_to", child).findUnique();
  }
}
