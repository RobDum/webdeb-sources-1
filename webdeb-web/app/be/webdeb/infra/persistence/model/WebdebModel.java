/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see COPYING file).  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.infra.persistence.model.annotation.Unqueryable;
import com.avaje.ebean.Model;

import javax.persistence.MappedSuperclass;

/**
 * The abstract class for regroup the main behavior between class models
 *
 * @author Martin Rouffiange
 */
@MappedSuperclass
@Unqueryable
public abstract class WebdebModel extends Model {

    /**
     * Get the string to join contribution in group table
     *
     * @param toJoin the key to join
     * @param contributorId a contributor id
     * @return the corresponding query string
     */
    static String getContributionStatsJoins(String toJoin, Long contributorId){
        return joinContributionInGroup(toJoin) + joinContributionHasContributor(toJoin) + joinContributorHasGroup(contributorId);
    }

    /**
     * Get the string to put in a where clause to only match contribution in given group id
     *
     * @param contributorId a contributor id
     * @param groupId a group id
     * @return the corresponding query string
     */
    static String getContributionStatsWhereClause(Long contributorId, int groupId){
        return " and (" + contributionInGroupRequest(groupId) + " or " + contributionHasContributor(contributorId) +
                " or " + contributorHasGroup() + ")";
    }

    /**
     * Get the string to join contribution in group table
     *
     * @param toJoin the key to join
     * @return the corresponding query string
     */
    private static String joinContributionInGroup(String toJoin){
        return " left join contribution_in_group on contribution_in_group.id_contribution = " + toJoin;
    }

    /**
     * Get the string to put in a where clause to only match contribution in given group id
     *
     * @param groupId a group id
     * @return the corresponding query string
     */
    private static String contributionInGroupRequest(int groupId){
        return " contribution_in_group.id_group in (" + String.join(",",
                Group.getVisibleGroupsFor(groupId != -1  ? groupId : Group.getPublicGroup().getIdGroup())) + ") ";
    }

    /**
     * Get the string to join contribution has contributor table
     *
     * @param toJoin the key to join
     * @return the corresponding query string
     */
    private static String joinContributionHasContributor(String toJoin){
        return " left join contribution_has_contributor on contribution_has_contributor.id_contribution = " + toJoin;
    }

    /**
     * Get the string to put in a where clause to only match contribution that can be view by given contributor id
     *
     * @param contributorId a contributor id
     * @return the corresponding query string
     */
    private static String contributionHasContributor(Long contributorId){
        return " contribution_has_contributor.id_contributor = " + contributorId;
    }

    /**
     * Get the string to join contributor has group table
     *
     * @param contributorId a contributor id
     * @return the corresponding query string
     */
    private static String joinContributorHasGroup(Long contributorId){
        return " left join contributor_has_group on contributor_has_group.id_contributor = " + contributorId;
    }

    /**
     * Get the string to put in a where clause to only match contribution that can be viewed by given contributor id
     *
     * @return the corresponding query string
     */
    private static String contributorHasGroup(){
        return " contributor_has_group.id_group = contribution_in_group.id_group";
    }

    /**
     * Transform the given string token to well match in DB.
     *
     * @param token the token to treat
     * @return the treated token
     */
    static String getSearchToken(String token){
        return /* "\"" + */ token.trim()
                // protect single quotes
                .replace("'", "\\'")
                // add '%' for spaces
                .replace(" ", "%");
    }

    /**
     * Get the string to limit a select in sql
     *
     * @param lowerIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
     * @param upperIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
     * @return the corresponding query string
     */
    static String getSearchLimit(int lowerIndex, int upperIndex){
        return getSearchLimit(lowerIndex, upperIndex, Integer.MAX_VALUE);
    }

    /**
     * Get the string to limit a select in sql
     *
     * @param lowerIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
     * @param upperIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
     * @param maxValue the maximum value if upper index is wrong
     * @return the corresponding query string
     */
    static String getSearchLimit(int lowerIndex, int upperIndex, int maxValue){
        return " limit " + (lowerIndex > 0 ? lowerIndex : 0)  +
                "," + (upperIndex > 0 && upperIndex > lowerIndex ? upperIndex - lowerIndex : maxValue);
    }

    /**
     * Get the string to order by contribution date
     *
     * @return the corresponding query string
     */
    static String getOrderByContributionDate(){
        return " order by c.version desc ";
    }

}
