/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.core.api.folder.EFolderType;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


/**
 * The persistent class for the t_folder_type database table, holding predefined values for types of folder,
 * Root, node and leaf.
 *
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "t_folder_type")
public class TFolderType extends TechnicalTable {

  /**
   * Finder to access predefined values
   */
  public static final Model.Finder<Integer, TFolderType> find = new Model.Finder<>(TFolderType.class);

  @Id
  @Column(name = "id_folder_type", unique = true, nullable = false)
  private int idType;

  @OneToMany(mappedBy = "foldertype", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private List<Folder> folders;

  /**
   * Get the folder type id
   *
   * @return an id
   */
  public int getIdType() {
    return this.idType;
  }

  /**
   * Set the folder type id
   *
   * @param idType an id
   */
  public void setIdType(int idType) {
    this.idType = idType;
  }

  /**
   * Get the list of folders with this type
   *
   * @return a (possibly empty) list of folders
   */
  public List<Folder> getFolders() {
    return folders == null ? new ArrayList<>() : folders;
  }

  /**
   * Set the list of folders with this type
   *
   * @param folders a list of folders
   */
  public void setFolders(List<Folder> folders) {
    this.folders = folders;
  }

  /**
   * Get the EFolderType corresponding to this db folder type
   *
   * @return the EFolderType corresponding
   */
  public EFolderType getEContributionType(){
    return EFolderType.value(idType);
  }
}
