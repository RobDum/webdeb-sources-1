/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import com.avaje.ebean.*;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The persistent class for the argument dictionary database table, conceptual subtype of contribution.
 * May have translation links to / from other argument dictionary (materialized by ArgumentTranslation.
 *
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "argument_dictionary")
public class ArgumentDictionary extends WebdebModel {

    protected static final org.slf4j.Logger logger = play.Logger.underlying();
    private static final Model.Finder<Long, ArgumentDictionary> find = new Model.Finder<>(ArgumentDictionary.class);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_argument_dictionary", unique = true, nullable = false)
    private Long idArgumentDictionary;

    @Column(name = "title", nullable = false)
    private String title;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "language", nullable = true)
    private TLanguage language;

    // all dictionary translations
    @OneToMany(mappedBy = "title", cascade = CascadeType.PERSIST)
    private List<Argument> arguments;

    // all dictionary translations
    @OneToMany(mappedBy = "argumentTo", cascade = CascadeType.PERSIST)
    private List<ArgumentTranslation> translationsFrom;

    // all dictionary translations
    @OneToMany(mappedBy = "argumentFrom", cascade = CascadeType.PERSIST)
    private List<ArgumentTranslation> translationsTo;

    public ArgumentDictionary(String title, String lang){
        this.title = title;
        this.language = TLanguage.find.byId(lang);
    }

    /**
     * Get the id of argument dictionary
     *
     * @return the argument dictionary id
     */
    public Long getIdDictionary() {
        return idArgumentDictionary;
    }

    /**
     * Set the id of argument dictionary
     *
     * @param idDictionary the argument dictionary id
     */
    public void setIdDictionary(Long idDictionary) {
        this.idArgumentDictionary = idDictionary;
    }

    /**
     * Get the title of this argument dictionary
     *
     * @return the title of this argument dictionary
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the title of this argument dictionary
     *
     * @param title a title for this argument dictionary
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Get the language of the title of this argument dictionary
     *
     * @return the argument dictionary language
     */
    public TLanguage getLanguage() {
        return language;
    }

    /**
     * Set the language of the title of this argument dictionary
     *
     * @param language a language
     */
    public void setLanguage(TLanguage language) {
        this.language = language;
    }

    /**
     * Get the list of arguments that have this arguments dictionary as title
     *
     * @return a possibly empty list of arguments
     */
    public List<Argument> getArguments() {
        return arguments;
    }

    /**
     * Set the list of arguments that have this arguments dictionary as title
     *
     * @param arguments a list of arguments
     */
    public void getArguments(List<Argument> arguments) {
        this.arguments = arguments;
    }

    /**
     * Get the list of arguments dictionary that are a translation of this one
     *
     * @return a list of argument dictionary
     */
    public List<ArgumentDictionary> getTranslations() {
        List<ArgumentDictionary> similar = new ArrayList<>();
        similar.addAll(translationsTo.stream().map(ArgumentTranslation::getArgumentDictionaryFrom).collect(Collectors.toList()));
        similar.addAll(translationsFrom.stream().map(ArgumentTranslation::getArgumentDictionaryTo).collect(Collectors.toList()));
        return similar;
    }

    /**
     * Set the list of translation where this argument dictionary is the origin
     *
     * @param translationsFrom a list of translations
     */
    public void setTranslationsFrom(List<ArgumentTranslation> translationsFrom) {
        this.translationsFrom = translationsFrom;
    }

    /**
     * Set the list of translation where this argument dictionary is the destination
     *
     * @param translationsTo a list of translations
     */
    public void setTranslationsTo(List<ArgumentTranslation> translationsTo) {
        this.translationsTo = translationsTo;
    }

    @Override
    public String toString() {
        // must use getters and explicitly loop into references, otherwise ebean may send back deferred beanlist
        // (lazy load not triggered from toString methods)
        return new StringBuffer("argument [").append(getIdDictionary())
                .append(", title: ").append(getTitle()).append(", lang: ").append(getLanguage().getCode()).toString();
    }

    /*
     * QUERIES
     */

    /**
     * Retrieve an argument dictionary by its id
     *
     * @param id an id
     * @return the argument dictionary corresponding to the given id, null if not found
     */
    public static ArgumentDictionary findById(Long id) {
        return id == null || id == -1L ? null : find.byId(id);
    }

    /**
     * Check if an argument dictionary is a translation to another one
     *
     * @param argToCompare the argument dictionary to compare with the other one
     * @param argument an argument dictionary
     * @return true if both arguments are translation
     */
    public static boolean translationArguments(ArgumentDictionary argToCompare, ArgumentDictionary argument){
        String select = "SELECT id_contribution FROM argument_translation where " +
                "((id_argument_dictionary_from = " + argToCompare.getIdDictionary() + " and id_argument_dictionary_to = " + argument.getIdDictionary() + ") " +
                "or (id_argument_dictionary_from = " + argument.getIdDictionary()+ " and id_argument_dictionary_to = " + argToCompare.getIdDictionary() + "))";
        return Ebean.find(Contribution.class).setRawSql(RawSqlBuilder.parse(select).create()).findRowCount() > 0;
    }

    /**
     * Find an unique argument by a complete title and lang
     *
     * @param title an argument dictionary title
     * @param lang a i18 lang
     * @return an argument dictionary matching the given title and lang or null
     */
    public static ArgumentDictionary findUniqueByTitle(String title, String lang) {
        List<ArgumentDictionary> result = null;
        if (title != null && lang != null) {
            // will be automatically ordered by relevance
            String token = /* "\"" + */ title.trim()
                    // protect single quotes
                    .replace("'", "\\'")
                    // add '%' for spaces
                    .replace(" ", "%");

            String select = "select distinct id_argument_dictionary from argument_dictionary " +
                    "where title like '" + token + "' and language = '" + lang + "'";

            logger.debug("search for argument dictionary : " + select);
            result = Ebean.find(ArgumentDictionary.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
        }
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }

    /**
     * Find a list of arguments dictionary by a complete title
     *
     * @param title an argument title
     * @return the list of ArgumentDictionary matching the given title
     */
    public static List<ArgumentDictionary> findByTitle(String title) {
        return findByTitleAndLang(title, null, -1, -1);
    }

    /**
     * Find a list of arguments dictionary by a complete title and lang
     *
     * @param title an argument title
     * @param lang a i18 lang
     * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
     * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
     * @return the list of ArgumentDictionary matching the given title and lang
     */
    public static List<ArgumentDictionary> findByTitleAndLang(String title, String lang, int fromIndex, int toIndex) {
        List<ArgumentDictionary> result = null;
        if (title != null) {
            // will be automatically ordered by relevance
            String select = "select distinct ad.id_argument_dictionary from argument_dictionary ad " +
                    "left join argument a on ad.id_argument_dictionary = a.id_argument_dictionary " +
                    "left join contribution c on c.id_contribution = a.id_contribution " +
                    "where ad.title like '" + getSearchToken(title) + "%'" + (lang == null ? "" : " and argument_dictionary.lang= '" + lang + "'")
                    + " and ad.id_argument_dictionary > 0 " +
                    "order by c.version desc, ad.title" + getSearchLimit(fromIndex, toIndex);

            logger.debug("search for argument dictionary : " + select);
            result = Ebean.find(ArgumentDictionary.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
        }
        return result != null ? result : new ArrayList<>();
    }
}
