/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the t_argument_linktype database table, being the different types of links
 * between arguments. Excerpt links are typed by link shades.
 *
 * @author Fabian Gilson
 * @see be.webdeb.core.api.argument.EArgumentLinkShade
 */
@Entity
@Table(name = "t_argument_linktype")
@CacheBeanTuning
public class TArgumentLinktype extends TechnicalTable {

  /**
   * Finder to access predefined values
   */
  public static final Model.Finder<Integer, TArgumentLinktype> find = new Model.Finder<>(TArgumentLinktype.class);

  @Id
  @Column(name = "id_linktype", unique = true, nullable = false)
  private int idLinktype;

  // bi-directional many-to-one association to TLinkShadeType
  @OneToMany(mappedBy = "argumentLinktype")
  private List<TLinkShadeType> linkShadeTypes;

  /**
   * Get the argument linktype id
   *
   * @return an id
   */
  public int getIdLinktype() {
    return this.idLinktype;
  }

  /**
   * Set the argument linktype id
   *
   * @param idLinktype an id
   */
  public void setIdLinktype(int idLinktype) {
    this.idLinktype = idLinktype;
  }

  /**
   * Get the applicable link shades for this linktype
   *
   * @return the list of applicable link shades for this linktype
   */
  public List<TLinkShadeType> getLinkShadeTypes() {
    return this.linkShadeTypes;
  }

  /**
   * Set the applicable link shades for this linktype
   *
   * @param linkShadeTypes the list of applicable link shades for this linktype
   */
  public void setLinkShadeTypes(List<TLinkShadeType> linkShadeTypes) {
    this.linkShadeTypes = linkShadeTypes;
  }
}
