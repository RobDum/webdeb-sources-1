/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import be.webdeb.infra.persistence.model.annotation.Unqueryable;
import com.avaje.ebean.*;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


/**
 * The persistent class for temporary excerpts from external services (like browser extension, tweets, ...) that will
 * be added manually by contributor.
 *
 * Used as temporary repository for an easiest communication between services and the platform.
 *
 * @author Martin Rouffiange
 */
@Entity
@CacheBeanTuning
@Table(name = "external_excerpt")
@Unqueryable
public class ExternalExcerpt extends Model {

    protected static final org.slf4j.Logger logger = play.Logger.underlying();
    private static final Model.Finder<Long, ExternalExcerpt> find = new Model.Finder<>(ExternalExcerpt.class);

    @Id
    @Column(name = "id_contribution", unique = true, nullable = false)
    private Long idContribution;

    // forcing updates from this object, deletions are handled at the contribution level
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "id_contribution", nullable = false, insertable = false, updatable = false)
    private ExternalContribution contribution;

    @Column(name = "original_excerpt", nullable = false)
    private String originalExcerpt;

    @Column(name = "reworded_excerpt")
    private String workingExcerpt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_internal_text")
    private Text text;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_external_text")
    private ExternalText externalText;

    /**
     * Get parent external contribution object
     *
     * @return the parent external contribution
     */
    public ExternalContribution getExternalContribution() {
        return contribution;
    }

    /**
     * Set parent external contribution object
     *
     * @param contribution the parent external contribution
     */
    public void setExternalContribution(ExternalContribution contribution) {
        this.contribution = contribution;
    }

    /**
     * Get the id of external  excerpt
     *
     * @return the external  excerpt id
     */
    public Long getIdContribution() {
        return idContribution;
    }

    /**
     * Set the id of external  excerpt
     *
     * @param idContribution the external  excerpt id
     */
    public void setIdContribution(Long idContribution) {
        this.idContribution = idContribution;
    }

    /**
     * Get the original excerpt
     *
     * @return an original excerpt
     */
    public String getOriginalExcerpt() {
        return originalExcerpt;
    }

    /**
     * Set the original excerpt
     *
     * @param originalExcerpt the original excerpt
     */
    public void setOriginalExcerpt(String originalExcerpt) {
        this.originalExcerpt = originalExcerpt;
    }

    /**
     * Get the working excerpt
     *
     * @return the working excerpt
     */
    public String getWorkingExcerpt() {
        return workingExcerpt;
    }

    /**
     * Set the working excerpt
     *
     * @param workingExcerpt the working excerpt
     */
    public void setWorkingExcerpt(String workingExcerpt) {
        this.workingExcerpt = workingExcerpt;
    }

    /**
     * Get the text that is linked with this external  excerpt, if any
     *
     * @return a text
     */
    public Text getText() {
        return text;
    }

    /**
     * Set the text that is linked with this external  excerpt, if any
     *
     * @param text a text
     */
    public void setText(Text text) {
        this.text = text;
    }

    /**
     * Get the external text that is linked with this external  excerpt, if any
     *
     * @return a external text
     */
    public ExternalText getExternalText() {
        return externalText;
    }

    /**
     * Set the external text that is linked with this external  excerpt, if any
     *
     * @param externalText a external text
     */
    public void setExternalText(ExternalText externalText) {
        this.externalText = externalText;
    }

    /**
     * Get the current version of this external  excerpt
     *
     * @return a timestamp with the latest update moment of this external  excerpt
     */
    public Timestamp getVersion() {
        return getExternalContribution().getContribution().getVersion();
    }

    @Override
    public String toString() {
        // must use getters and explicitly loop into references, otherwise ebean may send back deferred beanlist
        // (lazy load not triggered from toString methods)
        return new StringBuffer("external excerpt [").append(getIdContribution())
                .append(", from text: ").append(getText() != null ? getText().getIdContribution() : getExternalText() != null ? getExternalText().getIdContribution() : "null")
                .append(" [version:").append(getExternalContribution().getContribution().getVersion()).append("]").toString();
    }

    /*
     * QUERIES
     */

    /**
     * Retrieve an external excerpt by its id
     *
     * @param id an id
     * @return the external excerpt corresponding to the given id, null if not found
     */
    public static ExternalExcerpt findById(Long id) {
        return id == null || id == -1L ? null : find.byId(id);
    }

    /**
     * Retrieve an external excerpt by its id and the external source id where it comes from
     *
     * @param id an id
     * @param externalSource the source where the excerpts come from
     * @return the external excerpt corresponding to the given id and source, null if not found
     */
    public static ExternalExcerpt findByIdAndSource(Long id, int externalSource) {
        return id == null || id == -1L ? null : find.where().eq("id_contribution", id).eq("external_source", externalSource).findUnique();
    }

    /**
     * Retrieve an external excerpt by its id and its contributor id
     *
     * @param contributor a contributor id
     * @param id an id
     * @return the external excerpt corresponding to the given id and added by the given contributor, null if not found
     */
    public static ExternalExcerpt findByIdAndContributor(Long contributor, Long id) {
        String select = "select contribution.id_contribution from contribution left join contribution_has_contributor on " +
                "contribution.id_contribution = contribution_has_contributor.id_contribution " +
                "where contribution_has_contributor.id_contributor = " + contributor + " and contribution.id_contribution = " + id;
        return Ebean.find(ExternalExcerpt.class).setRawSql(RawSqlBuilder.parse(select).create()).findUnique();
    }

    /**
     * Find already recorded external excerpt that hasn't has been added to webdeb yet.
     *
     * @param contributor a contributor id
     * @param excerpt the original excerpt
     * @param textId the text id of the excerpt
     * @return the retrieved external excerpt or null
     */
    public static ExternalExcerpt findExisting(Long contributor, String excerpt, Long textId){
        List<ExternalExcerpt> results = null;

        if(excerpt != null) {
            // will be automatically ordered by relevance
            String token = excerpt.trim()
                    // protect single quotes
                    .replace("'", "\\'")
                    // add '%' for spaces
                    .replace(" ", "%");

            String select = "select contribution.id_contribution from contribution left join contribution_has_contributor on " +
                    "contribution.id_contribution = contribution_has_contributor.id_contribution left join external_excerpt on " +
                    "contribution.id_contribution = external_excerpt.id_contribution " +
                    "where contribution_has_contributor.id_contributor = " + contributor +
                    " and external_excerpt.original_excerpt like '" + token + "' and external_excerpt.id_external_text = " + textId;
            results = Ebean.find(ExternalExcerpt.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();

        }
        return results != null && !results.isEmpty() ? results.get(0) : null;
    }

    /**
     * Get all external excerpts in database (ordered by publication_date descending) for a given external source id and
     * with a maximum of result rows.
     *
     * @param externalSource the given external source id where external contribution must come from
     * @param maxRows the maximum of result rows
     * @return a (possibly empty) list of external excerpts
     */
    public static List<ExternalExcerpt> findAll(int externalSource, int maxRows) {
        List<ExternalExcerpt> results;
        String select = "select c.id_contribution from external_contribution c where " +
                "c.external_source = " + externalSource + " and c.rejected = 0 order by c.publication_date desc limit " + maxRows;
        results = Ebean.find(ExternalExcerpt.class).setRawSql(RawSqlBuilder.parse(select).create()).findList();
        return results != null ? results : new ArrayList<>();
    }

}
