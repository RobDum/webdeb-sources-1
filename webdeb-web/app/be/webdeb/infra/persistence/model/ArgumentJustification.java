/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.persistence.model;

import com.avaje.ebean.*;
import com.avaje.ebean.annotation.CacheBeanTuning;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the argument_justification database table. Holds a justification link between
 * two arguments in a context, as well as the type of link between them and the validation state.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 * @see be.webdeb.core.api.argument.EArgumentLinkShade
 */
@Entity
@CacheBeanTuning
@Table(name = "argument_justification")
public class ArgumentJustification extends Model {

    protected static final org.slf4j.Logger logger = play.Logger.underlying();
    private static final Model.Finder<Long, ArgumentJustification> find = new Model.Finder<>(ArgumentJustification.class);

    @Id
    @Column(name = "id_contribution", unique = true, nullable = false)
    private Long idContribution;

    // forcing updates from this object, deletions are handled at the contribution level
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "id_contribution", nullable = false, insertable = false, updatable = false)
    private Contribution contribution;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_argument_from", nullable = false)
    private ArgumentContext argumentFrom;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_argument_to", nullable = false)
    private ArgumentContext argumentTo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_context", nullable = false)
    private Contribution context;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "shade", nullable = false)
    private TLinkShadeType shade;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "validated", nullable = false)
    private TValidationState validation;

    /*
     * GETTERS / SETTERS
     */

    /**
     * Get the justification link id
     *
     * @return an id
     */
    public Long getIdContribution() {
        return idContribution;
    }

    /**
     * Set the justification link id
     *
     * @param idContribution an id
     */
    public void setIdContribution(Long idContribution) {
        this.idContribution = idContribution;
    }

    /**
     * Get "supertype" contribution object
     *
     * @return a contribution
     */
    public Contribution getContribution() {
        return contribution;
    }

    /**
     * Set "supertype" contribution object
     *
     * @param contribution a contribution
     */
    public void setContribution(Contribution contribution) {
        this.contribution = contribution;
    }

    /**
     * Get the justification link shade
     *
     * @return a link shade
     */
    public TLinkShadeType getShade() {
        return shade;
    }

    /**
     * Set the justification link shade
     *
     * @param shade a link shade
     */
    public void setShade(TLinkShadeType shade) {
        this.shade = shade;
    }

    /**
     * Get origin contextualized argument of this justification link
     *
     * @return a contextualized argument
     */
    public ArgumentContext getArgumentFrom() {
        return argumentFrom;
    }

    /**
     * Set origin contextualized argument of this justification link
     *
     * @param argumentFrom a contextualized argument
     */
    public void setArgumentFrom(ArgumentContext argumentFrom) {
        this.argumentFrom = argumentFrom;
    }

    /**
     * Get destination contextualized argument of this justification link
     *
     * @return a contextualized argument
     */
    public ArgumentContext getArgumentTo() {
        return argumentTo;
    }

    /**
     * Set destination contextualized argument of this justification link
     *
     * @param argumentTo a contextualized  argument
     */
    public void setArgumentTo(ArgumentContext argumentTo) {
        this.argumentTo = argumentTo;
    }

    /**
     * Get the context of this justification link
     *
     * @return the contribution that is the context where the link exists
     */
    public Contribution getContext() {
        return context;
    }

    /**
     * Set the context of this justification link
     *
     * @param context a contribution that is the context where the link exists
     */
    public void setContext(Contribution context) {
        this.context = context;
    }

    /**
     * Get the validation state of this justification link
     *
     * @return the validation state
     */
    public TValidationState getValidationState() {
        return validation;
    }

    /**
     * Set the validation state of this justification link
     *
     * @param validation a validation state
     */
    public void setValidationState(TValidationState validation) {
        this.validation = validation;
    }

    /*
     * CONVENIENCE METHODS
     */

    /**
     * Get the current version of this argument link
     *
     * @return a timestamp with the latest update moment of this argument link
     */
    public Timestamp getVersion() {
        return getContribution().getVersion();
    }

    @Override
    public String toString() {
        // because of lazy load, must explicitly call getter
        return new StringBuffer("justification link [").append(getIdContribution()).append("] between ")
                .append(getArgumentFrom().getContribution().getIdContribution()).append(" and ")
                .append(getArgumentTo().getContribution().getIdContribution())
                .append(", context: [").append(getContext().getIdContribution()).append("] ").append(getContext().getSortkey())
                .append(", shade: [").append(getShade().getIdLinkShade()).append("] ").append(getShade().getEn())
                .append(", validation: ").append(getValidationState().getEn())
                .append(" [version:").append(getContribution().getVersion()).append("]").toString();
    }

    /*
     * QUERIES
     */

    /**
     * Retrieve a justification link by its id
     *
     * @param id the justification link id
     * @return the justification link corresponding to that id, null otherwise
     */
    public static ArgumentJustification findById(Long id) {
        return id == null || id == -1L ? null : find.byId(id);
    }

    /**
     * Retrieve a justification link by contextualized argument origin and destination.
     *
     * @param origin the contextualized argument origin id
     * @param destination the contextualized argument destination id
     * @return the corresponding justification link, null otherwise
     */
    public static ArgumentJustification findByOriginAndDestination(Long origin, Long destination) {
        return find.where().eq("id_argument_from", origin).eq("id_argument_to", destination).findUnique();
    }

    /**
     * Get the list of justifications to linked to the given argument
     *
     * @param id a context argument id
     * @return a list of argumentJustification
     */
    public static List<ArgumentJustification> getJustificationsTo(Long id) {
        return find.where().eq("id_argument_from", id).findList();
    }

}
