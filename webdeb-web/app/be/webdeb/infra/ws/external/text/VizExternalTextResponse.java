/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */
package be.webdeb.infra.ws.external.text;

import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.text.ExternalText;
import be.webdeb.core.api.text.Text;
import be.webdeb.infra.ws.external.VizExternalContributionResponse;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class VizExternalTextResponse extends VizExternalContributionResponse {

    /**
     * Text title
     */
    @JsonSerialize
    protected String title;

    /**
     * Default constructor
     *
     * @param text the external text to send
     * @param contributor the contributor id that ask for the data
     * @param lang the contributor language
     */
    public VizExternalTextResponse(ExternalText text, Long contributor, String lang){
        super(text.getId(), text.getSourceUrl(), text.getLanguage().getCode(), EContributionType.EXTERNAL_TEXT.id(), text.getInternalContribution(), text.getVersionAsString());
        Text internalText = textFactory.retrieve(text.getInternalContribution());
        if(internalText != null){
            title = internalText.getTitle(lang);
        }else{
            idInternalContribution = null;
            title = text.getTitle();
        }
    }

}
