/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.infra.ws.external.text;

import be.webdeb.core.api.excerpt.ExternalExcerpt;
import be.webdeb.core.api.contributor.Group;
import be.webdeb.core.api.text.ExternalText;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.ws.external.ExternalForm;
import be.webdeb.infra.ws.external.excerpt.ExcerptRequest;
import play.data.validation.ValidationError;

import java.util.*;

/**
 * Simple POJO class to handle JSON request from external service (like browser extension).
 * This request is use to add a new text to webdeb from external service
 *
 * @author Martin Rouffiange
 */
public class TextRequest extends ExternalForm {

    /**
     * The text title
     */
    private String title;

    /**
     * The excerpts excerpts in the text
     */
    private List<ExcerptRequest> excerpts;

    /**
     * Constructor.
     *
     */
    public TextRequest() {
        this.id = -1L;
    }

    /**
     * Validate the creation of an ExternalText
     *
     * @return null if validation ok, map of errors for each fields in error otherwise
     */
    public Map<String, List<ValidationError>> validate() {
        Map<String, List<ValidationError>> errors = new HashMap<>();

        if (values.isBlank(url) && !values.isURL(url)) {
            errors.put("url", Collections.singletonList(new ValidationError("url", "nlp.text.invalidurl")));
        }

        // must return null if errors is empty
        return errors.isEmpty() ? null : errors;
    }

    /**
     * Store the External Text into the database
     *
     * @param contributor the contributor id
     *
     * @throws FormatException if this contribution has invalid field values (should be pre-checked before-hand)
     * @throws PermissionException if given contributor may not perform this action or if such action would cause
     * integrity problems
     * @throws PersistenceException if any error occurred at the persistence layer (concrete error is wrapped)
     */
    public void save(Long contributor) throws FormatException, PermissionException, PersistenceException {
        logger.debug("try to save external text " + title + " (" + url + ")");

        ExternalText text = textFactory.getExternalText();
        initExternalContribution(text);
        text.setTitle(title);

        if(excerpts != null) {
            for (ExcerptRequest exc : excerpts) {
                ExternalExcerpt excerpt = excerptFactory.getExternalExcerpt();
                initExternalContribution(excerpt);
                excerpt.setOriginalExcerpt(exc.getOriginalExcerpt());
                excerpt.setWorkingExcerpt(exc.getWorkingExcerpt());
                text.addExcerpt(excerpt);
            }
        }

        text.save(contributor, Group.getGroupPublic());
        // do not forget to update the id, since the controller needs it for redirection
        id = text.getId();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ExcerptRequest> getExcerpts() {
        return excerpts;
    }

    public void setExcerpts(List<ExcerptRequest> excerpts) {
        this.excerpts = excerpts;
    }

    @Override
    public String toString() {
        return super.toString() + " - TextRequest{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", excerpts=" + excerpts +
                '}';
    }
}