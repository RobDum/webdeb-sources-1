/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package be.webdeb.application.rest.object;

import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.folder.FolderName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is devoted to exchange Folders in JSON format. All details of an API Folder are represented.
 * Links to other contributions are represented by their ids. Enumerated (predefined) data are passed as
 * typed objects with their ids and i18n descriptions.
 *
 * @author Martin Rouffiange
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class WebdebFolder extends WebdebContribution {

  /**
   * the type of the folder (root for a folder without parent folder, node for normal folder and leaf for folder
   * without children folder)
   */
  @JsonSerialize
  private String folderType;

  /**
   * the names of the folder
   */
  @JsonSerialize
  private List<FolderName> names;

  /**
   * the rewording names of the folder
   */
  @JsonSerialize
  private List<FolderName> rewordingNames = new ArrayList<>();

  /**
   * the list of parents folders
   */
  @JsonSerialize
  private List<WebdebSimpleFolder> parents = new ArrayList<>();

  /**
   * the list of children folders
   */
  @JsonSerialize
  private List<WebdebSimpleFolder> children = new ArrayList<>();

  /**
   * @api {get} WebdebFolder Folder
   * @apiName Folder
   * @apiGroup Structures
   * @apiDescription A folder regroup texts and arguments on the same theme. They are lined one another to make a hierarchy.
   * @apiSuccess {String} folderType the type of the folder (root for a folder without parent folder, node for normal
   *                      folder and leaf for folder without children folder)
   * @apiSuccess {Object} name and rewording name a structure containing all spellings for this folder's name
   * @apiSuccess {String} name.name the spelling of the name
   * @apiSuccess {String} name.lang the 2-char ISO code of the language corresponding to this spelling
   * @apiSuccess {WebdebSimpleFolder[]} parents the parents folder in the hierarchy
   * @apiSuccess {WebdebSimpleFolder[]} children the children folder in the hierarchy
   * @apiSuccess {WebdebPlace[]} places a list of places involved in this folder
   * @apiExample Folder
   * {
   *   "id": 100000,
   *   "type": "folder",
   *   "version": 07465823022018,
   *   "folderType": "node",
   *   "name": [ { "name": "actualité", "lang": "fr" } ],
   *   "rewordingNames": [ { "name": "nouvelle", "lang": "fr",  "name": "nouveauté", "lang": "fr"} ],
   *   "parents": {
   *      "id": 100000,
   *      "folderType": "node",
   *      "name": [ { "name": "actualité", "lang": "fr" } ],
   *   },
   *   "children": {
   *      "id": 100000,
   *      "folderType": "node",
   *      "name": [ { "name": "actualité", "lang": "fr" } ],
   *   },
   *   "argumentPlace": {
   *     { "lang": "fr", "name": "Namur", "subregion": "Province de Namur", "region": "Wallonie", "Country": "Belgique", "continent": "Europe" },
   *   }
   * }
   * @apiVersion 0.0.3
   */
  public WebdebFolder(Folder folder) {
    super(folder);
    folderType = folder.getType().name();
    names = folder.getNamesAsFolderName();
    for(List<FolderName> rNames : folder.getRewordingNames().values()){
      rewordingNames.addAll(rNames);
    }

    folder.getParents().forEach(f -> parents.add(new WebdebSimpleFolder(f)));
    folder.getChildren().forEach(f -> children.add(new WebdebSimpleFolder(f)));
  }
}
