/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.argument;

import be.webdeb.core.api.argument.ArgumentDictionary;
import be.webdeb.core.api.argument.ArgumentFactory;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.presentation.web.controllers.entry.ContributionHelper;
import be.webdeb.util.ValuesHelper;
import play.api.Play;
import play.data.validation.ValidationError;
import play.i18n.MessagesApi;

import javax.inject.Inject;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This wrapper is used to contains data about an argument dictionary, like its title, language and translations.
 *
 * @author Martin Rouffiange
 */
public class ArgumentDictionaryForm {

    @Inject
    protected MessagesApi i18n = Play.current().injector().instanceOf(MessagesApi.class);
    @Inject
    protected ContributionHelper helper = Play.current().injector().instanceOf(ContributionHelper.class);
    @Inject
    protected ArgumentFactory argumentFactory = Play.current().injector().instanceOf(ArgumentFactory.class);
    @Inject
    protected ValuesHelper values = Play.current().injector().instanceOf(ValuesHelper.class);


    // custom logger
    protected static final org.slf4j.Logger logger = play.Logger.underlying();

    protected Long id;
    protected String title;
    protected String lang;


    /**
     * Constructor (Play / Json compliant)
     */
    public ArgumentDictionaryForm () {

    }

    /**
     * Constructor a form from ArgumentDictionary api object
     *
     * @param dictionary an argument dictionary
     */
    public ArgumentDictionaryForm (ArgumentDictionary dictionary) {
        this.id = dictionary.getId();
        this.title = dictionary.getTitle();
        this.lang = dictionary.getLanguage().getCode();
    }

    /*
     * Getters and setters
     */

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getLang() {
        return lang;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    /**
     * Form validation (implicit call from form submit)
     *
     * @return a map of ValidationError if any error in form was found, null otherwise
     */
    public Map<String, List<ValidationError>> validate() {
        Map<String, List<ValidationError>> errors = new HashMap<>();

        if (values.isBlank(title)) {
            errors.put("title", Collections.singletonList(new ValidationError("title", "argument.error.title.blank")));
        }else if(title.length() > ArgumentHolder.TITLE_MAX_LENGTH){
            errors.put("title", Collections.singletonList(new ValidationError("title", "argument.error.title.size")));
        }

        if (values.isBlank(lang)) {
            errors.put("lang", Collections.singletonList(new ValidationError("lang", "folder.error.name.lang")));
        }

        // must return null if errors is empty
        return errors.isEmpty() ? null : errors;
    }

    /**
     * Save an argument dictionary into the database. This id is updated if it was not set before.
     *
     * @throws FormatException if any dictionary field is not valid or missing
     * @throws PersistenceException if an error occurred, a.o., unset required field.
     * The exception message will contain a more complete description of the error.
     */
    public void save() throws FormatException, PersistenceException {
        logger.debug("try to save argument dictionary " + id + " " + toString());

        ArgumentDictionary dictionary = argumentFactory.getArgumentDictionary();
        dictionary.setId(id != null ? id : -1L);

        // remove ending "." if any
        title =  title.trim();
        if ( title.endsWith(".")) {
            title =  title.substring(0,  title.lastIndexOf('.'));
        }
        dictionary.setTitle(title);

        // argument language
        try {
            dictionary.setLanguage(argumentFactory.getLanguage(lang));
        } catch (FormatException e) {
            logger.error("unknown language code " + lang, e);
        }

        dictionary.save();

        // update this id (in case of creation)
        id = dictionary.getId();
    }

}