/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.folder;

import be.webdeb.core.api.argument.Argument;
import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.folder.EFolderType;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.folder.FolderFactory;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.folder.FolderName;
import be.webdeb.core.api.text.Text;
import be.webdeb.presentation.web.controllers.entry.ContributionHolder;
import be.webdeb.presentation.web.controllers.entry.EFilterName;
import be.webdeb.presentation.web.controllers.entry.PlaceForm;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentContextHolder;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentHolder;
import be.webdeb.presentation.web.controllers.entry.debate.DebateHolder;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptHolder;
import be.webdeb.presentation.web.controllers.entry.text.TextHolder;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.api.Play;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class holds concrete values of a Folder (no IDs, but their description, as defined in the
 * database). Except by using a constructor, no value can be edited outside of this package or by
 * subclassing.
 *
 * @author Martin Rouffiange
 */
public class FolderHolder extends ContributionHolder {

  @Inject
  protected FolderFactory folderFactory = Play.current().injector().instanceOf(FolderFactory.class);

  // used for lazy loading of folder name
  protected Folder folder;

  // Note: as for all wrappers, all fields MUST hold empty values for proper form validation
  protected List<FolderNameForm> folderNames = new ArrayList<>();
  protected List<FolderNameForm> folderRewordingNames = new ArrayList<>();
  protected String name = "";
  protected int folderType = EFolderType.TAG.id();
  protected String folderTypeName = "";
  private int nbContributions = 0;

  // needed for content management
  protected Long contributor;

  // all parent folders of this folder
  protected List<SimpleFolderForm> parents = new ArrayList<>();
  // all normal parent folders of this folder
  protected List<SimpleFolderForm> normalParents = new ArrayList<>();
  // all child folders of this folder
  protected List<SimpleFolderForm> children = new ArrayList<>();
  // all normal child folders of this folder
  protected List<SimpleFolderForm> normalChildren = new ArrayList<>();
  // all debates link with this folder
  protected List<DebateHolder> debates = null;
  // all contextualized arguments link with this folder
  protected List<ArgumentContextHolder> argumentContexts = null;
  // all texts link with this folder
  protected List<TextHolder> texts = null;
  // all excerpts link with this folder
  protected List<ExcerptHolder> excerpts = null;

  /**
   * Play / JSON compliant constructor
   */
  public FolderHolder() {
    super();
    type = EContributionType.FOLDER;
  }

  /**
   * Constructor. Create a holder for a Folder (i.e. no type/data IDs, but their descriptions, as defined in
   * the database).
   *
   * @param folder an Folder
   * @param user a WebDeb user
   * @param lang 2-char ISO code of context language (among play accepted languages)
   */
  public FolderHolder(Folder folder, WebdebUser user, String lang) {
    this(folder, user.getId(), lang);
    this.nbContributions = folder.getNbContributions(user.getId(), user.getGroup().getGroupId());
  }

  /**
   * Constructor. Create a holder for a Folder (i.e. no type/data IDs, but their descriptions, as defined in
   * the database).
   *
   * @param folder an Folder
   * @param contributor a contributor id for whom the folder contributions content must be displayed
   * @param lang 2-char ISO code of context language (among play accepted languages)
   */
  public FolderHolder(Folder folder, Long contributor, String lang) {
    super(folder, lang);
    this.folder = folder;
    name = folder.getName(lang);
    addFilterable(EFilterName.FOLDER, name);

    this.folderNames = folder.getNames().entrySet().stream().map(n -> new FolderNameForm(n.getKey(), n.getValue())).collect(Collectors.toList());
    initRewordingNames(folder, lang, false);

    this.contributor = contributor;
    folderType = folder.getFolderType().getType();
    folderTypeName = folder.getFolderType().getName(lang);
    addFilterable(EFilterName.FTYPE, folderTypeName);

    initLinks();
  }

  /**
   * Initialize rewording names in depends of the lang
   *
   * @param folder an Folder
   * @param lang 2-char ISO code of context language (among play accepted languages)
   * @param onlyLang only keep rewording names for the given lang
   */
  private void initRewordingNames(Folder folder, String lang, boolean onlyLang) {
    List<FolderName> rewordingNames =
            (onlyLang ? folder.getRewordingNamesByLang(lang) : folderFactory.getFolderRewordingNames(folder.getId()));
    this.folderRewordingNames = rewordingNames.stream().map(n ->
            new FolderNameForm(n.getLang(), n.getName())).collect(Collectors.toList());
  }

  /**
   * Lazy loading of parents and children folders links
   */
  private void initLinks() {
    folder.getParents().forEach(p -> createLinks(p, parents, normalParents));
    folder.getChildren().forEach(c -> createLinks(c, children, normalChildren));
  }

  private void createLinks(Folder folder, List<SimpleFolderForm> globalList, List<SimpleFolderForm> normalList) {
    SimpleFolderForm form = new SimpleFolderForm(folder, lang);
    globalList.add(form);
    if(folder.getFolderType().getEFolderType() != EFolderType.COMPOSED)
      normalList.add(form);
  }

  @Override
  public String getContributionDescription(){
    List<String> descriptions = new ArrayList<>();
    descriptions.addAll(getFolderRewordingNames(3).stream().map(FolderNameForm::getName).collect(Collectors.toList()));
    //descriptions.addAll(getLinkedDebates(new WebdebUser(), 3).stream().map(DebateHolder::getTitle).collect(Collectors.toList()));
    //descriptions.addAll(getLinkedArgumentContexts(new WebdebUser(), 3).stream().map(e -> e.getArgument().getTitle()).collect(Collectors.toList()));
    //descriptions.addAll(getLinkedTexts(new WebdebUser(), 3).stream().map(TextHolder::getTitle).collect(Collectors.toList()));

    return String.join(", ", descriptions);
  }

  @Override
  public MediaSharedData getMediaSharedData(){
    if(mediaSharedData == null){
      mediaSharedData = new MediaSharedData(name, "folder");
    }
    return mediaSharedData;
  }

  @Override
  public String getDefaultAvatar(){
    return "";
  }

  @Override
  public String toString() {
    return "folder [" + id + "] "+name;
  }

  /*
   * GETTERS
   */

  /**
   * Get the folder name
   *
   * @return a folder name
   */
  public String getFolderName() {
    return name;
  }

  /**
   * Get the folder full name
   *
   * @return a folder full name (with description)
   */
  public String getFolderFullName(boolean full) {
      String ch = name;
      if(full && !folderRewordingNames.isEmpty())
        ch += String.join(", ",
                getFolderRewordingNames(3).stream().map(FolderNameForm::getName).collect(Collectors.toList()));
    return ch;
  }

  /**
   * Get the list of parent folder links
   *
   * @return a list of SimpleFolderForm
   */
  public List<SimpleFolderForm> getParents() {
    if (parents == null) {
      initLinks();
    }
    return parents;
  }

  /**
   * Get the list of parent normal (not composed) folder links
   *
   * @return a list of SimpleFolderForm
   */
  public List<SimpleFolderForm> getNormalParents() {
    if (normalParents == null) {
      initLinks();
    }
    return normalParents;
  }

  /**
   * Get the list of child folder links
   *
   * @return a list of SimpleFolderForm
   */
  public List<SimpleFolderForm> getChildren() {
    if (children == null) {
      initLinks();
    }
    return children;
  }

  /**
   * Get the list of normal (not composed) child folder links
   *
   * @return a list of SimpleFolderForm
   */
  public List<SimpleFolderForm> getNormalChildren() {
    if (normalChildren == null) {
      initLinks();
    }
    return normalChildren;
  }

  /**
   * Get the list of parent folder links
   *
   * @param nbParents the number to maximum parents in the list
   * @return a list of folders name
   */
  public List<String> getParentsName(int nbParents) {
    List<SimpleFolderForm> parents = getParents();
    parents = (parents.size() > nbParents ? parents.subList(0, nbParents) : parents);
    return parents.stream().map(SimpleFolderForm::getName).collect(Collectors.toList());
  }

  /**
   * Get the list of child folder links
   *
   * @param nbChildren the number to maximum children in the list
   * @return a list of folders names
   */
  public List<String> getChildrenName(int nbChildren) {
    List<SimpleFolderForm> children = getChildren();
    children = (children.size() > nbChildren ? children.subList(0, nbChildren) : children);
    return children.stream().map(SimpleFolderForm::getName).collect(Collectors.toList());
  }

  /**
   * Get the list of complete hierachy of folder links
   *
   * @return a list of SimpleFolderForm
   */
  @JsonIgnore
  public List<SimpleFolderForm> getHierarchy() {
    if (parents == null || children == null) {
      initLinks();
    }
    parents.addAll(children);
    return parents;
  }

  /**
   * Get the folder type from which this folder originates from
   *
   * @return the folder type
   */
  public int getFolderType() {
    return folderType;
  }

  /**
   * Get the folder type name from which this folder originates from
   *
   * @return the folder type name
   */
  public String getFolderTypeName() {
    return folderTypeName;
  }

  /**
   * Get the list of debates linked with this folder (lazy loaded)
   * @param user a webdeb user
   * @return a (possibly empty) list of debates
   */
  @JsonIgnore
  public List<DebateHolder> getLinkedDebates(WebdebUser user) {
    if (debates == null) {
      debates = user.filterContributionList(folder.getDebates()).
              stream().map(d -> new DebateHolder((Debate) d, lang)).collect(Collectors.toList());
    }
    return debates;
  }

  /**
   * Get the list of debates linked with this folder (lazy loaded) and limited
   * @param user a webdeb user
   * @param limit the limit of debates in the list
   * @return a (possibly empty) list of debates
   */
  @JsonIgnore
  public List<DebateHolder> getLinkedDebates(WebdebUser user, int limit) {
    return getLinkedDebates(user).size() <= limit ? debates : debates.subList(0, limit);
  }

  /**
   * Get the list of excerpts linked with this folder (lazy loaded)
   * @param user a webdeb user
   * @return a (possibly empty) list of excerpts
   */
  @JsonIgnore
  public List<ExcerptHolder> getLinkedExcerpts(WebdebUser user) {
    if (excerpts == null) {
      excerpts = user.filterContributionList(folder.getExcerpts()).
              stream().map(e -> new ExcerptHolder((Excerpt) e, lang)).collect(Collectors.toList());
    }
    return excerpts;
  }

  /**
   * Get the list of excerpts linked with this folder (lazy loaded) and limited
   * @param user a webdeb user
   * @param limit the limit of excerpts in the list
   * @return a (possibly empty) list of excerpts
   */
  @JsonIgnore
  public List<ExcerptHolder> getLinkedExcerpts(WebdebUser user, int limit) {
    return getLinkedDebates(user).size() <= limit ? excerpts : excerpts.subList(0, limit);
  }

  /**
   * Get the list of contextualized arguments linked with this folder (lazy loaded)
   * @param user a webdeb user
   * @return a (possibly empty) list of contextualized arguments
   */
  @JsonIgnore
  public List<ArgumentContextHolder> getLinkedArgumentContexts(WebdebUser user) {
    if (argumentContexts == null) {
      argumentContexts = user.filterContributionList(folder.getContextualizedArguments()).
              stream().map(a -> new ArgumentContextHolder((ArgumentContext) a, lang)).collect(Collectors.toList());
    }
    return argumentContexts;
  }

  /**
   * Get the list of contextualized arguments linked with this folder (lazy loaded) and limited
   * @param user a webdeb user
   * @param limit the limit of contextualized arguments in the list
   * @return a (possibly empty) list of contextualized arguments
   */
  @JsonIgnore
  public List<ArgumentContextHolder> getLinkedArgumentContexts(WebdebUser user, int limit) {
    return getLinkedArgumentContexts(user).size() <= limit ? argumentContexts : argumentContexts.subList(0, limit);
  }

  /**
   * Get the texts that are linked with this folder (lazy loaded)
   *
   * @param user a webdeb user
   * @return a possibly empty list of texts
   */
  @JsonIgnore
  public List<TextHolder> getLinkedTexts(WebdebUser user) {
    if (texts == null) {
      texts = user.filterContributionList(folder.getTexts()).
              stream().map(t -> new TextHolder((Text) t, user.getId(), lang)).collect(Collectors.toList());
    }
    return texts;
  }

  /**
   * Get the texts that are linked with this folder (lazy loaded) and limited
   *
   * @param user a webdeb user
   * @param limit the limit of texts in the list
   * @return a possibly empty list of texts
   */
  @JsonIgnore
  public List<TextHolder> getLinkedTexts(WebdebUser user, int limit) {
    return getLinkedTexts(user).size() <= limit ? texts : texts.subList(0, limit);
  }

  /**
   * Get the folder names
   *
   * @return the possibly empty list of folder names
   */
  public List<FolderNameForm> getFolderNames() {
    return (folderNames != null ? folderNames : new ArrayList<>());
  }

  /**
   * Get the folder names limited by number
   *
   * @param limit the maximum number of names in the returned list
   * @return the possibly empty list of folder names
   */
  public List<FolderNameForm> getFolderNames(int limit) {
    if(folderNames == null){
      return new ArrayList<>();
    }
    return (limit < folderNames.size() ? folderNames.subList(0, limit) : folderNames);
  }

  /**
   * Get the rewording folder names
   *
   * @return the possibly empty list of rewording folder names
   */
  public List<FolderNameForm> getFolderRewordingNames() {
    return (folderRewordingNames != null ? folderRewordingNames : new ArrayList<>());
  }

  /**
   * Get the folder rewording name limited by number
   *
   * @param limit the maximum number of names in the returned list
   * @return the possibly empty list of folder names
   */
  public List<FolderNameForm> getFolderRewordingNames(int limit) {
    getFolderRewordingNames();
    return (limit < folderRewordingNames.size() ? folderRewordingNames.subList(0, limit) : folderRewordingNames);
  }

  /**
   * Get the parents hierarchy of this folder as JSON
   *
   * @return the parents hierarchy as json
   */
  @JsonIgnore
  public String getParentsHierarchyAsJson() {
    return folder.getParentsHierarchyAsJson();
  }

  /**
   * Get the children hierarchy of this folder as JSON
   *
   * @return the children hierarchy as json
   */
  @JsonIgnore
  public String getChildrenHierarchyAsJson() {
    return folder.getChildrenHierarchyAsJson();
  }

  /**
   * Get the number of contributions contained in this folder
   *
   * @return the number of contributions contained in this folder
   */
  public int getNbContributions(){
    return nbContributions;
  }

}
