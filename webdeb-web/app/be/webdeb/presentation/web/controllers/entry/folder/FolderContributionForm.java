/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.folder;

/**
 * Simple form class that contains all attributes to link a contribution to a folder
 *
 * @author Martin Rouffiange
 */
public class FolderContributionForm {

  /*
   * Link attributes
   */
  protected Long folderId;
  protected Long contributionId;
  protected String contributionName;

  /**
   * Construct a FolderContributionForm for a given folder and contribution
   *
   * @param folderId a folder id
   * @param contributionId a contribution id
   */
  public FolderContributionForm(Long folderId, Long contributionId) {
    this.folderId = folderId;
    this.contributionId = contributionId;
  }

  @Override
  public String toString() {
    return "folder [" + folderId + "], contribution [" + contributionId + "]";
  }

  /*
   * GETTERS
   */

  public Long getFolderId() {
    return folderId;
  }

  public Long getContributionId() {
    return contributionId;
  }

  public String getContributionName() {
    return contributionName;
  }
}
