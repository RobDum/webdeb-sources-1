/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.argument;

import be.webdeb.core.api.argument.ArgumentFactory;
import be.webdeb.core.api.argument.ArgumentShadedLink;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.presentation.web.controllers.entry.ContributionHolder;
import play.api.Play;
import play.data.validation.ValidationError;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class holds supertype wrapper for every type of argument links.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public abstract class BaseLinkForm extends ContributionHolder {

  // custom logger
  protected static final org.slf4j.Logger logger = play.Logger.underlying();

  @Inject
  protected ArgumentFactory factory = Play.current().injector().instanceOf(ArgumentFactory.class);

  // Note: as for all wrappers, all fields MUST hold empty values for proper form validation
  protected Long originId = -1L;
  protected String originTitle = "";

  protected Long destinationId = -1L;
  protected String destinationTitle = "";

  protected String linktype = "";
  protected String linkshade = "";
  protected int linkshadeId;

  protected EArgumentLinkShade shade;

  /**
   * Play / JSON compliant constructor
   */
  public BaseLinkForm() {
    super();
  }

  /**
   * Constructor from a given contribution
   *
   * @param contribution a contribution
   * @param lang 2-char ISO code of context language (among play accepted languages)
   */
  public BaseLinkForm(Contribution contribution, String lang) {
    super(contribution, lang);
  }

  /**
   * Check if all fields are empty
   *
   * @return true if all fields of this are considered as empty (@see Values.isBlank)
   */
  public boolean empty() {
    return (values.isBlank(destinationTitle) || "-1".equals(destinationTitle))
        && (values.isBlank(linktype) || "-1".equals(linktype))
        && (values.isBlank(linkshade) || "-1".equals(linkshade));
  }

  /**
   * Form validation (implicit call from form submit)
   *
   * @return a map of ValidationError if any error in form was found, null otherwise
   */
  public Map<String, List<ValidationError>> validate() {
    Map<String, List<ValidationError>> errors = new HashMap<>();
    if (!empty()) {
      if (!values.isNumeric(destinationTitle)) {
        List<ValidationError> list = new ArrayList<>();
        list.add(new ValidationError("destinationArgument", "argument.links.error.destination"));
        errors.put("destinationArgument", list);
      }

      if (!values.isNumeric(linktype)) {
        List<ValidationError> list = new ArrayList<>();
        list.add(new ValidationError("linktype", "argument.links.error.linktype"));
        errors.put("linktype", list);
      }

      if (!values.isNumeric(linkshade)) {
        List<ValidationError> list = new ArrayList<>();
        list.add(new ValidationError("linkshade", "argument.links.error.linkshade"));
        errors.put("linkshade", list);
      }
    }

    // must return null if errors is empty
    return errors.isEmpty() ? null : errors;
  }

  /**
   * Save this LinkForm in persistence layer
   *
   * @param contributor the contributor id that created this link
   *
   * @throws FormatException if any field contains error
   * @throws PermissionException if given contributor may not perform this action or if such action would cause
   * integrity problems
   * @throws PersistenceException if any error occurred at the persistence layer (concrete error is wrapped)
   *
   */
  public void save(Long contributor) throws FormatException, PermissionException, PersistenceException {
    logger.debug("try to save link from argument " + toString() + " with version " + version + " in group " + inGroup);
    toLink().save(contributor, inGroup);
  }

  /**
   * Transform this form into an API illustration link
   *
   * @return an API illustration link corresponding to this illustration link form
   * @throws PersistenceException if given linkshade could not be casted into an int value
   */
  public abstract ArgumentShadedLink toLink() throws PersistenceException ;

  @Override
  public MediaSharedData getMediaSharedData() {
    return null;
  }

  @Override
  public String getContributionDescription() {
    return null;
  }

  @Override
  public String getDefaultAvatar(){
    return "";
  }

  @Override
  public String toString() {
    return "link from " + originTitle + "[" + originId + "] to " + destinationTitle + "[" + destinationId +
            "] with type " + linktype + ", shade " + linkshade;
  }

  /*
   * GETTERS
   */

  /**
   * Get the id of the origin of the link (argument or contextualized argument)
   *
   * @return  an argument id
   */
  public Long getOriginId() {
    return originId;
  }

  /**
   * Get the id of the destination of the link (argument or contextualized argument or excerpt)
   *
   * @return an argument or excerpt id
   */
  public Long getDestinationId() {
    return destinationId;
  }

  /**
   * Get the title of the origin (argument title)
   *
   * @return an argument title
   */
  public String getOriginArgument() {
    return originTitle;
  }

  /**
   * Get the title of the origin (argument or excerpt title)
   *
   * @return a text excerpt or argument title
   */
  public String getDestinationArgument() {
    return destinationTitle;
  }

  /**
   * Get the linktype label (language specific)
   *
   * @return a linktype label
   */
  public String getLinktype() {
    return linktype;
  }

  /**
   * Get the link shade label (language specific)
   *
   * @return a shade label
   */
  public String getLinkshade() {
    return linkshade;
  }

  /**
   * Get the link shade id
   *
   * @return a shade id
   */
  public int getLinkshadeId() {
    return linkshadeId;
  }

  /**
   * Get the link shade as EArgumentLinkShade
   *
   * @return a link shade
   * @see  EArgumentLinkShade
   */
  public EArgumentLinkShade getShade() {
    return shade;
  }

  /*
   * SETTERS
   */

  /**
   * Set the id of the origin of the link (argument or contextualized argument)
   *
   * @param originId an argument id
   */
  public void setOriginId(Long originId) {
    this.originId = originId;
  }

  /**
   * Set  the id of the destination of the link (argument or contextualized argument or excerpt)
   *
   * @param destinationId an argument id
   */
  public void setDestinationId(Long destinationId) {
    this.destinationId = destinationId;
  }

  /**
   * Set the title of the origin (argument title)
   *
   * @param originTitle an argument title
   */
  public void setOriginArgument(String originTitle) {
    this.originTitle = originTitle;
  }

  /**
   * Set the title of the origin (argument or excerpt title)
   *
   * @param destinationTitle a text excerpt or argument title
   */
  public void setDestinationArgument(String destinationTitle) {
    this.destinationTitle = destinationTitle;
  }

  /**
   * Set the linktyp id (as string)
   *
   * @param linktype either "0" (similarity) or "1" (justification)
   */
  public void setLinktype(String linktype) {
    this.linktype = linktype;
  }

  /**
   * Set the link shade
   *
   * @param linkshade a linkshade id (as String)
   * @see EArgumentLinkShade
   */
  public void setLinkshade(String linkshade) {
    this.linkshade = linkshade;
  }

}
