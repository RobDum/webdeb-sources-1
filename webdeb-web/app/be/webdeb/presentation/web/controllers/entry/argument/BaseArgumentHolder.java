/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.argument;

import be.webdeb.core.api.argument.*;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.Language;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.presentation.web.controllers.entry.ContributionHolder;
import play.api.Play;

import javax.inject.Inject;

/**
 * This class holds supertype wrapper for argument and contextualized argument.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public abstract class BaseArgumentHolder extends ContributionHolder {

    @Inject
    protected ArgumentFactory argumentFactory = Play.current().injector().instanceOf(ArgumentFactory.class);

    // used for lazy loading of argument and text title
    protected Long argumentDictionaryId = -1L;
    protected String language;
    protected String languageCode;
    protected String title = "";
    protected String fullTitle = "";
    // list of texts having the same title
    protected boolean isNotSame = false;

    // type, subtype, etc are initialized with -1 instead of empty values
    protected String shade = "-1";

    /**
     * Play / JSON compliant constructor
     */
    public BaseArgumentHolder() {
        super();
    }

    /**
     * Constructor from a given contribution
     *
     * @param contribution a contribution
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    public BaseArgumentHolder(Contribution contribution, String lang) {
        super(contribution, lang);
    }

    /**
     * Get the carto box background color in terms of link shade
     *
     * @param shade the EArgumentLinkShade
     * @return the correct class to color in terms of link shade
     */
    public static String getCartoColor(EArgumentLinkShade shade){
        switch(shade){
            case SIMILAR:
            case SUPPORTS :
            case ILLUSTRATE:
                return "arg-carto-green";
            case SHADED_EXAMPLE:
                return "arg-carto-orange";
            default :
                return "arg-carto-red";
        }
    }

    /**
     * Get the shade color in terms of link shade
     *
     * @param shade the EArgumentLinkShade
     * @return the correct class to color in terms of link shade
     */
    public static String getShadeColor(EArgumentLinkShade shade){
        switch(shade){
            case SIMILAR:
            case SUPPORTS :
            case ILLUSTRATE:
                return "similar";
            case SHADED_EXAMPLE:
                return "qualifies";
            default :
                return "opposes";
        }
    }

    public Argument getArgument(Long argumentId, Long contributor, EArgumentShade debateShade, String argType) throws PersistenceException, PermissionException, FormatException {
        // attached the newly created debate to argument
        Argument argument = argumentFactory.retrieve(argumentId);

        if (argument == null || !title.equals(argument.getDictionary().getTitle()) || !argType.equals(String.valueOf(argument.getArgumentType().getArgumentShade()))) {
            argument = argument == null ? argumentFactory.getArgument() : argument;
            ArgumentDictionary dictionary = argumentFactory.getArgumentDictionary();
            dictionary.setId(argumentDictionaryId);
            argument.addInGroup(inGroup);
            dictionary.setTitle(values.correctArgumentTitle(title));

            // argument type
            try {
                argument.setArgumentType(argumentFactory.getArgumentType(debateShade != null ? debateShade.id() : Integer.parseInt(shade)));
            } catch (NumberFormatException | FormatException e) {
                argument.setArgumentType(argumentFactory.getArgumentType(EArgumentShade.NO_DOUBT.id()));
            }
            dictionary.setLanguage(argumentFactory.getLanguage(languageCode));
            argument.setDictionary(dictionary);
            argument.save(contributor, inGroup);
        }
        return argument;
    }

    /*
     * GETTERS
     */

    /**
     * Get the argument title of this argument
     *
     * @return an argument title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Get the argument full title (shade and title)
     *
     * @return an argument full title
     */
    public String getFullTitle() {
        return fullTitle;
    }

    /**
     * Get the argument language
     *
     * @return the language description in this.lang
     *
     * @see Language
     */
    public String getLanguage() {
        return language;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    /**
     * Get the argument shade type (see ArgumentType interface)
     *
     * @return an argument shade label (language-specific)
     */
    public String getShade() {
        return shade;
    }

    /**
     * Check whether this argument has been explicitly flagged as a new one (used when other arguments exists with
     * same title)
     *
     * @return true if this argument is a new one despite it has the same argument as another one in db
     */
    public boolean getIsNotSame() {
        return isNotSame;
    }

    /**
     * Set whether this argument has been explicitly flagged as a new one (used when other arguments exists with
     * same title)
     *
     * @param notSame true if this argument is a new one despite it has the same title as another one in db
     */
    public void setIsNotSame(boolean notSame) {
        isNotSame = notSame;
    }


}
