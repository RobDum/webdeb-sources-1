/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.argument;

import be.webdeb.core.api.argument.*;
import be.webdeb.core.api.contribution.*;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.presentation.web.controllers.entry.PlaceForm;
import be.webdeb.presentation.web.controllers.entry.actor.ActorSimpleForm;
import be.webdeb.presentation.web.controllers.entry.folder.SimpleFolderForm;
import play.data.validation.ValidationError;

import java.util.*;
import java.util.stream.Collectors;


/**
 * Wrapper class for forms used to encode a new contextualized argument into the database.
 *
 * Note that all supertype getters corresponding to predefined values (ie, types) are sending
 * ids instead of language-dependent descriptions.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class ArgumentContextForm extends ArgumentContextHolder {

    protected ArgumentForm argumentForm;
    protected Long argumentDictionnaryId = -1L;
    protected boolean withLink = false;
    protected String shadeLink;

    /**
     * Play / JSON compliant constructor
     */
    public ArgumentContextForm() {
        super();
        this.argumentForm = new ArgumentForm();
    }


    /**
     * Create a new contextualized argument wrapper for a given lang and a contribution context id
     *
     * @param contextId the context contribution id
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    public ArgumentContextForm(Long contextId, String lang) {
        this(-1L, contextId, lang);
    }

    /**
     * Create a new contextualized argument wrapper for a given lang and a contribution context id
     *
     * @param contextId the context contribution id
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    public ArgumentContextForm(Long contextId, String lang, boolean withLink) {
        this(-1L, contextId, lang);
        this.withLink = withLink;
    }

    /**
     * Create a new contextualized argument wrapper for a given argument id and a contribution context id
     *
     * @param argumentId an argument id
     * @param contextId the context contribution id
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    public ArgumentContextForm(Long argumentId, Long contextId, String lang) {
        this.contextId = contextId;
        this.lang = lang;
        this.argumentId = argumentId;
    }

    /**
     * Create a new contextualized argument wrapper for a given argument id and a contribution context id
     *
     * @param argumentId an argument id
     * @param contextId the context contribution id
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    public ArgumentContextForm(Long argumentId, Long contextId, String shade, String title, List<SimpleFolderForm> folders, List<PlaceForm> places, String lang) {
        this(argumentId, contextId, lang);
        this.lang = lang;
        this.contextId = contextId;
        this.shade = shade;
        this.title = title;
        setFolders(folders);
        setPlaces(places);
    }

    /**
     * Create a contextualized argument form from a given argumentContext
     *
     * @param argumentContext a contextualized argument
     * @param lang 2-char ISO code of context language (among play accepted languages)
     */
    public ArgumentContextForm(ArgumentContext argumentContext, String lang) {
        super(argumentContext, lang, true);

        argumentDictionnaryId = argumentContext.getArgument().getDictionary().getId();

        initFolders(argumentContext.getFoldersAsList(), lang, true);
    }

    /**
     * Form validation (implicit call from form submit)
     *
     * @return a map of ValidationError if any error in form was found, null otherwise
     */
    public Map<String, List<ValidationError>> validate() {
        return validate(true);
    }

    /**
     * Form validation (implicit call from form submit)
     *
     * @param allFieldRequired true if all field are required
     * @return a map of ValidationError if any error in form was found, null otherwise
     */
    public Map<String, List<ValidationError>> validate(boolean allFieldRequired) {
        Map<String, List<ValidationError>> errors = new HashMap<>();
        List<ValidationError> list;

        argumentForm = new ArgumentForm(argumentId, shade, title, lang);
        Map<String, List<ValidationError>> subErrors = argumentForm.validate();
        languageCode = argumentForm.getLanguageCode();
        if(subErrors != null)
            errors.putAll(subErrors);

        if (allFieldRequired && (contextId == null || contextId == -1L)) {
            errors.put("contextId", Collections.singletonList(new ValidationError("contextId", "argument.context.error.contextId")));
        }

        if (withLink && !values.isNumeric(shadeLink, 0, true)) {
            errors.put("shadeLink", Collections.singletonList(new ValidationError("shadeLink", "argument.error.shade")));
        }

        // check if there is not the same place
        if(places != null){
            subErrors = checkPlaces(places);
            if(subErrors != null) errors.putAll(subErrors);
        }

        if (folders == null || folders.isEmpty() || values.isBlank(folders.get(0).getName())) {
            String fieldName = "folders[0].name";
            errors.put(fieldName, Collections.singletonList(new ValidationError(fieldName, "text.error.folder.name")));
        }else{
            subErrors = checkFolders(folders, "folders");
            if(subErrors != null) errors.putAll(subErrors);
        }

        // if we have cited actors
        if (citedactors != null) {
            citedactors.stream().filter(a -> values.isBlank(a.getLang())).forEach(a -> a.setLang(lang));
            list = helper.checkActors(citedactors, "cited");
            if (list != null) {
                list.forEach(e -> errors.put(e.key(), Collections.singletonList(new ValidationError(e.key(), e.message()))));
            }
        }

        // must return null if errors is empty
        return errors.isEmpty() ? null : errors;
    }

    /**
     * Save a contextualized argument into the database. This id is updated if it was not set before.
     *
     * @param contributor the contributor id that ask to save this contribution
     * @return the map of Contribution type and a list of contribution (actors or folders) that have been created during
     * this insertion(for all unknown contributions), an empty list if none had been created
     *
     * @throws FormatException if this contribution has invalid field values (should be pre-checked before-hand)
     * @throws PermissionException if given contributor may not perform this action or if such action would cause
     * integrity problems
     * @throws PersistenceException if any error occurred at the persistence layer (concrete error is wrapped)
     */
    public Map<Integer, List<Contribution>> save(Long contributor) throws FormatException, PermissionException, PersistenceException {
        logger.debug("try to save contextualized argument " + id + " " + toString() + " with version " + version + " in group " + inGroup);

        ArgumentContext argumentContext = argumentFactory.getContextualizedArgument();
        argumentContext.setVersion(version);
        argumentContext.addInGroup(inGroup);

        argumentContext.setContextId(contextId);
        argumentContext.setArgumentId(argumentForm.getId());

        Argument argument = getArgument(argumentId, contributor, null, argumentForm.argtype);

        // folders
        argumentContext.initFolders();
        for (SimpleFolderForm f : folders.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList()))  {
            try {
                argumentContext.addFolder(helper.toFolder(f, lang));
            } catch (FormatException e) {
                logger.error("unparsable folder " + f, e);
            }
        }

        // bound places
        argumentContext.initPlaces();
        for (PlaceForm place : places) {
            if(!place.isEmpty()) {
                Place placeToAdd = createPlaceFromForm(place);
                if(placeToAdd != null)
                    argumentContext.addPlace(placeToAdd);
            }
        }

        // bound cited actors
        argumentContext.getActors();
        for (ActorSimpleForm a : citedactors.stream().filter(n -> !n.isEmpty()).collect(Collectors.toList())) {
            a.setCited(true);
            argumentContext.addActor(helper.toActorRole(a, argument, lang));
        }

        argumentContext.setId(id != null ? id : -1L);
        argumentContext.setArgumentId(argument.getId());
        argumentContext.setArgument(argument);
        // save this contextualized argument
        Map<Integer, List<Contribution>> result = argumentContext.save(contributor, inGroup);
        // update this id (in case of creation
        id = argumentContext.getId();
        return result;
    }

    /*
     * GETTERS / SETTERS
     */

    public void setArgument(ArgumentForm argument) {
        this.argument = argument;
    }

    public void setContextId(Long contextId) {
        this.contextId = contextId;
    }

    public void setContextTitle(String contextTitle) {
        this.contextTitle = contextTitle;
    }

    public void setArgumentId(Long argumentId) {
        this.argumentId = argumentId;
    }

    /**
     * Set the title of this argument
     *
     * @param title a argument title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Set the argument shade type (see ArgumentType interface)
     *
     * @param shade an argument shade id
     */
    public void setShade(String shade) {
        this.shade = shade;
    }

    public String getShadeLink() {
        return shadeLink;
    }

    public void setShadeLink(String shadeLink) {
        this.shadeLink = shadeLink;
    }

    /**
     * Set the list of folders linked to this contextualized argument
     *
     * @param folders a list of folders
     */
    public void setFolders(List<SimpleFolderForm> folders) {
        this.folders = folders;
    }

    /**
     * Set the places concerned by this contextualized argument
     *
     * @param places a possibly empty list of places
     */
    public void setPlaces(List<PlaceForm> places) {
        this.places = places;
    }

    public Long getArgumentDictionaryId() {
        return argumentDictionaryId;
    }

    public void setArgumentDictionaryId(Long argumentDictionaryId) {
        this.argumentDictionaryId = argumentDictionaryId;
    }

    public void setLanguage(String lang){
        this.language = lang;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * Set the list of cited actors of this argument
     *
     * @param cited a list of actors being cited in this argument
     */
    public void setCitedactors(List<ActorSimpleForm> cited) {
        this.citedactors = cited;
    }
}
