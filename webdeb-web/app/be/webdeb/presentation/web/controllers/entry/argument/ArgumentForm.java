/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.argument;

import be.webdeb.core.api.argument.Argument;
import be.webdeb.core.api.argument.ArgumentDictionary;
import be.webdeb.core.api.contribution.*;
import be.webdeb.core.api.contributor.Group;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.presentation.web.controllers.entry.EFilterName;
import play.data.validation.ValidationError;
import play.i18n.Lang;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Wrapper class for forms used to encode a new argument into the database.
 *
 * Note that all supertype getters corresponding to predefined values (ie, types) are sending
 * ids instead of language-dependent descriptions.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class ArgumentForm extends ArgumentHolder {

  // suggestions from WDTAL-argument service
  private String suggestedType = "";

  /**
   * Play / JSON compliant constructor
   */
  public ArgumentForm() {
    super();
  }

  /**
   * Create a new argument wrapper for a given argument title and user lang
   *
   * @param argumentId the id of the argument
   * @param shade the argument shade
   * @param title the argument title
   * @param lang the user language
   */
  public ArgumentForm(Long argumentId, String shade, String title, String lang) {
    super();
    this.id = argumentId;
    this.shade = shade;
    this.title = title;
    this.lang = lang;
  }

  /**
   * Create an argument form from a given argument
   *
   * @param argument an argument
   * @param lang 2-char ISO code of context language (among play accepted languages)
   */
  public ArgumentForm(Argument argument, String lang) {
    // must be set manually, may not call super(argument, lang) constructor because their handling is too different
    id = argument.getId();
    type = EContributionType.ARGUMENT;
    addFilterable(EFilterName.CTYPE, i18n.get(Lang.forCode(lang), "general.filter.ctype."+ type.id()));
    version = argument.getVersion();
    validated = argument.getValidated().getEState();
    groups = argument.getInGroups().stream().map(Group::getGroupId).collect(Collectors.toList());

    this.argument = argument;
    this.lang = lang;
    argtype = String.valueOf(argument.getArgumentType().getArgumentType());
    shade = String.valueOf(argument.getArgumentType().getArgumentShade());
    shadeterm = argument.getArgumentType().getShadeName(lang);
    addFilterable(EFilterName.ARGTYPE, shadeterm);

    title = argument.getDictionary().getTitle();
    fullTitle = argument.getFullTitle();
    language = argument.getDictionary().getLanguage().getName(lang);
    languageCode = argument.getDictionary().getLanguage().getCode();
    addFilterable(EFilterName.LANGUAGE, language);
  }

  /**
   * Form validation (implicit call from form submit)
   *
   * @return a map of ValidationError if any error in form was found, null otherwise
   */
  public Map<String, List<ValidationError>> validate() {
    Map<String, List<ValidationError>> errors = new HashMap<>();


    if (!values.isNumeric(shade, 0, true)) {
      errors.put("shade", Collections.singletonList(new ValidationError("shade", "argument.error.shade")));
    }

    if (values.isBlank(title)) {
      errors.put("title", Collections.singletonList(new ValidationError("title", "argument.error.title.blank")));
    }else if(title.length() > TITLE_MAX_LENGTH){
      errors.put("title", Collections.singletonList(new ValidationError("title", "argument.error.title.size")));
    }

    if (errors.isEmpty()) {
      // all good, compute the standardForm
      // set real shade values in case of argument types 2 or 3
      try {
        fullTitle = argumentFactory.computeShadeAndTitle(argumentFactory.getArgumentType(Integer.parseInt(shade)), title, languageCode);
      } catch (FormatException e) {
        logger.warn("cannot compute title (and this should not happen)", e);
      }
    }


    // in the case of nlp service problem, set the text language as the user language
    if (values.isBlank(languageCode)) {
      languageCode = lang;
    }

    // must return null if errors is empty
    return errors.isEmpty() ? null : errors;
  }

  /**
   * Save an argument into the database. This id is updated if it was not set before.
   *
   * @param contributor the contributor id that ask to save this contribution
   * @return the map of Contribution type and a list of contribution (actors or folders) that have been created during
   * this insertion(for all unknown contributions), an empty list if none had been created
   *
   * @throws FormatException if this contribution has invalid field values (should be pre-checked before-hand)
   * @throws PermissionException if given contributor may not perform this action or if such action would cause
   * integrity problems
   * @throws PersistenceException if any error occurred at the persistence layer (concrete error is wrapped)
   */
  public Map<Integer, List<Contribution>> save(Long contributor) throws FormatException, PermissionException, PersistenceException {
    logger.debug("try to save argument " + id + " " + toString() + " with version " + version + " in group " + inGroup);

    Argument argument = argumentFactory.getArgument();
    argument.setId(id != null ? id : -1L);
    argument.setVersion(version);
    argument.addInGroup(inGroup);

    ArgumentDictionary dictionary = argumentFactory.getArgumentDictionary();

    // remove ending "." if any
    title =  title.trim();
    if ( title.endsWith(".")) {
      title =  title.substring(0,  title.lastIndexOf('.'));
    }
    dictionary.setTitle( title);

    // argument type
    try {
      argument.setArgumentType(argumentFactory.getArgumentType(Integer.valueOf(shade)));
    } catch (NumberFormatException | FormatException e) {
      logger.error("unable to set argument shade " + shade);
      throw new PersistenceException(PersistenceException.Key.SAVE_ARGUMENT, e);
    }

    // argument language
    try {
      dictionary.setLanguage(argumentFactory.getLanguage(languageCode));
    } catch (FormatException e) {
      logger.error("unknown language code " + languageCode, e);
    }
    argument.setDictionary(dictionary);

    // save this argument
    Map<Integer, List<Contribution>> result = argument.save(contributor, inGroup);
    // update this id (in case of creation
    id = argument.getId();
    return result;
  }

  /*
   * GETTERS / SETTERS
   */

  /*
   * own fields
   */

  /**
   * Get the suggested type (from WDTAL external service) (see ArgumentType interface)
   *
   * @return an argument type id
   */
  public String getSuggestedType() {
    return suggestedType;
  }

  /**
   * Set the suggested type id (from WDTAL external service) (see ArgumentType interface)
   *
   * @param suggestedType a type id
   */
  public void setSuggestedType(String suggestedType) {
    this.suggestedType = suggestedType;
  }

  /*
   * other supertype fields
   */

  /**
   * Set the title of this argument
   *
   * @param title a argument title
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * Set the full title of this argument (shade and title)
   *
   * @param fullTitle a argument full title
   */
  public void setFullTitle(String fullTitle) {
    this.fullTitle = fullTitle;
  }

  /**
   * Set the argument type id (see ArgumentType interface)
   *
   * @param argtype an argument type id
   */
  public void setArgtype(String argtype) {
    this.argtype = argtype;
  }

  /**
   * Set the argument shade type (see ArgumentType interface)
   *
   * @param shade an argument shade id
   */
  public void setShade(String shade) {
    this.shade = shade;
  }

  /**
   * Set the shade term (language specific)
   *
   * @param shadeterm the shade term
   */
  public void setShadeterm(String shadeterm) {
    this.shadeterm = shadeterm;
  }

  /**
   * Set the argument language id
   *
   * @param language a language id
   * @see Language
   */
  public void setLanguage(String language) {
    if(lang == null){
      lang = language;
    }
    this.language = language;
  }

  public void setLanguageCode(String languageCode) {
    this.languageCode = languageCode;
  }

  public Long getArgumentDictionaryId() {
    return argumentDictionaryId;
  }

  public void setArgumentDictionaryId(Long argumentDictionaryId) {
    this.argumentDictionaryId = argumentDictionaryId;
  }
}
