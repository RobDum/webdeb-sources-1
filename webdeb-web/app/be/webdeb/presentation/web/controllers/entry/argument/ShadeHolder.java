/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.argument;

import be.webdeb.core.api.argument.EArgumentLinkShade;

/**
 * Hold an argument/link shade (for dynamic drop down lists passed by Json).
 *
 * @author Fabian Gilson
 */
public class ShadeHolder {

  private String id;
  private String shade;

  /**
   * Play / JSON compliant constructor
   */
  public ShadeHolder() {
    // needed by play
  }

  /**
   * Construct a ShadeHolder
   *
   * @param id the shade id
   * @param shade the shade description
   */
  public ShadeHolder(String id, String shade) {
    this.id = id;
    this.shade = shade;
  }

  /*
     * GETTERS / GETTERS
   */

    /**
   * Get the argument/link shade id
   *
   * @return a shade id
   * @see be.webdeb.core.api.argument.ArgumentType
   * @see EArgumentLinkShade
   */
  public String getId() {
    return id;
  }

    /**
   * Set the argument shade id
   *
   * @param id a shade id
   * @see be.webdeb.core.api.argument.ArgumentType
   * @see EArgumentLinkShade
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Get the shade label
   *
   * @return a shade label (language specific)
   */
  public String getShade() {
    return shade;
  }

  /**
   * Set the shade label
   *
   * @param shade a shade label (language specific)
   */
  public void setShade(String shade) {
    this.shade = shade;
  }
}
