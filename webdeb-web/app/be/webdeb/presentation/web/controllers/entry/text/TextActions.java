/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.entry.text;

import be.objectify.deadbolt.java.actions.Restrict;
import be.webdeb.application.nlphelper.WDTALAnnotator;
import be.webdeb.application.query.BadQueryException;
import be.webdeb.application.query.EQueryKey;
import be.webdeb.core.api.actor.Actor;
import be.webdeb.core.api.actor.ActorFactory;
import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentJustification;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.contribution.ContextContribution;
import be.webdeb.core.api.debate.DebateFactory;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.excerpt.ExternalExcerpt;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contributor.Contributor;
import be.webdeb.core.api.text.ETextVisibility;
import be.webdeb.core.api.text.ExternalText;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.fs.FileSystem;
import be.webdeb.infra.ws.external.excerpt.ExcerptRequest;
import be.webdeb.infra.ws.external.text.TextRequest;
import be.webdeb.infra.ws.nlp.RequestProxy;
import be.webdeb.presentation.web.controllers.CommonController;
import be.webdeb.presentation.web.controllers.EDataSendType;
import be.webdeb.presentation.web.controllers.entry.actor.ActorSimpleForm;
import be.webdeb.presentation.web.controllers.entry.argument.JustificationLinkForm;
import be.webdeb.presentation.web.controllers.entry.debate.DebateHolder;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptHolder;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptSimpleForm;
import be.webdeb.presentation.web.controllers.permission.WebdebRole;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import be.webdeb.presentation.web.controllers.viz.EVizPane;
import be.webdeb.presentation.web.controllers.SessionHelper;
import be.webdeb.presentation.web.controllers.viz.EVizPaneName;
import be.webdeb.presentation.web.controllers.viz.argument.ArgumentContextVizHolder;
import be.webdeb.presentation.web.controllers.viz.text.TextVizHolder;
import be.webdeb.presentation.web.views.html.entry.text.editText;
import be.webdeb.presentation.web.views.html.entry.text.textExcerpts;
import be.webdeb.presentation.web.views.html.util.message;
import be.webdeb.presentation.web.views.html.util.displayTextContentModal;
import be.webdeb.presentation.web.views.html.viz.privateContribution;
import be.webdeb.presentation.web.views.html.viz.text.*;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import play.cache.CacheApi;
import play.data.Form;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import javax.inject.Inject;
import java.io.File;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;


/**
 * Handle submissions from web forms for Text-related actions.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
// TODO create new controller ContextActions and put all methods for context contributions presents here
public class TextActions extends CommonController {
  
  @Inject
  private FileSystem files;

  @Inject
  protected RequestProxy proxy;

  @Inject
  private WDTALAnnotator annotator;

  @Inject
  private CacheApi cache;

  private TextVizHolder viz;

  private static final String NOTFOUND = "text.args.notfound";

  /**
   * Get the text edition page for given text id
   *
   * @param id a text id (-1 or null for a new text)
   * @return the text form to either add a new text or modify an existing one
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> edit(Long id) {
    logger.debug("GET edit text page " + id);
    Map<String, String> messages = new HashMap<>();
    WebdebUser user = sessionHelper.getUser(ctx());
    Text text = textFactory.retrieve(id);
    ExternalText externalText = null;
    TextForm form;

    if (text != null) {
      if(!user.mayView(text)){
        return CompletableFuture.completedFuture(Results.unauthorized(
                privateContribution.render(user)));
      }
      form = new TextForm(text, user.getId(), ctx().lang().code());
      form.setInGroup(sessionHelper.getCurrentGroup(ctx()));
      if (!form.getMayViewContent()) {
        messages.put(SessionHelper.INFO, i18n.get(ctx().lang(), "permission.text.not.visible"));
      }

    } else {
      if (id != -1L) {
        externalText = textFactory.retrieveExternal(user.getId(), id);
        if(externalText == null || (externalText.getInternalContribution() != null && externalText.getInternalContribution() != -1))
          messages.put(SessionHelper.ERROR, i18n.get(ctx().lang(), NOTFOUND));
      }
      // set "from internet" to true by default for new texts and set current group
      form = new TextForm(externalText, ctx().lang().code());
      // set default group
      form.setInGroup(sessionHelper.getCurrentGroup(ctx()));
      // set current language
      form.setLang(ctx().lang().code());
      // set default visibility to private
      form.setTextVisibility(String.valueOf(ETextVisibility.PEDAGOGIC.id()));
      // set mayViewContent to true by default (otherwise form will be blocked)
      form.setMayViewContent(true);
    }

    return CompletableFuture.supplyAsync(() ->
        ok(editText.render(formFactory.form(TextForm.class).fill(form), helper, user, messages)), context.current());
  }

  /**
   * Update or save a text into the database.
   *
   * @param id the text id (-1 of null for a new text)
   * @return either the addText page if form contains error, if it appears to already exist or if author(s) seems to be
   * known and must be disambiguated (400 response), its visualisation page if the action succeeded, or
   * redirect to main entry page if an error occurred.
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public synchronized CompletionStage<Result> save(Long id) {
    logger.debug("POST save text " + id);
    Form<TextForm> form = formFactory.form(TextForm.class).bindFromRequest();
    Map<String, String> messages = new HashMap<>();
    WebdebUser user = sessionHelper.getUser(ctx());

    // handle file upload
    Http.MultipartFormData<File> body = request().body().asMultipartFormData();
    Optional<Http.MultipartFormData.FilePart<File>> picture =
            body.getFiles().stream().filter(f -> f != null && f.getFile() != null && f.getFile().length() > 0).findFirst();

    File file = null;
    if (picture.isPresent()) {
      file = picture.get().getFile();
    }

    // sends back form if there are some errors
    if (form.hasErrors()) {
      logger.debug("form has errors " + form.errors().toString());
      // save file in temp fs
      if (file != null) {
        files.saveToCache(file, form.data().get("filename"));
      }
      messages.put(SessionHelper.WARNING, i18n.get(ctx().lang(),SessionHelper.ERROR_FORM));
      return CompletableFuture.supplyAsync(() ->
              badRequest(editText.render(form, helper, sessionHelper.getUser(ctx()), messages)), context.current());
    }
    TextForm text = form.get();
    // check if we do not have a text with same title or url
    if (values.isBlank(text.getId()) && !text.getIsNotSame()) {
      Set<Text> same = new HashSet<>(textFactory.findByTitle(text.getTitle()));
      if(values.isURL(text.getUrl())) {
        Text candidate = textFactory.findByUrl(text.getUrl());
        if (candidate != null)
          same.add(candidate);
      }

      if (!same.isEmpty()) {
        // send back form to ask if the found texts are not this one
        text.setCandidates(new ArrayList<>(same));
        // save file in temp fs
        if (file != null) {
          files.saveToCache(file, form.data().get("filename"));
        }
        return CompletableFuture.supplyAsync(() ->
                badRequest(editText.render(form, helper, sessionHelper.getUser(ctx()), messages)), context.current());
      }
    }
    // check name matches for authors and source authors
    if (!helper.searchForNameMatches(text.getAuthors(), ctx().lang().code()).isEmpty()
            || !helper.searchForFolderNameMatches(text.getFolders(), ctx().lang().code()).isEmpty()) {
      // save file in temp fs
      if (file != null) {
        files.saveToCache(file, form.data().get("filename"));
      }
      return CompletableFuture.supplyAsync(() ->
              badRequest(editText.render(form, helper, sessionHelper.getUser(ctx()), messages)), context.current());
    }

    // get text language if none
    if(text.getLanguage() == null || text.getLanguage().isEmpty()) {
      String toAnalyze = text.getTextarea() == null || text.getTextarea().isEmpty() ? text.getTitle() : text.getTextarea();
      text.setLanguage(values.detectTextLanguage(toAnalyze, getLanguageDetectionApiKey(), ctx().lang().code()));
    }

    // check if the functions are in DB, if isn't create them and return their id
    List<Integer> newProfessions = helper.checkIfNewProfessionsMustBeCreated(
            helper.convertActorSimpleFormToProfessionForm(text.getAuthors(), ctx().lang().code()));
    newProfessions.forEach(idP -> sessionHelper.addValue(ctx(), SessionHelper.KEY_NEWPROFESSION, idP+""));

    // store contribution and get list of unknown actors to be shown
    try {
      Long contributor = sessionHelper.getUser(ctx()).getContributor().getId();
      treatSaveContribution(text.save(contributor));
      messages.put("success", i18n.get(ctx().lang(),"entry.text.success"));

      // this may not crash since we just created it
      String textFilename = textFactory.retrieve(text.getId()).getFilename(contributor);

      // clear cache and xml annotated file to force reload of parsed content
      if (!text.getId().equals(-1L)) {
        cache.remove("annotated.raw." + text.getId());
        cache.remove("annotated.user." + text.getId());
        cache.remove("file." + textFilename + ".xml");
        cache.remove("file." + textFilename);
        files.deleteAnnotatedText(textFilename);
      }

      // launch annotation (async) if text is plain
      if (!text.getNoContent()) {
        if ("text/plain".equals(files.getContentType(textFilename))) {
          getAnnotatedText(text.getId(), true);
        } else {
          // check where is the file
          if (file == null || file.length() == 0) {
            // check in cache with form file name
            logger.debug("try to retrieve from temp cache " + text.getFilename());
            file = files.getFromCache(text.getFilename());
          }

          if (file != null) {
            logger.debug("will save external file " + text.getFilename() + " to " + textFilename);
            files.saveContributionTextFile(textFilename, file);
          }
        }
      }
      // redirect to VizActions text
      flash("success", i18n.get(ctx().lang(),"entry.text.success"));
      ExternalText externalText = textFactory.retrieveExternal(user.getId(), text.getExternalTextId());
      if(externalText != null) {
        ExternalExcerpt a = externalText.getFirstNotSavedExternalExcerpt();
        if(a != null) {
          return CompletableFuture.supplyAsync(() ->
                  redirect(be.webdeb.presentation.web.controllers.entry.excerpt.routes.ExcerptActions.edit(
                          text.getId(), a.getId())), context.current());
        }
      }
      return CompletableFuture.supplyAsync(() ->
                      redirect(be.webdeb.presentation.web.controllers.viz.routes.VizActions.text(text.getId(), EVizPane.DETAILS.id(), 0)),
              context.current());

    } catch (FormatException | PersistenceException | PermissionException e) {
      logger.error("unable to save text", e);
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(),e.getMessage()));
      return CompletableFuture.supplyAsync(() ->
              redirect(be.webdeb.presentation.web.controllers.entry.routes.EntryActions.contribute()), context.current());
    }
  }

  /**
   * Display the "textExcerpts" page for a given text
   *
   * @param id an id to a text to be retrieved from database
   * @return the "work with text" page with the given text, or a the blank page to search for
   * texts. Annotation and highlighting of text content will be performed by ajax request because it may be (very) slow.
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> textExcerpts(Long id) {
    logger.debug("GET work with text " + id);
    Map<String, String> messages = new HashMap<>();
    Text text;
    Form<ExcerptSimpleForm> form = formFactory.form(ExcerptSimpleForm.class).fill(new ExcerptSimpleForm(id));
    Form<ActorSimpleForm> actorForm = formFactory.form(ActorSimpleForm.class).fill(new ActorSimpleForm());

    // check if a valid id was given, load it with its data if any
    if (id != null && id != -1 && (text = textFactory.retrieve(id)) != null) {

      // return text annotation page
      WebdebUser user = sessionHelper.getUser(ctx());
      final TextHolder textHolder = new TextHolder(text, user.getId(), ctx().lang().code());
      if (text.isContentVisibleFor(user.getId())) {
        textHolder.textarea = textHolder.getExcerpt(user.getId(), true);
      } else {
        textHolder.textarea = i18n.get(ctx().lang(), "permission.text.not.visible");
      }

      return CompletableFuture.supplyAsync(() ->
          ok(textExcerpts.render(textHolder, form, actorForm, sessionHelper.getUser(ctx()), messages)), context.current());
    }

    // no ID was given or it has not been found in database, return an empty form
    if (id != null && id != -1) {
      logger.error("ID unfound in database " + id);
      messages.put(SessionHelper.WARNING, i18n.get(ctx().lang(),NOTFOUND));
    }
    return CompletableFuture.supplyAsync(() ->
        ok(textExcerpts.render(new TextHolder(), form, actorForm, sessionHelper.getUser(ctx()), messages)), context.current());
  }

  /**
   * Reload a text with an id passed in the POST content
   *
   * @return redirect to the "work with text" page with the id passed in the "id" body excerpt
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> reloadText() {
    Map<String, String[]> map = request().body().asFormUrlEncoded();
    long id = -1L;
    try {
      id = Long.parseLong(map.get("id")[0]);
      logger.debug("POST reload text " + id);
    } catch (NullPointerException | NumberFormatException e) {
      logger.error("POST reload text: unable to parse text id " + String.join(", ", map.get("id")), e);
      flash(SessionHelper.WARNING, i18n.get(ctx().lang(), NOTFOUND));
    }
    return textExcerpts(id);
  }

  /**
   * Ask to work with a randomly chosen text
   *
   * @return redirect to "work with text page" with a randomly chosen text
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> randomText() {
    logger.debug("GET random text");

    Text random = textFactory.random();
    if(random != null) {
      return CompletableFuture.supplyAsync(() ->
              redirect(routes.TextActions.textExcerpts(random.getId())), context.current());
    }

    return CompletableFuture.completedFuture(Results.badRequest());
  }

  /**
   * Add a "cited" actor to given text
   *
   * @param textId a text id
   * @return redirect to the textExcerpts call to display the text edit page, with a message to show the result
   * of the adding of submitted actor, if the actor name is ambiguous, the handleNameMatches will be triggered by the
   * textExcerpts page template to ask the user to choose what to do with the possibly known actor
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> addActor(Long textId) {
    logger.debug("POST add actor for text " + textId);

    Form<ActorSimpleForm> form = formFactory.form(ActorSimpleForm.class).bindFromRequest();
    if (form.hasErrors()) {
      logger.debug("form has errors " + form.errors().toString());
      flash(SessionHelper.WARNING, i18n.get(ctx().lang(),"text.args.no.actorname"));
      return CompletableFuture.supplyAsync(() ->
          redirect(routes.TextActions.textExcerpts(textId)), context.current());
    }

    Text text = textFactory.retrieve(textId);
    if (text == null) {
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(),NOTFOUND));
      return CompletableFuture.supplyAsync(() ->
          redirect(routes.TextActions.textExcerpts(-1L)), context.current());
    }

    Long contributor = sessionHelper.getUser(ctx()).getContributor().getId();
    ActorSimpleForm actorName = form.get();
    // must ensure actorname has a language code
    if (values.isBlank(actorName.getLang())) {
      actorName.setLang(ctx().lang().code());
    }

    // name matches (namesake) handling
    if (!helper.searchForNameMatches(actorName, ctx().lang().code()).isEmpty()) {
      // send back full page that will trigger the modal frame to handle matches
      logger.debug("send modal page to choose what to do with name matches for " + actorName);
      return CompletableFuture.supplyAsync(() ->
              badRequest(textExcerpts.render(new TextHolder(text, contributor, ctx().lang().code()),
                  formFactory.form(ExcerptSimpleForm.class).fill(new ExcerptSimpleForm()), form,
                  sessionHelper.getUser(ctx()), null)),
          context.current());
    }

    // check if the function is in DB, if isn't create it and return its id
    Integer autocreatedProfessions = helper.checkIfNewProfessionMustBeCreated(
        actorName.getFunctionid(), actorName.getFunction(), ctx().lang().code(), actorName.getFunctionGender());
    if(autocreatedProfessions != null && autocreatedProfessions != -1)
      sessionHelper.addValue(ctx(), SessionHelper.KEY_NEWPROFESSION, autocreatedProfessions.toString());
    actorName.setFunctionid(autocreatedProfessions);
    // check and update ids (actor/affiliation/function) and tries to bind to exisitng affiliation, if any
    actorName.checkFieldsIntegrity();

    // now bind this actor as cited actor to this text
    try {
      List<Contribution> autocreated = text.bindActor(helper.toActorRole(actorName, text, ctx().lang().code()),
          sessionHelper.getCurrentGroup(ctx()), contributor);

      // for all retrieved unknown actors, add them to session (cookie) to be shown to contributor
      sessionHelper.addValues(ctx(), SessionHelper.KEY_NEWACTOR, autocreated.stream().map(a ->
          String.valueOf(a.getId())).collect(Collectors.toList()));

    } catch (PersistenceException | PermissionException e) {
      logger.error("unable to bind actor " + actorName.toString(), e);
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(), e.getMessage()));
      return CompletableFuture.supplyAsync(() -> redirect(routes.TextActions.textExcerpts(textId)));
    }

    // all good
    flash(SessionHelper.SUCCESS, i18n.get(ctx().lang(),"text.args.actorname.added", actorName.getFullname()));
    return CompletableFuture.supplyAsync(() ->
        redirect(routes.TextActions.textExcerpts(textId)), context.current());
  }

  /**
   * Remove given "cited" actor from given text. Actor must be bound to given text.
   *
   * @param textId a text id
   * @param actorId an actor id
   * @return redirect to textExcerpt page with a message saying if given actor has been removed or not,
   * if given text does not exist, the textExcerpts will display no text
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> removeActor(Long textId, Long actorId) {

    // check if text exists
    Text text = textFactory.retrieve(textId);
    if (text == null) {
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(),NOTFOUND));
      return CompletableFuture.supplyAsync(() ->
          redirect(routes.TextActions.textExcerpts (-1L)), context.current());
    }

    // check if actor exists
    Actor actor = actorFactory.retrieve(actorId);
    if (actor == null) {
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(), "text.args.no.actorname"));
      return CompletableFuture.supplyAsync(() ->
          redirect(routes.TextActions.textExcerpts (-1L)), context.current());
    }

    // try to unbind
    try {
      text.unbindActor(actorId, sessionHelper.getUser(ctx()).getId());
    } catch (PersistenceException e) {
      logger.error("unable to unbind actor " + actor.getFullname(ctx().lang().code()) + " from text " + text.getId(), e);
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(), e.getMessage()));
      return CompletableFuture.supplyAsync(() ->
          redirect(routes.TextActions.textExcerpts (textId)), context.current());
    }

    // all good, rebuild page
    flash(SessionHelper.SUCCESS, i18n.get(ctx().lang(),"text.args.actorname.removed", actor.getFullname(ctx().lang().code())));
    return CompletableFuture.supplyAsync(() ->
        redirect(routes.TextActions.textExcerpts (textId)), context.current());
  }

  /**
   * Save justification links for given text
   *
   * @param id a text id
   * @return simple ok / badRequest messages depending on saving action result
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> saveJustificationLinks(Long id, int group) {
    logger.debug("POST save links for text " + id + " in group " + group);
    Text text = textFactory.retrieve(id);

    if (text == null) {
      logger.error("text could not be retrieved " + id);
      return CompletableFuture.supplyAsync(() -> badRequest(""), context.current());
    }

    // convert to argument justification links and set current group
    List<ArgumentJustification> tosave = convertLinksFormToApi();
    // save links now
    try {
      text.saveJustificationLinks(tosave, group, sessionHelper.getUser(ctx()).getId());
    }catch(PersistenceException  | PermissionException e){
      logger.debug("Error while save text links : " + e);
      return CompletableFuture.supplyAsync(() -> internalServerError(""), context.current());
    }

    // all good, just return empty json
    logger.debug("all justification links have been updated for text " + id);
    return CompletableFuture.supplyAsync(() -> ok(""), context.current());
  }

  /**
   * Search a text by its title or authors
   *
   * @param term a search term (from title)
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a List<TextName> as a json structure with all texts containing the given term in their title
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public Result searchText(String term, int fromIndex, int toIndex) {
    List<TextName> result = new ArrayList<>();
    List<Map.Entry<EQueryKey, String>> query = new ArrayList<>();
    query.add(new AbstractMap.SimpleEntry<>(EQueryKey.CONTRIBUTION_TYPE, String.valueOf(EContributionType.TEXT.id())));
    query.add(new AbstractMap.SimpleEntry<>(EQueryKey.AUTHOR, term));
    query.add(new AbstractMap.SimpleEntry<>(EQueryKey.TEXT_TITLE, term));
    try {
      executor.searchContributions(query, fromIndex, toIndex).stream().filter(sessionHelper.getUser(ctx())::mayView).forEach(c -> {
        Text text = (Text) c;
        List<ActorSimpleForm> authors = text.getAuthors().stream().map(r ->
                new ActorSimpleForm(r, ctx().lang().code())).collect(Collectors.toList());
        result.add(new TextName(text.getId(), text.getTitle(ctx().lang().code()), authors));
      });
    } catch (BadQueryException e) {
      logger.warn("unable to search for texts with given term " + term, e);
    }
    return ok(Json.toJson(result));
  }

  /**
   * Get all source titles with given term
   *
   * @param term a term to search in text's source_title
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a list of TextSourceNames that contain the given searched term
   */
  public Result searchTextSource(String term, int fromIndex, int toIndex) {
    logger.debug("search sources for " + term);
    return ok(Json.toJson(textFactory.findSourceNames(term, fromIndex, toIndex)));
  }

  /**
   * Get the annotation of a given text, if context group allows it, otherwise, simply send back the content
   *
   * @param textId the text id to get the annotated version
   * @param highlighted true if the text must be highlighted
   * @return either the annotated text (from annotator helper) or the text content if no annotation could be created
   * from external service or cache, with existing and viewable excerpts highlighted in it. If given text
   * does not exist or is not viewable for context user (from cookie), a message is returned in respectively a
   * unauthorized (401) a bad request (400)
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> getAnnotatedText(Long textId, boolean highlighted) {
    logger.info("get annotated text for " + textId);
    Map<String, String> messages = new HashMap<>();
    messages.put(SessionHelper.INFO, i18n.get(ctx().lang(), "text.args.nocontent"));
    Http.Context ctx = ctx();
    Text text = textFactory.retrieve(textId);
    if (text != null) {
      WebdebUser user = sessionHelper.getUser(ctx);

      // must explicitly cast back to Excerpt after filter
      List<ExcerptHolder> filtered = highlighted ? sessionHelper.getUser(ctx).filterContributionList(text.getExcerpts())
          .stream().map(e -> new ExcerptHolder((Excerpt) e, ctx.lang().code())).collect(Collectors.toList()) : new ArrayList<>();
      // retrieve annotated file, or ask annotation if not yet annotated
      try {
        Gson gson = new Gson();
        String ch = "\"text\":" + gson.toJson(getAnnotatedTextContent(textId));
        ch += ", \"excerpts\" : " + Json.toJson(filtered);
        ch = "{" + ch + "}";

        return CompletableFuture.completedFuture(ok(ch));
      } catch (PermissionException e) {
        logger.info("contributor " + user.getId() + " has no (right to see) content of " + textId, e);
        return CompletableFuture.completedFuture(unauthorized(Json.toJson(message.render(messages).toString())));
      }
    } else {
      logger.error("unable to retrieve text " + textId);
    }
    return CompletableFuture.completedFuture(badRequest(Json.toJson(message.render(messages).toString())));
  }

  /**
   * Get the content (may be annotated, but not highlighted) of a given text on a modal page.
   *
   * @param textId the text id
   * @return either the modal page with the content text. If given text does not exist or is not viewable for context
   * user (from cookie), a message is returned in respectively a unauthorized (401) a bad request (400)
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> getTextContentModal(Long textId) {
    logger.info("get modal text content for " + textId);
    Map<String, String> messages = new HashMap<>();
    messages.put(SessionHelper.INFO, i18n.get(ctx().lang(), "text.args.nocontent"));
    Http.Context ctx = ctx();
    Text text = textFactory.retrieve(textId);
    if (text != null) {
      WebdebUser user = sessionHelper.getUser(ctx);
      TextHolder holder = new TextHolder(text, user.getId(), ctx.lang().code());
      return CompletableFuture.completedFuture(ok(displayTextContentModal.render(holder, user)));
    } else {
      logger.error("unable to retrieve text " + textId);
    }
    return CompletableFuture.completedFuture(badRequest(Json.toJson(message.render(messages).toString())));
  }

  /**
   * Call NLP service to retrieve the content behind a url
   *
   * @param url a url
   * @return a json structure according to NLP service with, a.o, text content, title, date, etc, or
   * a bad request (400) if given url is not valid or gave no result
   */
  public CompletionStage<Result> getTextContent(String url) {
    logger.debug("get " + url);
    Http.Context ctx = ctx();
    Map<String, String> messages = new HashMap<>();
    if (values.isURL(url)) {
      return proxy.getTextContentAsJson(url).thenApply(result -> {
        if (result != null) {
          return ok(result);
        }
        messages.put(SessionHelper.WARNING, i18n.get(ctx.lang(),"nlp.text.error"));
        return badRequest(message.render(messages));
      });
    }
    messages.put(SessionHelper.WARNING, i18n.get(ctx.lang(),"nlp.text.invalidurl"));
    return CompletableFuture.completedFuture(badRequest(message.render(messages)));
  }

  /**
   * Call NLP service to retrieve the language of a given text
   *
   * @param content a (truncated) content to analyze
   * @return a json json structure according to NLP service of the form { language=id } or a message in
   * a bad request (400) if WDTAL call did not succeed
   */
  @Restrict(@be.objectify.deadbolt.java.actions.Group(WebdebRole.NOTVIEWER))
  public CompletionStage<Result> getTextLanguage(String content) {
    return CompletableFuture.completedFuture(ok(values.detectTextLanguage(content, getLanguageDetectionApiKey(), ctx().lang().code())));

  }

  /**
   * Extract content of pdf from given url
   *
   * @param url an url to get the PDF from
   * @return either a JSON structure with the language and content of the text or a message to be rendered
   * in a bad request (400)
   */
  public CompletionStage<Result> extractPDFContent(String url) {
    logger.debug("GET pdf content from " + url);
    String trimmed = url != null ? url.trim() : null;
    Map<String,String> messages = new HashMap<>();
    // we don't use isBlank method fron valuesHelper to avoid compilator warning
    if (trimmed != null && !trimmed.trim().isEmpty()
            && ".pdf".equalsIgnoreCase(trimmed.substring(trimmed.lastIndexOf('.'), trimmed.lastIndexOf('.') + 4))) {
      Http.Context ctx = ctx();
      return proxy.retrievePDF(url).thenApplyAsync(response -> {
        if (response != null) {
          return ok(response);
        }
        messages.put(SessionHelper.ERROR, i18n.get(ctx.lang(), "text.pdf.error"));
        return badRequest(message.render(messages));
      }, context.current());
    }
    messages.put(SessionHelper.WARNING, i18n.get(ctx().lang(), "text.pdf.nourl"));
    return CompletableFuture.completedFuture(badRequest(message.render(messages)));
  }

  /**
   * Reload all linked contributions by type
   *
   * @param text the text id
   * @return a jsonified list of holders or bad request if actor is unknown or pov is unknown
   */
  public CompletionStage<Result> reloadLinkedContributions(Long text) {
    Text t = textFactory.retrieve(text);
    WebdebUser user = sessionHelper.getUser(ctx());

    if(t != null) {
      viz = new TextVizHolder(t, user.getId(), ctx().lang().code());
      return CompletableFuture.supplyAsync(Results::ok, context.current());
    }
    return CompletableFuture.supplyAsync(Results::badRequest, context.current());
  }

  /**
   * Get all linked contributions by type
   *
   * @param text the text id
   * @param type the name of the viz pane type
   * @return a jsonified list of holders or bad request if actor is unknown
   */
  public CompletionStage<Result> getLinkedContributions(Long text, String type) {
    logger.debug("GET linked contributions with text " + text + " for " + type);
    Text t = textFactory.retrieve(text);
    WebdebUser user = sessionHelper.getUser(ctx());

    if(t != null) {
      if(viz == null) {
        viz = new TextVizHolder(t, user.getId(), ctx().lang().code());
      }

      switch(EVizPaneName.value(type)){
        case RADIO :
            return CompletableFuture.supplyAsync(() ->
                    ok(textRadioContent.render(viz, user)), context.current());
      }
    }
    Map<String, String> messages = new HashMap<>();
    messages.put(SessionHelper.ERROR, i18n.get(ctx().lang(),"viz.text.error"));
    return CompletableFuture.supplyAsync(() -> badRequest(message.render(messages)), context.current());
  }

  /**
   * Find the list of contextualized arguments that are part of the given text limited by index
   *
   * @param text a text id
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a jsonified list of holders or bad request if text is unknown
   */
  public CompletionStage<Result> findTextArguments(Long text, int fromIndex, int toIndex) {
    logger.debug("Find arguments for text " + text + " limited : " + fromIndex + ", " + toIndex);
    Text t = textFactory.retrieve(text);

    if(t != null) {
      return sendPartialLoadedContributions(t.getAllContextualizedArguments(fromIndex, toIndex),
              fromIndex, toIndex, text, EContributionType.ARGUMENT_CONTEXTUALIZED, EDataSendType.AS_HTML_TEMPLATE_1);
    }
    return CompletableFuture.supplyAsync(Results::badRequest, context.current());
  }

  /**
   * Find the list of excerpts that are part of the given text limited by index
   *
   * @param text a text id
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a jsonified list of holders or bad request if text is unknown
   */
  public CompletionStage<Result> findTextExcerpts(Long text, int fromIndex, int toIndex) {
    logger.debug("Find excerpts for text " + text + " limited : " + fromIndex + ", " + toIndex);
    Text t = textFactory.retrieve(text);

    if(t != null) {
      return sendPartialLoadedContributions(t.getExcerpts(fromIndex, toIndex),
              fromIndex, toIndex, text, EContributionType.EXCERPT, EDataSendType.AS_HTML_TEMPLATE_1);
    }
    return CompletableFuture.supplyAsync(Results::badRequest, context.current());
  }

  /**
   * Save a text from external service into the database
   *
   * @return the saved text id or bad request if an error occured
   */
  public CompletionStage<Result> saveFromExternal() {
    logger.debug("POST save external text");
    Form<TextRequest> form = formFactory.form(TextRequest.class).bindFromRequest();

    if (!form.hasErrors()) {
      TextRequest textForm = form.get();
      Contributor contributor = textForm.getUser().getContributor();
      if(contributor != null){
        session(SessionHelper.KEY_USERMAILORPSEUDO, contributor.getEmail());
        sessionHelper.getUser(ctx());
        try{
          Long textId;
          ExternalText text = textFactory.findByContributorAndUrl(contributor.getId(), textForm.getUrl());
          if(text == null) {
              textForm.save(contributor.getId());
              textId = textForm.getId();
          }else{
            textId = text.getId();
            if(textForm.getExcerpts() != null) {
              for (ExcerptRequest exc : textForm.getExcerpts()) {
                if (text.getExcerpts().stream().noneMatch(e -> e.getOriginalExcerpt().equals(exc.getOriginalExcerpt()))) {
                  exc.setTextId(textId);
                  exc.setSourceName(textForm.getSourceName());
                  exc.setPublicationDate(textForm.getPublicationDate());
                  exc.save(contributor.getId());
                }
              }
            }
          }

          return CompletableFuture.supplyAsync(() ->
                  ok(be.webdeb.presentation.web.controllers.entry.text.routes.TextActions.edit(textId).toString()), context.current());
        } catch (FormatException | PersistenceException | PermissionException e) {
          logger.error("unable to save external text", e);
        }
      }else{
        return CompletableFuture.supplyAsync(Results::unauthorized, context.current());
      }
    }

    return CompletableFuture.supplyAsync(Results::badRequest, context.current());
  }


    /**
     * Get the first argument of the given context contribution if exists, otherwise create it.
     *
     * @param id a context contribution id
     * @return the context contribution first argument if given text exists, null otherwise
     */
    public CompletionStage<Result> getFirstArgument(Long id){
        ContextContribution c = textFactory.retrieveContextContribution(id);
        if(c != null){
            return CompletableFuture.supplyAsync(() ->
                    ok(Json.toJson(c.getFirstArgumentId())), context.current());
        }
        return CompletableFuture.supplyAsync(Results::badRequest);
    }

  /**
   * Get all contextualized arguments of a given context contribution as jsonified ArgumentContextForm
   *
   * @param id a text id
   * @return a json array of ArgumentContextForm of given context, or an empty bad request (400) if text does not exist
   */
  public CompletionStage<Result> getContextualizedArguments(Long id) {
    ContextContribution c = textFactory.retrieveContextContribution(id);
    if (c == null) {
      return CompletableFuture.supplyAsync(Results::badRequest);
    }

    return CompletableFuture.supplyAsync(() ->
            ok(Json.toJson(getContextualizedArgumentHolders(textFactory.getContextualizedArguments(id)))), context.current());
  }

  /**
   * Get all contextualized arguments of a given context contribution as jsonified ArgumentContextForm for a given link shade type
   *
   * @param id a text id
   * @param shade the link shade to filter
   * @return a json array of ArgumentContextForm of given context, or an empty bad request (400) if text does not exist
   */
  public CompletionStage<Result> getContextualizedArgumentsByShade(Long id, int shade) {
    ContextContribution c = textFactory.retrieveContextContribution(id);
    EArgumentLinkShade s = EArgumentLinkShade.value(shade);
    if (s == null || c == null) {
      return CompletableFuture.supplyAsync(Results::badRequest);
    }
    Long firstArgumentId = c.getFirstArgumentId();

    List<ArgumentJustification> links = getJustificationLinksFromContext(id).stream().filter(e ->
            !e.getOrigin().getId().equals(firstArgumentId) || e.getArgumentLinkType().getEType() == s).collect(Collectors.toList());
    Map<Long, ArgumentContext> argsMap = new HashMap<>();
    links.forEach(e -> {
      if(!argsMap.containsKey(e.getDestination().getArgumentId())){
        argsMap.put(e.getId(), e.getDestination());
      }
    });
    return CompletableFuture.supplyAsync(() ->
            ok(Json.toJson(getContextualizedArgumentHolders(new ArrayList<>(argsMap.values())))), context.current());
  }

  /**
   * Get all justification links of a given context contribution as jsonified JustificationLinkForms
   *
   * @param id a text id
   * @return a json array of JustificationLinkForms or an empty bad request (400) if given context does not exist
   */
  public CompletionStage<Result> getJustificationLinks(Long id) {
    ContextContribution c = textFactory.retrieveContextContribution(id);
    if (c == null) {
      return CompletableFuture.supplyAsync(Results::badRequest);
    }

    return CompletableFuture.supplyAsync(() ->
            ok(Json.toJson(buildJustificationLinksForm(getJustificationLinksFromContext(id)))), context.current());
  }

  /**
   * Get all justification links of a given context contribution as jsonified JustificationLinkForms for a given link shade type
   *
   * @param id a text id
   * @param shade the link shade to filter
   * @return a json array of JustificationLinkForms or an empty bad request (400) if given context does not exist
   */
  public CompletionStage<Result> getJustificationLinksByShade(Long id, int shade) {
    ContextContribution c = textFactory.retrieveContextContribution(id);
    EArgumentLinkShade s = EArgumentLinkShade.value(shade);
    if (s == null || c == null) {
      return CompletableFuture.supplyAsync(Results::badRequest);
    }
    Long firstArgumentId = c.getFirstArgumentId();
    List<ArgumentJustification> links = getJustificationLinksFromContext(id).stream().filter(e ->
            !e.getOrigin().getId().equals(firstArgumentId) || e.getArgumentLinkType().getEType() == s).collect(Collectors.toList());
    return CompletableFuture.supplyAsync(() -> ok(Json.toJson(buildJustificationLinksForm(links))), context.current());
  }

  /**
   * Get the context contribution behind the contribution id
   *
   * @param id a context contribution id
   * @return a debate holder or a text holder as json
   */
  public CompletionStage<Result> getContextContribution(Long id) {
    Contribution c = textFactory.retrieveContribution(id);
    if (c == null) {
      return CompletableFuture.supplyAsync(Results::badRequest);
    }
    WebdebUser user = sessionHelper.getUser(ctx());
    String lang = ctx().lang().code();

    switch (c.getContributionType().getEContributionType()){
      case TEXT :
        return CompletableFuture.supplyAsync(() ->
                ok(Json.toJson(new TextHolder(textFactory.retrieve(id), user.getId(), lang))), context.current());
      case DEBATE:
        return CompletableFuture.supplyAsync(() ->
                ok(Json.toJson(new DebateHolder(debateFactory.retrieve(id), lang))), context.current());
      default:
        return CompletableFuture.supplyAsync(Results::badRequest);
    }
  }

  /**
   * Get the text content of a html content found by the given url
   *
   * @param url a url for which we need is content
   * @return the html text content of the given url
   */
  public CompletionStage<Result> getHtmlContent(String url){
    logger.debug("Get html content of url : " + url);

    try{
      Document doc = Jsoup.connect(url).get();

      Map<String, String> response = new HashMap<>();

      Elements article = getHtmlArticleElement(doc.children());
      boolean isTwitter = article.hasClass("js-original-tweet");

      response.put("content", getHtmlArticleContent(article, isTwitter));
      response.put("title", getHtmlArticleTitle(article));
      response.put("date", getHtmlArticlePublicationDate(article, isTwitter));
      response.put("author", getHtmlArticleAuthor(article, isTwitter));

      return CompletableFuture.supplyAsync(() -> ok(Json.toJson(response)), context.current());
    }
    catch(Exception e) {
      logger.debug("Html content problem : ", e);
    }

    return CompletableFuture.supplyAsync(Results::badRequest, context.current());
  }

  /**
   * Check if a given domain name is a free source or not. Free means free of rights.
   *
   * @param domain the domain name to check
   * @return true if the given domain name is a free source
   */
  public CompletionStage<Result> checkFreeSource(String domain){
    logger.debug("Get is free source : " + domain);

    return CompletableFuture.supplyAsync(() -> ok(Json.toJson(textFactory.sourceIsCopyrightfree(domain, sessionHelper.getUser(ctx()).getId()))), context.current());
  }

  /*
   * Private helpers
   */

  /**
   * Get all contextualized arguments holder from as list of ArgumentContext api
   *
   * @param args a list of api ArgumentContext
   * @return the list of contextualized arguments holder
   */
  private List<ArgumentContextVizHolder> getContextualizedArgumentHolders(List<ArgumentContext> args){
    WebdebUser user = sessionHelper.getUser(ctx());
    Http.Context ctx = ctx();

    // filter arguments of this context
    return user.filterContributionList(args).stream().map(e ->
            new ArgumentContextVizHolder((ArgumentContext) e, ctx.lang().code())).collect(Collectors.toList());
  }

  /**
   * Get all justification links of a given context contribution
   *
   * @param id the context contribution id
   * @return the list of justification links
   */
  private List<ArgumentJustification> getJustificationLinksFromContext(Long id){
    WebdebUser user = sessionHelper.getUser(ctx());
    return user.filterContributionList(textFactory.getArgumentJustificationLinks(id)).stream()
            .map(e -> (ArgumentJustification) e).sorted().collect(Collectors.toList());
  }

  /**
   * Build a list of justification links form from a list of api ArgumentJustification list
   *
   * @param links the ArgumentJustification list
   * @return the list of justification links as form
   */
  private List<JustificationLinkForm> buildJustificationLinksForm(List<ArgumentJustification> links){
    Http.Context ctx = ctx();
    return links.stream().map(e -> new JustificationLinkForm(e, ctx.lang().code())).collect(Collectors.toList());
  }

  /**
   * Get the annotation of a given text
   *
   * @param textId the text id to get the annotated version
   * @return the annotated context text
   */
  private String getAnnotatedTextContent(Long textId) throws PermissionException {
    String content = "";
    Http.Context ctx = ctx();

    Text text = textFactory.retrieve(textId);
    if (text != null) {
      WebdebUser user = sessionHelper.getUser(ctx);
      String filename = text.getFilename(user.getId());
      content = files.getContributionTextFile(filename);
      content = content.replaceAll("\\r\\n|\\r|\\n", " <br>")
              .replaceAll("’", "'");
    }

    return content;
  }

  /**
   * HTML article element selection
   *
   * @return an html element(s)
   */
  private Elements searchArticleElement(Elements article, String [] selectors){
    int iArticle;

    for(iArticle = 0; iArticle < selectors.length && article.select(selectors[iArticle]).isEmpty(); iArticle++);

    return iArticle < selectors.length ? article.select(selectors[iArticle]) : null;
  }

  /**
   * HTML article element selection
   *
   * Search element like article content, authors, publication date and title in the given html article.
   */
  private String searchArticleElementContent(Elements element){

    if(element != null){
      if(element.text().length() > 0){
        return element.text();
      }else if(element.attr("content").length() > 0){
        return element.attr("content");
      }
    }

    return null;
  }

  /**
   * Get the article element
   *
   * @return the article element or null
   */
  private Elements getHtmlArticleElement(Elements article){
    String [] selectors = {".js-original-tweet",
            "[itemprop^='article']",
            "article", ".article",
            ".article-entry",
            "[itemprop*='article']",
            "[class*='article']",
            "main",
            "body"};
    return searchArticleElement(article, selectors);
  }

  /**
   * Get the article content
   *
   * @param isTwitter does the article come from twitter
   * @return the article content or null
   */
  private String getHtmlArticleContent(Elements article, boolean isTwitter){

    if(isTwitter) {
      String [] selectors = {".tweet-text"};
      return searchArticleElementContent(searchArticleElement(article, selectors));
    }

    return searchArticleElementContent(article);
  }

  /**
   * Get the article title
   *
   * @return the article title or null
   */
  private String getHtmlArticleTitle(Elements article){
    String [] selectors = {"[itemprop='headline']",
            "h1",
            ".headline",
            "[class*='headline']",
            ".title",
            "[class='title']",
            "[class='article-title']",
            "[class*='title']",
            "[itemprop='name']",
            "h2", "h3", "h4", "h6"};
    return searchArticleElementContent(searchArticleElement(article, selectors));
  }

  /**
   * Get the article publication date
   *
   * @param isTwitter does the article come from twitter
   * @return the article publication date or null
   */
  private String getHtmlArticlePublicationDate(Elements article, boolean isTwitter){
    if(isTwitter){
      String [] selectors = {".metadata"};
      return searchArticleElementContent(searchArticleElement(article, selectors));
    }
    String [] selectors = {"[itemprop^='date']",
            "[itemprop*='date']",
            ".date",
            "time",
            "[class*='date']"};
    return searchArticleElementContent(searchArticleElement(article, selectors));
  }

  /**
   * Get the article author
   *
   * @param isTwitter does the article come from twitter
   * @return the article author or null
   */
  private String getHtmlArticleAuthor(Elements article, boolean isTwitter){
    if(isTwitter){
      return article.attr("data-name");
    }
    String [] selectors = {"[itemprop='author']",
            "[itemprop='creator']",
            "[itemprop*='author']",
            ".author",
            "[class*='author']",
            ".fullname",
            ".name"};
    return searchArticleElementContent(searchArticleElement(article, selectors));
  }
}

