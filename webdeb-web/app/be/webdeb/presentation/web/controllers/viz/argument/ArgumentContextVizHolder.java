/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.viz.argument;

import be.webdeb.core.api.actor.*;
import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentIllustration;
import be.webdeb.core.api.argument.ArgumentJustification;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.presentation.web.controllers.entry.actor.ActorHolder;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentContextHolder;
import be.webdeb.presentation.web.controllers.entry.argument.ArgumentHolder;
import be.webdeb.presentation.web.controllers.entry.debate.DebateHolder;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptHolder;
import be.webdeb.presentation.web.controllers.entry.folder.FolderHolder;
import be.webdeb.presentation.web.controllers.entry.text.TextHolder;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import be.webdeb.presentation.web.controllers.viz.EVizPane;
import be.webdeb.presentation.web.controllers.viz.VizActions;
import be.webdeb.presentation.web.controllers.viz.excerpt.*;
import play.api.Play;
import play.i18n.Lang;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This class extends an argument holder with visualization-related properties, such as a heat property
 * (for graph colors) and LinkVizHolder that also contain dedicated properties for visualization purposes.
 *
 * It also initialises more filterable keys-values to enhance filterable features of arguments
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class ArgumentContextVizHolder extends ArgumentContextHolder {

    private final Map<Integer, String> ages = new HashMap<>();
    private Map<Long, ExcerptHolder> excerptsMap = null;
    private Map<Long, EArgumentLinkShade> excerptsShadeMap;
    private Map<Long, Set<Long>> authorsExcerptsMap;
    private Map<Long, EArgumentLinkShade> authorsShadeMap;
    private Map<Long, Map<Long, ActorRole>> authorsRolesMap;
    private Map<Long, ActorHolder> authorsMap;

    private boolean allLinks;

    private List<ArgumentJustification> justificationLinksThatJustify = null;
    private List<ArgumentIllustration> illustrationsLinks = null;

    // keep data to not load them twice
    // carto data
    private Map<EArgumentLinkShade, List<ArgumentContextVizHolder>> cartoData = null;
    private Map<Long, Set<Long>> similarMap = null;
    // radio data
    private Map<EArgumentLinkShade, List<ExcerptHolder>> radioData = null;
    private List<Map<EArgumentLinkShade, List<ExcerptHolder>>>  radioDataByAuthor = null;
    // cited data
    private Map<EArgumentLinkShade, List<ExcerptCitedViz>> citedData = null;
    // socio data
    private ExcerptSociography socioData = null;
    // text data
    private List<TextHolder> textDataTexts = null;
    private List<DebateHolder> textDataDebates = null;
    private List<FolderHolder> textDataFolders = null;

    private Map<Integer, Integer> excerptUserMetric = null;

    /**
     * Construct an argument visualization holder from an argument and for a given language.
     * Will not set comments type, since more
     *
     * @param argumentContext the API argument that will be rendered
     * @param lang 2-char ISO 639-1 code of context language (among play accepted languages)
     */
    public ArgumentContextVizHolder(ArgumentContext argumentContext, String lang) {
        this(argumentContext, lang, true);
    }

    /**
     * Construct an argument visualization holder from an argument and for a given language.
     * Will not set comments type, since more
     *
     * @param argumentContext the API argument that will be rendered
     * @param lang 2-char ISO 639-1 code of context language (among play accepted languages)
     * @param allLinks true if we must display similars arguments too
     */
    public ArgumentContextVizHolder(ArgumentContext argumentContext, String lang, boolean allLinks) {
        super(argumentContext, lang);
        this.allLinks = allLinks;
        initMetricsAndMap();

        isForDebate = !allLinks;

        // build categories for ages
        int[] rangeLimit = new int[] {15, 25, 35, 45, 55, 65};
        int low = 0;
        for (int range : rangeLimit) {
            for (int i = low; i < range; i++) {
                ages.put(i, i18n.get(Lang.forCode(lang),"viz.actor.socio.age." + range));
            }
            low = range;
        }
    }

    /**
     * Init all metrics for argument viz.
     */
    private void initMetricsAndMap(){
        authorsExcerptsMap = new LinkedHashMap<>();
        authorsShadeMap = new LinkedHashMap<>();
        excerptsShadeMap = new LinkedHashMap<>();
        authorsRolesMap = new HashMap<>();
        authorsMap = new HashMap<>();
        similarMap = new LinkedHashMap<>();
    }

    /**
     * Build the cartography view data. Will build a map of EArgumentLinkShade, list of arguments (as
     * ArgumentHolder) to be displayed. It shows le list of arguments that argue this arguments in contexts.
     *
     * @param user a user
     * @return a map of EArgumentLinkShade, list of associated argument holders
     */
    public Map<EArgumentLinkShade, List<ArgumentContextVizHolder>> viewCartography(WebdebUser user) {
        if(cartoData == null) {
            Map<EArgumentLinkShade, List<Long>> result = new LinkedHashMap<>();
            Map<Long, ArgumentContextVizHolder> resultHolder = new LinkedHashMap<>();
            result.put(EArgumentLinkShade.SUPPORTS, new ArrayList<>());
            result.put(EArgumentLinkShade.REJECTS, new ArrayList<>());

            initLinks();
            Map<Long, Long> synonymMap = new LinkedHashMap<>();
            int order = 0;

            // TODO should be replaced by lambda, but it has no effects...
            Collections.sort(justificationLinksThatJustify, new Comparator<ArgumentJustification>() {
                @Override
                public int compare(ArgumentJustification lhs, ArgumentJustification rhs) {
                    return lhs.getArgumentLinkType().getLinkShade() > rhs.getArgumentLinkType().getLinkShade() ? -1
                    : (lhs.getArgumentLinkType().getLinkShade() < rhs.getArgumentLinkType().getLinkShade()) ? 1 : 0;
                }
            });

            // Put all similar argument side by side for the viz carto
            for(ArgumentJustification e : justificationLinksThatJustify){
                EArgumentLinkShade shade = e.getArgumentLinkType().getEType();

                if(user.mayView(e.getDestination())) {
                    ArgumentContextVizHolder holder = new ArgumentContextVizHolder( e.getDestination(), lang);
                    Long idDictionary = e.getDestination().getArgument().getDictionary().getId();

                    if(shade == EArgumentLinkShade.REJECTS){
                        synonymMap.put(idDictionary, e.getDestination().getId());
                        similarMap.put(e.getDestination().getId(), e.getDestination().getArgument().getSimilarIds(shade.getOpposite()));
                        result.get(shade).add(e.getDestination().getId());
                        resultHolder.put(e.getDestination().getId(), holder);
                    }else{
                        Long iSimilarity = -1L;
                        if(!synonymMap.containsKey(idDictionary))
                            iSimilarity = searchAfterSimilar(e.getDestination().getId());

                        if(synonymMap.containsKey(idDictionary) || iSimilarity > -1){
                            Long argumentId = iSimilarity > -1 ? synonymMap.get(iSimilarity) : synonymMap.get(idDictionary);

                            if(!result.get(shade.getOpposite()).get(order).equals(argumentId)) {
                                result.get(shade.getOpposite()).remove(argumentId);
                                result.get(shade.getOpposite()).add(order, argumentId);
                            }

                            result.get(shade).add(order, e.getDestination().getId());

                            resultHolder.put(e.getDestination().getId(), holder);
                            order++;

                            if(iSimilarity > -1){
                                synonymMap.remove(iSimilarity);
                            }
                        }else{
                            result.get(shade).add(e.getDestination().getId());
                            resultHolder.put(e.getDestination().getId(), holder);
                        }
                    }
                }
            }

            cartoData = new LinkedHashMap<>();
            cartoData.put(EArgumentLinkShade.SUPPORTS, transformCartoResultMap(result.get(EArgumentLinkShade.SUPPORTS), resultHolder));
            cartoData.put(EArgumentLinkShade.REJECTS, transformCartoResultMap(result.get(EArgumentLinkShade.REJECTS), resultHolder));
        }
        return cartoData;
    }

    /**
     * Build the radiography view data. Will build a map of EArgumentLinkShade, list of excerpts (as
     * ExcerptHolder) to be displayed. That regroups all citations linked to this arguments by link type.
     *
     * @param user a user
     * @return a map of EArgumentLinkShade, list of associated argument holders
     */
    public Map<EArgumentLinkShade, List<ExcerptHolder>> viewRadiography(WebdebUser user) {
        if(radioData == null) {
            Map<EArgumentLinkShade, List<ExcerptHolder>> result = initRadioMap();

            fillAuthorsMaps(user);

            for (ExcerptHolder excerpt : new ArrayList<>(excerptsMap.values())) {
                EArgumentLinkShade shade = excerptsShadeMap.get(excerpt.getId());
                result.get(shade).add(excerpt);
            }

            radioData = result;
        }
        return radioData;
    }

    /**
     * Build the radiography view data separating the given contribution with the others
     *
     * @param contributionId a contribution id
     * @param contributionType a contribution type
     * @param user a user
     * @return a map of EArgumentLinkShade, list of associated argument holders
     */
    public List<Map<EArgumentLinkShade, List<ExcerptHolder>>> viewRadiography(Long contributionId, EContributionType contributionType, WebdebUser user) {
        if (radioDataByAuthor == null) {
            List<Map<EArgumentLinkShade, List<ExcerptHolder>>> result = new ArrayList<>();
            result.add(initRadioMap());
            result.add(initRadioMap());

            fillAuthorsMaps(user);

            for (ExcerptHolder excerpt : new ArrayList<>(excerptsMap.values())) {
                EArgumentLinkShade shade = excerptsShadeMap.get(excerpt.getId());
                if ((contributionType == EContributionType.TEXT && contributionId.equals(excerpt.getTextId())) ||
                    (contributionType == EContributionType.ACTOR && excerpt.isThinker(contributionId))) {
                    result.get(0).get(shade).add(excerpt);
                } else {
                    result.get(1).get(shade).add(excerpt);
                }

            }

            radioDataByAuthor = result;
        }
        return radioDataByAuthor;
    }

    private Map<EArgumentLinkShade, List<ExcerptHolder>> initRadioMap(){
        Map<EArgumentLinkShade, List<ExcerptHolder>> result = new LinkedHashMap<>();
        result.put(EArgumentLinkShade.ILLUSTRATE, new ArrayList<>());
        result.put(EArgumentLinkShade.SHADED_EXAMPLE, new ArrayList<>());
        result.put(EArgumentLinkShade.COUNTER_EXAMPLE, new ArrayList<>());

        return result;
    }

    /**
     * Build the radiography view data. Will build a map of EArgumentLinkShade, list of excerpts (as
     * ExcerptCitedViz) to be displayed. That regroups all citations linked to this arguments by link type and by author.
     *
     * @param user an user
     * @return a map of EArgumentLinkShade, list of ExcerptCitedViz
     * @see ExcerptCitedViz
     */
    public Map<EArgumentLinkShade, List<ExcerptCitedViz>> viewCited(WebdebUser user) {
        if(citedData == null) {
            Map<EArgumentLinkShade, List<ExcerptCitedViz>> result = new LinkedHashMap<>();
            result.put(EArgumentLinkShade.ILLUSTRATE, new ArrayList<>());
            result.put(EArgumentLinkShade.SHADED_EXAMPLE, new ArrayList<>());
            result.put(EArgumentLinkShade.COUNTER_EXAMPLE, new ArrayList<>());

            fillAuthorsMaps(user);

            authorsMap.forEach((k, v) -> {
                EArgumentLinkShade shade = authorsShadeMap.get(k);
                List<ExcerptHolder> holders = new ArrayList<>();
                authorsExcerptsMap.get(k).forEach(e -> holders.add(excerptsMap.get(e)));
                result.get(shade).add(new ExcerptCitedViz(v, authorsRolesMap.get(k), holders));
            });

            // sorts the lists by actor number of excerpts and for each actor by publication date
            result.forEach((shade, list) -> {
                list.sort(Comparator.comparingInt(ExcerptCitedViz::comparator));
                list.forEach(e -> e.getExcerpts().sort((o1, o2) ->
                        values.compareDatesAsString(o2.getPublicationDate(), o1.getPublicationDate()))
                );
            });

            citedData = result;
        }
        return citedData;
    }

    /**
     * Build the sociography view data (Similarity link) for a given key, i.e., a list of nodes (whose semantics depends on given groupKey)
     * where all excerpts having a similarity link to an excerpts stated by this actor are grouped following
     * given groupKey. The nodes defines aggregated similarity statistics regarding the actor linked to this argument and its similare one.
     *
     * Sociography nodes are grouped regarding the given groupKey (eg. age, functions, countries, etc)
     *
     * @param user the WebDeb user
     * @return a collection of  ExcerptSociography containing labels and statistics
     */
    public ExcerptSociography viewSociography(WebdebUser user) {
        if(socioData == null) {
            Map<EExcerptGroupKey, Map<String, ExcerptSociographyNode>> result = new LinkedHashMap<>();
            EExcerptGroupKey.getAll().forEach(e -> result.put(e, new HashMap<>()));
            LocalDate now = LocalDate.now();

            fillAuthorsMaps(user);

            authorsExcerptsMap.forEach((authorId, excerpts) ->
                excerpts.forEach(excerptId -> {
                    EArgumentLinkShade shade = authorsShadeMap.get(authorId);
                    ActorRole role = authorsRolesMap.get(authorId).get(excerptId);
                    ExcerptHolder excerpt = excerptsMap.get(excerptId);

                    for(EExcerptGroupKey groupKey : EExcerptGroupKey.values()) {
                        // create sociography node for all authors and all view key
                        String label = null;
                        // now switch on viewKey
                        switch (groupKey) {
                            case AGE:
                                // only use persons
                                if (role.getActor().getActorType().equals(EActorType.PERSON)) {
                                    Person p = (Person) role.getActor();
                                    if (p.getBirthdate() != null) {
                                        LocalDate birth = values.toDate(p.getBirthdate()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                                        String temp = ages.get(Period.between(birth, now).getYears());
                                        label = temp != null ? temp : i18n.get(Lang.forCode(lang), "viz.actor.socio.age.65plus");
                                    }
                                    updateNodeMap(result, groupKey, role.getActor(), shade, label);
                                }
                                break;
                            case COUNTRY:
                                switch (role.getActor().getActorType()) {
                                    case PERSON:
                                        Person p = (Person) role.getActor();
                                        if (p.getResidence() != null) {
                                            label = p.getResidence().getName(lang);
                                        }
                                        break;
                                    case ORGANIZATION:
                                        Organization o = (Organization) role.getActor();
                                        if (o.getPlaces() != null && !o.getPlaces().isEmpty() && o.getPlaces().get(0).getCountry() != null) {
                                            label = o.getPlaces().get(0).getCountry().getName(lang);
                                        }
                                        break;
                                    default:
                                        // do nothing
                                }
                                updateNodeMap(result, groupKey, role.getActor(), shade, label);
                                break;
                            case FUNCTION:
                                if (role.getAffiliation() != null && role.getActor().getActorType() == EActorType.PERSON && role.getAffiliation().getFunction() != null) {
                                    // use substitute, if any
                                    Profession substitute = role.getAffiliation().getFunction().getSubstitute();
                                    label = substitute != null ? substitute.getName(lang) : role.getAffiliation().getFunction().getName(lang);
                                }
                                updateNodeMap(result, groupKey, role.getActor(), shade, label);
                                break;
                            case ORGANIZATION:
                                if (role.getActor() != null && role.getActor().getActorType() == EActorType.ORGANIZATION) {
                                    updateNodeMap(result, groupKey, role.getActor(), shade);
                                }
                                else if (role.getAffiliation() != null && role.getAffiliation().getActor() != null) {
                                    updateNodeMap(result, groupKey, role.getAffiliation().getActor(), shade);
                                }

                                break;
                            case GENDER:
                                if (role.getActor().getActorType() == EActorType.PERSON && role.getActor().getGenderAsString() != null) {
                                    updateNodeMap(result, groupKey, role.getActor(), shade, role.getActor().getGenderAsString());
                                }
                                break;
                            case PUBLISHER:
                                if (!values.isBlank(excerpt.getSource())) {
                                    label = excerpt.getSource();
                                }
                                updateNodeMap(result, groupKey, role.getActor(), shade, label);
                                break;
                            default:
                                logger.warn("unknown key " + groupKey.name());
                        }
                    }
                }));

            Map<EExcerptGroupKey, Collection<ExcerptSociographyNode>> groupKeyMap = new LinkedHashMap<>();
            result.forEach((k, v) -> {
                ArrayList<ExcerptSociographyNode> nodes = new ArrayList<>(result.get(k).values());
                nodes.sort(new ExcerptSociographyNode.ExcerptSocioComparator());
                groupKeyMap.put(k, nodes);
            });

            socioData = new ExcerptSociography(new ArrayList<>(authorsMap.values()), groupKeyMap);
        }
        return socioData;
    }

    /**
     * Build the text view data. Will build a list of text where this argument (or similar) are present.
     *
     * @param user an user
     * @return a list of text holders
     */
    public List<TextHolder> viewTextTexts(WebdebUser user) {
        if(textDataTexts == null) {
            textDataTexts = new ArrayList<>();
            argumentContext.getAllTexts().stream().filter(user::mayView).forEach(e -> textDataTexts.add(new TextHolder(e, user.getId(), lang)));
        }
        return textDataTexts;
    }

    /**
     * Build the text view data. Will build a list of debates where this argument (or similar) are present.
     *
     * @return a list of debate holders
     */
    public List<DebateHolder> viewTextDebates() {
        if(textDataDebates == null) {
            textDataDebates = new ArrayList<>();
            argumentContext.getAllDebates().forEach(e -> textDataDebates.add(new DebateHolder(e, lang)));
        }
        return textDataDebates;
    }

    /**
     * Build the text view data. Will build a list of folders linked to this argument (or similars).
     *
     * @param user an user
     * @return a list of folder holders
     */
    public List<FolderHolder> viewTextFolders(WebdebUser user) {
        if(textDataFolders == null) {
            textDataFolders = new ArrayList<>();
            argumentContext.getAllFolders().stream().filter(user::mayView).forEach(e -> textDataFolders.add(new FolderHolder(e, user.getId(), lang)));
        }
        return textDataFolders;
    }

    /*
     * PRIVATE HELPERS
     */

    /**
     * Init all maps (metrics) needed for viz. That contains map of excerpts directly linked to this argument (and similar)
     * and for sociography, a map of position with others authors of excerpts linked in text or debate.
     *
     * @param user the webdeb user
     */
    private synchronized void fillAuthorsMaps(WebdebUser user){
        if(excerptsMap == null) {
            excerptsMap = new LinkedHashMap<>();
            initLinks();
            illustrationsLinks.forEach(e -> {
                if (user.mayView(e.getExcerpt())) {
                    EArgumentLinkShade shade = e.getArgumentLinkType().getEType();
                    ExcerptHolder excerptHolder = new ExcerptHolder(e.getExcerpt(), lang);

                    if(!excerptsMap.containsKey(excerptHolder.getId())){
                        excerptsMap.put(excerptHolder.getId(), excerptHolder);
                        excerptsShadeMap.put(excerptHolder.getId(), shade);
                    }

                    for (ActorRole role : e.getExcerpt().getThinkerActors()) {
                        Long actorId = role.getActor().getId();
                        if (!authorsShadeMap.containsKey(actorId)) {
                            authorsShadeMap.put(actorId, shade);
                            authorsExcerptsMap.put(actorId, new HashSet<>());
                            authorsRolesMap.put(actorId, new HashMap<>());
                            authorsMap.put(actorId, new ActorHolder(role.getActor(), lang));
                        } else if (shade != authorsShadeMap.get(actorId)){
                            authorsShadeMap.put(actorId, EArgumentLinkShade.INDECISION);
                        }
                        authorsExcerptsMap.get(actorId).add(excerptHolder.getId());
                        authorsRolesMap.get(actorId).put(excerptHolder.getId(), role);
                    }
                }
            });
            authorsExcerptsMap.forEach((k, v) -> {
                if(authorsShadeMap.get(k) == EArgumentLinkShade.INDECISION){
                    v.forEach(e -> excerptsMap.get(e).setIndecision(true));
                    authorsMap.get(k).setIndecision(true);
                    authorsShadeMap.put(k, EArgumentLinkShade.SHADED_EXAMPLE);
                }
            });
        }
    }

    /**
     * Helper method to update a given map of sociography nodes with a new ExcerptSociographyNode for given actor
     *
     * @param list a list of sociography node (will be updated)
     * @param key the key that will be used to group sociography node in the map
     * @param actor the actor to put in node map
     * @param type the link type depicting the position of a particular argument for given label
     */
    private void updateNodeMap(Map<EExcerptGroupKey, Map<String, ExcerptSociographyNode>> list, EExcerptGroupKey key, Actor actor, EArgumentLinkShade type) {
        String label = actor.getFullname(lang);
        String avatar = actor.getAvatar() != null ? actor.getAvatar() : helper.computeAvatar(actor.getActorType().id(), null);
        ExcerptSociographyNode node = list.get(key).containsKey(label) ? list.get(key).get(label)
                : new ExcerptSociographyNode(actor.getId(), label, label, avatar,
                new ActorHolder(actor, lang).getFilterable(), false);
        list.get(key).put(label, node.increment(type, actor.getId()));
    }


    /**
     * Helper method to update a given map of sociography nodes with a new ExcerptSociographyNode with given label and type
     *
     * @param list a list of sociography node (will be updated)
     * @param key the key that will be used to group sociography node in the map
     * @param type the link type depicting the position of a particular argument for given label
     * @param actor the actor for which we update the node
     * @param label a label (semantics depend on the actual key used to group nodes behind a label), may be null
     */
    private void updateNodeMap(Map<EExcerptGroupKey, Map<String, ExcerptSociographyNode>> list, EExcerptGroupKey key, Actor actor, EArgumentLinkShade type, String label) {
        boolean isUnknown = false;
        String updatedLabel = label;
        if (updatedLabel == null) {
            isUnknown = true;
            switch (key) {
                case AGE:
                    updatedLabel = i18n.get(Lang.forCode(lang),"viz.actor.socio.age.unknown");
                    break;

                case COUNTRY:
                    updatedLabel = i18n.get(Lang.forCode(lang),"viz.actor.socio.country.unknown");
                    break;

                case ORGANIZATION:
                    updatedLabel = i18n.get(Lang.forCode(lang),"viz.actor.socio.org.unknown");
                    break;

                case FUNCTION:
                    updatedLabel = i18n.get(Lang.forCode(lang),"viz.actor.socio.function.unknown");
                    break;

                case GENDER:
                    updatedLabel = i18n.get(Lang.forCode(lang),"viz.actor.socio.gender.unknown");
                    break;

                case PUBLISHER:
                    updatedLabel = i18n.get(Lang.forCode(lang),"viz.actor.socio.publisher.unknown");
                    break;

                default:
                    // do nothing
            }
        }
        ExcerptSociographyNode node = list.get(key).containsKey(updatedLabel) ? list.get(key).get(updatedLabel)
                : new ExcerptSociographyNode(updatedLabel, isUnknown);
        list.get(key).put(updatedLabel, node.increment(type, actor.getId()));
    }

    /**
     * Search after a similarity link for the given argument id in the map of similarity.
     *
     * @return the index where the similarity appears, -1 if there is no similarity found.
     */
    private Long searchAfterSimilar(Long argToSearch){
        for(Map.Entry<Long, Set<Long>> entry : similarMap.entrySet()){
            if(entry.getValue().contains(argToSearch)){
                return entry.getKey();
            }
        }
        return -1L;
    }

    /*
     * GETTERS / SETTERS
     */

    /**
     * Retrieve all directly linked arguments (do not explore similarity links)
     *
     * @return a possibly empty list of ArgumentHolders
     */
    public List<ArgumentHolder> getLinkedArguments(WebdebUser user) {
        return new ArrayList<>();
    }

    /*
     * STATS AND METRICS MANAGEMENT
     */

    /**
     * Retrieve the amount of excerpts linked to this argument by shade as integer map
     *
     * @return the amount of excerpts that are connected to this argument by shade as integer map
     */
    public synchronized Map<Integer, Integer> getExcerptUserMetrics() {
        if(excerptUserMetric == null) {
            excerptUserMetric = new LinkedHashMap<>();
            excerptUserMetric.put(EArgumentLinkShade.ILLUSTRATE.id(), 0);
            excerptUserMetric.put(EArgumentLinkShade.SHADED_EXAMPLE.id(), 0);
            excerptUserMetric.put(EArgumentLinkShade.COUNTER_EXAMPLE.id(), 0);

            argumentContext.getIllustrationLinks().forEach(e -> excerptUserMetric.put(e.getArgumentLinkType().getLinkShade(),
                    excerptUserMetric.get(e.getArgumentLinkType().getLinkShade()) + 1));
        }
        return excerptUserMetric;
    }

    /**
     * Initialize justification and illustrations
     */
    private synchronized void initLinks(){
        if(justificationLinksThatJustify == null || illustrationsLinks == null) {
            justificationLinksThatJustify = allLinks ?
                    argumentContext.getAllJustificationLinksThatJustify() : argumentContext.getJustificationLinksThatJustifyAsFirstArgument();

            illustrationsLinks = argumentContext.getAllIllustrationLinks(isForDebate);
        }
    }

    /**
     * Transform the result set to a list of ArgumentContextVizHolder for cartography view.
     *
     * @param result the set of argumentContext id
     * @param resultHolder the map of holders linked to theirs id
     */
    private List<ArgumentContextVizHolder> transformCartoResultMap(List<Long> result, Map<Long, ArgumentContextVizHolder> resultHolder){
        List<ArgumentContextVizHolder> r = new ArrayList<>();
        result.forEach(v -> r.add(resultHolder.get(v)));
        return r;
    }
}
