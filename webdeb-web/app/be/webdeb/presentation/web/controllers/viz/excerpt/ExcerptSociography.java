/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.viz.excerpt;

import be.webdeb.presentation.web.controllers.entry.actor.ActorHolder;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * This class contains list and collection to make the sociography content and filters
 *
 * @author Martin Rouffiange
 */
public class ExcerptSociography {

    private List<ActorHolder> actors;
    private Map<EExcerptGroupKey, Collection<ExcerptSociographyNode>> viewed;

    /**
     * Construct an argument sociography for view
     *
     * @param args the list of argument holder concerned (for make filters)
     * @param viewed a formated Map of point of view (EExcerptGroupKey) and collection for the pov
     */
    public ExcerptSociography(List<ActorHolder> args, Map<EExcerptGroupKey, Collection<ExcerptSociographyNode>> viewed){
        this.actors = args;
        this.viewed = viewed;
    }

    /**
     * Get the list of actor holder concerned (for make filters)
     *
     * @return the possibly empty list of actor holder concerned (for make filters)
     */
    public List<ActorHolder> getActors() {
        return actors;
    }

    /**
     * Get the formated collection for the view
     *
     * @return the possibly empty collection for the view
     */
    public Map<EExcerptGroupKey, Collection<ExcerptSociographyNode>> getViewed() {
        return viewed;
    }
}