/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.viz.excerpt;

import be.webdeb.core.api.argument.EArgumentLinkShade;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Inner class from ActorVizHolder to handle actor sociography nodes, ie, data series objects used to build a sociography graph.
 * Contains an id in case the node refers to an actor (author or organization), a label to be displayed and
 * an array of integer values corresponding to the amount of arguments resp. citation, shaded citation or counter citation
 * to all excerpts of the actor holding those nodes.
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class ExcerptSociographyNode {

    // custom logger
    protected static final org.slf4j.Logger logger = play.Logger.underlying();

    private Long id;
    private String label;
    private String avatar;
    private String sortkey;
    // amount of similar, qualifying and opposed actors
    private Map<EArgumentLinkShade, Integer> statistics = new LinkedHashMap<>();
    private Map<EArgumentLinkShade, Set<Long>> actorInStatistics = new LinkedHashMap<>();
    private boolean isUnknown;
    private String filterable;

    /**
     * Construct a node for any type of sortkey (being the label itself) except actors
     *
     * @param label the label displayed in the sociography graph
     * @param isUnknown boolean saying if this node is unknown (to be put at end of list in graph)
     */
    public ExcerptSociographyNode(String label, boolean isUnknown) {
        this(-1L, label, label, null, "", isUnknown);
    }

    /**
     * Construct a node for actors
     *
     * @param id the actor id
     * @param sortkey the key on which this node will be ordered
     * @param label the label displayed in the sociography graph
     * @param avatar the name of the actor's avatar (may be null)
     * @param filterable the filterable key-values for this node
     * @param isUnknown boolean saying if this node is unknown (to be put at end of list in graph)
     */
    public ExcerptSociographyNode(Long id, String sortkey, String label, String avatar, String filterable, boolean isUnknown) {
        this.id = id;
        this.sortkey = sortkey;
        this.label = label;
        this.avatar = avatar;
        this.filterable = filterable;
        this.isUnknown = isUnknown;

        statistics.put(EArgumentLinkShade.ILLUSTRATE, 0);
        statistics.put(EArgumentLinkShade.SHADED_EXAMPLE, 0);
        statistics.put(EArgumentLinkShade.COUNTER_EXAMPLE, 0);

        actorInStatistics.put(EArgumentLinkShade.ILLUSTRATE, new HashSet<>());
        actorInStatistics.put(EArgumentLinkShade.SHADED_EXAMPLE, new HashSet<>());
        actorInStatistics.put(EArgumentLinkShade.COUNTER_EXAMPLE, new HashSet<>());
    }

    /**
     * Get the idof this node, if any
     *
     * @return either an actor id, or -1 if this node is not an actor
     */
    public Long getId() {
        return id;
    }

    /**
     * Get the value on which the nodes are sorted
     *
     * @return a sorting value
     */
    String getSortkey() {
        return sortkey;
    }

    /**
     * Get the label used to show this node
     *
     * @return a label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Get the avatar file name, if this node is an actor
     *
     * @return a file name, null if unset
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * Get the statistics array with, resp. IS_SUPPORTED, IS_SHADED and IS_REJECTED amounts for this node
     *
     * @return the statistics array
     */
    public Map<EArgumentLinkShade, Integer> getStatistics() {
        return statistics;
    }

    /**
     * Get the statistics as string separated by ","
     *
     * @return the statistics as string
     */
    public String getStatisticsAsString() {
        return statistics.values().stream().map(String::valueOf).collect(Collectors.joining(","));
    }

    /**
     * Re-implement the ContributionHolder's filterable property
     *
     * @return a stringified list of (key, value) pairs
     */
    public String getFilterable() {
        // does not need to jsonify it as it has been added in dedicated constructor
        return filterable;
    }

    /**
     * Boolean flag to kwow if this node is the special "unknown" node
     *
     * @return true if this node is the unknown one (will be put at the end of visualization)
     */
    public boolean isUnknown() {
        return isUnknown;
    }

    /**
     * Increment statistics value depending on given link shade
     *
     * @param type a link shade to increment
     * @param actorId the actor id for which we want to increment stats
     * @return this updated node
     */
    public ExcerptSociographyNode increment(EArgumentLinkShade type, Long actorId) {
        if(!actorInStatistics.get(type).contains(actorId))
            statistics.put(type, statistics.get(type) + 1);
        return this;
    }

    /**
     * Comparator class to sort sociography nodes based on the statistics
     */
    public static class ExcerptSocioComparator implements Comparator<ExcerptSociographyNode> {
        @Override
        public int compare(ExcerptSociographyNode o1, ExcerptSociographyNode o2) {
            if (o1 == null || o1.getStatistics().isEmpty() || o1.isUnknown()) {
                return 1;
            }
            if (o2 == null || o2.getStatistics().isEmpty() || o2.isUnknown()) {
                return -1;
            }

            int sumO1 = o1.getStatistics().values().stream().mapToInt(Number::intValue).sum();
            int sumO2 = o2.getStatistics().values().stream().mapToInt(Number::intValue).sum();


            // (similar - opposed from second node relative to the sum of all elements)
            // - (similar - opposed from first node relative to the sum of all elements)
            double stat = ((double)(o2.getStatistics().get(EArgumentLinkShade.ILLUSTRATE) - o2.getStatistics().get(EArgumentLinkShade.COUNTER_EXAMPLE)) / sumO1)
                    - ((double)(o1.getStatistics().get(EArgumentLinkShade.ILLUSTRATE) - o1.getStatistics().get(EArgumentLinkShade.COUNTER_EXAMPLE)) / sumO2);

            if (BigDecimal.valueOf(stat).compareTo(BigDecimal.ZERO) == 0) {
                return (int) Math.round(stat);
            }

            int total = sumO2 - sumO1;
            if (total != 0) {
                return total;
            }

            return o1.getSortkey().compareTo(o2.getSortkey());
        }
    }
}