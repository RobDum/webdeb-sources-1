/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.presentation.web.controllers.viz;

import be.webdeb.core.api.actor.Actor;
import be.webdeb.core.api.actor.ActorFactory;
import be.webdeb.core.api.argument.Argument;
import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentFactory;
import be.webdeb.core.api.argument.EArgumentLinkShade;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contributor.Contributor;
import be.webdeb.core.api.contributor.Group;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.debate.DebateFactory;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.folder.FolderFactory;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.api.text.TextFactory;
import be.webdeb.infra.ws.external.text.VizTextForm;
import be.webdeb.infra.ws.external.text.VizTextResponse;
import be.webdeb.presentation.web.controllers.CommonController;
import be.webdeb.presentation.web.controllers.SessionHelper;
import be.webdeb.presentation.web.controllers.account.AdvicesForm;
import be.webdeb.presentation.web.controllers.browse.SearchForm;
import be.webdeb.presentation.web.controllers.entry.ContributionHolder;
import be.webdeb.presentation.web.controllers.entry.excerpt.ExcerptHolder;
import be.webdeb.presentation.web.controllers.entry.text.TextHolder;
import be.webdeb.presentation.web.controllers.permission.WebdebUser;
import be.webdeb.presentation.web.controllers.viz.actor.ActorVizHolder;
import be.webdeb.presentation.web.controllers.viz.argument.ArgumentContextVizHolder;
import be.webdeb.presentation.web.controllers.viz.debate.DebateVizHolder;
import be.webdeb.presentation.web.controllers.viz.folder.FolderVizHolder;
import be.webdeb.presentation.web.controllers.viz.text.TextVizHolder;
import be.webdeb.presentation.web.views.html.browse.search;
import be.webdeb.presentation.web.views.html.oops.oops;
import be.webdeb.presentation.web.views.html.viz.actor.actorVisualization;
import be.webdeb.presentation.web.views.html.viz.argument.argumentDetails;
import be.webdeb.presentation.web.views.html.viz.argument.argumentVisualization;
import be.webdeb.presentation.web.views.html.viz.debate.debateVisualization;
import be.webdeb.presentation.web.views.html.viz.excerpt.excerptDetails;
import be.webdeb.presentation.web.views.html.viz.explore;
import be.webdeb.presentation.web.views.html.viz.explore2;
import be.webdeb.presentation.web.views.html.viz.text.textVisualization;
import be.webdeb.presentation.web.views.html.viz.text.textRadioContent;
import be.webdeb.presentation.web.views.html.viz.folder.folderVisualization;
import be.webdeb.presentation.web.views.html.viz.privateContribution;
import be.webdeb.presentation.web.views.html.util.message;
import play.data.Form;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import play.twirl.api.Content;

import javax.inject.Inject;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;


/**
 * Simple routing class for contribution visualization
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public class VizActions extends CommonController {

  /**
   * main page for visualization -> redirect to search
   *
   * @return the main page with the available possibilities as new entries
   */
  public CompletionStage<Result> index() {
    logger.debug("GET explore page");

    WebdebUser user = sessionHelper.getUser(ctx());
    String lang = ctx().lang().code();

    Map<Integer, Integer> amount = new HashMap<>();

    amount.put(EContributionType.DEBATE.id(), helper.getAmountOf(EContributionType.DEBATE, Group.getGroupPublic()));
    amount.put(EContributionType.ACTOR.id(), helper.getAmountOf(EContributionType.ACTOR, Group.getGroupPublic()));
    amount.put(EContributionType.ARGUMENT_CONTEXTUALIZED.id(), helper.getAmountOf(EContributionType.ARGUMENT_CONTEXTUALIZED, Group.getGroupPublic()));
    amount.put(EContributionType.EXCERPT.id(), helper.getAmountOf(EContributionType.EXCERPT, Group.getGroupPublic()));

    // build list of latest contributions
    List<ContributionHolder> latestEntries =
            helper.toHolders(textFactory.getLatestEntries(EContributionType.ALL, -1L,
                    configuration.getInt("latest.contributions.size"), user.getGroup().getGroupId()),
                    user,
                    ctx().lang().code());

    Map<Integer, List<ContributionHolder>> map = new LinkedHashMap<>();
    putContributionsToExploreInMapForType(map, EContributionType.ACTOR, user, lang);
    putContributionsToExploreInMapForType(map, EContributionType.DEBATE, user, lang);
    putContributionsToExploreInMapForType(map, EContributionType.FOLDER, user, lang);
    putContributionsToExploreInMapForType(map, EContributionType.TEXT, user, lang);
    putContributionsToExploreInMapForType(map, EContributionType.EXCERPT, user, lang);

    AdvicesForm advicesForms = new AdvicesForm(contributorFactory.getAdvices(), lang);

    return CompletableFuture.supplyAsync(() -> ok(explore2.render(latestEntries, map, advicesForms, amount, user)), context.current());
    //return CompletableFuture.supplyAsync(() -> ok(explore.render(null, user, null)), context.current());
  }

  private void putContributionsToExploreInMapForType(Map<Integer, List<ContributionHolder>> map, EContributionType type, WebdebUser user, String lang){
    map.put(type.id(), helper.fromExploreToHolders(contributorFactory.getContributionsToExploreForGroup(type.id(), Group.getGroupPublic()), user, lang));
  }

    /**
     * Dispatcher method to redirect to right visualization page depending on given id and type.
     * Does not perform any validation on parameters, except that type must exist
     *
     * @param id a contribution id
     * @param type a valid type id
     * @param viz the pane wanted
     * @return redirect to appropriate viz page depending on given type, otherwise redirect to browse page
     */
    public CompletionStage<Result> dispatch(Long id, int type, int viz) {
        EContributionType ctype = EContributionType.value(type);
        if (ctype == null) {
            logger.warn("invalid call on dispatcher visualisation index page");
            return CompletableFuture.completedFuture(redirect(be.webdeb.presentation.web.controllers.browse.routes.BrowseActions.search()));
        }

        // in case we have a flashing scope, must pass it for redirect
        Http.Flash redirected = flash();
        if (!redirected.isEmpty()) {
            redirected.forEach((key, value) -> flash().put(key, value));
        }

        // switch on type and redirect appropriately
        switch (ctype) {
            case ACTOR:
                return CompletableFuture.completedFuture(redirect(routes.VizActions.actor(id, EVizPane.value(viz) != null ? EVizPane.value(viz).id() : EVizPane.CARTO.id(), 0)));
            case ARGUMENT_CONTEXTUALIZED:
                return CompletableFuture.completedFuture(redirect(routes.VizActions.argumentContext(id, EVizPane.value(viz) != null ? EVizPane.value(viz).id() : EVizPane.CARTO.id(), 0)));
          case ARGUMENT:
            return CompletableFuture.completedFuture(redirect(routes.VizActions.argument(id, 0)));
            case DEBATE:
                return CompletableFuture.completedFuture(redirect(routes.VizActions.debate(id, EVizPane.value(viz) != null ? EVizPane.value(viz).id() : EVizPane.CARTO.id(), 0)));
            case TEXT:
                return CompletableFuture.completedFuture(redirect(routes.VizActions.text(id, EVizPane.value(viz) != null ? EVizPane.value(viz).id() : EVizPane.DETAILS.id(), 0)));
            case FOLDER:
                return CompletableFuture.completedFuture(redirect(routes.VizActions.folder(id, EVizPane.value(viz) != null ? EVizPane.value(viz).id() : EVizPane.DETAILS.id(), 0)));
            default:
                logger.warn("invalid type given for dispatcher index " + ctype);
                return CompletableFuture.completedFuture(redirect(be.webdeb.presentation.web.controllers.browse.routes.BrowseActions.search()));
        }
    }

  /**
   * Display the visualization screen for given contextualized argument and pane
   *
   * @param id a contextualized argument id
   * @param pane the visualization pane id (aka EVIzPane key)
   * @return the visualization page for given contextualized argument
   */
  public CompletionStage<Result> argumentContext(Long id, int pane, int pov) {
    logger.debug("GET argument viz for " + id + " and pane " + pane);
    ArgumentContext argumentContext = sessionHelper.isBot(request()) ? argumentFactory.retrieveContextualized(id)
            : argumentFactory.retrieveContextualizedWithHit(id);
    WebdebUser user = sessionHelper.getUser(ctx());
    EVizPane viz = EVizPane.value(pane);

    if (argumentContext == null || viz == null) {
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(),"argumentcontext.not.found"));
      return CompletableFuture.completedFuture(Results.notFound(
              oops.render("argument visualization page for id=" + id, user)));
    }

    if(!user.mayView(argumentContext)){
      return CompletableFuture.completedFuture(Results.unauthorized(
              privateContribution.render(user)));
    }

    return CompletableFuture.supplyAsync(() ->
                    ok(argumentVisualization.render(
                            new ArgumentContextVizHolder(argumentContext, ctx().lang().code()),
                            null,
                            viz,
                            pov,
                            user,
                            null))
            , context.current()
    );
  }

  /**
   * Display the visualization screen for given contextualized argument from an argument id or a dictionary id and pane
   *
   * @param id an argument id or an argument dictionary id
   * @param dic from a dictionary id or not
   * @return the visualization page for given contextualized argument
   */
  public CompletionStage<Result> argument(Long id, int dic) {
    logger.debug("GET argument viz for " + id);

    ArgumentContext arg = argumentFactory.retrieveContextualizedFromArg(id, dic != 0);

    return argumentContext(arg == null ? -1L : arg.getId(), EVizPane.TEXTS.id(), 0);
  }

  /**
   * Display the visualization screen for given debate and pane
   *
   * @param id a debate id
   * @param pane the visualization pane id (aka EVIzPane key)
   * @param pov the point of vue wanted for the pane
   *
   * @return the visualization page for given debate
   */
  public CompletionStage<Result> debate(Long id, int pane, int pov) {
    logger.debug("GET debate viz for " + id + " and pane " + pane);
    Debate debate = sessionHelper.isBot(request()) ? debateFactory.retrieve(id) : debateFactory.retrieveWithHit(id);
    WebdebUser user = sessionHelper.getUser(ctx());
    EVizPane viz = EVizPane.value(pane);

    if (debate == null || viz == null) {
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(),"debate.not.found"));
      return CompletableFuture.completedFuture(Results.notFound(
              oops.render("debate visualization page for id=" + id, user)));
    }

    if(!user.mayView(debate)){
      return CompletableFuture.completedFuture(Results.unauthorized(
              privateContribution.render(user)));
    }

    return CompletableFuture.supplyAsync(() ->
                    ok(debateVisualization.render(
                            new DebateVizHolder(debate, ctx().lang().code()),
                            null,
                            viz,
                            pov,
                            user,
                            null))
            , context.current()
    );
  }

  /**
   * Display the visualization screen for a given actor and pane
   *
   * @param id an actor id
   * @param pane the visualization pane id (aka EVIzPane key)
   *
   * @return the visualization page for given actor
   */
  public CompletionStage<Result> actor(Long id, int pane, int pov) {
    logger.debug("GET actor viz for " + id + " and pane " + pane);
    Actor actor = sessionHelper.isBot(request()) ? actorFactory.retrieve(id) : actorFactory.retrieveWithHit(id);
    WebdebUser user = sessionHelper.getUser(ctx());
    EVizPane viz = EVizPane.value(pane);

    if (actor == null || viz == null) {
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(),"actor.not.found"));
      return CompletableFuture.completedFuture(Results.notFound(
          oops.render("actor visualization page for id=" + id, user)));
    }

    if(!user.mayView(actor)){
      return CompletableFuture.completedFuture(Results.unauthorized(
              privateContribution.render(user)));
    }

    return CompletableFuture.supplyAsync(() ->
        ok(actorVisualization.render(
            new ActorVizHolder(actor, ctx().lang().code()),
            null,
            viz,
            pov,
            user,
            null)),
        context.current()
    );
  }

  /**
   * Display the visualization screen for a given text and pane
   *
   * @param id a text id
   * @param pane the visualization pane id (aka EVIzPane key)
   *
   * @return the visualization page for given text
   */
  public CompletionStage<Result> text(Long id, int pane, int pov) {
    logger.debug("GET text viz for " + id + " and pane " + pane);

    Text text = sessionHelper.isBot(request()) ? textFactory.retrieve(id) : textFactory.retrieveWithHit(id);
    WebdebUser user = sessionHelper.getUser(ctx());
    EVizPane viz = EVizPane.value(pane);

    if (text == null || viz == null) {
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(),"text.not.found"));
      return CompletableFuture.completedFuture(Results.notFound(
          oops.render("text visualization page for id=" + id, user)));
    }

    if(!user.mayView(text)){
      return CompletableFuture.completedFuture(Results.unauthorized(
              privateContribution.render(user)));
    }

    CompletionStage<Result>r =  CompletableFuture.supplyAsync(() ->
        ok(textVisualization.render(
            new TextVizHolder(text, user.getId(), ctx().lang().code()),
            null,
            viz,
            pov,
            user,
            null)),
        context.current()
    );

    return r;
  }

  /**
   * Display the visualization screen for a given folder and pane
   *
   * @param id a folder id
   * @param pane the visualization pane id (aka EVIzPane key)
   *
   * @return the visualization page for given folder
   */
  public CompletionStage<Result> folder(Long id, int pane, int pov) {
    logger.debug("GET folder viz for " + id + " and pane " + pane);
    Folder folder = sessionHelper.isBot(request()) ? folderFactory.retrieve(id) : folderFactory.retrieveWithHit(id);
    EVizPane viz = EVizPane.value(pane);
    if (folder == null || viz == null) {
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(),"folder.notfound"));
      return CompletableFuture.completedFuture(Results.notFound(
          oops.render("folder visualization page for id=" + id, sessionHelper.getUser(ctx()))));
    }

    return CompletableFuture.supplyAsync(() ->
            ok(folderVisualization.render(
                new FolderVizHolder(folder, sessionHelper.getUser(ctx()).getId(), ctx().lang().code()),
                null,
                viz,
                pov,
                sessionHelper.getUser(ctx()),
                null)),
        context.current()
    );
  }

  /**
   * Display the details page of the given contribution
   *
   * @param id a contribution id
   *
   * @return the details page of the given contribution
   */
  public CompletionStage<Result> details(Long id) {
    logger.debug("GET details of " + id);
    Contribution contribution = textFactory.retrieveContribution(id);
    WebdebUser user = sessionHelper.getUser(ctx());
    String lang = ctx().lang().code();

    if (contribution == null) {
      flash(SessionHelper.ERROR, i18n.get(ctx().lang(),"contribution.notfound"));
      return CompletableFuture.completedFuture(Results.notFound(
              oops.render("details page for id=" + id, sessionHelper.getUser(ctx()))));
    }

    if(!user.isAdminOf(contribution)){
      return CompletableFuture.supplyAsync(Results::unauthorized, context.current());
    }

    Content html;

    switch (contribution.getType()){
      case ARGUMENT_CONTEXTUALIZED:
            html = argumentDetails.render(new ArgumentContextVizHolder((ArgumentContext) contribution, lang), user);
            break;
        case EXCERPT:
            html = excerptDetails.render(new ExcerptHolder((Excerpt) contribution, lang), user);
            break;
        default:
            html = null;
    }

    if(html == null){
        return CompletableFuture.supplyAsync(Results::badRequest, context.current());
    }

    return CompletableFuture.supplyAsync(() -> ok(html), context.current());
  }

  /*
   * ARGUMENT PARTIAL PAGES
   */

  /**
   * Get the justification map for a given argument
   *
   * @param argument the argument for which we need to draw the justification links
   * @param pov the point of vue
   * @return the argument cartography pane
   */
  public CompletionStage<Result> getJustificationMap(Long argument, int pov) {
    WebdebUser user = sessionHelper.getUser(ctx());
    Argument arg = argumentFactory.retrieve(argument);

    if (arg == null) {
      logger.error("argument not found");
      return CompletableFuture.completedFuture(notFound());
    }

    return CompletableFuture.supplyAsync(Results::ok, context.current());
  }

  /*
   * TEXT PARTIAL PAGES
   */

  /**
   * Get the partial text radiography pane (all texts having similar excerpts to any excerpt from this text)
   *
   * @param id the text id
   * @param pov the point of view (aka ERadiographyViewKey)
   * @return the radiography partial page, or an error message if given text / viz is not found
   */
  public CompletionStage<Result> textRadiography(Long id, int pov) {
    logger.debug("GET text radio content for " + id + " and pov " + pov);
    Text text = textFactory.retrieve(id);
    Map<String, String> messages = new HashMap<>();
    ERadiographyViewKey view = ERadiographyViewKey.value(pov);
    if (text == null || view == null) {
      messages.put(SessionHelper.ERROR, i18n.get(ctx().lang(),"viz.text.radio.error"));
      return CompletableFuture.supplyAsync(() -> badRequest(message.render(messages)), context.current());
    }

    WebdebUser user = sessionHelper.getUser(ctx());
    return CompletableFuture.supplyAsync(() ->
        ok(textRadioContent.render(new TextVizHolder(text, user.getId(), ctx().lang().code()), user)),
        context.current());
  }

  /*
   * External text requests
   */

  /**
   * Get all data about the text (text details and text arguments). The post method is used to hide the toke from spy.
   *
   * @return the text data or bad request if the text is unknown
   */
  public CompletionStage<Result> getTextData() {
    logger.debug("[POST] get data about text");
    Form<VizTextForm> form = formFactory.form(VizTextForm.class).bindFromRequest();

    if (!form.hasErrors()) {
      VizTextForm textForm = form.get();
      Contributor contributor = textForm.getUser().getContributor();
      if(contributor != null) {
        session(SessionHelper.KEY_USERMAILORPSEUDO, contributor.getEmail());
      }
      WebdebUser user = sessionHelper.getUser(ctx());
      Text text = textFactory.findByUrl(textForm.getUrl());

      if(text != null) {
        VizTextResponse textsResponse =
                new VizTextResponse(new TextHolder(text, user.getId(), textForm.getUser().getLang()), user);

        return CompletableFuture.supplyAsync(() ->
                ok(Json.toJson(textsResponse).toString()), context.current());
      }
    }
    return CompletableFuture.supplyAsync(Results::badRequest, context.current());
  }

  /*
   * HELPER METHODS
   */

  /**
   * Sort a map of values based on the RadioKey values
   * key = AUTHOR: sort alphabetically and handle last name and name separately
   * key = DATE: sort them in reverse order
   * key = SOURCE: sort them alphabetically
   * key = TEXT : sort text alphabetically on their titles
   *
   * @param unsortedMap an unsorted map
   * @param <V> the actual type of values in the map
   * @return a sorted map
   */
  public <V> Map<RadiographyKey, V> sortByRadioKey(Map<RadiographyKey, V> unsortedMap) {
    @SuppressWarnings("fallthrough")
    Map<RadiographyKey, V> sortedMap = new TreeMap<>((o1, o2) -> {
      switch (o1.getKey()) {
        case AUTHOR:
          // if we have a lastname, use it (ie, we are handling persons)
          String name1 = o1.getLastname() != null ? o1.getLastname() : o1.getName();
          String name2 = o2.getLastname() != null ? o2.getLastname() : o2.getName();
          if (name1.equals(name2)) {
            return o1.getName().compareTo(o2.getName());
          }
          return name1.compareTo(name2);

        case DATE:
        case SOURCE:
        case TEXT:
          if(o1.getKey().equals(ERadiographyViewKey.DATE)){
            // IMPORTANT NOTE -> compare is reversed (want to sort desc)
            Date date1 = values.toDate(o1.getName());
            Date date2 = values.toDate(o2.getName());
            if (date1 != null && date2 != null) {
              return date2.compareTo(date1);
            }
          }
          return o1.getName().compareTo(o2.getName());

        default:
          return -1;
      }
    });

    sortedMap.putAll(unsortedMap);
    return sortedMap;
  }

  /**
   * Fill in a given map with default values (following the order of similar, qualifies, opposes, supports,
   * shades and rejects links)
   *
   * @param map a map to be filled in with default values
   * @param defaults exactly 6 integer values to be put as default metrics in map
   */
  public void initMetrics(Map<EArgumentLinkShade, Integer> map, int[] defaults) {
    map.put(EArgumentLinkShade.ILLUSTRATE, defaults[0]);
    map.put(EArgumentLinkShade.SHADED_EXAMPLE, defaults[1]);
    map.put(EArgumentLinkShade.COUNTER_EXAMPLE, defaults[2]);
  }

  /**
   * Update given statistics of given type for given (author, argument or text) id
   *
   * @param metrics the map of metrics to update (any contribution type), usage is dependent to the actual viz
   * @param pane the pane id for the stat
   * @param type the linktype to update for given id
   */
  public void updateMetricsById(Map<EVizPane, Map<Long, Map<EArgumentLinkShade, Integer>>> metrics,
                                 EVizPane pane, Long id, EArgumentLinkShade type) {
    if (!metrics.get(pane).containsKey(id)) {
      metrics.get(pane).put(id, new HashMap<>());
      metrics.get(pane).get(id).put(EArgumentLinkShade.ILLUSTRATE, 0);
      metrics.get(pane).get(id).put(EArgumentLinkShade.SHADED_EXAMPLE, 0);
      metrics.get(pane).get(id).put(EArgumentLinkShade.COUNTER_EXAMPLE, 0);
    }
    metrics.get(pane).get(id).put(type, metrics.get(pane).get(id).get(type) + 1);
  }


  /**
   * Aggregate the map of statistics by id and calculate final metrics such as for each similarity link shade,
   * the amount of authors is calculated.
   *
   * @param metrics metrics map to aggregate values for
   * @param pane the pane for which the aggregation must be performed
   * @param optOut actor id to ignore when aggregating the author stats
   */
  public Map<EArgumentLinkShade, Integer> aggregateMetricsById(Map<EVizPane, Map<Long, Map<EArgumentLinkShade, Integer>>> metrics,
                                                               EVizPane pane, Long optOut) {

    Map<EArgumentLinkShade, Integer> result = new EnumMap<>(EArgumentLinkShade.class);
    initMetrics(result, new int[] {0, 0, 0});

    metrics.get(pane).entrySet().stream().filter(e -> !e.getKey().equals(optOut)).forEach(e -> {
      EArgumentLinkShade dominant = getDominantShade(e.getValue());
      result.put(dominant, result.get(dominant) + 1);
    });
    return result;
  }

  /**
   * Calculate the dominant shade for a map of shades and their amount such as:
   * <ul>
   *   <li>if the amount of similar is strictly higher than the amount of opposes and qualifies, the dominant is similar</li>
   *   <li>if the amount of opposes is strictly higher than the amount of similar and qualifies, the dominant is opposes</li>
   *   <li>the dominant is qualifies in any other cases</li>
   * </ul>
   *
   * @param metric a map of shades with their total metrics
   * @return the dominant shade
   */
  public EArgumentLinkShade getDominantShade(Map<EArgumentLinkShade, Integer> metric) {
    if(metric != null) {

      int illustrate = metric.get(EArgumentLinkShade.ILLUSTRATE) == null ? 0 : metric.get(EArgumentLinkShade.ILLUSTRATE);
      int counter = metric.get(EArgumentLinkShade.COUNTER_EXAMPLE) == null ? 0 : metric.get(EArgumentLinkShade.COUNTER_EXAMPLE);
      int shaded = metric.get(EArgumentLinkShade.SHADED_EXAMPLE) == null ? 0 : metric.get(EArgumentLinkShade.SHADED_EXAMPLE);

      if (illustrate > counter + shaded) {
        return EArgumentLinkShade.ILLUSTRATE;
      } else {
        if (illustrate + shaded <= counter) {
          return EArgumentLinkShade.COUNTER_EXAMPLE;
        }
      }
    }
    return EArgumentLinkShade.SHADED_EXAMPLE;
  }

}
