/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.excerpt;

import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.exception.PersistenceException;

import java.util.Map;

/**
 * This interface represents a context element in the webdeb system. Holds string element to add description on element
 * like ncitation.
 *
 * @author Martin Rouffiange
 */
public interface ContextElement {

    /**
     * Retrieve the context element unique id
     *
     * @return the context element id, or -1 if unset
     */
    Long getId();

    /**
     * Set the context element id
     *
     * @param id the context element id to set
     */
    void setId(Long id);

    /**
     * Get the contribution linked to this element, if any
     *
     * @return a possibly null contribution
     */
    Contribution getContribution();

    /**
     * Set the contribution linked to this element
     *
     * @param contribution a contribution
     */
    void setContribution(Contribution contribution);

    Map<String, String> getSpellings();

    String getSpelling(String lang);

    void addSpelling(String lang, String spelling);

    void setSpellings(Map<String, String> spellings);

    /**
     * Save context in element
     *
     * @throws PersistenceException if the save action(s) could not been performed because of an issue with
     * the persistence layer
     */
    void save() throws PersistenceException;
}
