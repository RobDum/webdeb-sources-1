/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.excerpt;

import be.webdeb.core.api.contribution.ExternalContribution;
import be.webdeb.core.api.text.ExternalText;
import be.webdeb.core.api.text.Text;

/**
 * This interface represents a external excerpt Contribution.
 * It's a temporary excerpt that is keep for an easiest communication between external services.
 * Il also represents (former TMPArgument) an excerpt suggestion built upon processed temporary excerpts from external WDTAL
 * services. It contains (most of) all details of an excerpt (as defined in BaseExcerpt) and, after manual
 * validation, will be inserted into the database. As excerpts must be linked to Texts (because some properties
 * are specified at that level) those Texts will use a special naming convention and property to be filtered out
 * and somehow hidden since they do not have a life per se. Regular excerpts "inherits" the language from
 * their attached Text, Tweets have their own language and do not have links to other excerpts since they
 * are simple suggestions.
 *
 * @author Martin Rouffiange
 */
public interface ExternalExcerpt extends ExternalContribution, Comparable<ExternalExcerpt>  {

    /**
     * Get the Text or ExternalText id bound to this excerpt
     *
     * @return the Text or ExternalText id
     */
    Long getTextId();

    /**
     * Set the Text or ExternalText id bound to this excerpt
     *
     * @param textId the Text or ExternalText id
     */
    void setTextId(Long textId);

    /**
     * Get the Text object bound to this excerpt, if any
     *
     * @return the Text object
     */
    Text getText();

    /**
     * Set the Text object bound to this excerpt
     *
     * @param text the Text object
     */
    void setText(Text text);

    /**
     * Get the ExternalText object bound to this excerpt, if any
     *
     * @return the ExternalText object
     */
    ExternalText getExternalText();

    /**
     * Set the ExternalText object bound to this excerpt
     *
     * @param text the ExternalText object
     */
    void setExternalText(ExternalText text);

    /**
     * Get the original excerpt
     *
     * @return the original excerpt
     */
    String getOriginalExcerpt();

    /**
     * Set the original excerpt
     *
     * @param originalExcerpt the original excerpt
     */
    void setOriginalExcerpt(String originalExcerpt);

    /**
     * Get the working excerpt.
     *
     * @return the reworded excerpt
     */
    String getWorkingExcerpt();

    /**
     * Set working excerpt.
     *
     * @param workingExcerpt working excerpt
     */
    void setWorkingExcerpt(String workingExcerpt);

}
