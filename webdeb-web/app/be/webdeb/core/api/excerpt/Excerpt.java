/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.excerpt;

import be.webdeb.core.api.argument.ArgumentIllustration;
import be.webdeb.core.api.contribution.TextualContribution;
import be.webdeb.core.api.text.Text;

import java.util.List;

/**
 * This interface represents an excerpt in the webdeb system. An excerpt is taken from a Text (the original excerpt).
 * And the contributor can add some information to contextualized the excerpt (the working excerpt).
 *
 * @author Martin Rouffiange
 */
public interface Excerpt extends TextualContribution {

    /**
     * Get the original excerpt from the Text content to which this excerpt belongs to
     *
     * @return the excerpt
     */
    String getOriginalExcerpt();

    /**
     * Set the original excerpt from the Text content ot which this excerpt belongs to.
     * The actual belonging of the argument is not checked at this level, but at save time, implicitly via
     * the call to isContributorValid()
     *
     * @param originalExcerpt the original Text excerpt
     */
    void setOriginalExcerpt(String originalExcerpt);

    /**
     * Get the contextualized excerpt.
     *
     * @param lang the user lang
     * @param toDisplay true if it must be displayed
     * @return the contextualized excerpt
     */
    String getWorkingExcerpt(String lang, boolean toDisplay);

    /**
     * Set the contextualized excerpt.
     *
     * @param workingExcerpt contextualized excerpt
     */
    void setWorkingExcerpt(String workingExcerpt);


    /**
     * Get the technical (contextualization is [ContributionType.id, Contribution.id]) contextualized excerpt.
     *
     * @return the technical contextualized excerpt
     */
    String getTechWorkingExcerpt();

    /**
     * Set the technical (contextualization is [ContributionType.id, Contribution.id]) contextualized excerpt.
     *
     * @param techWorkingExcerpt technical contextualized excerpt
     */
    void setTechWorkingExcerpt(String techWorkingExcerpt);

    /**
     * Get the Text id from which this Excerpt belongs to
     *
     * @return the owning Text id
     */
    Long getTextId();

    /**
     * Set the Text id from which this Excerpt belongs to
     *
     * @param textId a Text id
     */
    void setTextId(Long textId);

    /**
     * Get the Text object bound to this Excerpt, i.e. ContributionAccessor.retrieve(getTextId())
     *
     * @return the Text object with getTextId if found, null otherwise
     */
    Text getText();

    /**
     * Get the external excerpt id  where this excerpt comes from, if any
     *
     * @return the external excerpt id, may be null
     */
    Long getExternalExcerptId();

    /**
     * Set the external excerpt id where this argument comes from
     *
     * @param externalExcerptId an external excerpt id
     */
    void setExternalExcerptId(Long externalExcerptId);

    /**
     * Get the list of directly linked arguments
     *
     * @return the list of similar arguments
     */
    List<ArgumentIllustration> getArguments();

    /**
     * Get the list of similar excerpts
     *
     * @return the list of similar excerpts of this one
     */
    List<ArgumentIllustration> getSimilars();

}
