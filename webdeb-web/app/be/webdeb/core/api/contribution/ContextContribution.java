/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */
package be.webdeb.core.api.contribution;

import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentJustification;
import be.webdeb.core.api.argument.ArgumentType;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;

import java.util.List;

/**
 * This interface represents a context contribution in the webdeb system. A context has a first argument and folders as theme.
 * It also has some justification links between contextualized arguments that create the context.
 *
 * @author Martin Rouffiange
 */
public interface ContextContribution extends TextualContribution  {

    /**
     * Get the first contextualized argument id of the debate
     *
     * @return a contextualized argument
     */
    Long getFirstArgumentId();

    /**
     * Set the first contextualized argument id of the debate
     *
     * @param firstArgumentId a contextualized argument id
     */
    void setFirstArgumentId(Long firstArgumentId);

    /**
     * Get the first contextualized argument of the debate
     *
     * @return a contextualized argument
     */
    ArgumentContext getFirstArgument();

    /**
     * Set the first contextualized argument of the debate
     *
     * @param firstArgument a contextualized argument
     */
    void setFirstArgument(ArgumentContext firstArgument);

    /**
     * Get the first debate argument type
     *
     * @return an ArgumentType object
     * @see ArgumentType
     */
    ArgumentType getFirstArgumentType();

    /**
     * Set the first debate argument type
     *
     * @param argumentType an ArgumentType object
     * @throws FormatException if the passed ArgumentType is incomplete
     * @see ArgumentType
     */
    void setFirstArgumentType(ArgumentType argumentType) throws FormatException;


    /**
     * Get the title of this context contribution
     *
     * @param lang the user lang
     * @return the title of this context contribution
     */
    String getTitle(String lang);

    /**
     * Get the list of contextualized arguments in the debate
     *
     * @return a possibly empty list of contextualized arguments
     */
    List<ArgumentContext> getAllContextualizedArguments();

    /**
     * Get the list of contextualized arguments in the debate limited by indexes
     *
     * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
     * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
     * @return a possibly empty list of contextualized arguments
     */
    PartialContributions<ArgumentContext> getAllContextualizedArguments(int fromIndex, int toIndex);

    /**
     * Get the list of all justification links that are in the debate
     *
     * @return a possibly empty list of argument justification links
     */
    List<ArgumentJustification> getAllArgumentJustificationLinks();

    /**
     * Save given list of argument links as the justification links of this debate
     *
     * @param links a list of justification links
     * @param currentGroup the current group id from which the contributor triggered the save action (for auto-created actors)
     * @param contributor the Contributor id that initiated the save action
     *
     * @throws PermissionException if the save action(s) could not been performed because of an issue regarding µ
     * contributor permissions or because this operation would cause a problem of integrity.
     * @throws PersistenceException if the save action(s) could not been performed because of an issue with
     * the persistence layer
     */
    void saveJustificationLinks(List<ArgumentJustification> links, int currentGroup, Long contributor) throws PersistenceException, PermissionException;

    /**
     * Temporary save this context contribution to get an id for sub objects
     *
     * @param contributor the Contributor id that initiated the save action
     * @param currentGroup the current group id from which the contributor triggered the save action (for auto-created actors)
     *
     * @throws PermissionException if the save action(s) could not been performed because of an issue regarding a
     * contributor permissions or because this operation would cause a problem of integrity.
     * @throws PersistenceException if the save action(s) could not been performed because of an issue with
     * the persistence layer
     */
    void saveContextContribution(Long contributor, int currentGroup) throws PermissionException, PersistenceException;

    /**
     * Delete the temporary context contribution in case of error
     *
     * @param contributor the Contributor id that initiated the save action
     */
    void deleteContextContribution(Long contributor);

}
