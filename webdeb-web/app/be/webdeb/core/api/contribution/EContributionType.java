/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.contribution;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This enumeration holds types for contributions
 * <ul>
 *   <li>-1 for ACTOR, TEXT and ARGUMENT</li>
 *   <li>0 for ACTOR</li>
 *   <li>1 for DEBATE</li>
 *   <li>2 for ARGUMENT</li>
 *   <li>3 for ARGUMENT_CONTEXT</li>
 *   <li>4 for EXCERPT</li>
 *   <li>5 for TEXT</li>
 *   <li>6 for FOLDER</li>
 *   <li>7 for AFFILIATION</li>
 *   <li>8 for ARG_JUSTIFICATION</li>
 *   <li>9 for ARG_SIMILARITY</li>
 *   <li>10 for ARG_ILLUSTRATION</li>
 *   <li>11 for ARG_TRANSLATION</li>
 *   <li>12 for FOLDERLINK</li>
 *   <li>13 for EXTERNAL_TEXT</li>
 *   <li>14 for EXTERNAL_EXCERPT</li>
 * </ul>
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public enum EContributionType {

  /**
   * any type of concrete contribution, ie, actors, texts and arguments
   */
  ALL(-1),

  /**
   * individual or organizational actor
   */
  ACTOR(0),

  /**
   * debate a subject that can regroups arguments
   */
  DEBATE(1),

  /**
   * argument that may be linked to a context
   */
  ARGUMENT(2),

  /**
   * contextualized argument
   */
  ARGUMENT_CONTEXTUALIZED(3),

  /**
   * excerpt extracted from text
   */
  EXCERPT(4),

  /**
   * text (holding excerpts)
   */
  TEXT(5),

  /**
   * folder that regroup texts and arguments
   */
  FOLDER(6),

  /**
   * actor or contributor's affiliation (organization and/or function)
   */
  AFFILIATION(7),

  /**
   * justification link between two contextualized arguments in a debate or a text
   */
  ARG_JUSTIFICATION(8),

  /**
   * similarity link between two arguments
   */
  ARG_SIMILARITY(9),

  /**
   * illustration link between an argument and an excerpt in a debate or a text
   */
  ARG_ILLUSTRATION(10),

  /**
   * translation link between two arguments
   */
  ARG_TRANSLATION(11),

  /**
   * folder link between two folders
   */
  FOLDERLINK(12),

  /**
   * temporary text from external sources, that wait for user confirmation
   */
  EXTERNAL_TEXT(13),

  /**
   * temporary excerpt from external sources, that wait for user confirmation
   */
  EXTERNAL_EXCERPT(14),

  /**
   * Technical enum value to contains additional value
   */
  TECH_VALUE(15);

  private int id;
  private static Map<Integer, EContributionType> map = new LinkedHashMap<>();

  static {
    for (EContributionType type : EContributionType.values()) {
      map.put(type.id, type);
    }
  }

  /**
   * Constructor
   *
   * @param id an int representing a contribution type
   */
  EContributionType(int id) {
    this.id = id;
  }


  /**
   * Get the enum value for a given id
   *
   * @param id an int representing an EContributionType
   * @return the EContributionType enum value corresponding to the given id, null otherwise.
   */
  public static EContributionType value(int id) {
    return map.get(id);
  }

  /**
   * Get this id
   *
   * @return an int representation of this contribution type
   */
  public int id() {
    return id;
  }

  /**
   * Get true if the contribution type is a context contribution
   *
   * @return true if the contribution type is a context contribution
   */
  public boolean isContextContribution(){
    return this.id == EContributionType.TEXT.id || this.id == EContributionType.DEBATE.id;
  }

  /**
   * Get true if the contribution type is a context contribution
   *
   * @return true if the contribution type is a context contribution
   */
  public boolean isArgumentLink(){
    return this == EContributionType.ARG_JUSTIFICATION || this == EContributionType.ARG_ILLUSTRATION || this == EContributionType.ARG_SIMILARITY|| this == EContributionType.ARG_TRANSLATION;
  }

  public boolean isMajorContributionType(){
    return this == EContributionType.ACTOR || this == EContributionType.DEBATE || this == EContributionType.ARGUMENT_CONTEXTUALIZED
          || this == EContributionType.EXCERPT || this == EContributionType.FOLDER || this == EContributionType.TEXT;
  }
}
