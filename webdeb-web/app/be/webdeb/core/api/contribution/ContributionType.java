/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.contribution;

import java.util.Map;

/**
 * This interface represents a contribution type. This is a typed object concurrent to the EContributionType enum.
 *
 * @author Fabian Gilson
 */
public interface ContributionType {

  /**
   * Get the contribution type id value
   *
   * @return a string representing a contribution type
   */
  int getContributionType();

  /**
   * Get the name of this contribution type
   *
   * @param lang a two-char ISO-639-1 code representing the language for the name
   * @return a name for this contribution type
   */
  String getName(String lang);

  /**
   * Get the whole map of names in all known languages
   *
   * @return a map of (2-char ISO-639-1 code, name in this language) of this contribution type
   */
  Map<String, String> getNames();

  /*
   * CONVENIENCE METHOD
   */

  /**
   * Get the corresponding EContributionType value to this.
   *
   * @return the EContributionType enum value corresponding to this contribution type
   */
  EContributionType getEContributionType();
}
