/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.contribution;

import java.util.Map;

/**
 * This interface represents a language in the webdeb database.
 *
 * @author Fabian Gilson
 */
public interface Language {

  /**
   * Get 2-char ISO 639-1 code of this language
   *
   * @return a two char ISO code for this language
   */
    String getCode();

  /**
   * Get this language name in given language
   *
   * @param lang a two-char ISO-639-1 code representing the language for the name, or the "own" key for the language in
   * its own language
   * @return this language name in given language
   */
  String getName(String lang);

  /**
   * Get the whole map of names in all known languages
   *
   * @return a map of (2-char ISO-639-1 code, name in this language) of this language (+ a dedicated "own" key
   * for the language in its own language and writing).
   */
  Map<String, String> getNames();

}
