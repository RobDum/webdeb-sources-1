/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.folder;

import java.util.Map;

/**
 * This interface represents a type of folder, like root, node and leaf.
 * Accepted values are loaded at run time from a dedicated configuration database table.
 *
 * For more info, see data in table t_folder_type
 *
 * Currently, those types are
 * <pre>
 *   0 simple folder
 *   1 tag folder
 *   2 composite folder
 * </pre>
 *
 * @see EFolderType
 * @author Martin Rouffiange
 */
public interface FolderType {

  /**
   * Get this type id
   *
   * @return the int representation of this FolderType
   */
  int getType();

  /**
   * Get the name corresponding to this folder type.
   *
   * @param lang a two-char ISO code representing the language for the names
   * @return the name associated to this folder type
   */
  String getName(String lang);

  /**
   * Get the whole map of names in all known languages
   *
   * @return a map of (2-char ISO code, name in this language) of all folder types
   */
  Map<String, String> getNames();

  /*
   * CONVENIENCE METHOD
   */

  /**
   * Get the corresponding EFolderType value to this.
   *
   * @return the EFolderType enum value corresponding to this validation state
   */
  EFolderType getEFolderType();
}
