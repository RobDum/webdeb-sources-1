/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.folder;

import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.PartialContributions;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.exception.FormatException;

import java.util.List;
import java.util.Map;

/**
 * This interface represents a folder in the webdeb system. A folder consists
 * to sort arguments and texts that share the same theme or category.
 * A folder has commonly parents and children that form a tree hierarchy structure.
 *
 * @author Martin Rouffiange
 */
public interface Folder extends Contribution, Comparable<Folder> {

  /**
   * Get parents folders of this folder
   *
   * @return a possibly empty list of folders
   */
  List<Folder> getParents();

  /**
   * Initialize the list of parents
   */
  void initParents();

  /**
   * Get parents folders of this folder as FolderLink
   *
   * @return a possibly empty list of FolderLink
   *
   */
  List<FolderLink> getParentsAsLink();

  /**
   * Set parents folders of this folder
   *
   * @param folders a list of folders to set
   */
  void setParents(List<Folder> folders);

  /**
   * Get children folders of this folder
   *
   * @return a possibly empty list of folders
   */
  List<Folder> getChildren();

  /**
   * Initialize the list of children
   */
  void initChildren();

  /**
   * Get children folders of this folder as FolderLink
   *
   * @return a possibly empty list of FolderLink
   *
   */
  List<FolderLink> getChildrenAsLink();

  /**
   * Set children folders of this folder
   *
   * @param folders a list of folders to set
   */
  void setChildren(List<Folder> folders);

  /**
   * Get parents and children folders of this folder
   *
   * @return a possibly empty list of folders
   */
  List<Folder> getHierarchy();

  /**
   * Get parents and children folders of this folder as FolderLink
   *
   * @return a possibly empty list of FolderLink
   */
  List<FolderLink> getHierarchyAsLink();

  /**
   * Add a parent folder to this one.
   * As other fields, additions are persisted when calling save().
   *
   * @param folder a parent folder to add
   * @throws FormatException if the given folder link is incomplete
   */
  void addParent(Folder folder) throws FormatException;

  /**
   * Remove given folder from this one. If the folder is unfound, this one is unchanged.
   * As other fields, removals are persisted when calling save().
   *
   * @param folder a parent folder id to remove from this Folder
   */
  void removeParent(Long folder);

  /**
   * Add a child folder to this one.
   * As other fields, additions are persisted when calling save().
   *
   * @param folder a child folder to add
   * @throws FormatException if the given folder link is incomplete
   */
  void addChild(Folder folder) throws FormatException;

  /**
   * Remove given folder from this one. If the folder is unfound, this one is unchanged.
   * As other fields, removals are persisted when calling save().
   *
   * @param folder a child folder id to remove from this Folder
   */
  void removeChild(Long folder);

  /**
   * Get the english name or default one
   *
   * @return the corresponding value
   */
  String getDefaultName();

  /**
   * Get the folder i18 name for given lang key
   *
   * @param lang a two char iso-639-1 language code
   * @return the corresponding value
   */
  String getName(String lang);

  /**
   * Get the folder i18 names
   *
   * @return a map of iso-639-1 language code and corresponding values
   */
  Map<String, String> getNames();

  /**
   * Get the folder names as FolderName
   *
   * @return a possibly empty list of FolderName
   */
  List<FolderName> getNamesAsFolderName();

  /**
   * Set the folder names
   *
   * @param i18names a map of iso-639-1 language code and corresponding names
   */
  void setNames(Map<String, String> i18names);

  /**
   * Add a new name for this folder. If such a spelling existed, simply overwrite it.
   *
   * @param lang a two char ISO 639-1 code
   * @param name a lang-dependent name
   */
  void addName(String lang, String name);

  /**
   * Get the folder rewording i18 names for given lang key
   *
   * @param lang a two char iso-639-1 language code
   * @return the list of rewording names for a given lang
   * @see FolderName
   */
  List<FolderName> getRewordingNamesByLang(String lang);

  /**
   * Get the folder rewording i18 names
   *
   * @return the list of rewording names as a map of lang, list of FolderName
   * @see FolderName
   */
  Map<String, List<FolderName>> getRewordingNames();

  /**
   * Set the folder rewording names
   *
   * @param i18names a map of lang, list of FolderName
   * @see FolderName
   */
  void setRewordingNames(Map<String, List<FolderName>> i18names);

  /**
   * Add a new rewording name for this folder.
   *
   * @param lang a two char ISO 639-1 code
   * @param name a lang-dependent name
   */
  void addRewordingName(String lang, String name);

  /**
   * Add a Text contribution to this Folder
   *
   * @param text a text contribution to add to this Folder
   * @throws FormatException if given text is invalid
   */
  void addText(Text text) throws FormatException;

  /**
   * Add a contextualized argument to this Folder
   *
   * @param argument a contextualized argument contribution to add to this Folder
   * @throws FormatException if given contextualized argument is invalid
   */
  void addContextualizedArgument(ArgumentContext argument) throws FormatException;

  /**
   * Remove a contextualized argument to this Folder
   *
   * @param argument the contextualized argument id to remove
   */
  void removeContextualizedArgument(Long argument);

  /**
   * Remove a Text to this Folder
   *
   * @param text the Text id to remove
   */
  void removeText(Long text);

  /**
   * Get debates linked with this folder
   *
   * @return a possibly empty list of debates
   */
  List<Debate> getDebates();

  /**
   * Set debates linked with this folder
   *
   * @param debates a list of debates to set
   */
  void setDebates(List<Debate> debates);

  /**
   * Get excerpts linked with this folder
   *
   * @return a possibly empty list of excerpts
   */
  List<Excerpt> getExcerpts();

  /**
   * Set excerpts  linked with this folder
   *
   * @param excerpts  a list of excerpts  to set
   */
  void setExcerpts(List<Excerpt> excerpts );

  /**
   * Get the contextualized arguments linked with this folder
   *
   * @return a possibly empty list of contextualized arguments
   */
  List<ArgumentContext> getContextualizedArguments();

  /**
   * Set the contextualized arguments linked with this folder
   *
   * @param arguments a list of contextualized arguments to set
   */
  void setContextualizedArguments(List<ArgumentContext> arguments);

  /**
   * Get the texts linked with this folder
   *
   * @return a possibly empty list of texts
   */
  List<Text> getTexts();

  /**
   * Set the texts linked with this folder
   *
   * @param texts a list of texts to set
   */
  void setTexts(List<Text> texts);

  /**
   * Get the list of contextualized arguments that are contained in this folder limited by index
   *
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return the list of contextualized arguments in this folder
   */
  PartialContributions<ArgumentContext> getContextualizedArguments(int fromIndex, int toIndex);

  /**
   * Get the list of excerpts that are contained in this folder limited by index
   *
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return the list of excerpts in this folder
   */
  PartialContributions<Excerpt> getExcerpts( int fromIndex, int toIndex);

  /**
   * Get the list of debates that are contained in this folder limited by index
   *
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return the list of debates in this folder
   */
  PartialContributions<Debate> getDebates(int fromIndex, int toIndex);

  /**
   * Find the list of texts that are contained in this folder limited by index
   *
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return the list of texts in this folder
   */
  PartialContributions<Text> getTexts(int fromIndex, int toIndex);

  /**
   * Get the folder type
   *
   * @return a FolderType object
   *
   * @see FolderType
   */
  FolderType getFolderType();

  /**
   * Set the folder type
   *
   * @param type a FolderType object
   * @see FolderType
   */
  void setFolderType(FolderType type);

  /**
   * Get the hierarchy tree of this folder
   *
   * @return the folder hierarchy tree
   */
  HierarchyTree getHierarchyTree();

  /**
   * Set the hierarchy tree of this folder
   *
   * @param tree the hierarchy tree
   */
  void setHierarchyTree(HierarchyTree tree);

  /**
   * Get the parents hierarchy of this folder as JSON
   *
   * @return the parents hierarchy as json
   */
  String getParentsHierarchyAsJson();

  /**
   * Get the children hierarchy of this folder as JSON
   *
   * @return the children hierarchy as json
   */
  String getChildrenHierarchyAsJson();

  /**
   * Get the number of contributions contained in this folder
   *
   * @param contributor a contributor id
   * @param group a group id
   * @return the number of contributions contained in this folder
   */
  int getNbContributions(Long contributor, int group);
}
