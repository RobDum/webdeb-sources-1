/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.argument;

import be.webdeb.core.api.contribution.Language;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PersistenceException;

import java.util.List;

/**
 * This interface represents a dictionary for manage argument title.
 *
 * @author martin Rouffiange
 */
public interface ArgumentDictionary {

    /**
     * Get the argument dictionary id
     *
     * @return an id
     */
    Long getId();

    /**
     * Set the argument dictionary id
     *
     * @param id an id
     */
    void setId(Long id);

    /**
     * Get the argument dictionary title
     *
     * @return a title
     */
    String getTitle();

    /**
     * Set the argument dictionary title
     *
     * @param title a title
     */
    void setTitle(String title);

    /**
     * Get the argument dictionary title language
     *
     * @return a Language object
     *
     * @see Language
     */
    Language getLanguage();

    /**
     * Set the argument dictionary title language
     *
     * @param language a Language object
     * @see Language
     */
    void setLanguage(Language language);

    /**
     * Get the list of translation of this argument dictionary
     *
     * @return a possibly empty list of translations
     */
    List<ArgumentDictionary> getTranslations();

    /**
     * Save an argument dictionary on behalf of a given contributor If argument.getId has been set, update the
     * argument, otherwise create argument and update contribution id.
     *
     * @throws FormatException if any dictionary field is not valid or missing
     * @throws PersistenceException if an error occurred, a.o., unset required field.
     * The exception message will contain a more complete description of the error.
     */
    void save() throws FormatException, PersistenceException;

    /**
     * Check if this dictionary contains all needed values to be considered as valid.
     *
     * @return a List of field names in error if any, an empty list if this Dictionary may be considered as valid
     */
    List<String> isValid();
}
