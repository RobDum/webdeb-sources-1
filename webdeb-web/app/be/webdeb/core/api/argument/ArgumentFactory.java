/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.argument;

import be.webdeb.core.api.contribution.ContextContribution;
import be.webdeb.core.api.contribution.ContributionFactory;
import be.webdeb.core.exception.FormatException;

import java.util.List;
import java.util.Map;

/**
 * This interface represents an abstract factory to handle arguments.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public interface ArgumentFactory extends ContributionFactory {

  /**
   * Retrieve an argument by its id
   *
   * @param id a Contribution id
   * @return an argument if given id is an argument, null otherwise
   */
  Argument retrieve(Long id);

  /**
   * Retrieve an argument by its id and increment visualization hit of this contribution
   *
   * @param id an argument id
   * @return the argument concrete object corresponding to the given id, null if no found
   */
  Argument retrieveWithHit(Long id);

  /**
   * Retrieve a contextualized argument by its id
   *
   * @param id a contextualized argument id
   * @return the contextualized argument concrete object corresponding to the given id, null if not found
   */
  ArgumentContext retrieveContextualized(Long id);

  /**
   * Retrieve a contextualized argument by its id and increment visualization hit of this contribution
   *
   * @param id a contextualized argument id
   * @return the contextualized argument concrete object corresponding to the given id, null if not found
   */
  ArgumentContext retrieveContextualizedWithHit(Long id);

  /**
   * Retrieve a contextualized argument from an argument or dictionary id
   *
   * @param id an argument or dictionary id
   * @param dic true if given id is a dictionary id
   * @return the contextualized argument concrete object corresponding to the given id, null if not found
   */
  ArgumentContext retrieveContextualizedFromArg(Long id, boolean dic);

  /**
   * Retrieve an argument dictionary from a dictionary id
   *
   * @param id an argument dictionary id
   * @return the argument dictionary concrete object corresponding to the given id, null if not found
   */
  ArgumentDictionary retrieveDictionary(Long id);

  /**
   * Retrieve an argument justification link by its id
   *
   * @param id a Contribution id
   * @return an ArgumentJustification if given id is an argument justification link, null otherwise
   */
  ArgumentJustification retrieveJustificationLink(Long id);

  /**
   * Retrieve an argument similarity link by its id
   *
   * @param id a Contribution id
   * @return an ArgumentSimilarity if given id is an argument similarity link, null otherwise
   */
  ArgumentSimilarity retrieveSimilarityLink(Long id);

  /**
   * Retrieve an argument illustration link between an excerpt by its id
   *
   * @param id a Contribution id
   * @return an ArgumentIllustration if given id is an argument illustration link, null otherwise
   */
  ArgumentIllustration retrieveIllustrationLink(Long id);

  /**
   * Construct an empty argument instance
   *
   * @return a new Argument instance
   */
  Argument getArgument();

  /**
   * Construct an empty contextualized argument instance
   *
   * @return a new ArgumentContext instance
   */
  ArgumentContext getContextualizedArgument();

  /**
   * Construct an empty argument dictionary instance
   *
   * @return a new ArgumentDictionary instance
   */
  ArgumentDictionary getArgumentDictionary();

  /**
   * Construct an empty argument justification link instance
   *
   * @return a new ArgumentJustification instance
   */
  ArgumentJustification getArgumentJustificationLink();

  /**
   * Construct an empty argument similarity link instance
   *
   * @return a new ArgumentSimilarity instance
   */
  ArgumentSimilarity getArgumentSimilarityLink();

  /**
   * Construct an empty argument illustration link instance
   *
   * @return a new ArgumentIllustration instance
   */
  ArgumentIllustration getArgumentIllustrationLink();

  /**
   * Retrieve the debate where the given contextualized argument is the first argument of the context contribution
   *
   * @param id a contextualized argument id
   * @return the contextualized argument concrete object corresponding to the given id, null if not found
   */
  ContextContribution retrieveDebateFirstArgument(Long id);

  /**
   * Construct an ArgumentType with a type, subtype, timing and singular flag
   *
   * @param type the argument type id
   * @param i18tNames a map of pairs of the form (2-char iso-code, type name)
   * @param shade the argument shade id
   * @param i18shNames a map of pairs of the form (2-char iso-code, shade name)
   * @return an ArgumentType instance
   */
  ArgumentType createArgumentType(int type, Map<String, String> i18tNames, int shade, Map<String, String> i18shNames);

  /**
   * Get an ArgumentType object from a given shade id.
   *
   * @param shade a shade id
   * @return the Excerpt type corresponding to the given shade
   *
   * @throws FormatException if the given id does not exist
   */
  ArgumentType getArgumentType(int shade) throws FormatException;

  /**
   * Retrieve all argument types
   *
   * @return the list of all argument types
   */
  List<ArgumentType> getArgumentTypes();

  /**
   * Retrieve all argument subtypes of given type id, if any
   *
   * @param type the argument type id
   * @return the list of all argument shades, should not be empty
   */
  List<ArgumentType> getArgumentShades(int type);

  /**
   * Construct an argument link type
   *
   * @param type a type id
   * @param i18tNames a map of pairs of the form (2-char iso-code, type name)
   * @param shade a shade id
   * @param i18sNames a map of pairs of the form (2-char iso-code, shade name)
   * @return an ArgumentLinkType instance
   */
  ArgumentLinkType createArgumentLinkType(int type, Map<String, String> i18tNames, int shade,
      Map<String, String> i18sNames);

  /**
   * Get an ArgumentLinkType object from a given linkshade id
   *
   * @param shade an EArgumentLinkType enum object
   * @return the ArgumentLinkType corresponding to the given shade
   */
  ArgumentLinkType getArgumentLinkType(EArgumentLinkShade shade);

  /**
   * Retrieve all argument link types
   *
   * @return the list of all link types
   */
  List<ArgumentLinkType> getArgumentLinkTypes();

  /**
   * Check if a link exists between two given contributions.
   *
   * @param origin an argument or contextualized argument id
   * @param destination an argument, a contextualized argument or an excerpt id
   * @return true if such a link already exists
   */
  boolean linkAlreadyExists(Long origin, Long destination);

  /**
   * Find an Argument by title
   *
   * @param title an argument title
   * @return a corresponding argument or null
   */
  Argument findUniqueByTitle(String title);

  /**
   * Find a list of ArgumentDictionary by title
   *
   * @param title an argument dictionary title
   * @param lang a two char iso-639-1 language code
   * @param fromIndex the low endpoint of contributions to retrieve (only strictly positive values are considered)
   * @param toIndex the high endpoint of contributions to retrieve (only strictly positive values are considered)
   * @return a possibly empty list of ArgumentDictionary with their title containing the given title
   */
  List<ArgumentDictionary> findByTitle(String title, String lang, int fromIndex, int toIndex);

}
