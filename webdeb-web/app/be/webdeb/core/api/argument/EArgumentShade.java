/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.argument;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This enumeration holds shade types for arguments
 * <pre>
 *   0 for There is no doubt that
 *   1 for It is likely that
 *   2 for It is not known whether
 *   3 for It is unlikely that
 *   4 for It is untrue that
 *   5 for It is necessary to
 *   6 for It may be necessary to
 *   7 for It is not known whether we have to
 *   8 for It is unnecessary to
 *   9 for It is necessary not to
 *   10 for debate shade
 * </pre>
 *
 * @author Martin Rouffiange
 */
public enum EArgumentShade {

    NO_DOUBT(0),
    UNTRUE(1),
    NECESSARY(2),
    NECESSARY_NOT_TO(3),
    DEBATE_NECESSARY(4),
    DEBATE_NO_DOUBT(5);

    private int id;

    private static Map<Integer, EArgumentShade> map = new LinkedHashMap<>();

    static {
        for (EArgumentShade type : EArgumentShade.values()) {
            map.put(type.id, type);
        }
    }

    /**
     * Constructor
     *
     * @param id an int representing a shade type
     */
    EArgumentShade(int id) {
        this.id = id;
    }

    /**
     * Get the enum value for a given id
     *
     * @param id an int representing an argument shade type
     * @return the EArgumentShade enum value corresponding to the given id, null otherwise.
     */
    public static EArgumentShade value(int id) {
        return map.get(id);
    }

    /**
     * Get this id
     *
     * @return an int representation of this argument shade type
     */
    public int id() {
        return id;
    }

    /**
     * Convert this shade into its corresponding debate value
     *
     * @return the corresponding shade for debate
     */
    public EArgumentShade convertToDebateShade() {
        switch (this){
            case NO_DOUBT :
            case UNTRUE :
                return DEBATE_NO_DOUBT;
            case NECESSARY :
            case NECESSARY_NOT_TO :
                return DEBATE_NECESSARY;
            default :
                return this;
        }
    }

    /**
     * Convert this shade into its corresponding argument value
     *
     * @return the corresponding shade for argument
     */
    public EArgumentShade convertToArgumentShade() {
        switch (this) {
            case DEBATE_NECESSARY :
                return NECESSARY;
            case DEBATE_NO_DOUBT :
                return NO_DOUBT;
            default :
                return this;
        }
    }

    /**
     * Convert this shade into its corresponding positive value
     *
     * @return the corresponding positive shade
     */
    public EArgumentShade getPositive() {
        switch (this) {
            case DEBATE_NO_DOUBT :
            case UNTRUE :
                return NO_DOUBT;
            case DEBATE_NECESSARY :
            case NECESSARY_NOT_TO :
                return NECESSARY;
            default :
                return this;
        }
    }

    /**
     * Convert this shade into its corresponding negative value
     *
     * @return the corresponding negative shade
     */
    public EArgumentShade getNegative() {
        switch (this) {
            case DEBATE_NO_DOUBT :
            case NO_DOUBT :
                return UNTRUE;
            case DEBATE_NECESSARY :
            case NECESSARY :
                return NECESSARY_NOT_TO;
            default :
                return this;
        }
    }

    /**
     * Get the opposite value of this shade
     *
     * @return the opposite shade
     */
    public EArgumentShade reverse() {
        return isApprobation() ? getNegative() : getPositive();
    }

    /**
     * Check whether this shade is an approbation
     *
     * @return true if this argument is an approbation
     */
    public boolean isApprobation() {
        return id == NO_DOUBT.id() || id == NECESSARY.id();
    }

    /**
     * Check whether this shade is an opposition
     *
     * @return true if this argument is an opposition
     */
    public boolean isOpposition() {
        return id == UNTRUE.id() || id == NECESSARY_NOT_TO.id();
    }
}
