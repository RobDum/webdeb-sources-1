/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.argument;

import be.webdeb.core.exception.FormatException;

import java.util.Map;

/**
 * This interface represents a type of argument. An ArgumentType is composed by a type, a timing
 * (unknown, past, present, future) and an optional shade (verb). For the two first types, a shade must also be specified
 * (shades values are given between [] ).
 * <p>
 * Acceptable values are :
 * <pre>
 *   0 descriptive (present timing, 5 shades) [ 0 -> 4 ]
 *   1 prescriptive (3 timings 5 shades) [ 5 -> 19 ]
 *   2 opinion (unknown timing, no shade) [20 -> 21] (20 for I and 21 for We)
 *   3 performative (unknown timing, no shade) [22 -> 23] (22 for I and 23 for We)
 * </pre>
 *
 * @see EArgumentType
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public interface ArgumentType {

  /**
   * Get the argument type
   *
   * @return an int value representing the argument type
   */
  int getArgumentType();

  /**
   * Set the argument type
   *
   * @param type an int representing the argument type
   * @throws FormatException if the given argument type does not exist
   */
  void setArgumentType(int type) throws FormatException;

  /**
   * Get the argument type name
   *
   * @param lang a two-char ISO code representing the language for the names
   * @return a name for this ArgumentType
   */
  String getTypeName(String lang);

  /**
   * Set the names for all known languages for the type
   *
   * @param i18names a map of pairs of the form (2-char iso-code, name)
   */
  void setTypeNames(Map<String, String> i18names);

  /**
   * Get the whole map of names in all known languages
   *
   * @return a map of (2-char ISO code, name in this language) of this argument type
   */
  Map<String, String> getTypeNames();

  /**
   * Get this argument shade
   *
   * @return an int representing the shade of this ArgumentType
   */
  int getArgumentShade();

  /**
   * Set the argument shade
   *
   * @param shade an argument shade value
   * @throws FormatException if the given shade is not applicable to this ArgumentType
   */
  void setArgumentShade(int shade) throws FormatException;

  /**
   * Get the shade verb
   *
   * @param lang a two-char ISO code representing the language for the name
   * @return the shade verb, depending on the singular/plural form
   */
  String getShadeName(String lang);

  /**
   * Get the shade verb ended with dots (for forms)
   *
   * @param lang a two-char ISO code representing the language for the name
   * @return the shade verb, depending on the singular/plural form
   */
  String getShadeNameWithDots(String lang);

  /**
   * Set the names for all known languages for this shade
   *
   * @param i18names a map of pairs of the form (2-char iso-code, name)
   */
  void setShadeNames(Map<String, String> i18names);

  /**
   * Get the whole map of names in all known languages
   *
   * @return a map of (2-char ISO code, name in this language) of this argument shade
   */
  Map<String, String> getShadeNames();


  /*
     * CONVENIENCE METHODS
   */

  /**
   * Check if this Excerpt type is fully defined
   *
   * @return true if this ArgumentType is fully defined (type, timing, subtype, shade and singular)
   */
  boolean isValid();

  /**
   * Get the corresponding EArgumentShade value to this.
   *
   * @return the EArgumentShade enum value corresponding to this argument type
   */
  EArgumentShade getEType();
}
