/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.argument;

import be.webdeb.core.api.contribution.Language;
import be.webdeb.core.exception.FormatException;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This interface represents an argument in the webdeb system. An argument consists in standardized form of an
 * excerpt taken from a Text with a couple of formal properties and types specified to ease automated
 * definitions of relationships as well as other natural language processing features.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
public interface Argument extends BaseArgument {

  /**
   * Get the argument dictionary that contains the title, language and translations of the argument
   *
   * @return the argument dictionary
   */
  ArgumentDictionary getDictionary();

  /**
   * Set the argument dictionary that contains the title, language and translations of the argument
   *
   * @param dictionary an argument dictionary
   */
  void setDictionary(ArgumentDictionary dictionary);

  /**
   * Get the debate title
   *
   * @return the debate title
   */
  String getTitle();

  /**
   * Get the fully readable title with shade
   *
   * @return a fully readable reconstructed title with shade
   */
  String getFullTitle();

  /**
   * Get the fully readable title with shade, or the opposite depending of the given argument type
   *
   * @param type the type of the argument
   * @return a fully readable reconstructed title with shade
   */
  String getFullTitle(EArgumentType type);

  /**
   * Get this Argument type
   *
   * @return an ArgumentType object
   * @see ArgumentType
   */
  ArgumentType getArgumentType();

  /**
   * Set this Argument type
   *
   * @param argumentType an ArgumentType object
   * @throws FormatException if the passed ArgumentType is incomplete
   * @see ArgumentType
   */
  void setArgumentType(ArgumentType argumentType) throws FormatException;

  /**
   * Get all similarity links that are bound to this argument, all similarity links will have this argument as source.
   *
   * @return the (possibly empty) list of similarity links involving this argument
   */
  List<ArgumentSimilarity> getSimilarityLinks();

  /**
   * Get all similar arguments id that match with given shade.
   *
   * @param shade the link shade to keep
   * @return the (possibly empty) list of similar argument id involving this argument
   */
  Set<Long> getSimilarIds(EArgumentLinkShade shade);

  /**
   * Add a similarity link to this argument
   *
   * @param link a similarity link to add to this argument
   * @throws FormatException if given link is invalid
   */
  void addSimilarityLink(ArgumentSimilarity link) throws FormatException;

  /**
   * Get all translations that are bound to this argument.
   *
   * @return the (possibly empty) list of translations involving this argument
   */
  List<Argument> getTranslations();

  /**
   * Set all translation that are bound to this argument.
   *
   * @param translations the (possibly empty) list of translations involving this argument
   */
  void setTranslations(List<Argument> translations);

  /**
   * Add a translation to this argument
   *
   * @param translation a translation to add to this argument
   */
  void addTranslation(Argument translation);

  /**
   * Retrieve a map of "similar" arguments, i.e. arguments that are (in) directly linked to this argument
   * with a similarity link (whatever shade it is, so "qualifies" and "opposes" are also deduced from transitive
   * propagation of links). This argument is also considered as a similar and is also returned in map.
   *
   * @return a map of arguments and their calculated similarity link with this argument
   */
  Map<ArgumentSimilarMap, EArgumentLinkShade> getSimilar();

  /**
   * Check if this argument is similar to given one
   *
   * @param argument an argument
   * @return true if both arguments are similar
   */
  boolean isSimilarWith(Long argument);

  /**
   * This inner class contains argument and similar link to make similar map
   *
   * @author Martin Rouffiange
   */
  class ArgumentSimilarMap {

    private Long argumentSimilarityId;
    private Argument argument;
    private ArgumentSimilarity argumentSimilarity;

    /**
     * Construct an argument similar element for similar map
     *
     * @param argument the concerned argument
     * @param argumentSimilarity the similar link to edit
     */
    public ArgumentSimilarMap(Argument argument, ArgumentSimilarity argumentSimilarity){
      this.argument = argument;
      this.argumentSimilarity = argumentSimilarity;
      this.argumentSimilarityId = (argumentSimilarity != null ? argumentSimilarity.getId() : -1L);
    }

    /**
     * Get the argument
     *
     * @return an argument
     */
    public Argument getArgument() {
      return argument;
    }

    /**
     * Get the argumentLink to edit
     *
     * @return an argument link
     */
    public ArgumentSimilarity getArgumentLink() {
      return argumentSimilarity;
    }

    /**
     * Get the argumentSimilarity id to edit
     *
     * @return an argument link id
     */
    public Long getArgumentSimilarityId() {
      return argumentSimilarityId;
    }
  }
}
