/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */
package be.webdeb.core.api.argument;

import be.webdeb.core.api.contribution.ContextContribution;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.TextualContribution;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.exception.FormatException;
import java.util.List;
import java.util.Map;

public interface ArgumentContext extends BaseArgument, TextualContribution {

    /**
     * Get the context contribution
     *
     * @return the contribution that is the context
     */
    Contribution getContext();

    /**
     * Get the context contribution where this contextualized argument is the first argument.
     *
     * @return the context contribution, if any
     */
    ContextContribution getContextFirstArgument();

    /**
     * Get the contextualized argument
     *
     * @return an argument
     */
    Argument getArgument();

    /**
     * Set the contextualized argument
     *
     * @param argument an argument
     */
    void setArgument(Argument argument);

    /**
     * Get the context contribution id
     *
     * @return the contribution id that is the context
     */
    Long getContextId();

    /**
     * Set the context contribution id
     *
     * @param contextId the contribution id that is the context
     */
    void setContextId(Long contextId);

    /**
     * Get the contextualized argument id
     *
     * @return an argument id
     */
    Long getArgumentId();

    /**
     * Set the contextualized argument id
     *
     * @param argumentId an argument id
     */
    void setArgumentId(Long argumentId);

    /**
     * Get the opposite contextualized argument in the context, if any
     *
     * @return an opposite contextualized argument
     */
    ArgumentContext getOppositeArgumentContext();

    /**
     * Get all justification links that are justified by this contextualized argument.
     *
     * @return the (possibly empty) list of justification links that are justified by this contextualized argument
     */
    List<ArgumentJustification> getJustificationLinksThatAreJustified();

    /**
     * Get all justification links that justify this contextualized argument as first argument of a context
     *
     * @return the (possibly empty) list of justification links that justify this contextualized argument as first argument of a context
     */
    List<ArgumentJustification> getJustificationLinksThatJustifyAsFirstArgument();

    /**
     * Get all justification links that justify this contextualized argument.
     *
     * @return the (possibly empty) list of justification links that justify this contextualized argument
     */
    List<ArgumentJustification> getJustificationLinksThatJustify();

    /**
     * Get all justification links that justify this contextualized argument and their similars arguments.
     *
     * @return the (possibly empty) list of justification links that justify this contextualized argument and similars
     */
    List<ArgumentJustification> getAllJustificationLinksThatJustify();

    /**
     * Add a justification link to this contextualized argument
     *
     * @param link a justification link to add to this contextualized argument
     * @throws FormatException if given link is invalid
     */
    void addJustificationLink(ArgumentJustification link) throws FormatException;

    /**
     * Get all illustration links that are bound to this contextualized argument.
     *
     * @return the (possibly empty) list of illustration links involving this contextualized argument
     */
    List<ArgumentIllustration> getIllustrationLinks();

    /**
     * Get all illustration links that are bound to this contextualized argument.
     *
     * @return the (possibly empty) list of illustration links involving this contextualized argument
     */
    List<ArgumentIllustration> getIllustrationLinksOfJustification();

    /**
     * Get all illustration links that are bound to this contextualized argument and the illustrations linked to
     * the opposite argument / similars of this one if any.
     *
     * @param positive true if the given argument must be concidered as positive
     * @return the (possibly empty) list of illustration links involving this contextualized argument and its possible
     * opposite / similars.
     */
    List<ArgumentIllustration> getAllIllustrationLinks(boolean positive);

    /**
     * Add a illustration link to this contextualized argument
     *
     * @param link a illustration link to add to this contextualized argument
     * @throws FormatException if given link is invalid
     */
    void addIllustrationLink(ArgumentIllustration link) throws FormatException;

    /**
     * Get all texts where this argument (or similars) appeared.
     *
     * @return a possibly empty list of texts
     */
    List<Text> getAllTexts();

    /**
     * Get all debates where this argument (or similars) appeared.
     *
     * @return a possibly empty list of debates
     */
    List<Debate> getAllDebates();

    /**
     * Get all folders linked with this argument and similars
     *
     * @return a possibly empty list of folders
     */
    List<Folder> getAllFolders();

    /**
     * Get all potential arguments that could justify this contextualized argument.
     *
     * @param contextId the context where whe need to found candidates
     * @return the (possibly empty) list of justification links that could justify this one
     */
    List<ArgumentContext> getPotentialJustificationLinks(Long contextId);

    /**
     * Get all excerpts that could be illustrated this argument.
     *
     * @param contextId the context where whe need to found candidates
     * @return the (possibly empty) list of illustration links that could be illustrated this one
     */
    List<Excerpt> getPotentialIllustrationLinks(Long contextId);

    /**
     * Retrieve the amount of excerpts linked to this argument by shade as integer map
     *
     * @param contributorId the id of the contributor for which we need that stats
     * @param groupId the group where see the stats
     * @return the amount of excerpts that are connected to this argument by shade as integer map
     */
    Map<Integer, Integer> getSimpleExcerptMetrics(Long contributorId, int groupId);

}