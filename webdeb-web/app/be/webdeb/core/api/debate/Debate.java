/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.api.debate;

import be.webdeb.core.api.contribution.ContextContribution;

/**
 * This interface represents a debate in the webdeb system. A debate has a first argument and folders as theme for
 * the debate. It also has some justification links between contextualized arguments that create the debate.
 *
 * @author Martin Rouffiange
 */
public interface Debate extends ContextContribution {

    /**
     * Get the english name or default one
     *
     * @return the corresponding value
     */
    String getTitle();

    /**
     * Get the fully readable title with shade
     *
     * @return a fully readable reconstructed title with shade
     */
    String getFullTitle();

}
