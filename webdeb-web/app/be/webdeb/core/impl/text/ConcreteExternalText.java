/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.text;

import be.webdeb.core.api.excerpt.ExternalExcerpt;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.api.text.*;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.core.impl.contribution.AbstractExternalContribution;
import be.webdeb.infra.persistence.accessor.api.TextAccessor;

import java.util.*;

/**
 * This class implements an ExternalText.
 *
 * @author Martin Rouffiange
 */
class ConcreteExternalText extends AbstractExternalContribution<TextFactory, TextAccessor> implements ExternalText {

    private String title;

    private List<ExternalExcerpt> excerpts = new ArrayList<>();

    /**
     * Create a ExternalText instance
     *
     * @param factory       the text factory
     * @param accessor      the text accessor
     */
    ConcreteExternalText(TextFactory factory, TextAccessor accessor, ContributorFactory contributorFactory) {
        super(factory, accessor, contributorFactory);
        this.accessor = accessor;
        type = EContributionType.EXTERNAL_TEXT;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public List<ExternalExcerpt> getExcerpts(){
        if (excerpts.isEmpty()) {
            excerpts = accessor.getExternalExcerpts(id);
        }
        return excerpts;
    }

    @Override
    public void addExcerpt(ExternalExcerpt excerpt) {
        if(excerpt != null)
            excerpts.add(excerpt);
    }

    @Override
    public ExternalExcerpt getFirstNotSavedExternalExcerpt(){
        if(internalContributionId != null && internalContributionId != -1) {
            Optional<ExternalExcerpt> ea = getExcerpts().stream().filter(e -> e.getInternalContribution() == null).findFirst();
            return ea.isPresent() ? ea.get() : null;
        }
        return null;
    }

    @Override
    public Map<Integer, List<Contribution>> save(Long contributor, int currentGroup) throws FormatException, PersistenceException, PermissionException {
        List<String> isValid = isValid();
        if (!isValid.isEmpty()) {
            logger.error("external text contains errors " + isValid.toString());
            throw new FormatException(FormatException.Key.TEXT_ERROR, String.join(",", isValid.toString()));
        }
        return accessor.save(this, currentGroup, contributor);
    }

    @Override
    public int compareTo(ExternalText o) {
        return sourceUrl.compareToIgnoreCase(o.getSourceUrl());
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof ExternalText && hashCode() == obj.hashCode();
    }

    @Override
    public int hashCode() {
        return getTitle().hashCode() + super.hashCode();
    }

    @Override
    public String toString() {
        return "external text with title " + title + " " + super.toString();
    }
}
