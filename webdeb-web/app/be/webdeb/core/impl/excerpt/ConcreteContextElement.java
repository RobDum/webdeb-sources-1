/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.excerpt;

import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.excerpt.ContextElement;
import be.webdeb.core.exception.PersistenceException;

import java.util.HashMap;
import java.util.Map;

public class ConcreteContextElement implements ContextElement {

    private Long id;
    private Contribution contribution;
    private Map<String, String> spellings = new HashMap<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Contribution getContribution() {
        return contribution;
    }

    @Override
    public void setContribution(Contribution contribution) {
        this.contribution = contribution;
    }

    @Override
    public Map<String, String> getSpellings() {
        return spellings;
    }

    @Override
    public String getSpelling(String lang) {
        return spellings.get(lang);
    }

    @Override
    public void addSpelling(String lang, String spelling) {
        spellings.put(lang, spelling);
    }

    @Override
    public void setSpellings(Map<String, String> spellings) {
        this.spellings = spellings;
    }

    @Override
    public void save() throws PersistenceException {

    }
}
