/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.debate;

import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.debate.DebateFactory;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.core.impl.contribution.AbstractContextContribution;
import be.webdeb.infra.persistence.accessor.api.ActorAccessor;
import be.webdeb.infra.persistence.accessor.api.DebateAccessor;

import java.util.*;

public class ConcreteDebate extends AbstractContextContribution<DebateFactory, DebateAccessor> implements Debate {

    private String title = null;

    /**
     * Create a Debate instance
     *
     * @param factory the debate factory
     * @param accessor the debate accessor
     * @param actorAccessor an actor accessor (to retrieve/update involved actors)
     * @param contributorFactory the contributor accessor
     */
    ConcreteDebate(DebateFactory factory, DebateAccessor accessor, ActorAccessor actorAccessor, ContributorFactory contributorFactory) {
        super(factory, accessor, actorAccessor, contributorFactory);
        type = EContributionType.DEBATE;
    }

    @Override
    public String getTitle() {
        if(title == null) {
            title = (firstArgument != null && firstArgument.getArgument() != null && firstArgument.getArgument().getDictionary() != null
                    && firstArgument.getArgument().getDictionary().getTitle() != null ? firstArgument.getArgument().getDictionary().getTitle() : "");
        }
        return title;
    }

    @Override
    public String getFullTitle() {
        return (language != null ? factory.computeShadeAndTitle(argumentType, getTitle(), language.getCode()) : getTitle()) + " ?";
    }

    @Override
    public Map<Integer, List<Contribution>> save(Long contributor, int currentGroup) throws FormatException, PermissionException, PersistenceException {
        List<String> isValid = isValid();
        if (!isValid.isEmpty()) {
            logger.error("debate contains errors " + isValid.toString());
            throw new FormatException(FormatException.Key.DEBATE_ERROR, String.join(",", isValid.toString()));
        }
        return accessor.save(this, currentGroup, contributor);
    }

    @Override
    public List<String> isValid() {
        List<String> fieldsInError = new ArrayList<>();
        if (firstArgumentId == null || firstArgumentId == -1L) {
            fieldsInError.add("first argument id is null");
        }

        return fieldsInError;
    }

    @Override
    public int hashCode() {
        return 107 * (id != -1L ? id.hashCode() : 83) + getTitle().hashCode() + firstArgument.hashCode();
    }


    @Override
    public String toString() {
        return "debate {" +
                "id=" + id +
                "title=" + getTitle() +
                "first argument=" + firstArgumentId +
                '}';
    }
}
