/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */
package be.webdeb.core.impl.contribution;

import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.argument.ArgumentJustification;
import be.webdeb.core.api.argument.ArgumentType;
import be.webdeb.core.api.contribution.*;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.persistence.accessor.api.ActorAccessor;
import be.webdeb.infra.persistence.accessor.api.ContributionAccessor;

import java.util.*;

public abstract class AbstractContextContribution<T extends ContributionFactory,V extends ContributionAccessor> extends AbstractTextualContribution<T, V> implements ContextContribution {

    protected Long firstArgumentId = -1L;
    protected ArgumentContext firstArgument = null;
    protected ArgumentType argumentType;
    protected Language language;
    protected String titleContextContribution;

    protected List<ArgumentJustification> argumentJustifications = null;
    protected List<ArgumentContext> arguments = null;

    /**
     * Constructor
     *
     * @param factory       a ContributionFactory the factory to construct concrete instances
     * @param accessor      a ContributionAccessor the accessor to retrieve and persist concrete Contribution
     * @param actorAccessor an ActorAccessor to retrieve bound actors
     */
    public AbstractContextContribution(T factory, V accessor, ActorAccessor actorAccessor, ContributorFactory contributorFactory) {
        super(factory, accessor, actorAccessor, contributorFactory);
    }

    @Override
    public Long getFirstArgumentId() {
        return firstArgumentId;
    }

    @Override
    public void setFirstArgumentId(Long firstArgumentId) {
        this.firstArgumentId = firstArgumentId;
    }

    @Override
    public ArgumentContext getFirstArgument() {
        if(firstArgument == null) {
            firstArgument = accessor.getOrCreateFirstArgument(id);
            if(firstArgument != null)
                firstArgumentId = firstArgument.getId();
        }
        return firstArgument;
    }

    @Override
    public void setFirstArgument(ArgumentContext firstArgument) {
        if(this.language == null && firstArgument != null && firstArgument.getArgument() != null){
            this.language = firstArgument.getArgument().getDictionary().getLanguage();
        }
        this.firstArgument = firstArgument;
    }

    @Override
    public ArgumentType getFirstArgumentType() {
        return argumentType;
    }

    @Override
    public void setFirstArgumentType(ArgumentType argumentType) throws FormatException {
        if(!argumentType.isValid()){
            throw new FormatException(FormatException.Key.ARGUMENT_ERROR, argumentType.toString());
        }
        this.argumentType = argumentType;
    }

    @Override
    public String getTitle(String lang) {
        if(titleContextContribution == null) {
            titleContextContribution = type == EContributionType.TEXT ? ((Text) this).getTitle(lang) : ((Debate) this).getFullTitle();
        }
        return titleContextContribution;
    }

    @Override
    public List<ArgumentContext> getAllContextualizedArguments() {
        if(arguments == null){
            arguments = accessor.getContextualizedArgumentsFromContext(id);
        }
        return arguments;
    }

    @Override
    public PartialContributions<ArgumentContext> getAllContextualizedArguments(int fromIndex, int toIndex) {
        return accessor.getContextualizedArgumentsFromContext(id, fromIndex, toIndex);
    }

    @Override
    public List<ArgumentJustification> getAllArgumentJustificationLinks() {
        if(argumentJustifications == null){
            argumentJustifications = accessor.getArgumentJustificationLinks(id);
        }
        return argumentJustifications;
    }

    @Override
    public void saveJustificationLinks(List<ArgumentJustification> links, int currentGroup, Long contributor) throws PersistenceException, PermissionException {
        accessor.saveJustificationLinks(links, this, currentGroup, contributor);
    }


    @Override
    public void saveContextContribution(Long contributor, int currentGroup) throws PermissionException, PersistenceException {
        accessor.saveContextContribution(this, type, contributor, currentGroup);
    }

    @Override
    public void deleteContextContribution(Long contributor) {
        try{
            accessor.deleteContextContribution(id, contributor);
        }catch (PermissionException e){
            logger.debug("Undeletable contribution " + id);
        }
    }
}
