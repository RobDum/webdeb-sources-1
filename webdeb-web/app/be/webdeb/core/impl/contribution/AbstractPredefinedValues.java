/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.contribution;

import java.util.Map;

/**
 * Abstract class meant to handle a map of iso-639-1 language codes and corresponding i18 names
 * for a range of predefined values (countries, legal statuses, genders, etc.)
 *
 * @author Fabian Gilson
 */
public abstract class AbstractPredefinedValues {

  protected Map<String, String> i18names;

  /**
   * Get the i18 name for given lang key
   *
   * @param lang a two char iso-639-1 language code
   * @return the corresponding value
   */
  public String getName(String lang) {
    return i18names.get(lang);
  }

  /**
   * Get the map of language code and corresponding i18 names
   *
   * @return a map of iso-639-1 language code and corresponding values
   */
  public Map<String, String> getNames() {
    return i18names;
  }
}

