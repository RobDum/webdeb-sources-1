/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.contribution;

import be.webdeb.core.api.actor.ExternalAuthor;
import be.webdeb.core.api.contribution.ContributionFactory;
import be.webdeb.core.api.contribution.EExternalSource;
import be.webdeb.core.api.contribution.ExternalContribution;
import be.webdeb.core.api.contribution.Language;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.infra.persistence.accessor.api.ContributionAccessor;

import java.util.ArrayList;
import java.util.List;

/**
 * This class implements a Contribution in the webdeb system. It implements a set of common
 * methods to handle any type of contributions.
 *
 * @author Martin Rouffiange
 */
public abstract class AbstractExternalContribution <T extends ContributionFactory, V extends ContributionAccessor>
        extends AbstractContribution<T, V>
        implements ExternalContribution {

    protected Long internalContributionId;
    protected String sourceUrl;
    protected int sourceId = -1;
    protected Language language;
    protected String publicationDate;

    protected List<ExternalAuthor> authors = new ArrayList<>();

    /**
     * Abstract constructor
     *
     * @param factory a contribution factory
     * @param accessor a contribution accessor
     */
    public AbstractExternalContribution(T factory, V accessor, ContributorFactory contributorFactory) {
        super(factory, accessor, contributorFactory);
    }

    @Override
    public String getSourceUrl() {
        return sourceUrl;
    }

    @Override
    public void setSourceUrl(String url) {
        this.sourceUrl = url;
    }

    @Override
    public int getSourceId() {
        return sourceId;
    }

    @Override
    public void setSourceId(Integer source) {
        if(source != null && EExternalSource.value(source) != null){
            this.sourceId = source;
        }
    }

    @Override
    public String getSourceName() {
        if(EExternalSource.value(sourceId) != null){
            return EExternalSource.value(sourceId).name();
        }
        return "";
    }

    @Override
    public Long getInternalContribution() {
        return internalContributionId;
    }

    @Override
    public void setInternalContribution(Long id) {
        this.internalContributionId = id;
    }

    @Override
    public Language getLanguage() {
        return language;
    }

    @Override
    public void setLanguage(Language language) {
        this.language = language;
    }


    @Override
    public String getPublicationDate() {
        return publicationDate;
    }

    @Override
    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    @Override
    public List<ExternalAuthor> getAuthors() {
        return authors;
    }

    @Override
    public void addAuthor(ExternalAuthor author) {
        if(author != null)
            authors.add(author);
    }

    @Override
    public List<String> isValid() {
        List<String> fieldsInError = new ArrayList<>();
        if (sourceUrl == null || sourceUrl.equals("")) {
            fieldsInError.add("url is empty");
        }
        if (publicationDate != null && !factory.getValuesHelper().isDate(publicationDate)) {
            publicationDate = null;
        }
        return fieldsInError;
    }

    @Override
    public int hashCode() {
        return 47 * (id != -1L ? id.hashCode() : 83) + (publicationDate != null ? publicationDate.hashCode() : 97);
    }

    @Override
    public String toString() {
        return "AbstractExternalContribution{" +
                "internalContributionId=" + internalContributionId +
                ", sourceUrl='" + sourceUrl + '\'' +
                ", sourceId='" + sourceId + '\'' +
                ", language='" + language + '\'' +
                ", publicationDate='" + publicationDate + '\'' +
                '}';
    }
}
