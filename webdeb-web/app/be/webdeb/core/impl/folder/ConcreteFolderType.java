/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.folder;

import be.webdeb.core.api.folder.EFolderType;
import be.webdeb.core.api.folder.FolderType;
import be.webdeb.core.impl.contribution.AbstractPredefinedValues;

import java.util.Map;

/**
 * This class implements a FolderType
 *
 * @author Martin Rouffiange
 */
class ConcreteFolderType extends AbstractPredefinedValues implements FolderType {

  private EFolderType ftype;
  private int type;

  ConcreteFolderType(int type, Map<String, String> i18names) {
    this.type = type;
    this.ftype = EFolderType.value(type);
    this.i18names = i18names;
  }

  @Override
  public int getType() {
    return type;
  }

  @Override
  public String toString() {
    return "folder type " + type;
  }

  @Override
  public EFolderType getEFolderType() {
    return ftype;
  }
}
