/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.folder;

import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.folder.*;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.impl.contribution.AbstractContributionFactory;
import be.webdeb.infra.persistence.accessor.api.FolderAccessor;

import javax.inject.Singleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class implements an factory for folders, folders types and links.
 *
 * @author Martin Rouffiange
 */
@Singleton
public class ConcreteFolderFactory extends AbstractContributionFactory<FolderAccessor> implements FolderFactory {

  // key is shade id
  private Map<Integer, FolderType> folderTypes;


  @Override
  public Folder retrieve(Long id) {
    return accessor.retrieve(id, false);
  }

  @Override
  public Folder retrieveWithHit(Long id) {
    return accessor.retrieve(id, true);
  }

  @Override
  public FolderLink retrieveLink(Long parent, Long child) {
    return accessor.retrieveLink(parent, child);
  }

  @Override
  public Folder getFolder() {
    return new ConcreteFolder(this, accessor, contributorFactory);
  }

  @Override
  public FolderLink getFolderLink() {
    return new ConcreteFolderLink(this, accessor, contributorFactory);
  }

  @Override
  public Folder findUniqueByNameAndLang(String name, String lang){
    return accessor.findUniqueByNameAndLang(name, lang);
  }

  @Override
  public Folder random() {
    return accessor.random();
  }

  @Override
  public FolderType createFolderType(int type, Map<String, String> i18names) {
    return new ConcreteFolderType(type, i18names);
  }

  @Override
  public FolderType getFolderType(int type) throws FormatException {
    if (folderTypes == null) {
      getFolderTypes();
    }
    return folderTypes.get(type);
  }

  @Override
  public List<FolderType> getFolderTypes() {
    if (folderTypes == null) {
      folderTypes = accessor.getFolderTypes().stream().collect(Collectors.toMap(FolderType::getType, e -> e));
    }
    return new ArrayList<>(folderTypes.values());
  }

  @Override
  public FolderName createFolderName(String lang, String name){
    return new ConcreteFolderName(lang, name);
  }

  @Override
  public List<Folder> findByName(String name){
    return accessor.findByName(name, null);
  }

  @Override
  public List<Folder> findByName(String name, EFolderType type){
    return accessor.findByName(name, type);
  }

  @Override
  public List<FolderName> getFolderRewordingNames(Long folder){
    return accessor.getFolderRewordingNames(folder);
  }

  @Override
  public HierarchyTree createHierarchyTree() {
    return new ConcreteHierarchyTree();
  }

  @Override
  public HierarchyNode createHierarchyNode(long id, String name) {
    return new ConcreteHierarchyNode(id, name);
  }

  @Override
  public HierarchyNode createHierarchyNode(long id, String name, int depth) {
    return new ConcreteHierarchyNode(id, name, depth);
  }

  @Override
  public HierarchyTree getFolderHierarchyTree(Long folder) {
    return accessor.getFolderHierarchyTree(folder);
  }

  @Override
  public EHierarchyCode checkHierarchy(Folder folder, Folder hierarchy, boolean isParent) {
    return accessor.checkHierarchy(folder, hierarchy, isParent);
  }

  @Override
  public Folder getRssDefaultFolder(){
    Folder folder = accessor.retrieve(100000L, false);
    if(folder == null){
      folder = accessor.random();
    }
    return folder;
  }
}