/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.folder;

import be.webdeb.core.api.argument.ArgumentContext;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contribution.PartialContributions;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.api.contributor.Group;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.folder.*;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.core.impl.contribution.AbstractContribution;
import be.webdeb.infra.persistence.accessor.api.FolderAccessor;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This class implements a Webdeb Folder, that is linked with contributions.
 *
 * @author Martin Rouffiange
 */
class ConcreteFolder extends AbstractContribution<FolderFactory, FolderAccessor> implements Folder {

  protected FolderType folderType;

  private List<Folder> parents;
  private List<Folder> children;
  private HierarchyTree hierarchyTree = null;

  private Map<String, String> names = new HashMap<>();
  private List<FolderName> folderNames = new ArrayList<>();
  private Map<String, List<FolderName>> rewordingNames = new HashMap<>();

  private List<Debate> debates = null;
  private List<ArgumentContext> contextualizedArguments = null;
  private List<Text> texts = null;
  private List<Excerpt> excerpts = null;

  protected static final org.slf4j.Logger logger = play.Logger.underlying();

  ConcreteFolder(FolderFactory factory, FolderAccessor actorAccessor, ContributorFactory contributorFactory) {
    super(factory, actorAccessor, contributorFactory);
    type = EContributionType.FOLDER;
  }


  @Override
  public List<Folder> getParents() {
    if (parents == null) {
      parents = accessor.getParents(id);
    }
    return parents;
  }

  @Override
  public void initParents(){
    parents = new ArrayList<>();
  }


  @Override
  public List<FolderLink> getParentsAsLink() {
    getParents();
    return asLink(parents, false);
  }

  @Override
  public void setParents(List<Folder> folders) {
    if (folders != null) {
      parents = folders;
    }
  }

  @Override
  public List<Folder> getChildren() {
    if (children == null) {
      children = accessor.getChildren(id);
    }
    return children;
  }

  @Override
  public void initChildren(){
    children = new ArrayList<>();
  }

  @Override
  public List<FolderLink> getChildrenAsLink() {
    getChildren();
    return asLink(children, true);
  }

  @Override
  public void setChildren(List<Folder> folders) {
    if(children != null){
      children = folders;
    }
  }

  @Override
  public void addParent(Folder folder) throws FormatException {
    if(folder != null && (folder.getFolderType().getEFolderType() == EFolderType.TAG ||
            (folder.getFolderType().getEFolderType() == EFolderType.COMPOSED && folderType.getEFolderType() == EFolderType.COMPOSED))) {
      List<String> fieldsInError = folder.isValid();
      if (!fieldsInError.isEmpty()) {
        throw new FormatException(FormatException.Key.UNKNOWN_FOLDER, fieldsInError.toString());
      }
      getParents().add(folder);
    }
  }

  @Override
  public void removeParent(Long folder) {
    getParents();
    if (parents != null) {
      parents.removeIf(a -> a.getId().equals(folder));
    }
  }

  @Override
  public void addChild(Folder folder) throws FormatException {
    if(folder != null && (folder.getFolderType().getEFolderType() == EFolderType.COMPOSED ||
            (folder.getFolderType().getEFolderType() == EFolderType.TAG && folderType.getEFolderType() == EFolderType.TAG))) {
      List<String> fieldsInError = folder.isValid();
      if (!fieldsInError.isEmpty()) {
        throw new FormatException(FormatException.Key.UNKNOWN_FOLDER, fieldsInError.toString());
      }
      getChildren().add(folder);
    }
  }

  @Override
  public void removeChild(Long folder) {
    getChildren();
    if (children != null) {
      children.removeIf(a -> a.getId().equals(folder));
    }
  }

  @Override
  public List<Folder> getHierarchy(){
    List<Folder> hierarchy = getParents();
    hierarchy.addAll(getChildren());
    return hierarchy;
  }

  @Override
  public List<FolderLink> getHierarchyAsLink(){
    List<FolderLink> hierarchy = getParentsAsLink();
    hierarchy.addAll(getChildrenAsLink());
    return hierarchy;
  }

  @Override
  public String getDefaultName() {
    String name = "";
    if(names != null && !names.isEmpty()){
      if(names.containsKey("en")){
        name = names.get("en");
      }else if(names.containsKey("fr")){
        name = names.get("fr");
      }else{
        Optional<Map.Entry<String, String>> firstMatchedName = names.entrySet().stream().findFirst();
        name = (firstMatchedName.isPresent() ? firstMatchedName.get().getValue() : "");
      }
    }else if(folderType != null && folderType.getEFolderType() == EFolderType.COMPOSED && !getParents().isEmpty()){
      name = parents.stream().map(Folder::getDefaultName).collect(Collectors.joining(" / "));
    }

    return name;
  }

  @Override
  public String getName(String lang) {
    if(names != null && names.containsKey(lang)){
      return names.get(lang);
    }else{
      return getDefaultName();
    }
  }

  @Override
  public Map<String, String> getNames() {
    return names;
  }

  @Override
  public List<FolderName> getNamesAsFolderName(){
    if(names.size() != folderNames.size()){
      folderNames = names.entrySet().stream()
          .map(n -> factory.createFolderName(n.getKey(), n.getValue())).collect(Collectors.toList());
    }
    return folderNames;
  }

  @Override
  public void setNames(Map<String, String> i18names) {
    if(i18names != null){
      names = i18names;
    }
  }

  @Override
  public void addName(String lang, String name) {
    if(names != null && !"".equals(name.trim()) && lang != null && !"".equals(lang.trim())) {
      names.put(lang, name);
    }
  }

  @Override
  public List<FolderName> getRewordingNamesByLang(String lang) {
    if(lang != null && rewordingNames != null && rewordingNames.containsKey(lang)){
      return rewordingNames.get(lang);
    }
    return new ArrayList<>();
  }

  @Override
  public Map<String, List<FolderName>> getRewordingNames() {
    return (rewordingNames != null ? rewordingNames : new HashMap<>());
  }

  @Override
  public void setRewordingNames(Map<String, List<FolderName>> i18names) {
    rewordingNames = i18names;
  }

  @Override
  public void addRewordingName(String lang, String name) {
    if(rewordingNames != null){
      if(!rewordingNames.containsKey(lang)){
        rewordingNames.put(lang, new ArrayList<>());
      }
      rewordingNames.get(lang).add(factory.createFolderName(lang, name));
    }
  }

  @Override
  public void addContextualizedArgument(ArgumentContext argument) throws FormatException {
    List<String> fieldsInError = argument.isValid();
    if (!fieldsInError.isEmpty()) {
      throw new FormatException(FormatException.Key.ARGUMENT_ERROR, fieldsInError.toString());
    }
    getContextualizedArguments().add(argument);
  }

  @Override
  public void addText(Text text) throws FormatException {
    List<String> fieldsInError = text.isValid();
    if (!fieldsInError.isEmpty()) {
      throw new FormatException(FormatException.Key.TEXT_ERROR, fieldsInError.toString());
    }
    getTexts().add(text);
  }

  @Override
  public void removeContextualizedArgument(Long argument) {
    if(contextualizedArguments != null){
      getContextualizedArguments().removeIf(l -> l.getId().equals(argument));
    }
  }

  @Override
  public void removeText(Long text) {
    if(texts != null){
      getTexts().removeIf(l -> l.getId().equals(text));
    }
  }

  @Override
  public List<Debate> getDebates() {
    if(debates == null){
      debates = getDebates(0, 0).getContributions();
    }
    return debates;
  }

  @Override
  public void setDebates(List<Debate> debates) {
    this.debates = debates;
  }

  @Override
  public List<Excerpt> getExcerpts() {
    if(excerpts == null){
      excerpts = getExcerpts(0, 0).getContributions();
    }
    return excerpts;
  }

  @Override
  public void setExcerpts(List<Excerpt> excerpts) {
    this.excerpts = excerpts;
  }

  @Override
  public List<ArgumentContext> getContextualizedArguments() {
    if (contextualizedArguments == null) {
      contextualizedArguments = getContextualizedArguments(0, 0).getContributions();
    }
    return contextualizedArguments;
  }

  @Override
  public void setContextualizedArguments(List<ArgumentContext> arguments) {
    this.contextualizedArguments = arguments;
  }

  @Override
  public List<Text> getTexts() {
    if (texts == null) {
      texts = getTexts(0, 0).getContributions();
    }
    return texts;
  }

  @Override
  public void setTexts(List<Text> texts) {
    this.texts = texts;
  }

  @Override
  public PartialContributions<ArgumentContext> getContextualizedArguments(int fromIndex, int toIndex) {
    return accessor.getContextualizedArguments(id, fromIndex, toIndex);
  }

  @Override
  public PartialContributions<Excerpt> getExcerpts(int fromIndex, int toIndex) {
    return accessor.getExcerpts(id, fromIndex, toIndex);
  }

  @Override
  public PartialContributions<Debate> getDebates(int fromIndex, int toIndex) {
    return accessor.getDebates(id, fromIndex, toIndex);
  }

  @Override
  public PartialContributions<Text> getTexts(int fromIndex, int toIndex) {
    return accessor.getTexts(id, fromIndex, toIndex);
  }

  @Override
  public FolderType getFolderType() {
    return folderType;
  }

  @Override
  public void setFolderType(FolderType type) {
    if(type != null){
      this.folderType = type;
    }
  }

  @Override
  public HierarchyTree getHierarchyTree(){
    if(hierarchyTree == null){
      hierarchyTree = factory.getFolderHierarchyTree(id);
    }
    return hierarchyTree;
  }

  @Override
  public void setHierarchyTree(HierarchyTree tree){
    this.hierarchyTree = tree;
  }

  @Override
  public String getParentsHierarchyAsJson() {
    return (getHierarchyTree() != null ? hierarchyTree.getParentsHierarchyAsJson(id) : "");
  }

  @Override
  public String getChildrenHierarchyAsJson() {
    return (getHierarchyTree() != null ? hierarchyTree.getChildrenHierarchyAsJson(id) : "");
  }

  @Override
  public int getNbContributions(Long contributor, int group) {
    return accessor.getNbContributions(id, contributor, group);
  }

  @Override
  public Map<Integer, List<Contribution>> save(Long contributor, int currentGroup) throws FormatException, PersistenceException, PermissionException {
    List<String> isValid = isValid();
    if (!isValid.isEmpty()) {
      logger.error("folder contains errors " + isValid.toString());
      throw new FormatException(FormatException.Key.FOLDER_ERROR, String.join(",", isValid.toString()));
    }
    return accessor.save(this, contributor);
  }

  @Override
  public List<String> isValid() {
    List<String> fieldsInError = new ArrayList<>();
    if (getDefaultName() == null) {
      fieldsInError.add("folder has no name");
    }
    return fieldsInError;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ConcreteFolder that = (ConcreteFolder) o;

    // if we have ids, use them
    if (id != null && id != -1L && that.getId() != null && that.getId() != -1L) {
      return id.equals(that.getId());
    }

    String name = getName(factory.getDefaultLanguage());
    if (folderType != that.folderType) {
      return false;
    }

    return name != null ? name.equals(that.getName(factory.getDefaultLanguage()))
        : that.getName(factory.getDefaultLanguage()) == null;
  }

  @Override
  public int compareTo(Folder o) {
    return getName(factory.getDefaultLanguage()).compareToIgnoreCase(o.getName(factory.getDefaultLanguage()));
  }

  @Override
  public int hashCode() {
    return 91 * (id != -1L ? id.hashCode() : 83) + getDefaultName().hashCode() + (folderType != null ? folderType.getType() : 0);
  }

  @Override
  public String toString() {
    return  getDefaultName()
        + " with parents " + (parents != null ? parents.stream().map(Folder::getDefaultName).collect(Collectors.joining(", ")) : "[]")
        + " with children " + (children != null ? children.stream().map(Folder::getDefaultName).collect(Collectors.joining(", ")) : "[]")
        + " of type " + (folderType != null ? folderType.getName("en") : " null");
  }

  /*
   * private helpers
   */

  /**
   * Convert hierarchy list of folders to list of folderlinks
   *
   * @param hierarchy the list of folders in the hierarchy
   * @param isParent true if this folder is the parent in the hierarchy
   * @return either the actor name in given language, or in English if not found and finally in any existing language if none found
   */
  private List<FolderLink> asLink(List<Folder> hierarchy, boolean isParent){
    List<FolderLink> links = new ArrayList<>();
    for(Folder f : hierarchy){
      FolderLink link = factory.getFolderLink();
      if(isParent){
        link.setParent(this);
        link.setChild(f);
      }else{
        link.setParent(f);
        link.setChild(this);
      }
      link.addInGroup(Group.getGroupPublic());
      links.add(link);
    }

    return links;
  }
}