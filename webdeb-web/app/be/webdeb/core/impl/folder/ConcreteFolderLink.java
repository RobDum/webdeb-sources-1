/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.folder;

import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.folder.FolderFactory;
import be.webdeb.core.api.folder.FolderLink;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.core.impl.contribution.AbstractContribution;
import be.webdeb.infra.persistence.accessor.api.FolderAccessor;

import java.util.*;

/**
 * This class implements a FolderLink representing a link between folders.
 *
 * @author Martin Rouffiange
 */
class ConcreteFolderLink extends AbstractContribution<FolderFactory, FolderAccessor> implements FolderLink {

  private Folder parent;
  private Folder child;

  ConcreteFolderLink(FolderFactory factory, FolderAccessor accessor, ContributorFactory contributorFactory) {
    super(factory, accessor, contributorFactory);
    type = EContributionType.FOLDERLINK;
  }

  @Override
  public Folder getParent() {
    return parent;
  }

  @Override
  public Folder getChild() {
    return child;
  }

  @Override
  public void setParent(Folder folder) {
    if(folder != null){
      this.parent = folder;
    }
  }

  @Override
  public void setChild(Folder folder) {
    if(folder != null){
      this.child = folder;
    }
  }

  @Override
  public Map<Integer, List<Contribution>> save(Long contributor, int currentGroup) throws FormatException, PermissionException, PersistenceException {
    List<String> errors = isValid();
    if (!errors.isEmpty()) {
      logger.error("folder link contains error " + errors.toString());
      throw new FormatException(FormatException.Key.FOLDER_LINK_ERROR, String.join(",", errors));
    }
    accessor.save(this, contributor);
    return new HashMap<>();
  }

  @Override
  public List<String> isValid() {
    List<String> fieldsInError = new ArrayList<>();
    fieldsInError.addAll(partialValid(parent));
    fieldsInError.addAll(partialValid(child));
    return fieldsInError;
  }


  @Override
  public String toString() {
    return " with parent " + (parent != null ? parent.getDefaultName() : "")
            + " child " + (child != null ? child.getDefaultName() : "");
  }

  /**
   * Validate a contribution by calling its isContributorValid method
   *
   * @param c a contribution
   * @return a (possibly empty) list of validation errors for given contribution
   */
  private List<String> partialValid(Contribution c) {
    List<String> isValid = new ArrayList<>();
    if (c == null) {
      isValid.add("contribution is null");
    } else {
      isValid = c.isValid();
      if (!isValid.isEmpty()) {
        isValid.add(String.join(",", isValid));
      }
    }
    return isValid;
  }
}