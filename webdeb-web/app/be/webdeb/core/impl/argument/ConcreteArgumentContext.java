/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */
package be.webdeb.core.impl.argument;

import be.webdeb.core.api.argument.*;
import be.webdeb.core.api.contribution.ContextContribution;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.api.debate.Debate;
import be.webdeb.core.api.excerpt.Excerpt;
import be.webdeb.core.api.folder.Folder;
import be.webdeb.core.api.text.Text;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.persistence.accessor.api.ActorAccessor;
import be.webdeb.infra.persistence.accessor.api.ArgumentAccessor;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This class implements a Webdeb contextualized argument, ie, a argument in a context : a text or a debate.
 *
 * @author Martin Rouffiange
 */
public class ConcreteArgumentContext extends AbstractBaseArgument implements ArgumentContext {

    private Contribution context;
    private ContextContribution contextFirstArgument = null;
    private Argument argument;
    private ArgumentContext oppositeArgument = null;
    private Long contextId;
    private Long argumentId;

    private Set<ArgumentJustification> linksThatAreJustified = null;
    private Set<ArgumentJustification> linksThatJustify = null;
    private Set<ArgumentJustification> linksThatJustifyAsFirstArgument = null;
    private Set<ArgumentJustification> allLinksThatJustify = null;
    private Set<ArgumentContext> potentialJustificationLinks = null;

    private Set<ArgumentIllustration> illustrationLinks = null;
    private Set<ArgumentIllustration> allIllustrationLinks = null;
    private Set<ArgumentIllustration> allIllustrationLinksAndSimilars = null;
    private Set<Excerpt> potentialIllustrationLinks = null;

    protected static final org.slf4j.Logger logger = play.Logger.underlying();

    /**
     * Default constructor.
     *
     * @param factory the argument factory (to get and build concrete instances of objects)
     * @param accessor the argument accessor to retrieve and persist arguments
     * @param actorAccessor an actor accessor (to retrieve/update involved actors)
     * @param contributorFactory the contributor factory to get bound contributors
     */
    ConcreteArgumentContext(ArgumentFactory factory, ArgumentAccessor accessor, ActorAccessor actorAccessor,
                            ContributorFactory contributorFactory) {
        super(factory, accessor, actorAccessor, contributorFactory);
        type = EContributionType.ARGUMENT_CONTEXTUALIZED;
        maxFolders = 3;
    }

    @Override
    public Contribution getContext() {
        if(contextId != null){
            if(context == null){
                // lazy loading
                context = factory.retrieve(contextId, EContributionType.ALL);
            }
            return context;
        }
        return null;
    }

    @Override
    public ContextContribution getContextFirstArgument() {
        if(contextFirstArgument == null){
            // lazy loading
            contextFirstArgument = factory.retrieveDebateFirstArgument(id);
        }
        return contextFirstArgument;
    }

    @Override
    public Argument getArgument() {
        if(argumentId != null){
            if(argument == null){
                // lazy loading
                argument = factory.retrieve(argumentId);
            }
            return argument;
        }
        return null;
    }

    @Override
    public void setArgument(Argument argument) {
        this.argument = argument;
    }

    @Override
    public Long getContextId() {
        return contextId;
    }

    @Override
    public void setContextId(Long contextId) {
        this.contextId = contextId;
    }

    @Override
    public Long getArgumentId() {
        return argumentId;
    }

    @Override
    public void setArgumentId(Long argumentId) {
        this.argumentId = argumentId;
    }

    @Override
    public ArgumentContext getOppositeArgumentContext() {
        if(oppositeArgument == null) {
            oppositeArgument = accessor.getOppositeArgumentContext(id);
        }

        return oppositeArgument;
    }

    @Override
    public List<ArgumentJustification> getJustificationLinksThatAreJustified() {
        if(linksThatAreJustified == null){
            linksThatAreJustified = new HashSet<>(accessor.getJustificationLinksThatAreJustified(id));
        }
        return new ArrayList<>(linksThatAreJustified);
    }

    @Override
    public List<ArgumentJustification> getJustificationLinksThatJustifyAsFirstArgument() {
        if(linksThatJustifyAsFirstArgument == null){
            List<ArgumentJustification> links = accessor.getJustificationLinksThatJustify(id);
            ContextContribution c = getContextFirstArgument();
            if(c != null){
                links = links.stream().filter(e -> e.getContextId().equals(c.getId())).collect(Collectors.toList());
            }
            linksThatJustifyAsFirstArgument = new HashSet<>(links);
        }
        return new ArrayList<>(linksThatJustifyAsFirstArgument);
    }

    @Override
    public List<ArgumentJustification> getJustificationLinksThatJustify() {
        if(linksThatJustify == null){
            linksThatJustify = new HashSet<>(accessor.getJustificationLinksThatJustify(id));
        }
        return new ArrayList<>(linksThatJustify);
    }

    @Override
    public List<ArgumentJustification> getAllJustificationLinksThatJustify() {
        if(allLinksThatJustify == null){
            allLinksThatJustify = new HashSet<>(accessor.getAllJustificationLinksThatJustify(id));
        }
        return new ArrayList<>(allLinksThatJustify);
    }

    @Override
    public void addJustificationLink(ArgumentJustification link) throws FormatException {
        List<String> fieldsInError = link.isValid();
        if (!fieldsInError.isEmpty()) {
            throw new FormatException(FormatException.Key.UNKNOWN_ARGUMENT_LINK, fieldsInError.toString());
        }
        if(link.getDestination().getId().equals(id)){
            getJustificationLinksThatJustify().add(link);
        }else{
            getJustificationLinksThatAreJustified().add(link);
        }
    }

    @Override
    public List<ArgumentIllustration> getIllustrationLinks() {
        if(illustrationLinks == null){
            illustrationLinks = new HashSet<>(accessor.getIllustrationLinks(id));
        }
        return new ArrayList<>(illustrationLinks);
    }

    @Override
    public List<ArgumentIllustration> getIllustrationLinksOfJustification() {
        if(illustrationLinks == null){
            illustrationLinks = new HashSet<>(accessor.getIllustrationLinksOfJustification(id));
        }
        return new ArrayList<>(illustrationLinks);
    }

    @Override
    public void addIllustrationLink(ArgumentIllustration link) throws FormatException {
        List<String> fieldsInError = link.isValid();
        if (!fieldsInError.isEmpty()) {
            throw new FormatException(FormatException.Key.UNKNOWN_ARGUMENT_LINK, fieldsInError.toString());
        }
        getIllustrationLinks().add(link);
    }

    @Override
    public List<ArgumentIllustration> getAllIllustrationLinks(boolean positive) {
        if(allIllustrationLinks == null){
            allIllustrationLinks = new HashSet<>(accessor.getAllIllustrationLinks(id, positive));
        }
        return new ArrayList<>(allIllustrationLinks);
    }

    @Override
    public List<Text> getAllTexts() {
        return accessor.getAllTexts(id);
    }

    @Override
    public List<Debate> getAllDebates() {
        return accessor.getAllDebates(id);
    }

    @Override
    public List<Folder> getAllFolders() {
        return accessor.getAllFolders(id);
    }

    @Override
    public List<ArgumentContext> getPotentialJustificationLinks(Long contextId) {
        if(potentialJustificationLinks == null){
            potentialJustificationLinks = new HashSet<>(accessor.getPotentialJustificationLinks(id, contextId));
        }
        return new ArrayList<>(potentialJustificationLinks);
    }

    @Override
    public List<Excerpt> getPotentialIllustrationLinks(Long contextId) {
        if(potentialIllustrationLinks == null){
            potentialIllustrationLinks = new HashSet<>(accessor.getPotentialIllustrationLinks(id, contextId));
        }
        return new ArrayList<>(potentialIllustrationLinks);
    }

    @Override
    public Map<Integer, Integer> getSimpleExcerptMetrics(Long contributorId, int groupId) {
        return accessor.getSimpleExcerptMetrics(id, contributorId, groupId);
    }

    @Override
    public Map<Integer, List<be.webdeb.core.api.contribution.Contribution>> save(Long contributor, int currentGroup) throws FormatException, PersistenceException, PermissionException {
        List<String> errors = isValid();
        if (!errors.isEmpty()) {
            logger.error("contextualized argument contains error " + errors.toString());
            throw new FormatException(FormatException.Key.CONTEXTUALIZED_ARGUMENT_ERROR, String.join(",", errors));
        }
        return accessor.save(this, currentGroup, contributor);
    }

    @Override
    public List<String> isValid() {
        List<String> fieldsInError = new ArrayList<>();

        if(argumentId == null || argumentId == -1L){
            fieldsInError.add("argument is null");
        }

        if(contextId == null || contextId == -1L){
            fieldsInError.add("context is null");
        }

        return fieldsInError;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof ArgumentContext && hashCode() == obj.hashCode();
    }

    @Override
    public int hashCode() {
        return 31 * (id == -1L ? 47 : id.hashCode()) + contextId.hashCode() + argumentId.hashCode();
    }

    @Override
    public String toString() {
        return "contextualized argument " + id + " with context " + contextId + " and argument " + argumentId;
    }
}
