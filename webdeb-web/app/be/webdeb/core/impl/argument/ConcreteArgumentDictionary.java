/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */
package be.webdeb.core.impl.argument;

import be.webdeb.core.api.argument.ArgumentDictionary;
import be.webdeb.core.api.argument.ArgumentFactory;
import be.webdeb.core.api.contribution.Language;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.persistence.accessor.api.ArgumentAccessor;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class ConcreteArgumentDictionary implements ArgumentDictionary {

    private Long id;
    private String title;
    private Language language;

    private List<ArgumentDictionary> translations = null;

    private ArgumentFactory factory;
    private ArgumentAccessor accessor;

    protected static final Logger logger = play.Logger.underlying();

    ConcreteArgumentDictionary(ArgumentFactory factory, ArgumentAccessor accessor) {
        this.factory =  factory;
        this.accessor = accessor;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public Language getLanguage() {
        return language;
    }

    @Override
    public void setLanguage(Language language) {
        this.language = language;
    }

    @Override
    public List<ArgumentDictionary> getTranslations() {
        if(translations == null){
            translations = accessor.getArgumentTranslations(id);
        }
        return translations;
    }

    @Override
    public void save() throws FormatException, PersistenceException {
        List<String> errors = isValid();
        if (!errors.isEmpty()) {
            logger.error("argument dictionary contains error " + errors.toString());
            throw new FormatException(FormatException.Key.ARGUMENT_DICTIONARY_ERROR, String.join(",", errors));
        }
        accessor.save(this);
    }

    @Override
    public List<String> isValid() {
        List<String> fieldsInError = new ArrayList<>();

        if (factory.getValuesHelper().isBlank(title)) {
            fieldsInError.add("title is null");
        }
        return fieldsInError;
    }
}
