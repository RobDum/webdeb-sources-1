/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package be.webdeb.core.impl.argument;

import be.webdeb.core.api.argument.*;
import be.webdeb.core.api.contribution.Contribution;
import be.webdeb.core.api.contribution.EContributionType;
import be.webdeb.core.api.contributor.ContributorFactory;
import be.webdeb.core.exception.FormatException;
import be.webdeb.core.exception.PermissionException;
import be.webdeb.core.exception.PersistenceException;
import be.webdeb.infra.persistence.accessor.api.ActorAccessor;
import be.webdeb.infra.persistence.accessor.api.ArgumentAccessor;

import java.util.*;

/**
 * This class implements a Webdeb argument, ie, a sentence written by a contributor.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */
class ConcreteArgument extends AbstractBaseArgument implements Argument {

  private ArgumentDictionary dictionary;
  protected ArgumentType argumentType;

  private Set<ArgumentSimilarity> similarityLinks = new HashSet<>();
  private Map<EArgumentLinkShade, Set<Long>> similarityLinksByShade = new LinkedHashMap<>();
  private Map<ArgumentSimilarMap, EArgumentLinkShade> similarMap;

  private Map<Long, Argument> translations = new HashMap<>();

  protected static final org.slf4j.Logger logger = play.Logger.underlying();

  /**
   * Default constructor.
   *
   * @param factory the argument factory (to get and build concrete instances of objects)
   * @param accessor the argument accessor to retrieve and persist arguments
   * @param actorAccessor an actor accessor (to retrieve/update involved actors)
   * @param contributorFactory the contributor factory to get bound contributors
   */
  ConcreteArgument(ArgumentFactory factory, ArgumentAccessor accessor, ActorAccessor actorAccessor,
                   ContributorFactory contributorFactory) {
    super(factory, accessor, actorAccessor, contributorFactory);
    type = EContributionType.ARGUMENT;
  }

  /*
   * GETTERS / SETTERS
   */

  @Override
  public ArgumentDictionary getDictionary() {
    return dictionary;
  }

  @Override
  public void setDictionary(ArgumentDictionary dictionary) {
    this.dictionary = dictionary;
  }

  @Override
  public String getTitle() {
    return dictionary.getTitle();
  }

  @Override
  public String getFullTitle() {
    return dictionary.getLanguage() != null ?
            factory.computeShadeAndTitle(argumentType, dictionary.getTitle(), dictionary.getLanguage().getCode()) :
            dictionary.getTitle();
  }

  @Override
  public String getFullTitle(EArgumentType type) {
    ArgumentType argType = null;
    try {
      argType = factory.getArgumentType(type.id());
      return dictionary.getLanguage() != null ?
              factory.computeShadeAndTitle(argType, dictionary.getTitle(), dictionary.getLanguage().getCode()) :
              dictionary.getTitle();
    } catch (FormatException e) {
      return getFullTitle();
    }
  }

  @Override
  public List<ArgumentSimilarity> getSimilarityLinks() {
    if (similarityLinks.isEmpty()) {
      similarityLinks = new HashSet<>(accessor.getSimilarityLinks(id));
    }
    return new ArrayList<>(similarityLinks);
  }

  @Override
  public Set<Long> getSimilarIds(EArgumentLinkShade shade) {
    if(!similarityLinksByShade.containsKey(shade)){
      similarityLinksByShade.put(shade, accessor.getSimilarIds(id, shade));
    }
    return similarityLinksByShade.get(shade);
  }

  @Override
  public void addSimilarityLink(ArgumentSimilarity link) throws FormatException {
    List<String> fieldsInError = link.isValid();
    if (!fieldsInError.isEmpty()) {
      throw new FormatException(FormatException.Key.UNKNOWN_ARGUMENT_LINK, fieldsInError.toString());
    }
    getSimilarityLinks().add(link);
  }

  @Override
  public ArgumentType getArgumentType() {
    return argumentType;
  }

  @Override
  public void setArgumentType(ArgumentType argumentType) throws FormatException {
    if(!argumentType.isValid()){
      throw new FormatException(FormatException.Key.ARGUMENT_ERROR, argumentType.toString());
    }
    this.argumentType = argumentType;
  }

  @Override
  public List<Argument> getTranslations() {
    return new ArrayList<>(translations.values());
  }

  @Override
  public void setTranslations(List<Argument> translations) {
    translations.forEach(e -> this.translations.put(e.getId(), e));
  }

  @Override
  public void addTranslation(Argument translation) {
    if(translation != null && !translations.containsKey(translation.getId())){
      translations.put(translation.getId(), translation);
    }
  }

  @Override
  public boolean isSimilarWith(Long argument) {
    return accessor.isSimilarWith(id, argument);
  }

  @Override
  public Map<Integer, List<Contribution>> save(Long contributor, int currentGroup) throws FormatException, PersistenceException, PermissionException {
    List<String> errors = isValid();
    if (!errors.isEmpty()) {
      logger.error("argument contains error " + errors.toString());
      throw new FormatException(FormatException.Key.ARGUMENT_ERROR, String.join(",", errors));
    }
    return accessor.save(this, currentGroup, contributor);
  }

  @Override
  public List<String> isValid() {
    List<String> fieldsInError = new ArrayList<>();

    if (argumentType == null) {
      fieldsInError.add("argumentType is null");
    }

    if (factory.getValuesHelper().isBlank(dictionary.getTitle())) {
      fieldsInError.add("title is null");
    }
    return fieldsInError;
  }

  @Override
  public boolean equals(Object obj) {
    return obj != null && obj instanceof Argument && hashCode() == obj.hashCode();
  }

  @Override
  public int hashCode() {
    return 31 * (id == -1L ? 47 : id.hashCode()) + dictionary.getTitle().hashCode() + argumentType.getArgumentShade();
  }

  @Override
  public String toString() {
    return "argument " + id + " of type " + argumentType.toString() + " with title " + dictionary.getTitle();
  }

  /*
   * GRAPH EXPLORATION
   */

  @Override
  public Map<ArgumentSimilarMap, EArgumentLinkShade> getSimilar() {
    if (similarMap == null) {
      similarMap = new HashMap<>();
      Map<Long, VerticesMap> working = new HashMap<>();

      // get all directly linked arguments with a similarity relationship
      getSimilarityLinks().parallelStream().filter(l -> l.getArgumentLinkType().getLinkShade() < EArgumentLinkShade.SUPPORTS.id()).forEach(
          l -> working.put(l.getDestination().getId(), new VerticesMap(createSimilarItem(l.getDestination(), l), EArgumentLinkShade.value(l.getArgumentLinkType().getLinkShade()))));
      // now follow graph until no more argument are added to hashmap => we found all related arguments
      Map<Long, VerticesMap> newVertices = new HashMap<>(working);
      // set given argument as similar
      working.put(id, new VerticesMap(createSimilarItem(this, null), EArgumentLinkShade.SIMILAR));

      while (!newVertices.isEmpty()) {
        // fixed-point -> until no more new similar are found
        Map<Long, VerticesMap> toAdd = new HashMap<>();
        newVertices.forEach((id, vertice) -> {
          vertice.map.getArgument().getSimilarityLinks().parallelStream()
              .filter(link -> link.getArgumentLinkType().getLinkShade() < EArgumentLinkShade.SUPPORTS.id())
              .forEach(link -> {
                Argument argTemp = link.getDestination().getId().equals(vertice.map.getArgument().getId()) ? link.getOrigin() : link.getDestination();
                ArgumentSimilarMap temp = createSimilarItem(argTemp, link);
                if (!working.containsKey(argTemp.getId())) {
                  // must calculate shade wrt initial argument => l contains the shade regarding this argument
                  switch (vertice.shade) {
                    case SIMILAR:
                      working.put(argTemp.getId(), new VerticesMap(temp, EArgumentLinkShade.value(link.getArgumentLinkType().getLinkShade())));
                      toAdd.put(argTemp.getId(), new VerticesMap(temp, link.getArgumentLinkType().getEType()));
                      break;

                    case OPPOSES:
                        EArgumentLinkShade shade = link.getArgumentLinkType().getEType().getOpposite();
                        working.put(argTemp.getId(), new VerticesMap(temp, shade));
                        toAdd.put(argTemp.getId(), new VerticesMap(temp, shade));
                      break;
                    default:
                      // unreachable
                  }
                }
              });}
        );

        // clear vertices we just used as origins and add new discovered ones
        newVertices.clear();
        newVertices.putAll(toAdd);
      }
      working.entrySet().forEach(e -> similarMap.put(e.getValue().map, e.getValue().shade));
    }
    return similarMap;
  }

  /**
   * Create a similar map item.
   *
   * @param argument the concerned argument
   * @param argumentSimilarity the similar link to edit
   */
  private ArgumentSimilarMap createSimilarItem(Argument argument, ArgumentSimilarity argumentSimilarity){
    return new ArgumentSimilarMap(argument, argumentSimilarity);
  }

  /**
   * This inner class to keep complexe structure
   *
   * @author Martin Rouffiange
   */
  public class VerticesMap {

    public ArgumentSimilarMap map;
    public EArgumentLinkShade shade;

    /**
     * Default constructor
     *
     * @param map the similar map
     * @param shade the link shade
     */
    public VerticesMap(ArgumentSimilarMap map,  EArgumentLinkShade shade){
      this.map = map;
      this.shade = shade;
    }
  }
}
