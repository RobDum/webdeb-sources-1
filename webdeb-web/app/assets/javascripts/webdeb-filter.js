/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This file handle filter-related functions, ie, when a filter is displayed on a search/viz page.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */


/**
 * Manage the whole filter bar in search page
 *
 * @param btn the btn used to toggle display of filter
 * @param pager the pager that must be adapted when filtering
 * @param filteredContainer the container where the filters must be applied (for multiple hidden screens like viz content)
 * @param container the container where the filter pane must be attached, null if it must be attach to main content
 * @param top the height of margin-top for vertical filter container
 * @param copy true if the filterbar must be copy and not detached
 * @param onlyVisible true if filter must match only visible filterable, false if onyl hidden and null if the whole.
 * @param filterbar the filterbar
 */
function handleFilter(btn, pager, filteredContainer, container, top, copy, filterbar, onlyVisible) {
    container = container || $("#main-content");
    top = (top === null || top === undefined ? null : top);
    copy = copy || false;
    onlyVisible = onlyVisible === undefined ? null : onlyVisible;
    btn.show();

    if(filterbar == null) {
        filterbar = filteredContainer != null ? filteredContainer.children(".vertical_filter") : $(".vertical_filter");
    }else{
        let existing = container.children(".vertical_filter");
        if(existing.exists())
            existing.remove();
    }
    killHandleFilterEvents(btn);

    if(top != null)
        filterbar.css("top", top+"px");
    if(copy){
        filterbar = filterbar.clone();
    }else{
        filterbar.detach();
    }

    setTimeout(function(){filterbar.show();}, 100);
    filterbar.appendTo(container);


    // open/close on button
    btn.on('click', function () {
        togglerFilterbar(filterbar, container);
    });

    $(document).on('click', '.toggle_filter', function () {
        if(!$(this).is(btn) && !btn.is(':visible') && $(this).is(':visible')) {
            togglerFilterbar(filterbar, container);
        }
    });

    // open/close on button
    $(document).on('open_filterbar', function () {
        togglerFilterbar(filterbar, container, true);
    });

    // open/close on button
    $(document).on('close_filterbar', function () {
        togglerFilterbar(filterbar, container, false);
    });

    // open/close on button
    filterbar.find('.close').on('click', function () {
        togglerFilterbar(filterbar, container, false);
    });

    // map with all filter key-values pair
    var filterKeys = {};
    // build date sliders
    filterbar.find('.sub_filter--range-link').each(function () {
        // get min and max values
        var current_min = parseInt($(this).find('[name="current_min"]').val());
        var current_max = parseInt($(this).find('[name="current_max"]').val());
        var min = parseInt($(this).find('[name="min"]').val());
        var max = parseInt($(this).find('[name="max"]').val());
        // create slider for this dual range
        $(this).find('.slider').slider({
            min: min,
            max: max,
            range: true,
            values: [current_min, current_max],
            change: function (event, ui) {
                // will filter elements on page
                applyFilter(handleSliderChange(filterKeys, ui), pager, filteredContainer, filterbar, onlyVisible);
            }
        }).slider("pips", {
            rest: "label",
            // putting seven labels at most
            step: (max - min) >= 7 ? Math.ceil((max - min) / 7) : (max - min)
        }).slider("float");
    });

    if(top === null) {
        // move filter tab to top of screen when scrolling down (since no more visible navbar)
        $(document).on('scroll', function () {
            filterbar.toggleClass('top', $(window).scrollTop() > 50);
        });
    }

    // toggle visibility of sub filters on click
    $('.filter').find('.filter--link').on('click', function() {
        $(this).parent().toggleClass('filter--subitems__opened');
    });

    // add listeners on all box filters to hide non-matching elements
    filterbar.find('.sub_filter--item').on('click', 'input', function () {
        var input = $(this);
        applyFilter(toggleAllChildrenNodes(filterKeys, input, filterbar), pager, filteredContainer, filterbar, onlyVisible);
    });

    if(filteredContainer !== undefined){
        filteredContainer.off('filter-reload');
        filteredContainer.on('filter-reload', function(){
            applyFilter(createFilterKeysFromFilterBars(filterbar), pager, filteredContainer, filterbar, onlyVisible);
        });
    }
}

/**
 * Toggle the filterbar
 *
 * @param filterbar the filterbar
 * @param container the container where the filterbar is
 * @param open if filter must be open or close
 */
function togglerFilterbar(filterbar, container, open){
    open = open === undefined ? !filterbar.hasClass('vertical_filter__opened') : open;

    filterbar.toggleClass('vertical_filter__opened', open);
    container.toggleClass('opened', open);
    if(container.is('#main-content')){
        container.next().toggleClass('opened', open);
    }

    setTimeout(function(){
        $(document).trigger("filter-resize");
    }, 50)
}

/**
 * Kill all events about filter because of possible multiple call of handleFilters
 *
 * @param btn the btn used to toggle display of filter
 */
function killHandleFilterEvents(btn){
    $(document).off( "scroll" );
    $(document).off('open_filterbar');
    $(document).off('close_filterbar');
    $('.filter').find('.filter--link').off( "click" );
    $('.sub_filter--item input').off('click');
    btn.off('click');
}

/**
 * Update given filterKeys with given (box) item, depending on its state
 *
 * @param filterKeys a map of key-values representing the values to filter on
 * @param item a checkbox filter item that has been either checked (will be added) or unchecked (will be removed)
 * @returns the updated filterKeys with given item's value either added or removed
 */
function updateFilterKeys(filterKeys, item) {
    // update list of active filters
    if (item.is(':checked')) {
        if (!(item.data('filter-name') in filterKeys)) {
            filterKeys[item.data('filter-name')] = [];
        }
        filterKeys[item.data('filter-name')].push(item.val());
    } else {
        if (item.data('filter-name') in filterKeys) {
            // we know this value is in filters
            filterKeys[item.data('filter-name')]
                .splice(filterKeys[item.data('filter-name')].indexOf(item.val()), 1);

            // remove key if nothing in it anymore
            if (filterKeys[item.data('filter-name')].length === 0) {
                delete filterKeys[item.data('filter-name')];
            }
        }
    }

    return filterKeys;
}


/**
 * Create filterKeys object from the current filter bar
 *
 * @param filterbar the bar of filter
 * @returns the filterKeys from the given filterbar
 */
function createFilterKeysFromFilterBars(filterbar) {
    let filterKeys = {};

    filterbar.find(".sub_filter--item").each(function(key, element){
        element = $(element);

        if(element.find('.sub_filter--range-link').exists()){
            let currentMin = parseInt(element.find('input[name="current_min"]').val());
            let currentMax = parseInt(element.find('input[name="current_max"]').val());
            let min = parseInt(element.find('input[name="min"]').val());
            let max = parseInt(element.find('input[name="max"]').val());
            let filterName = element.find('.slider').data('filter-name');

            if(currentMin > min)
                filterKeys[filterName + '0'] = [currentMin];

            if(currentMax < max)
                filterKeys[filterName + '1'] = [currentMax];

        } else if(element.find('.sub_filter--basic_item').exists()){
            let input = element.find('input');
            if (input.is(':checked'))
                updateFilterKeys(filterKeys, input);
        } else {
            for(let iTree in element.find(".filter-tree")){
                let input = element.eq(iTree).find('input');
                if (input.is(':checked'))
                    updateFilterKeys(filterKeys, input);
            }
        }
    });

    return filterKeys;
}

/**
 * Update given filter map with values currently in range from given slider
 *
 * @param filterKeys a map of key-values representing the values to filter on
 * @param slider the updated slider
 * returns the updated filterKeys with values in range for given slider (targeted by given event)
 */
function handleSliderChange(filterKeys, slider) {
    let filterName = $(slider.handle).parent('.slider').data('filter-name') + slider.handleIndex;

    // do we have already this filter name (why "in" doesn't work here ??)
    if (filterKeys[filterName] === undefined) {
        filterKeys[filterName] = [];
    }

    let input;
    if(slider.handleIndex === 0){
        input = $(slider.handle).parent('.slider').siblings('[name="min"]');
        $(slider.handle).parent('.slider').siblings('[name="current_min"]').val(slider.value);
    }else{
        input = $(slider.handle).parent('.slider').siblings('[name="max"]');
        $(slider.handle).parent('.slider').siblings('[name="current_max"]').val(slider.value);
    }

    // new value for this filter ? have to get the "min and max values" because they are not saved in this ui object
    var limit = parseInt(input.val());

    if (filterKeys.length === 0 || (filterKeys[filterName][0] !== slider.value && slider.value !== limit)) {
        filterKeys[filterName][0] = slider.value;
    } else {
        // otherwise, delete this key (because it has been reset to the initial state or unchanged)
        delete filterKeys[filterName];
    }

    return filterKeys;
}

let t, d;

/**
 * Apply newly selected filter item on current filterable page
 *
 * @param filterKeys a list of filters keys
 * @param pager pager element to be updated (since amount of page will probably vary after applying the filter item
 * @param filteredContainer the container where the filters must be applied (for multiple hidden screens like viz content)
 * @param filterbar the filterbar where all filter name are displayed
 * @param onlyVisible true if filter must match only visible filterable, false if onyl hidden and null if the whole.
 */
async function applyFilter(filterKeys, pager, filteredContainer, filterbar, onlyVisible) {
    let selector = onlyVisible === null ? "" : onlyVisible ? ":visible" : ":hidden";
    let items = filteredContainer == null ? $('.filterable-item' + selector) : filteredContainer.find('.filterable-item' + selector);
    let filters = filterbar.find('.sub_filter--basic_item');
    let toLoad = [];
    let toLoadJson = [];
    let d1 = new Date();
    // Filter only if there is some key to filtered, otherwise display everything
    if (!isEmpty(filterKeys)) {
        // Put filter count to 0
        updateFilterCount((isEmpty(filterKeys) ? true : 0), filters);

        items.each(function () {
            let filterable = $(this).data('filterable');
            let onlyfilteron = $(this).data('onlyfilteron');
            toLoad.push({item: $(this), filterable: filterable, onlyfilteron: onlyfilteron});
            toLoadJson.push({filterable: filterable, onlyfilteron: onlyfilteron});
        });

        await callWorkers("/assets/javascripts/workers/webdeb-filter-worker.js",
            toLoad, toLoadJson, {filterKeys: filterKeys, filters: filters}, {filterKeys: filterKeys}, applyFilterAsync, false, applyFilterAsyncResults, 150);
    }else{
        // Put the filter count to the default value
        updateFilterCount(true, filters);
        items.toggleClass('filtered', false);
    }

    //console.log(new Date() - d1);

    $(document).trigger("filtered");
    $(document).trigger('change-filter');
    // recalculate pager
    if (pager != null) pager.reset();
}

/**
 * Update filter item count
 *
 * @param filterable an filterable item to add to count if it matches the list of filters, 0 if the count must be
 *        erased and true if the count must has its default value
 * @param filters the list of filters that need a count update
 */
function updateFilterCount(filterable, filters){
    for(let i = 0; i < filters.length; i++){
        let item = $(filters[i]);
        let filterName = item.find('input').data('filter-name');
        let filterCount = item.find('.sub_filter-item-count');
        if (typeof (filterable) === "boolean") {
            filterCount.html("(" + (filterCount.data("count")) + ")");
            // show all filters and category
            updateFilterItemVisibility(item, true);
        } else if (!isNaN(filterable)) {
            filterCount.html("(0)");
            // hide all filters and category
            updateFilterItemVisibility(item, false);
        } else if (filterName in filterable && filterable[filterName].includes(item.find('input').val())) {
            let matches = filterCount.html().match(/\((.*?)\)/);
            let currentCount = parseInt(matches[1]);
            if(currentCount < 99)
                filterCount.html("(" + (parseInt(matches[1]) + 1) + ")");
            // show the concerned filter and its category
            updateFilterItemVisibility(item, true);
        }
    }
}

function updateFilterItemVisibility(item, show){
    item.parents(".sub_filter--item").toggle(show);
    item.parents(".filter--item").toggle(show);
    item.parents(".sub_filter--tree-collapse").toggle(show);
    item.parents(".sub_filter--tree").toggle(show);
}

/**
 * Expand all the tree of place, except for country nodes
 *
 * @param treeId the id of the tree root
 */
function managePlaceTree(treeId){
    var tree = $("#"+treeId);
    tree.find(".dropdown-toggle").attr('aria-expanded', 'true');
    tree.find(".div-collapsed").attr('aria-expanded', 'true').addClass('in');
    tree.find(".dropdown-toggle").parent().find(".node-2").attr('aria-expanded', 'false');
    tree.find(".div-collapsed").parent().find(".node-2").attr('aria-expanded', 'false').removeClass('in');
}

/**
 * Toggle checkbox for all item children if the item is a filter tree
 *
 * @param filterKeys a map of key-values representing the values to filter on
 * @param item a checkbox filter item that has been either checked (will be added) or unchecked (will be removed)
 * @param filterbar the filterbar
 * @returns the updated filterKeys with given item's value either added or removed
 */
function toggleAllChildrenNodes(filterKeys, item, filterbar){
    if(item.hasClass("filter-tree-linked")) {
        var checked = item.is(":checked");
        var a = item.next().find("button");
        filterKeys = updateFilterKeys(filterKeys, item);

        if (a !== undefined) {
            filterbar.find(a.attr('data-target')).find(".filter-tree").each(function () {
                $(this).prop('checked', checked);
                filterKeys = updateFilterKeys(filterKeys, $(this));
            });
        }
    }else{
        filterKeys = updateFilterKeys(filterKeys, item);
    }

    return filterKeys;
}

/**
 * handle filter after excecute search
 *
 * @param pager the pager that must be adapted when filtering
 * @param container the container where the filter pane must be attached, null if it must be attach to main content
 * @param filteredContainer the container where the filters must be applied (for multiple hidden screens like viz content)
 */
function postSearchExecuteFilter(pager, container, filteredContainer){
    handleFilter($(container).find(filteredContainer).find('#show-filter'), pager, $(container).find(filteredContainer), $(container));
}

function createFilterbar(filters, existingFilters) {
    let modifyFilters = existingFilters != null;
    let container;
    let filter_container;

    if(modifyFilters){
        container = existingFilters;
        filter_container = container.find('.js-filter');
    }else{
        container = $('<div class="vertical_filter" style="display:none">');

        $('<div class="col-xs-12" style="margin:10px 0">' +
            '<span class="text-muted small-caps">' + Messages("general.filter.title") + '</span>' +
            '<span class="pull-right" style="margin-top:4px">' +
            '<button class="small-font close toggle_filter"><i class="fa fa-times"></i></button></span></div>').appendTo(container);

        filter_container = $('<ul class="js-filter filter">').appendTo(container);
    }

    //managePlaceTree("place-tree");
    let filterId = filters["filtername"][0].name;

    for (let iElement in filters) {
        let element = filters[iElement];

        if(element.length > 0 && iElement !== "filtername" && (element[0].filter !== "BOX" || element.length > 1)) {
            let element_container = filter_container.find('#filter-item-' +  generateHashcode(iElement));
            if(modifyFilters && element_container.exists()){
                updateFilterElement(filterId, element_container, iElement, element);
            }else{
                filter_container.append(createFilterElement(filterId, modifyFilters, iElement, element));
            }
        }
    }

    return container;
}

function createFilterElement(filterId, modifyFilters, elementName, element){
    let container = $('<li id="filter-item-' +  generateHashcode(elementName) + '" class="filter--item  filter--item__has_sub_filter"></li>');

    $('<label class="filter--link" title="' + Messages("general.filter.title." + elementName) + '">' +
        '<span class="filter--icon ' + Messages("general.filter.icon." + elementName) + '"></span>' +
        '<span class="filter--label">' + Messages("general.filter.label." + elementName) + '</span></label>').appendTo(container);

    let filter_container = $('<ul class="sub_filter">').appendTo(container);

    let index = 0;
    for (let iItem in element) {
        let item = element[iItem];
        filter_container.append(createFilterItem(filterId, elementName, item.name, index++, item));
    }

    return container;
}

function updateFilterElement(filterId, existing, elementName, element){

    let filter_container = existing.find('.sub_filter');

    let index = filter_container.find(".sub_filter--item").length;
    let itemContainer = null;
    for (let iItem in element) {
        let item = element[iItem];
        switch(item.filter) {
            case "RANGE" :
                itemContainer = filter_container.find('#sub_filter_range-' + elementName);
                break;
            case "BOX" :
                itemContainer = filter_container.find('#sub_filter_box-' + generateHashcode(elementName + '-' + iItem));
                break;
            case "TREE" :
                filter_container.empty();
                break;
        }
        if(itemContainer != null && itemContainer.exists()){
            updateFilterItem(itemContainer, item);
        }else{
            filter_container.append(createFilterItem(filterId, elementName, iItem, index++, item));
        }
    }
}

function createFilterItem(filterId, elementName, itemName, iItem, item){
    let container = $('<li class="sub_filter--item col-xs-12 no-padding">');

    switch(item.filter){
        case "RANGE" :
            let splitted = itemName.split(",");
            if(splitted.length === 2) {
                $('<div id="sub_filter_range-' + elementName + '" class="col-xs-12 sub_filter--range-link" >' +
                    '<div class="slider" data-filter-name="' + elementName + '"></div>' +
                    '<input class="hidden" name="current_min" value="' + splitted[0] + '">' +
                    '<input class="hidden" name="current_max" value="' + splitted[1] + '">' +
                    '<input class="hidden" name="min" value="' + splitted[0] + '">' +
                    '<input class="hidden" name="max" value="' + splitted[1] + '">' +
                    '</div>').appendTo(container);
            }
            break;
        case "BOX" :
            let id = "f-" + elementName + "-" + iItem + "-" + filterId;
            $('<div id="sub_filter_box-' + generateHashcode(elementName + '-' + itemName) + '" class="sub_filter--basic_item funkyradio smaller-font">' +
                '<div class="funkyradio-primary col-xs-12">' +
                '<input id="' + id + '" type="checkbox" value="' + itemName + '" data-filter-name="' + elementName + '">' +
                '<label for="' + id + '" class="forcewrap" style="width : 240px">' + itemName + '</label>' +
                (item.nbOccurs > 0 ? '<span class="sub_filter-item-count" data-count="' + item.nbOccurs + '">(' + item.nbOccurs + ')</span>' : '')
                + '</div></div>').appendTo(container);
            break;
        case "TREE" :
            createFilterItemsTree(item.tree, filterId, elementName, elementName + "-tree", 0, item.linkedHierarchy, false, container);
            break;
    }

    return container;
}

function createFilterItemsTree(node, filterId, filterName, nodeId, iLevelNode, linked, collapse, container){
    if(node !== undefined && node.subNodes.length > 0) {
        let collapseClass = collapse ? ' collapse sub_filter--tree-collapse div-collapsed node-' + iLevelNode : '';
        let nodeElement = $('<div class="' + nodeId + "-" + filterId + collapseClass + '"></div>').appendTo(container);

        if(iLevelNode > 0 && !linked) {
            createFilterItemTree(node, filterId, filterName, nodeId, iLevelNode, linked, nodeElement, true, 0);
        }

        for (let i in node.subNodes) {
            let subNode = node.subNodes[i];

            createFilterItemTree(subNode, filterId, filterName, nodeId, iLevelNode, linked, nodeElement, false, i+1);

            let collapse = iLevelNode < 2;
            createFilterItemsTree(subNode, filterId, filterName, nodeId + "-" + (i+1), iLevelNode + 1, linked, collapse, nodeElement);
        }
    }
}

function createFilterItemTree(subNode, filterId, filterName, nodeId, iLevelNode, linked, nodeElement, bis, i){
    let caret = subNode.subNodes.length > 0 ?
        '<button class="btn btn-link dropdown-toggle node-' + iLevelNode + ' filter-nodelist-button" data-toggle="collapse" data-target=".'+nodeId+'-'+i+"-"+filterId+'"><span class="caret"></span></button>' : '';

    let n1 = $('<div class="funkyradio smaller-font sub_filter--tree"></div>').appendTo(nodeElement);
    let n2 = $('<div class="funkyradio-primary col-xs-12 no-padding ' + (subNode.nbOccurs > 0 ? 'sub_filter--basic_item' : '') + '" style="left:' + (iLevelNode*5) + 'px !important">').appendTo(n1);

    if(linked || subNode.subNodes.length === 0 || bis) {
        let inputId = filterName + '-' + subNode.id + '-' + filterId;
        $('<input id="f-' + inputId + '" class="filter-tree ' + (linked ? 'filter-tree-linked' : '') + '" type="checkbox" value="' + subNode.name + '" data-filter-name="' + filterName + '">').appendTo(n2);
        $('<label for="f-' + inputId + '" class="filter-nodelist-label forcewrap" title="' + subNode.name + '">' + subNode.name + caret + '</label>').appendTo(n2);
        if(subNode.nbOccurs > 0){
            $('<span class="sub_filter-item-count" data-count="' + subNode.nbOccurs + '">(' + subNode.nbOccurs + ')</span>').appendTo(n2);
        }
    }else{
        $('<span class="filter-nodelist-label forcewrap" title="' + subNode.name + '" style="padding-left: 45px;text-decoration: underline">' + subNode.name + caret + '</label>').appendTo(n2);
    }
}

function updateFilterItem(existing, item){

    switch(item.filter){
        case "BOX" :
            let e = existing.find(".sub_filter-item-count");
            let nbOccurs = item.nbOccurs + parseInt(e.data("count"));
            e.data("count", nbOccurs);
            e.text('(' + nbOccurs + ')');
            break;
        case "TREE" :
            break;
    }
}

function createFilterButton(className, show){
    show = show === undefined ? true : show;

    return $('<button type="button" class="btn btn-default toggle_filter '
        + (className === undefined ? '' : className) + '" ' + (show ? '' : 'style="display: none"') + '>'+
        '<i class="fa fa-filter"></i>&nbsp; ' + Messages("general.filter.button.msg") + '</button>');
}