/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This javascript file contains all reusable functions for debate contributions' screens
 *
 * @author Martin Rouffiange
 */


/*
 * DEBATE EDIT MANAGEMENT
 */

/**
 * Initialize listener on debate form fieldset
 *
 * @param content main div containing the form
 */
function manageDebatePanel(content) {
    var currentDebateId = content.find('#id').val();

    // handle detection of existing debate
    manageExistingDebate(content.find('input[id$="_title"]'), currentDebateId === "-1", currentDebateId);

    initArgumentListeners(content);

    // if we have a "same name" to handle
    if ($('#debate_candidate_modal').length > 0) {
        handleDebateCandidates(content.find('#isNotSame'), content.find('#submit'));
    }
}

/**
 * Handle modal frame where debates with same name are displayed to ask user if his newly introduced
 * debate is not already present
 *
 * @param isNotSame hidden input element that will hold "true" to tell that this text must be created
 * @param submit the modal submit button
 */
function handleDebateCandidates(isNotSame, submit) {
    var modal = "#debate_candidate_modal";
    var okbtn = "#createnew";
    var loadbtn = '#load';

    toggleSummaryBoxes('#candidate-boxes', '.toggleable');
    $(modal).modal('show');

    // handle click on create a "new debate" button
    $(modal).on('click', okbtn, function () {
        isNotSame.val('true');
        hideAndDestroyModal(modal);
        $(submit).off();
        $(submit).trigger('click');
    });

    // handle click on load, simply reload page with known debate
    $(modal).on('click', loadbtn, function () {
        redirectToEdit($('.selected').prop('id').split("_")[1]);
    });
}

/**
 * Manage folder panels as modal dialogs (automatic creations)
 *
 * @param modal a modal frame with a folder form in it
 * @param msgdiv the msg div panel where the alert messages will be put as fallback
 */
function manageDebateModal(modal, msgdiv) {
    $(modal).modal('show');
    manageModalFolderSubmit(modal, msgdiv);
    manageFolderPanel();
}

/**
 * Submit or cancel submission of the folder form via ajax (when form is displayed in a modal frame).
 *
 * Rebuild form if it contains error. Close frame and add the received data if the call succeeded in
 * either in the next modal if any, or in the main page.
 *
 * @param modal the modal frame containing the folder form
 * @param msgdiv the div where the messages (e.g. submission statuses) will be shown
 */
function manageModalDebateSubmit(modal, msgdiv) {
    // handle submit button
    $(modal).find('[name="submit"]').on('click', function () {
        saveFolderFromModal($(modal).find('form')).done(function (data) {
            handleContributionModalResponse(modal, data, msgdiv, manageFolderPanel);
        }).fail(function (jqXHR) {
            switch (jqXHR.status) {
                case 400:
                    // form has errors => clear actor modal and rebuild it with errors
                    $(modal).find('#folder-form').empty().append(jqXHR.responseText);
                    manageFolderPanel();
                    fadeMessage();
                    break;

                case 409:
                case 410:
                    // we got a modal frame to resolve name matches
                    // hide current folder modal
                    // show new one (name-matches resolution)
                    modal.css('z-index', 999);
                    var modal_name = $('#modal-namematches');
                    modal_name.html(jqXHR.responseText);
                    modal_name.css('z-index', 9999);

                    handleContributionNameMatches(modal.find(jqXHR.status === 409 ? '#parents' : '#children').find('[id$="_name"]'),
                        '[id$="_folderId"]', '[id$="_isDisambiguated"]', modal.find('[name="submit"]'), "Folder");
                    break;

                default:
                    // any other (probably a crash)
                    hideAndDestroyModal(modal);
                    showErrorMessage(jqXHR);
            }
        });
    });

    // handle cancel button
    $(modal).find('[name="cancel"]').on('click', function () {
        var result = cancelFolderFromModal($(modal).find('#id').val());
        // cancel success (no other possible result) => just call handle response that will close it
        result.done(function (data) {
            handleContributionModalResponse(modal, data, msgdiv, manageFolderPanel);
        });
    });
}

/**
 * Manage typeahead selection of a debate in fill information with data.
 * If current folder if a new one ask for editing existing. Otherwise, ask to merge.
 *
 * @param element the html field where the typeahead will be put
 * @param newDebate true if the current debate is a new one
 * @param currentDebate the current debate id
 */
function manageExistingDebate(element, newDebate, currentDebate) {
    addDebatesTypeahead(element, currentDebate);
    $(element).on('typeahead:selected', function (obj, datum) {
        // create modal popup for confirmation
        bootbox.dialog({
            message: Messages("entry.folder.existing." + (newDebate ? "" : "merge.") + "text"),
            title: Messages("entry.folder.existing.title"),
            buttons: {
                main: {
                    // do nothing if user chose not to load existing folder
                    label: Messages("entry.folder.new"),
                    className: "btn-primary",
                    callback: function () { /* do nothing, just dispose frame */
                    }
                },
                modify: {
                    label: Messages("entry.folder.btn." + (newDebate ? "modify" : "merge")),
                    className: "btn-default",
                    callback: function () {
                        if(newDebate) {
                            redirectToEditDebate(datum.id);
                        }else{
                            redirectToDoMerge(currentDebate, datum.id, 5);
                        }
                    }
                }
            }
        })
    })
}