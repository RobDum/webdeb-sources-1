/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This collection of functions are meant to handle excerpt-related javascript functions, mainly dedicated to
 * manage insertion of excerpts from either texts annotation or tweet validation workflow.
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */

/**
 * Add all needed listeners to panels
 *
 * @param content main div containing the form
 * @param textid the text id to which this argument belongs to
 * @param excid the current excerpt id
 * @param cancelAction callback to invoke when cancel button is pressed
 * @param submitAction (optional) callback to invoke when submit button is pressed. If passed, must change type of button.
 */
function initExcerptListeners(content, textid, excid, cancelAction, submitAction) {

    initExcerptFieldSet(content, submitAction);

    // bind "cancel" callback action on cancel butoon
    content.find('.cancel').on('click', function() {
        cancelAction($('#textId').val(), $('#id').val());
    });

    // if we have a callback for the submit, force type of submit button to be a simple button
    // and bind click event to callback action
    if (submitAction !== undefined) {
        var submit = $('#submit-form');
        if (submit.length > 0) {
            submit.prop('type', 'button');
            submit.on('click', function () {
                submitAction();
            });
        }
    }
}

/**
 * Initialize listener on argument form fieldset
 *
 * @param content the div containing the the argument form
 * @param submitBtn the button to submit form
 */
function initExcerptFieldSet(content, submitBtn) {

    // do we have a namesake modal
    if ($('#namesake-modal').length > 0) {
        if ($('#isActor').val() === 'true') {
            handleContributionNameMatches($(content).find('[id$="fullname"]'), '[id$="_id"]', '[id$="_isDisambiguated"]', submitBtn, "Actor");
        } else if($('#isFolder').val() === 'true') {
            handleContributionNameMatches($(content).find('#folders').find('[id$="name"]'), '[id$="_folderId"]', '[id$="_isDisambiguated"]', submitBtn, "Folder");
        }else{
            handleContributionNameMatches($(content).find('[id$="affname"]'), '[id$="_affid"]', '[id$="_isAffDisambiguated"]', submitBtn, "Actor");
        }
    }

    // manage tooltip (help) bubbles
    handleHelpBubble(content);

    // for all actors fields, bind typeahead and listeners
    var actors = [ $('#authors'), $('#reporters'), $('#citedactors') ];
    for (var i = 0; i < actors.length; i++) {
        addActorsTypeahead(actors[i], 'fullname', '-1');
        addActorsTypeahead(actors[i], 'affname', '1');
        manageActorButton(actors[i].prop('id'), ['fullname', 'affname'], '-1');
    }

    // first part (type and actors)
    var boxes = $('#b-argtype').find('input[type="checkbox"]');
    manageExclusiveCheckboxes(boxes, $('#argtype'), true);

    // reload shade at click event on timing
    var btiming = $('#b-timing');
    manageExclusiveCheckboxes(btiming.find('input[type="checkbox"]'), $('#timing'), true);
    btiming.each(function() {
        var group = $(this);
        $('input', group).each(function () {
            $(this).on('click', function () {
                // get appropriate shades
                manageShade($(this).val());
            });
        });
    });

    //worksWithExcerpts($("#workingExcerpt"), $('#techWorkingExcerpt'));

    // initialize folder typeahead
    addFoldersTypeahead($('#folders'));
    // initialize place typeahead
    addPlacesTypeahead(content.find("#places"));
    manageAddRmButton(content.find('#places'), ['completeName'], "entry.delete.confirm.", null, addPlacesTypeahead);
}

function worksWithExcerpts(containerView, containerValue){
    containerView.text(containerView.text().trim());
    containerValue.val(containerValue.val().trim());
    var lock = false;

    addContextElementsToExcerpt(containerView);
    convertToWorkingExcerpt(containerView, containerValue);

    containerView.on("click", function(e) {
        if(!$(e.target).hasClass("wkg-ctx") && (!e.target.localName === "path" || !$(e.target.parentElement).hasClass("wkg-ctx"))) {
            var that = $(this);

            var userSelection = window.getSelection();
            var cursorIndex = userSelection.anchorOffset;
            var areaContent = userSelection.anchorNode.textContent;

            var nextIndexOfSpace = areaContent.substring(cursorIndex, areaContent.length).indexOf(" ");
            cursorIndex += (nextIndexOfSpace > -1 ? nextIndexOfSpace : areaContent.length);

            var firstPart = areaContent.substring(0, cursorIndex);
            var secondPart = areaContent.substring(cursorIndex + 1, areaContent.length);
            userSelection.anchorNode.textContent = firstPart + " [...] " + secondPart;

            openContributionSearchModal(0);

            $(document).on("contribution-selected", function (e, id, type, name) {
                containerView.text(containerView.text().replace('[...]','[' + id + ',' + type + ',' + name + ']'));
                that.html(addContextElementsToExcerpt(containerView));
                convertToWorkingExcerpt(containerView, containerValue);
            });

            $(document).on("contribution-selection-canceled", function () {
                containerView.text(containerView.text().replace('[...] ',''));
            });
        }
    });

    containerView.on("click", ".wkg-ctx", function(e) {
        if(!lock) {
            lock = true;
            $(this).closest(".wkg-ctx-first").remove();
            convertToWorkingExcerpt(containerView, containerValue);
            setTimeout(function(){lock = false;}, 10);
        }
    });
}

function addContextElementsToExcerpt(containerView){
    var ch = "";
    // get the text of the container without the children text to avoid deal with previously created span elements
    containerView.contents().each(function (index) {
        if(this.nodeType === 1){
            ch += this.outerHTML;
        }else{
            ch += addContextElementsInText(this.textContent);
        }
    });
    containerView.html(ch);
}

function addContextElementsInText(text) {
    var working = text;
    var regex = /\[([^)]+)\]/;
    var index = working.search(regex);
    var ch = index === -1 ? text : text.substring(0, index);

    while (index > -1) {
        var endIndex = working.indexOf("]");
        var val = working.substring(index + 1, endIndex);
        var values = val.split(",");
        working = working.substring(endIndex + 1, working.length);
        index = working.search(regex);
        var span = '<span class="wkg-ctx wkg-ctx-first primary" ' +
            'data-id="' + values[0] + '" data-type="' + values[1] + '">[' + values[2] + '&nbsp;' +
            '<span class="delete-working-context wkg-ctx" style="color : red">' +
            '<i class="fa fa-times-circle wkg-ctx"></i></span>]</span>';
        ch += " " + span + (index > -1 ? working.substring(0, index) : working);
    }

    return ch;
}

function convertToWorkingExcerpt(containerView, containerValue){
    var ch = "";
    containerView.contents().each(function( index ) {
        if(this.nodeType === 1){
            ch += (ch.endsWith(" ") ? "" : " ") +  "[" + $(this).data("id") + "," + $(this).data("type") + "]";
        }else{
            ch += this.textContent.trimEnd();
        }
    });
    containerValue.val(ch);
}


function excerptOptionsHandler(container){
    var modalanchor = $('#merge-modal-anchor');

    // redirect to the text linked to an excerpt
    container.on('click', '.excerpt-see-text-btn', function() {
        redirectToTextViz(getContributionOptionData($(this), "text-id"), 5);
    });


    // redirect to the text linked to an excerpt, to the citations pane
    container.on('click', '.excerpt-see-excerpts-btn', function() {
        redirectToTextViz(getContributionOptionData($(this), "text-id"), 3);
    });

    // See argument linked to selected citation
    container.on('click', '.excerpt-see-arguments-btn', function() {
        let id = getContributionOptionData($(this), "id");
        openExcerptArgumentsModal(id);
    });

    // display the contribution history of the selected excerpt
    container.on('click', '.excerpt-see-history-btn', function() {
        getContributionHistoryModal(getContributionOptionData($(this), "id"), 4).done(function (html) {
            modalanchor.empty().append(html);
            modalanchor.find('.modal').modal('show');
        });
    });

    // open edit excerpt modal
    container.on('click', '.excerpt-edit-arg-btn', function() {
        openEditExcerptModal(getContributionOptionData($(this), "text-id"), getContributionOptionData($(this), "id"));
    });

    // delete the given justification link
    container.on('click', '.excerpt-delete-link-btn', function() {
        var that = $(this);
        findIllustrationLink(getContributionOptionData(that, "link-id"), getContributionOptionData(that, "id")).done(function (illustration) {
            showConfirmationPopup(function(){doDeleteContributionAsync(illustration, 10, 0, true)}, null, "viz.admin.modal.delete.10.");
        }).fail(function (jqXHR) {
            slideMessage($('#error-delete'));
            console.log("Error with find illustration link");
        });
    });

    // delete the given argument
    container.on('click', '.excerpt-delete-btn', function() {
        var that = $(this);
        showConfirmationPopup(function(){doDeleteContributionAsync(getContributionOptionData(that, "id"), 4, 0, true)}, null, "viz.admin.modal.delete.4.");
    });

    // show details
    container.on('click', '.excerpt-show-details-btn', function() {
        redirectToDetailsPage(getContributionOptionData($(this), "id"));
    });

    // add new argument to excerpt modal
    container.on('click', '.excerpt-add-argument-btn', function() {
        var sourceId = getContributionOptionData($(this), "id");
        var contextId = getContributionOptionData($(this), "context-id");
        var shade = $(this).data("linktype");
        // open modal with "search citation" bar
        openArgumentSearchModal(contextId, sourceId, shade);
    });
}


function initExcerptModalListeners(textId, excId, modal){
    // handle submit button
    modal.find('[name="submit"]').on('click', function () {
        doSaveExcerpt(textId, excId, modal.find('#excerpt-form'), modal, $(this))
    });
}

function doSaveExcerpt(textId, excId, form, modal, submitbtn){
    saveExcerpt(textId, excId, form).done(function () {
        hideAndDestroyModal(modal);
        triggerReloadVizEvent();
        slideMessage($('#success-save'));
    }).fail(function (xhr) {
        switch (xhr.status) {
            case 400:
                modal.show();
                // form has errors => clear form and rebuilt
                form.empty().append(xhr.responseText);
                initExcerptFieldSet(form, submitbtn);
                fadeMessage();
                break;

            case 409:
                // actor name-match to handle, hide this modal and show the other one
                modal.hide();
                // show new one (name-matches resolution)
                var container = $('#autocreated');
                loadAndShowModal(container, xhr.responseText);
                if (container.find('#isActor').val() === 'true') {
                    handleContributionNameMatches(modal.find('[id$="fullname"]'), '[id$="_id"]', '[id$="_isDisambiguated"]', submitbtn, "Actor");
                } else if(container.find('#isFolder').val() === 'true') {
                    handleContributionNameMatches(modal.find('#folders').find('[id$="name"]'), '[id$="_folderId"]', '[id$="_isDisambiguated"]', submitbtn, "Folder");
                }else{
                    handleContributionNameMatches(modal.find('[id$="affname"]'), '[id$="_affid"]', '[id$="_isAffDisambiguated"]', submitbtn, "Actor");
                }
                break;

            default:
                // any other (probably a crash)
                hideAndDestroyModal(modal);
                slideMessage($('#error-save'));
        }
    });
}

/**
 * open the modal to display all arguments linked to an excerpt
 *
 * @param id an excerpt id
 */
function openExcerptArgumentsModal(id) {
    seeExcerptArguments(id).done(function (html) {
        loadAndShowModal( $('#modal-anchor'), html);
    }).fail(function (jqXHR) {
        console.log("Error with excerpt arguments modal");
    });
}