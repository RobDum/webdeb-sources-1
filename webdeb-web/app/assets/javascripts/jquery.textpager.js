/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This javascript file deal with text paging
 *
 * @author Others
 * @author Martin Rouffiange
 */



/**
 * Register a textpager for a text area
 *
 * @param area a given selector to attach the pager class to
 * @param textareaHeightTemp the height of the area with one column
 * @param colGap the size of spacing between two columns
 */
function textPager(area, textareaHeightTemp, colGap) {
  textareaHeightTemp = textareaHeightTemp || null;
  colGap = colGap || null;
  $(area).textpager({
    controlArrows: ".custom-controls",
    controlPagesContent: "li",
    textareaHeight : textareaHeightTemp,
    colGap : colGap
  });
}

// TODO refaire totalement ce code en utilisant notamment les classes js
!function(e) {
  e.fn.textpager = function (options) {
    var textarea = $(this);
    var self = this;
    var parent = textarea.parent().hasClass("textarea-container") ? textarea.parent() : $('<div class="textarea-container"></div>').insertBefore(textarea);
    textarea.css('overflow','hidden').appendTo($(parent));

      self.changeControlPages = function(){
          if(pages > eOptions.maxControlPages + 2){
              self.initControlPages();
          }
      };

      self.initControlPages = function(){
          // add page buttons (paging)
          var controlElements = '';
          if(eOptions.controlArrowsEnabel){
              controlElements += $('<'+eOptions.controlPagesContent+'>')
                  .html('<a><span><</span></a>')
                  .addClass('tp-control-arrow-left').prop("outerHTML");
          }

          let pageBegin = pages > eOptions.maxControlPages && pageNow - 4 > 1 ? pageNow - 4 : 1;
          let nbElements = pages > eOptions.maxControlPages ? eOptions.maxControlPages : pages;

          if(pageBegin > 0){
              controlElements += $('<'+eOptions.controlPagesContent+'>')
                  .attr('data-page',''+(1))
                  .html('<a>'+(1)+'</a>')
                  .addClass('tp-page')
                  .addClass('page_link')
                  .addClass(1 === pageNow ? 'active' : '')
                  .prop("outerHTML");
          }

          for (let pageCount = pageBegin; pageCount < pageBegin + nbElements && pageCount < pages; pageCount++) {
              controlElements += $('<'+eOptions.controlPagesContent+'>')
                  .attr('data-page',''+(pageCount + 1))
                  .html('<a>'+(pageCount + 1)+'</a>')
                  .addClass('tp-page')
                  .addClass('page_link')
                  .addClass(pageCount + 1  === pageNow ? 'active' : '')
                  .prop("outerHTML");
          }

          if(pages > eOptions.maxControlPages && pageBegin + nbElements < pages){
              controlElements += $('<'+eOptions.controlPagesContent+'>')
                  .attr('data-page',''+(pages))
                  .html('<a>'+(pages)+'</a>')
                  .addClass('tp-page')
                  .addClass('page_link')
                  .addClass(pages  === pageNow ? 'active' : '')
                  .prop("outerHTML");
          }

          if(eOptions.controlArrowsEnabel){
              controlElements += $('<'+eOptions.controlPagesContent+'>')
                  .html('<a><span>></span></a>')
                  .addClass('tp-control-arrow-right').prop("outerHTML");
          }

          eOptions.controlPages.html(controlElements);

          // handler on arrows
          parent.find(eOptions.controlArrows).find('.tp-control-arrow-left').off('click').on('click', function (){
              parent.find(eOptions.controlArrows).find('.tp-control-arrow-right').removeClass('unactive');
              var r = changePageAction(pageNow-1, pageNow, nextPage, margLeft, eOptions, self, textareaWidth, pages, parent);
              if(r){
                  nextPage = r[0];
                  pageNow = r[1];
                  margLeft = r[2];
              }
              self.changeControlPages();
          });

          parent.find(eOptions.controlArrows).find('.tp-control-arrow-right').off('click').on('click', function (){
              parent.find(eOptions.controlArrows).find('.tp-control-arrow-left').removeClass('unactive');
              var r = changePageAction(pageNow+1, pageNow, nextPage, margLeft, eOptions, self, textareaWidth, pages, parent);
              if(r){
                  nextPage = r[0];
                  pageNow = r[1];
                  margLeft = r[2];
              }
              self.changeControlPages();
          });

          // click on page
          eOptions.controlPages.find(eOptions.controlPagesContent+'.tp-page').off('click').on('click', function (){
              var thisPage = $(this).data('page');
              var r = changePageAction(thisPage, pageNow, nextPage, margLeft, eOptions, self, textareaWidth, pages, parent);
              if(r){
                  nextPage = r[0];
                  pageNow = r[1];
                  margLeft = r[2];
              }
              self.changeControlPages();
          });
    };

    // handle passed options to textpager
    var eOptions;
    if (options !== null && typeof(options) !== 'undefined') {
      eOptions = {
        controlArrows: options.controlArrows !== undefined ? options.controlArrows : '',
        controlArrowsEnabel: options.controlArrowsEnabel !== undefined ? options.controlArrowsEnabel : true,
        controlPages: options.controlPages !== undefined ? options.controlPages : '',
        controlPagesEnabel: options.controlPagesEnabel !== undefined ? options.controlPagesEnabel : true,
        controlPagesContent: options.controlPagesContent !== undefined ? options.controlPagesContent : 'div',
        textareaHeight: options.textareaHeight !== undefined ? options.textareaHeight : $(this).prop('scrollHeight'),
        colGap: options.colGap !== undefined ? options.colGap : 20,
        maxControlPages : 6
      };
    } else {
      eOptions = {
        controlArrows: '',
        controlArrowsEnabel: true,
        controlPages: '',
        controlPagesEnabel: true,
        controlPagesContent: 'div',
        textareaHeight : $(this).prop('scrollHeight'),
        colGap : 20,
        maxControlPages : 6
      };
    }

    // set up area height and width
    var textareaWidth = getTextAreaWidth(textarea);
    var textareaHeight = getTextAreaHeight(textarea);

    // determine number of column
    var column;
    var nbColumns = 1;
    // using bootstrap col-md-* limit
    if (textareaWidth > 992) {
      nbColumns = 2;
    }

    // get fulltextheight
    var fulltextHeight;
    if(nbColumns === 1){
      fulltextHeight = textarea.prop('scrollHeight');
    }
    else{
      fulltextHeight = eOptions.textareaHeight;
    }

    // if textarea to small for all content
    if(textareaHeight < fulltextHeight){
      var pageNow = 1;
      var margLeft = 0;

      if (eOptions.controlArrows === '') {
        $('<div>').addClass('tp-control-arrows').appendTo($(parent));
        eOptions.controlArrows = $(parent).find('.tp-control-arrows');
        $('<a>').addClass('tp-control-arrow-left').addClass('unactive').html('<span><</span>').appendTo($(eOptions.controlArrows));
        $('<a>').addClass('tp-control-arrow-right').html('<span>></span>').appendTo($(eOptions.controlArrows));
      }

        if(parent.find(".custom-controls").exists()) {
            parent.find(".custom-controls").remove();
        }
      var container = $('<div class="pull-right custom-controls unselectable"></div>').appendTo(parent);
      eOptions.controlPages = $('<ul id="pager" class="pager pagination pagination-sm no-margin"></ul>').appendTo(container);

      // put div handlers around current page
      textarea.css('height',textareaHeight+'px').css('padding',0);
      var contentHtml = textarea.html();
        textarea.html('');
      $('<div>').addClass('tp-horizontalbox').css('height',textareaHeight+'px')
        .css('width',textareaWidth+'px').appendTo(textarea);
      var div = $('<div>').addClass('tp-verticalbox').html(contentHtml)
        .css('width',textareaWidth + 'px')
        .css('height', textareaHeight + 'px')
        .css('column-width', 'auto')
        .css('column-count', nbColumns);
      div.appendTo(textarea.find('.tp-horizontalbox'));

      var nextPage;

        let t = textarea.prop('scrollWidth') / textarea.width();
        let z = Math.round(t) > Math.floor(t) && nbColumns > 1 ? 1 : 0;
        var pages = Math.floor(t) + z;

    self.initControlPages();

      $(document).on('next-page', function (){
          var option = $(eOptions.controlArrows).find('.tp-page .page_link .active').removeClass('active');
          option.next().addClass('active');
          var r = changePageAction(pageNow+1, pageNow, nextPage, margLeft, eOptions, self, textareaWidth, pages, parent);
          if(r){
              nextPage = r[0];
              pageNow = r[1];
              margLeft = r[2];
          }
      });

      // Special event
      $(document).on( "textPage", function( event, arg1 ) {
        var thisPage = Math.floor(arg1 / textareaWidth)+pageNow;
        if(thisPage <= 0)thisPage = 1;
        else if(thisPage > pages)thisPage = pages;
        var r = changePageAction(thisPage, pageNow, nextPage, margLeft, eOptions, self, textareaWidth, pages, parent);
        if(r){
          nextPage = r[0];
          pageNow = r[1];
          margLeft = r[2];
        }
      });
    }

    // animation management (concrete switching of page)
    this.animateStep = function(parent, textareaWidth, margLeft, goAhead){
      var animationTime = 250;
      $(parent).find('.tp-horizontalbox')
          .animate({
            marginLeft: (goAhead ? "-=" + textareaWidth : "+=" + textareaWidth)
          }, animationTime, function(){
            $(this)
                .css({
                  marginLeft: (goAhead ? "+=" + (textareaWidth*2) : "-=" + (textareaWidth*2))
                })
                .find('.tp-verticalbox')
                .css({
                  marginLeft: margLeft
                });
            $(this)
                .animate({
                  marginLeft: (goAhead ? "-=" + textareaWidth : "+=" + textareaWidth)
                }, animationTime)
                .find('.tp-verticalbox')
                .animate({
                  opacity: 1
                }, animationTime);
          })
          .find('.tp-verticalbox')
          .animate({
            opacity: 0
          }, animationTime);
    };
  };

  function getTextAreaWidth(textarea){
      return Math.floor(textarea.outerWidth(true));
  }

  function getTextAreaHeight(textarea){
      return Math.floor(textarea.outerHeight(true));
  }

  /**
   * Common function to change page
   *
   */
  function changePage(nextPage, margLeft, eOptions, textareaWidth, self, page, goAhead, container){
      margLeft -= nextPage;
      eOptions.controlPages.find(eOptions.controlPagesContent+'.tp-page').removeClass('active');
      eOptions.controlPages.find(eOptions.controlPagesContent + '[data-page="'+page+'"]').addClass('active');
      self.animateStep(self, textareaWidth, margLeft, goAhead);
      return margLeft;
  }

  /**
   * Determine if the page movement is ahead or not
   *
   */
  function determineGoAhead(thisPage, pageNow){
      if(thisPage < pageNow) {
          return false;
      }
      return true;
  }

  function changePageAction(thisPage, pageNow, nextPage, margLeft, eOptions, self, textareaWidth, pages, container){
      if(thisPage === pageNow || thisPage === 0 || thisPage > pages) {
          return false;
      }
      var goAhead = determineGoAhead(thisPage, pageNow);

      if (thisPage === 1) {
          container.find(eOptions.controlArrows).find('.tp-control-arrow-left').addClass('unactive');
          container.find(eOptions.controlArrows).find('.tp-control-arrow-right').removeClass('unactive');
      } else if(thisPage === pages) {
          container.find(eOptions.controlArrows).find('.tp-control-arrow-right').addClass('unactive');
          container.find(eOptions.controlArrows).find('.tp-control-arrow-left').removeClass('unactive');
      } else {
          container.find(eOptions.controlArrows).find('.tp-control-arrow-right').removeClass('unactive');
          container.find(eOptions.controlArrows).find('.tp-control-arrow-left').removeClass('unactive');
      }
      //console.log(thisPage);
      nextPage = (thisPage - pageNow) * (textareaWidth + 20);
      //console.log(nextPage);
      pageNow = thisPage;
      //console.log(pageNow);
      //console.log(margLeft);
      margLeft = changePage(nextPage, margLeft, eOptions, textareaWidth, self, thisPage, goAhead, container);
      //console.log(margLeft);

      return [nextPage, pageNow, margLeft];
  }

}(jQuery);