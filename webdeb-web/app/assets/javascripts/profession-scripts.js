/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This javascript file contains all reusable functions for actor's professions
 *
 * @author Fabian Gilson
 * @author Martin Rouffiange
 */


/**
 * Manage the edit profession modal
 *
 */
function manageEditProfessionModal(){
  $(document).ready(function(){
    var modal = "#editProfession";
    addListeners(modal);
    addProfessionTypeahead($('#professionLink').find("input").first());
    manageExclusiveCheckboxes($('#b-displayHierarchy').find('input[type="checkbox"]'), $('#displayHierarchy'), true);
  });
  function addListeners(modal) {
    $(modal).find('#submit-btn').on('click', function () {
      sendEditProfession($('#profession-form')).done(function (data) {
        //$('#admin-profession').empty().append(data);
        hideAndDestroyModalAndCallFunction(modal, autoCreateModalRemoved);
        $(document).trigger("modalRemoved");
        //location.reload();
      }).fail(function (jqXHR) {
        if (jqXHR.status === 400) {
          // rebuild form from response text
          replaceContent('#profession-form', jqXHR.responseText, 'form');
          addProfessionTypeahead($('#professionLink').find("input").first());
          addListenerProfessionName(modal);
        } else {
          //$('#admin-professions').empty().append(jqXHR.responseText);
          hideAndDestroyModalAndCallFunction(modal, autoCreateModalRemoved);
        }
      });
    });

    // handle cancel button
    $(modal).find('[name="cancel"]').on('click', function () {
        var result = cancelFromModal();
        // cancel success (no other possible result) => just call handle response that will close it
        result.done(function (data) {
            handleContributionModalResponse(modal, data, $("#msg-div"), manageEditProfessionModal);
        });
    });

    addListenerProfessionName(modal);
    manageAddRemoveButton($(modal), "professionNames");
  }

  function addListenerProfessionName(modal){
    $(modal).find(".profession_name").focus(function () {
      $(modal).find("#userWarned").val('false');
    });
  }


}
/**
 * Manage adding and removing profession names:
 *
 * @param modal a modal frame with an profession form in it
 * @param control the name of the div concern by the field
 * @param typeahead a boolean, true if the first form field need a typeahead
 */
function manageAddRemoveButton(modal, control, typeahead) {
  // manage add and remove buttons
  var child = ['name'];
  var controlid = '#'+control;
  // magic of jquery, add click listener also on newly created elements
  modal.on('click', controlid + ' .btn-add', function () {
    addSpellingField($('#' + control), typeahead);
  });

  modal.on('click', controlid + ' .btn-remove', function () {
    removeSpellingField(this, child, control);
    return false;
  });
}

/**
 * Add a new profession name field
 *
 * @param element the div where the new field must be added
 * @param typeahead a boolean, true if the first form field need a typeahead
 */
function addSpellingField(element, typeahead) {
  typeahead = typeahead || false;
  // clone them and add clone at the end of list
  var entry = element.find('.entry:last');
  $(entry).find('input').typeahead('destroy'); // avoid having typeahead troubles on cloned input
  var newEntry = $(entry.clone());
  entry.after(newEntry);

  // build new ids for any elements in cloned elements
  // id of the form "somename_i_somefield", name of the form  "affiliation[i].field (ifexists)
  var newInputs = newEntry.find('input');
  var input = newInputs[0];
  var splitId = $(input).prop('id').split('_');
  var idx = parseInt(splitId[1]) + 1;
  $(newEntry).prop('id', splitId[0] + "_" + idx);

  $(newEntry).find('*').each(function () {
    if (this.hasAttribute('id')) {
      splitId = $(this).prop('id').split('_');
      $(this).prop('id', splitId[0] + "_" + idx + "_" + splitId[2]);
      $(this).prop('name', splitId[0] + "[" + idx + "]." + splitId[2]);
      $(this).val('');
    } else {
      if (this.hasAttribute("for")) { // labels
        splitId = $(this).prop('for').split('_');
        $(this).prop('for', splitId[0] + "_" + idx + "_" + splitId[2]);
      }
    }
  });

  if(typeahead){
    addProfessionTypeahead(newInputs[0]);
  }
}

/**
 * Remove a profession name field
 *
 * @param buttonRemove the button pushed
 * @param child the input field for the translation of a profession name
 * @param control the name of the div concern by the field
 */
function removeSpellingField(buttonRemove, child, control){
  var entry = $(buttonRemove).parents('.entry:first');
  var descr = $(entry).find('[name$="' + child[0] + '"]').val()
  var func = $(entry).find('[name$="lang"]');
  if (func !== undefined && func.val() !== undefined && func.val() !=='') {
    descr += " (" + func.val() + ")";
  }
  showConfirmationPopup(removeGenericEntry, buttonRemove, "profession." + control + ".delete.confirm.", descr);
  return false;
}

function addProfessionsTypeahead(inputs){
  $.each( inputs, function( key, input ) {
    addProfessionTypeahead(input);
  });
}