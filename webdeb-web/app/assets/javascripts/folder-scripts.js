/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This javascript file contains all reusable functions for folder contributions' screens
 *
 * @author Martin Rouffiange
 */


/*
 * FOLDER EDIT MANAGEMENT
 */

/**
 * Initialize listener on folder form fieldset
 */
function manageFolderPanel() {
    var currentFolderId = $('#id').val();
  var folderType = $('#b-folderType').find('input[type="checkbox"]');
  // manage exclusive boxes for folder type
  manageExclusiveCheckboxes(folderType , $('#folderType'));

  // Manage add / remove buttons for names
  manageAddRmButton('folderNames', ['name'], "folder.delete.confirm.", null, addFoldersTypeahead,  currentFolderId);
  manageAddRmButton('folderRewordingNames', ['name'], "folder.delete.confirm.", null, addFoldersTypeahead,  currentFolderId);

  // Manage add / remove buttons and typeahead for parents and children
  addFoldersTypeahead($('#normalParents'),  currentFolderId);
  addFoldersTypeahead($('#normalChildren'),  currentFolderId);
  manageAddRmButton('normalParents', ['name'], "folder.delete.confirm.", null, addFoldersTypeahead,  currentFolderId);
  manageAddRmButton('normalChildren', ['name'], "folder.delete.confirm.", null, addFoldersTypeahead,  currentFolderId);

  // handle detection of existing folder
  manageExistingFolder($('#folderNames'), currentFolderId === "-1", currentFolderId);
  manageExistingFolder($('#folderRewordingNames'), currentFolderId === "-1", currentFolderId);


  // if we have a "same name" to handle
  if ($('#folder_candidate_modal').length > 0) {
    handleFolderCandidates($('#isNotSame'), $('#submit'));
  }
}

/**
 * Handle modal frame where folders with same name are displayed to ask user if his newly introduced
 * folder is not already present
 *
 * @param isNotSame hidden input element that will hold "true" to tell that this text must be created
 * @param submit the modal submit button
 */
function handleFolderCandidates(isNotSame, submit) {
  var modal = "#folder_candidate_modal";
  var okbtn = "#createnew";
  var loadbtn = '#load';

  new Pager( modal.find('.pageable'), 5).reset();

  toggleSummaryBoxes('#candidate-boxes', '.toggleable');
  $(modal).modal('show');

  // handle click on create a "new text" button
  $(modal).on('click', okbtn, function () {
    isNotSame.val('true');
    hideAndDestroyModal(modal);
    $(submit).off();
    $(submit).trigger('click');
  });

  // handle click on load, simply reload page with known folder
  $(modal).on('click', loadbtn, function () {
    redirectToEdit($('.selected').prop('id').split("_")[1]);
  });
}

/**
 * Manage folder panels as modal dialogs (automatic creations)
 *
 * @param modal a modal frame with a folder form in it
 * @param msgdiv the msg div panel where the alert messages will be put as fallback
 */
function manageFolderModal(modal, msgdiv) {
  $(modal).modal('show');
  manageModalFolderSubmit(modal, msgdiv);
  manageFolderPanel();
}

/**
 * Submit or cancel submission of the folder form via ajax (when form is displayed in a modal frame).
 *
 * Rebuild form if it contains error. Close frame and add the received data if the call succeeded in
 * either in the next modal if any, or in the main page.
 *
 * @param modal the modal frame containing the folder form
 * @param msgdiv the div where the messages (e.g. submission statuses) will be shown
 */
function manageModalFolderSubmit(modal, msgdiv) {
  // handle submit button
  $(modal).find('[name="submit"]').on('click', function () {
    saveFolderFromModal($(modal).find('form')).done(function (data) {
      handleContributionModalResponse(modal, data, msgdiv, manageFolderPanel);
    }).fail(function (jqXHR) {
      switch (jqXHR.status) {
        case 400:
          // form has errors => clear actor modal and rebuild it with errors
          $(modal).find('#folder-form').empty().append(jqXHR.responseText);
          manageFolderPanel();
          fadeMessage();
          break;

        case 409:
        case 410:
          // we got a modal frame to resolve name matches
          // hide current folder modal
          // show new one (name-matches resolution)
            modal.css('z-index', 999);
          var modal_name = $('#modal-namematches');
          modal_name.html(jqXHR.responseText);
          modal_name.css('z-index', 9999);

          handleContributionNameMatches(modal.find(jqXHR.status === 409 ? '#parents' : '#children').find('[id$="_name"]'),
              '[id$="_folderId"]', '[id$="_isDisambiguated"]', modal.find('[name="submit"]'), "Folder");
          break;

        default:
          // any other (probably a crash)
          hideAndDestroyModal(modal);
          showErrorMessage(jqXHR);
      }
    });
  });

  // handle cancel button
  $(modal).find('[name="cancel"]').on('click', function () {
    var result = cancelFromModal();
    // cancel success (no other possible result) => just call handle response that will close it
    result.done(function (data) {
      handleContributionModalResponse(modal, data, msgdiv, manageFolderPanel);
    });
  });
}

/**
 * Manage typeahead selection of a folder in fill information with data.
 * If current folder if a new one ask for editing existing. Otherwise, ask to merge.
 *
 * @param element the html field where the typeahead will be put
 * @param newFolder true if the current folder is a new one
 * @param currentFolder the current folder id
 */
function manageExistingFolder(element, newFolder, currentFolder) {
    addFoldersTypeahead(element, currentFolder);
    $(element).on('typeahead:selected', function (obj, datum) {
        // create modal popup for confirmation
        bootbox.dialog({
            message: Messages("entry.folder.existing." + (newFolder ? "" : "merge.") + "text"),
            title: Messages("entry.folder.existing.title"),
            buttons: {
                main: {
                    // do nothing if user chose not to load existing folder
                    label: Messages("entry.folder.new"),
                    className: "btn-primary",
                    callback: function () { /* do nothing, just dispose frame */
                    }
                },
                modify: {
                    label: Messages("entry.folder.btn." + (newFolder ? "modify" : "merge")),
                    className: "btn-default",
                    callback: function () {
                      if(newFolder) {
                          redirectToEditFolder(datum.folderId);
                      }else{
                          redirectToDoMerge(currentFolder, datum.folderId, 6);
                      }
                    }
                }
            }
        })
    })
}