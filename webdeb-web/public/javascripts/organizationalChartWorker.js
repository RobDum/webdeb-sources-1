/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program (see COPYING file).
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This file create chart in a web worker (to multi thread the function)
 *
 * @author Martin Rouffiange
 */

onmessage = function(e) {
    let data = JSON.parse(e.data);
    let ch = [];

    if(data.message === "actors"){
        createChartActors(data.chart_data, data.nbColumns, data.eOptions, ch)
    }else if(data.message === "durations"){
        createChartDurations(data.chart_data, data.title_width, data.table_width, new Date(data.begin_date), new Date(data.end_date), data.messageUnknown, data.eOptions, ch);
    }

    postMessage(ch);
};

function createChartActors(chart_data, nbColumns, eOptions, ch){
    if (eOptions.separatedChart > 0) {
        for (let iData in chart_data) {
            fillDataTitle(chart_data[iData].label, nbColumns, eOptions, ch);
            fillDataActors(chart_data[iData].affiliations, eOptions, ch);
        }
    } else {
        fillDataActors(chart_data, eOptions, ch);
    }
}

function createChartDurations(chart_data, title_width, table_width, begin_date, end_date, messageUnknown, eOptions, ch){
    let chartTop = 20;

    if (eOptions.separatedChart > 0) {
        let index = 1;
        for (let iData in chart_data) {
            let obj = fillDataDurations(chart_data[iData].affiliations, chartTop + eOptions.box_height, title_width, table_width, index, begin_date, end_date, messageUnknown, eOptions, ch);
            chartTop = obj.top;
            index = obj.index + 1;
        }
    } else {
        fillDataDurations(chart_data, chartTop, title_width, table_width, 0, begin_date, end_date, messageUnknown, eOptions, ch);
    }
}

function fillDataActors(actors, eOptions, ch){
    for (let iLine = 0; iLine < actors.length; iLine++) {
        fillDataActor(actors[iLine], eOptions, ch);
    }
}

function fillDataActor(actor, eOptions, ch){
    ch.push('<tr><td style="height : ' + eOptions.box_height + 'px;">' + actor.name + '</td></tr>');
}

function fillDataDurations(actors, top, left, table_width, index, begin_date, end_date, messageUnknown, eOptions, ch){
    let nbMonths = monthDiff(begin_date, end_date);

    for(let iActor in actors) {
        fillDataDuration(actors[iActor], top, left, table_width, index, nbMonths, begin_date, end_date, messageUnknown, eOptions, ch);
        top += eOptions.box_height;
        index++;
    }

    return {top, index};
}

function fillDataTitle(title, nbColumns, eOptions, ch){
    ch.push('<tr class="highlight-line-standing no-highlight-line">' +
        '<td colspan="' + (nbColumns + 1) + '" style="height : ' + eOptions.box_height + 'px;padding-left : 10px;"><h4>' + title + '</h4></td>' +
        '</tr>');
}

function fillDataDuration(actor, top, left, table_width, index, nbMonths, begin_date, end_date, messageUnknown, eOptions, ch){
    for(let iAffiliation in actor.affiliations) {
        let affiliation = actor.affiliations[iAffiliation];
        affiliation.dateBegin = affiliation.dateBegin !== 0 ? new Date(affiliation.dateBegin) : 0;
        affiliation.dateEnd = affiliation.dateEnd ? new Date(affiliation.dateEnd) : 0;

        if (isDefined(affiliation, begin_date, end_date)) {
            let line_width = table_width / (nbMonths / affMonthDiff(affiliation, begin_date, end_date));
            let line_left = table_width / (nbMonths / monthDiff(begin_date, affiliation.dateBegin));

            if (line_width < 25) {
                if (line_left > table_width / 2) {
                    line_left -= line_width;
                }
                line_width = 25;
            }

            let style = 'style="width : ' + line_width + 'px; height : 30px; top : ' + top + 'px; left : ' + (left + line_left) + 'px;"';

            ch.push('<div class="organigram-line" data-index="' + index + '">' +
                '<div class="organigram-linecontent' + othersClasses(affiliation, eOptions) + '" title="' + duration(affiliation, messageUnknown, eOptions) + '" ' + style + '></div>' +
                '</div>');
        }
    }
}

function monthDiff(d1, d2) {
    let months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}

function affMonthDiff(affiliation, calendar_begin_date, calendar_end_date) {

    let from = this.getDefaultDate(this.dateBegin, this.dateEnd, 5);
    let to = this.getDefaultDate(this.dateEnd, this.dateBegin, -5);

    if(from != null && to != null) {
        return monthDiff(from < calendar_begin_date ? calendar_begin_date : from,
            to > calendar_end_date ? calendar_end_date : to);
    }
    return 0;
}

function getDefaultDate(date, otherDate, diff){
    if(date instanceof Date){
        return date;
    }
    if(otherDate instanceof Date){
        date = new Date();
        date.setFullYear(otherDate.getFullYear() - diff);
        return date;
    }
    return null;
}

function isDefined(affiliation, begin_date, end_date){
    return (affiliation.dateBegin instanceof Date || affiliation.dateEnd instanceof Date) &&
        (!(affiliation.dateBegin instanceof Date) || affiliation.dateBegin < end_date) &&
        (!(affiliation.dateBegin instanceof Date) || affiliation.dateEnd > begin_date)
}

function duration(affiliation, messageUnknown, eOptions){
    let title = (affiliation.type ? affiliation.type : (eOptions.completeAffiliation && affiliation.affiliatedName ? affiliation.affiliatedName + " - " : "") + affiliation.profession);
    let dates = (affiliation.dateBeginType !== -1 ? affiliation.dateBeginType.name + " " : "") +
        displayDate(affiliation.dateBegin, messageUnknown) + " - " +
        (affiliation.dateEndType !== -1 ? affiliation.dateEndType.name + " " : "" )+
        displayDate(affiliation.dateEnd, messageUnknown);
    return title + " ( " + dates + " )";
}

function othersClasses(affiliation, eOptions){
    let ch = "";
    ch += affiliation.dateBeginType.id === 1 ? " organigram-linecontent-unkownbegin" : "";
    ch += affiliation.dateEndType.id === 3 || affiliation.dateEndType.id === 5 ? " organigram-linecontent-unkownend" : "";
    ch += affiliation.filtered ? " organigram-linecontent-filtered" : "";
    ch += eOptions.affiliatedId && eOptions.affiliatedId !== affiliation.affiliatedId ? " organigram-linecontent-dissimilar" : "";
    return ch;
}

function displayDate(date, messageUnknown){
    return date instanceof Date ?
        displayZero(date.getDate()) + "/" + displayZero(date.getMonth() + 1) + "/" + date.getFullYear()
        : messageUnknown;
}

function displayZero(number){
    return (number <= 9 ? "0" : "") + number;
}