/*
 * WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
 *
 * List of the contributors to the development of WebDeb: see AUTHORS file.
 * Description and complete License: see LICENSE file.
 *
 * This program (WebDeb) is free software:
 * you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see COPYING file).  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 */

/*!
 * organizationalChartJs v1.0 is a jQuery extension to create a organization chart with html elements.
 * https://github.com/martini224
 *
 * Includes jquery.js
 * https://jquery.com/
 *
 * Copyright Martin Rouffiange (martini224) 2018
 * Released under the MIT license (http://opensource.org/licenses/MIT)
 */
(function(e) { e.fn.organizationalChartJs = function (options) {

    let eOptions,
        container = $(this),
        tables_container = null,
        table_title_container = null,
        table_container = null,
        header_y = null,
        begin_date = null,
        end_date = null,
        chart_data = null,
        year_ends = null,
        nbColumns,
        scroll = false;

    $(document).ready(function() {
        initOptions();
        createChartContainer();
        createChart();
        initListeners();
    });

    function initOptions(){
        // handle passed options to calendarJs

        if (options !== null && typeof(options) !== 'undefined') {
            eOptions = {
                year_scale : isNaN(options.year_scale) || options.year_scale <= 0 ? 40 : options.year_scale,
                box_height : isNaN(options.box_height) || options.box_height <= 0 ? 50 : options.box_height,
                nb_columns : isNaN(options.nb_columns) || options.nb_columns <= 0 ? 10 : options.nb_columns,
                year_step : isNaN(options.year_scale) || options.year_scale <= 0 ? 10 : options.year_scale,
                year_scale_max : isNaN(options.year_scale_max) || options.year_scale_max <= 0 ? 100 : options.year_scale_max,
                year_scale_min : isNaN(options.year_scale_min) || options.year_scale_min <= 0 ? 10 : options.year_scale_min,
                endYear : (options.endDate == null ? new Date() : options.endDate).getFullYear() + 1,
                beginYear : options.beginDate == null ? this.endYear - 250 : options.beginDate.getFullYear(),
                separatedChart : isNaN(options.separatedChart) || options.separatedChart < 0 || options.separatedChart > 1 ? 0 : options.separatedChart,
                waitForIt : options.waitForIt instanceof jQuery ? options.waitForIt : false,
                scrollAnimation : typeof(options.scrollAnimation) === "boolean" ? options.scrollAnimation : false,
                navbar : typeof(options.navbar) === "boolean" ? options.navbar : true,
                completeAffiliation : typeof(options.completeAffiliation) === "boolean" ? options.completeAffiliation : false,
                affiliatedId : isNaN(options.affiliatedId) || options.affiliatedId < 0 ? false : options.affiliatedId,
                filterBtn : options.filterBtn instanceof jQuery ? options.filterBtn : false,
            };
        } else {
            eOptions = {
                year_scale : 40,
                box_height : 50,
                nb_columns : 10,
                year_step : 10,
                year_scale_max : 100,
                year_scale_min : 10,
                endYear : new Date().getFullYear() + 1,
                beginYear : this.endYear - 250,
                separatedChart : 0,
                waitForIt : false,
                scrollAnimation: false,
                navbar : true,
                completeAffiliation : false,
                affiliatedId : false,
                filterBtn : false
            };
        }

        nbColumns = eOptions.nb_columns;

        treatData(options.data);
    }

    function initListeners(){

        if(Array.isArray(chart_data)) {
            /*
             * Rebuild chart on screen resize
             */
            $(window).on('resize', function () {
                createChart();
            });

            if(eOptions.navbar) {
                container.on("click", ".bZoom", function () {
                    let former = eOptions.year_scale;

                    if ($(this).hasClass("bZoomIn")) {
                        eOptions.year_scale = eOptions.year_scale - eOptions.year_step < eOptions.year_scale_min ?
                            eOptions.year_scale_min : eOptions.year_scale - eOptions.year_step;
                    } else {
                        eOptions.year_scale = eOptions.year_scale + eOptions.year_step > eOptions.year_scale_max ?
                            eOptions.year_scale_max : eOptions.year_scale + eOptions.year_step;
                    }

                    if (former !== eOptions.year_scale)
                        createChart();
                });

                container.on("click", ".bNav", function () {
                    let former = year_ends;

                    if ($(this).hasClass("bPrev")) {
                        year_ends = year_ends - eOptions.year_scale < eOptions.beginYear ? year_ends : year_ends - eOptions.year_scale;
                    } else {
                        year_ends = year_ends + eOptions.year_scale > eOptions.endYear ? eOptions.endYear : year_ends + eOptions.year_scale;
                    }

                    if (former !== year_ends)
                        createChart();
                });
            }

            table_container.on("mouseover", "tr", function () {
                highlightTableLine(true, $(this));

            });

            table_container.on("mouseout", "tr", function () {
                highlightTableLine(false, $(this));
            });

            table_container.on("mouseover", ".organigram-line", function () {
                highlightTableLine(true, $(this).data("index") + 1);
            });

            if(eOptions.scrollAnimation) {
                table_container.find(".organigram-container-table").scroll(function (e) {
                    if (scroll) {
                        $(this).prop("scrollTop", 0);

                        let scrollValue = container.parent().offset().top - 130;

                        $([document.documentElement, document.body]).animate({
                            scrollTop: scrollValue
                        }, 100);

                        $(this).css("max-height", ($(window).height() - (scrollValue + 50)) + "px");
                        scroll = false;
                    }
                });
            }

            table_container.on('click', function(e){
                let target = $(e.target);

                if(target.hasClass('organigram-linecontent')) {
                    table_container.find('.organigram-lineinfo').remove();

                    let info = $('<div class="organigram-lineinfo">' + target.prop("title") + '</div>').appendTo(target.parent());
                    info.css('top', (target.position().top - 50) + 'px');
                    let left = (table_container.width() / 2) < target.position().left ? 230 : 0;
                    info.css('left', ($(window).width() < 800 ? ($(window).width() / 2 - info.width() / 2) : (target.position().left - left)) + 'px');
                }else{
                    table_container.find('.organigram-lineinfo').remove();
                }
            });
        }
    }

    function highlightTableLine(highlight, element){

        container.find(".highlight-line").removeClass("highlight-line");

        if(!isNaN(element)){
            element = table_container.find("tr").eq(element);
        }

        if(!element.hasClass("no-highlight-line")) {
            element.toggleClass("highlight-line", highlight);
        }
    }

    function createChartContainer(){

        if (header_y == null) {
            header_y = $('<div class="organigram-container-header"></div>').appendTo(container);
        }

        if (table_container == null) {
            table_container = $('<div style="flex-grow: 1;overflow: hidden;"></div>').appendTo(container);
        }
    }

    function displayWaitForIt(show){
        if (eOptions.waitForIt) {
            eOptions.waitForIt.toggle(show);

            if(show && $('.waiting').is(':visible')){
                eOptions.waitForIt.hide();
            }
        }
    }

    async function createChart(){

        displayWaitForIt(true);

        await createChartAsync();

        displayWaitForIt(false);
    }

    function createChartAsync(){
        return new Promise(resolve => {
            setTimeout(async function(){

                let d1 = new Date();
                if (Array.isArray(chart_data) && chart_data.length > 0) {
                    table_container.empty();

                    let table_subcontainer = $('<div class="organigram-container-table"></div>').appendTo(table_container);
                    let table = $('<table class="organigram"></table>').appendTo(table_subcontainer);

                    // build table
                    if (eOptions.separatedChart > 0) {
                        fillDataActor(chart_data[0].affiliations[0], table);
                    } else {
                        fillDataActor(chart_data[0], table);
                    }

                    let title_width = table.find(".actor-thumbnail").width();
                    let table_width = table.width() - title_width;
                    table.empty();
                    nbColumns = computeNbColumns(table_width);
                    let box_width = table_width / nbColumns;
                    // compute year and dates
                    let year_by_column = Math.floor(eOptions.year_scale / nbColumns);
                    year_ends = year_ends == null ? eOptions.endYear : year_ends;
                    let year = year_ends - eOptions.year_scale;
                    begin_date = new Date(year, 0, 1, 1, 0, 0);
                    end_date = new Date(year_ends, 0, 1, 1, 0, 0);

                    createChartDurations(chart_data, title_width, table_width, table_subcontainer);

                    await createChartActors(chart_data, table);

                    let header = $('<tr class="no-highlight-line"></tr>').prependTo(table);
                    header.append("<th></th>");

                    for (let iHeader = 0; iHeader < nbColumns; iHeader++) {
                        header.append('<th style="width:' + box_width + 'px"></th>');
                    }

                    header_y.empty();

                    let subHeaderY1 = $('<div style="width:150px"></div>').appendTo(header_y);

                    if (eOptions.filterBtn) {
                        let filterBtnClone = eOptions.filterBtn.clone();
                        filterBtnClone.appendTo(subHeaderY1);
                        filterBtnClone.show();
                    }

                    let subHeaderY2 = $('<div></div>').appendTo(header_y);

                    if (eOptions.navbar) {
                        let menu = '<div style="height: 30px;margin-left: ' + (title_width - 150) + 'px">' +
                            createMenuBtn("bNav bNext", "general.btn.next", "fa fa-arrow-right", false, false) +
                            createMenuBtn("bZoom bZoomOut", "general.btn.zoomout", "fa fa-search-minus", true, false) +
                            createMenuBtn("bNav bPrev hidden-xs", "general.btn.prev", "fa fa-arrow-left", true, true) +
                            createMenuBtn("bZoom bZoomIn", "general.btn.zoomin", "fa fa-search-plus", true, true) +
                            createMenuBtn("bNav bPrev visible-xs", "general.btn.prev", "fa fa-arrow-left", true, true) +
                            '</div>';
                        subHeaderY2.append(menu);
                    }

                    let year_header = $('<div class="flex-container" style="height: 30px;margin-left: ' + (title_width - (box_width / 2) - 150) + 'px"></div>').appendTo(subHeaderY2);

                    for (let iHeader = 0; iHeader < nbColumns; iHeader++) {
                        year_header.append('<div style="width: ' + box_width + 'px; text-align: center;">' + year + '</div>');
                        year += year_by_column;
                    }

                    table_subcontainer.prop("scrollTop", 0);
                    scroll = true;
                }
                console.log(new Date() - d1);
                resolve();
            }, 100)
        });
    }

    function createChartActors(chart_data, table){
        return new Promise(resolve => {
            //if (typeof (Worker) !== "undefined") {
            if (false) {
                let w = new Worker("/assets/javascripts/organizationalChartWorker.js");

                w.postMessage(JSON.stringify({message: "actors", chart_data: chart_data, nbColumns: nbColumns, eOptions: eOptions}));
                w.onmessage = function (e) {
                    if (Array.isArray(e.data)) {
                       e.data.forEach(function(element){
                           table.append(element);
                       });
                        addActorsColumns(table);
                    }
                    w.terminate();
                    w = undefined;
                    resolve();
                };
            }else{
                if (eOptions.separatedChart > 0) {
                    for (let iData in chart_data) {
                        fillDataTitle(chart_data[iData].label, table);
                        fillDataActors(chart_data[iData].affiliations, table);
                    }
                } else {
                    fillDataActors(chart_data, table);
                }
                addActorsColumns(table);
                resolve();
            }
        });
    }

    function addActorsColumns(table){
        table.find("tr").each(function () {
            if (!$(this).hasClass("no-highlight-line"))
                addColumns($(this));
        });
    }

    function createChartDurations(chart_data, title_width, table_width, table_subcontainer){
        return new Promise(resolve => {
            //if (typeof (Worker) !== "undefined") {
            if (false) {
                let w = new Worker("/assets/javascripts/organizationalChartWorker.js");

                w.postMessage(JSON.stringify({message: "durations", chart_data: chart_data, title_width: title_width, table_width: table_width, begin_date: begin_date, end_date: end_date, messageUnknown: Messages("label.unknown.f"), eOptions: eOptions}));
                w.onmessage = function (e) {
                    if (Array.isArray(e.data)) {
                        e.data.forEach(function(element){
                            table_subcontainer.prepend(element);
                        });
                    }
                    w.terminate();
                    w = undefined;
                    resolve();
                };
            }else{
                let chartTop = 20;

                if (eOptions.separatedChart > 0) {
                    let index = 1;
                    for (let iData in chart_data) {
                        let obj = fillDataDurations(chart_data[iData].affiliations, chartTop + eOptions.box_height, title_width, table_width, table_subcontainer, index);
                        chartTop = obj.top;
                        index = obj.index + 1;
                    }
                } else {
                    fillDataDurations(chart_data, chartTop, title_width, table_width, table_subcontainer, 0);
                }
                resolve();
            }
        });
    }

    function createMenuBtn(classes, message, logo, logoLeft, orientationLeft){
        let orientation = orientationLeft ? " pull-left-xs" : " pull-right";
        let content = logoLeft ? '<i class="' + logo + ' btn-logo"></i>&nbsp;<span class="btn-text">' + Messages(message) + '</span>&nbsp;&nbsp;' :
            '<span class="btn-text">' + Messages(message) + '</span>&nbsp;<i class="' + logo + ' btn-logo"></i>&nbsp;';
        return '<button type="button" class="btn btn-link btn-org-chart ' + classes + orientation + ' primary btn-xs-sensible">' + content + '</button>';
    }

    function fillDataTitle(title, table_titles){
        let line = $('<tr class="highlight-line-standing no-highlight-line"></tr>').appendTo(table_titles);
        line.append('<td colspan="' + (nbColumns + 1) + '" style="height : ' + eOptions.box_height + 'px;padding-left : 10px;"><h4>' + title + '</h4></td>');
    }

    function fillDataActors(actors, table_titles){
        for (let iLine = 0; iLine < actors.length; iLine++) {
            fillDataActor(actors[iLine], table_titles);
        }
    }

    function fillDataActor(actor, table_titles){
        let line = $("<tr></tr>").appendTo(table_titles);
        line.append('<td style="height : ' + eOptions.box_height + 'px;">' + actor.name + '</td>');
    }

    function addColumns(line){
        for (let iElement = 0; iElement < nbColumns; iElement++) {
            line.append('<td></td>');
        }
    }

    function fillDataDurations(actors, top, left, table_width, line_container, index){
        let nbMonths = monthDiff(begin_date, end_date);

        for(let iActor in actors) {
            fillDataDuration(actors[iActor], top, left, table_width, line_container, index, nbMonths);
            top += eOptions.box_height;
            index++;
        }

       return {top, index};
    }

    function fillDataDuration(actor, top, left, table_width, line_container, index, nbMonths){
        for(let iAffiliation in actor.affiliations) {
            let affiliation = actor.affiliations[iAffiliation];

            if (affiliation.isDefined()) {
                let el = $('<div class="organigram-line" data-index="' + index + '"></div>').prependTo(line_container);
                let el2 = $('<div class="organigram-linecontent' + affiliation.othersClasses() + '" title="' + affiliation.duration() + '"></div>').appendTo(el);

                let line_width = table_width / (nbMonths / affiliation.monthDiff(begin_date, end_date));
                let line_left = table_width / (nbMonths / monthDiff(begin_date, affiliation.dateBegin));

                if (line_width < 25) {
                    if (line_left > table_width / 2) {
                        line_left -= line_width;
                    }
                    line_width = 25;
                }

                el2.width(line_width + 'px');
                el2.height('30px');
                el2.css('top', top + 'px');
                el2.css('left', (left + line_left) + 'px');
            }
        }
    }

    function computeNbColumns(table_width){
        if(table_width / eOptions.nb_columns < 50){
            return Math.floor(table_width / 50);
        }
        return eOptions.nb_columns;
    }

    function monthDiff(d1, d2) {
        let months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth();
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
    }

    class Actor {
        constructor(name, affiliations){
            this.name = name;
            this.affiliations = this.sortAffiliations(affiliations);
        }

        sortAffiliations(affiliations){
            return affiliations.sort(function(a, b) {
                return a.monthDiff() - b.monthDiff();
            });
        }
    }

    class Affiliation {
        constructor(aff){

            this.type = aff.type;
            this.profession = aff.profession;
            this.affiliatedId = aff.affiliatedId;
            this.affiliatedName = aff.affiliatedName;
            this.dateBegin = aff.dateBegin;
            this.dateBeginType = aff.dateBeginType;
            this.dateEnd = aff.dateEnd;
            this.dateEndType = aff.dateEndType;
            this.filtered = aff.filtered;
        }

        monthDiff(calendar_begin_date, calendar_end_date) {

            let from = this.getDefaultDate(this.dateBegin, this.dateEnd, 5);
            let to = this.getDefaultDate(this.dateEnd, this.dateBegin, -5);

            if(from != null && to != null) {
                return monthDiff(from < calendar_begin_date ? calendar_begin_date : from,
                    to > calendar_end_date ? calendar_end_date : to);
            }
            return 0;
        }

        getDefaultDate(date, otherDate, diff){
            if(date instanceof Date){
                return date;
            }
            if(otherDate instanceof Date){
                date = new Date();
                date.setFullYear(otherDate.getFullYear() - diff);
                return date;
            }
            return null;
        }

        isDefined(){
            return (this.dateBegin instanceof Date || this.dateEnd instanceof Date) &&
                (!(this.dateBegin instanceof Date) || this.dateBegin < end_date) &&
                (!(this.dateBegin instanceof Date) || this.dateEnd > begin_date)
        }

        duration(){
            let title = (this.type ? this.type : (eOptions.completeAffiliation && this.affiliatedName ? this.affiliatedName + " - " : "") + this.profession);
            let dates = (this.dateBeginType !== -1 ? this.dateBeginType.name + " " : "") +
                this.displayDate(this.dateBegin) + " - " +
                (this.dateEndType !== -1 ? this.dateEndType.name + " " : "" )+
                this.displayDate(this.dateEnd);
            return title + " ( " + dates + " )";
        }

        othersClasses(){
            let ch = "";
            ch += this.dateBeginType.id === 1 ? " organigram-linecontent-unkownbegin" : "";
            ch += this.dateEndType.id === 3 || this.dateEndType.id === 5 ? " organigram-linecontent-unkownend" : "";
            ch += this.filtered ? " organigram-linecontent-filtered" : "";
            ch += eOptions.affiliatedId && eOptions.affiliatedId !== this.affiliatedId ? " organigram-linecontent-dissimilar" : "";
            return ch;
        }

        displayDate(date){
            return date instanceof Date ?
                this.displayZero(date.getDate()) + "/" + this.displayZero(date.getMonth() + 1) + "/" + date.getFullYear()
                : Messages("label.unknown.f");
        }

        displayZero(number){
            return (number <= 9 ? "0" : "") + number;
        }
    }

    function toActors(actors_obj){

        let actors = [];

        for(let iActor in actors_obj){
            let actor = actors_obj[iActor];
            let affiliations = [];

            for(let iAffiliation in actor.affiliations){
                let aff = actor.affiliations[iAffiliation];
                affiliations.push(new Affiliation(aff));
            }

            actors.push(new Actor(actor.name, affiliations));
        }

        return actors;
    }

    function treatData(data){

        displayWaitForIt(true);

        if(eOptions.separatedChart > 0){
            chart_data = [];
            for(let iData in data){
                chart_data.push({label: data[iData].label, affiliations: toActors(data[iData].affiliations)});
            }
        }else {
            chart_data = toActors(data);
        }

        displayWaitForIt(false);
    }


};}(jQuery));